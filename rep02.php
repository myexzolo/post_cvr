<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>
<style>
.table{
  border: 0px;
}
@page {
  margin: 0mm;
  size: A4 landscape;
}
@media print {
  html, body {
    margin-left: 4mm;
    margin-right: 4mm;
  }
  .tdherder {
  }
  .table{
    border: 1px solid black;
  }
  .break {
    page-break-after: always;
  }
}
</style>
<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php
  include("inc/header.php");
  include("inc/utils.php");
?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>รายงานสรุปยอดรถร่วม</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>รายงาน</li>
        <li class="active">รายงานสรุปยอดรถร่วม</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          //$optionCustomer  = getoptionCustomer("");
          $date         = date('Y-m-d',strtotime("-1 month"));
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));

          $optionCustomer  = getoptionCustomer("");
          $TrailerNo        = getoptionTrailerNoByAffiliation("",'3');
          $optionEmp        = getoptionEmp("","");

      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black">
          <div class="panel-heading">ค้นหาข้อมูลลูกค้า</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">จากวันที่ : </td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $startDate ?>" id="startDate">
                        <input type="hidden" value="<?= $startDate ?>" id="startDateTmp">
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ถึงวันที่ :</td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $endDate ?>" id="endDate">
                        <input type="hidden" value="<?= $endDate ?>" id="endDateTmp">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">ทะเบียนรถ : </td>
                      <td style="padding:5px;" align="left">
                        <select id="trailerId" class="form-control select2" style="width: 100%;" required>
                          <option value="" ></option>
                          <?= $TrailerNo ?>
                        </select>
                      </td>
                      <td style="padding:5px;" align="right">ชื่อพนักงานขับรถ :</td>
                      <td style="padding:5px;" align="left">
                        <select id="employeeId" class="form-control select2" style="width: 100%;" required >
                          <option value="" ></option>
                          <?= $optionEmp ?>
                        </select>
                    </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;width:150px;" align="right">SPAC : </td>
                      <td style="padding:5px;" align="left">
                        <input type="text" class="form-control" value="" id="awaySpecNo">
                      </td>
                      <td style="padding:5px;" align="right">DP : </td>
                      <td style="padding:5px;" align="left">
                          <input type="text" class="form-control" value="" id="dp">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">ประเภทงาน : </td>
                      <td style="padding:5px;" align="left">
                        <select id="type_job" class="form-control select2" style="width: 100%;" required >
                          <option value="" >เลือกประเภทงาน</option>
                          <option value="1">งานประจำ</option>
                          <option value="2">งานเสริมประจำ</option>
                          <option value="3">งานเสริมทั่วไป</option>
                        </select>
                      </td>
                      <td style="padding:5px;" align="right"></td>
                      <td style="padding:5px;" align="left"></td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">ชื่อลูกค้า :</td>
                      <td style="padding:5px;" align="left" colspan="2">
                        <select id="cust_id" class="form-control select2" style="width: 100%;" required >
                          <option value="" >ทั้งหมด</option>
                          <?= $optionCustomer ?>
                        </select>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="search()" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="reset()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
        <div class="panel panel-black">
          <div class="panel-heading">รายงานสรุปยอดรถร่วม
            <button class="btn btn-success pull-right btn-flat" style="position: relative;top:-7px;right:-12px;width:100px;" onclick="exportExcel()">
              <i class="fa fa-file-excel-o"></i> Excel</button>
              <button class="btn btn-primary pull-right btn-flat" style="position: relative;top:-7px;width:100px;" onclick="printDiv('show-page')">
                <i class="fa fa-print"></i> Print</button>
          </div>
            <div class="box-body" >
              <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
                <div style="width:100%;">
                  <div id="show-page" >
                    <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                  </div>
                </div>
              </form>
            </div>
      <!--  # coding -->
      </div>

      <div style="display:none;">
        <div id="printableArea">
          <div id="show-page-exp" >
            <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
          </div>
      </div>
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/rep02.js" type="text/javascript"></script>
<script>
  search();
</script>
</body>
</html>
