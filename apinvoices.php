<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php
  include("inc/header.php");
  include("inc/utils.php");
?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>ข้อมูลเอกสารใบจ่ายรถร่วม</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>การเงินและบัญชี</li>
        <li class="active">ข้อมูลเอกสารใบจ่ายรถร่วม</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          $department_id = "";
          $apInvoiceNo = "";
          $apinvoiceDate = "";
          $remark = "";
          $display = "";


          $active = "ACC";
          if(isset($_POST['apInvoiceNo'])){
             $apInvoiceNo = $_POST['apInvoiceNo'];
             $sql = "SELECT * FROM  tb_apinvoice WHERE apinvoice_no = '$apInvoiceNo'";
             $query = mysqli_query($conn,$sql);
             $row = mysqli_fetch_assoc($query);

             // print_r($_POST);

             $department_id   = $row['department_id'];
             $apinvoiceDate   = $row['apinvoice_date'];
             $remark          = $row['remark'];
             $display         = "display:none";

            $active = "EDIT";
          }


          $date         = date('Y-m-d');
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));
          $optionDep    = getoptionDepByAffiliation(3,$department_id);
          $TrailerNo    = getoptionTrailerLicense("");
          $optionContract  = getoptionContract("");

          if($active == "ACC")
          {
      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black" style="<?= $display?>">
          <div class="panel-heading">ค้นหาข้อมูลเอกสารใบจ่ายรถร่วม</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">จากวันที่: </td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $startDate ?>" id="startDate">
                        <input type="hidden" value="<?= $startDate ?>" id="startDateTmp">
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ถึงวันที่ :</td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $endDate ?>" id="endDate">
                        <input type="hidden" value="<?= $endDate ?>" id="endDateTmp">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">เลขที่สัญญาจ้าง :</td>
                      <td style="padding:5px;" align="left" colspan="3">
                        <select id="contr" class="form-control select2 select2-hidden-accessible" style="width: 100%;" onchange="searchApInvoice();">
                          <option value="" >&nbsp;</option>
                          <?= $optionContract ?>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">ทะเบียน : </td>
                      <td colspan="3" style="padding:5px;" align="left">
                        <select id="trailer_id" class="form-control select2 select2-hidden-accessible" multiple="" style="width: 100%;"  onchange="searchApInvoice();">
                          <option value="" >&nbsp;</option>
                          <?= $TrailerNo ?>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">ชื่อเจ้ารถร่วม :</td>
                      <td colspan="3" style="padding:5px;" align="left">
                        <select id="departmentId" class="form-control select2 select2-hidden-accessible" style="width: 100%;"  onchange="searchApInvoice();">
                          <option value="" >&nbsp;</option>
                          <?= $optionDep ?>
                        </select>
                        <input type="hidden" id="apInvoiceNo"   value="<?= $apInvoiceNo ?>">
                        <input type="hidden" id="apinvoiceDate" value="<?= $apinvoiceDate ?>">
                        <input type="hidden" id="remark" value="<?= $remark ?>">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="searchApInvoice();" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="reset2()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
      <?php
      }else{
      ?>
        <input type="hidden" id="apInvoiceNo"   value="<?= $apInvoiceNo ?>">
        <input type="hidden" id="departmentId"   value="<?= $department_id ?>">
        <input type="hidden" id="trailer_id"   value="">
      <?php
        }
        ?>
        <div class="panel panel-black">
          <div class="panel-heading">รายการข้อมูลใบสั่งงาน</div>
          <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate >
          <!-- <form data-smk-icon="glyphicon-remove-sign" novalidate action="ajax/apinvoices/manage.php" method="post"> -->
            <div class="box-body" >
              <div style="padding:5px" align="center">
              </div>
                <div style="width:100%;overflow-x: auto;">
                  <div id="printableArea">
                    <div id="show-page" >
                      <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                    </div>
                </div>
                </div>
                <div style="margin-top:5px;" >
                  <div class="row">
                      <div class="col-md-6">
                        <button type="button" onclick="gotoPage('apinvoices_list.php')" class="btn btn-success btn-flat " style="width:100px">
                          <i class="fa fa-mail-reply"></i> ย้อนกลับ
                        </button>
                      </div>
                      <div class="col-md-6" align="right">
                        <button type="button" class="btn btn-flat " onclick="cal()" style="width:100px;">
                          <i class="fa fa-calculator"></i> คำนวณ
                        </button>
                        <button type="submit" id="btnSubmit" class="btn btn-primary btn-flat " style="width:100px" disabled>
                          <i class="fa fa-save"></i> บันทึก
                        </button>
                      </div>
                    </div>
                </div>
            </div>
          </form>
      <!--  # coding -->
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/select2.full.min.js"></script>
<script src="js/apinvoices.js" type="text/javascript"></script>
<script>
  $('.select2').select2();
  $(document).ready(function() {
    searchApInvoice();
  });
</script>
</body>
</html>
