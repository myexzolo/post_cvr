<!DOCTYPE html>
<?php
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

include('conf/connect.php');
include('inc/utils.php');
require_once 'mpdf2/vendor/autoload.php';



$con = "";
$oder = "";

$id     = @$_POST['id'];


$sql = "SELECT j.*, d.delivery_type_name, c.cust_name,e.employee_name,e.tel, t.license_plate, a.affiliation_name
        FROM tb_job_order j,
             tb_delivery_type d,
             tb_customer_master c,
             tb_trailer t,
             tb_employee_master e,
             tb_trailer_affiliation a
        where j.id ='$id'
              and j.delivery_type_id = d.delivery_type_id
              and j.cust_id = c.cust_id
              and j.employee_id = e.employee_id
              and j.trailer_id = t.trailer_id
              and j.affiliation_id = t.affiliation_id";
$query  = mysqli_query($conn,$sql);
$row    = mysqli_fetch_assoc($query);

$job_order_no       = $row['job_order_no'];
$delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
$delivery_type_name = $row['delivery_type_name'];//รูปแบบการขนส่ง
$job_order_date     = $row['job_order_date'];//วันที่ออกใบสั่งงาน
$job_delivery_date  = $row['job_delivery_date'];//วันที่กำหนดส่งสินค้า
$cust_id            = $row['cust_id'];//รหัสลูกค้า
$cust_name          = $row['cust_name'];//รหัสลูกค้า
$employee_id        = $row['employee_id'];//รหัสพนักงาน
$employee_name      = $row['employee_name'];//รหัสพนักงาน
$tel                = $row['tel'];//รหัสพนักงาน
$employee_id2       = $row['employee_id2'];//รหัสพนักงาน
$trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
$license_plate      = $row['license_plate'];//หัว หมายเลขทะเบียนรถ
$away_regi_no       = $row['away_regi_no'];//หาง
$away_spec_no       = $row['away_spec_no'];//สเปคหาง
$affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
$affiliation_name   = $row['affiliation_name'];//รหัส รถ(สังกัด)
$route_id           = $row['route_id'];//รหัสเส้นทาง
$source             = $row['source'];//ต้นทาง
$destination        = $row['destination'];//ปลายทาง
$distance           = $row['distance'];//ระยะทาง
$cust_dp            = $row['cust_dp'];//DP
$delivery_doc       = $row['delivery_doc'];//ใบส่งของ
$department         = $row['department'];//หน่วยงาน
$product_name       = $row['product_name'];//สินค้า
$number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
$weights            = $row['weights'];//น้ำหนัก(ตัน)
$shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
$date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
$time_product_in    = $row['time_product_in'];//เวลารับสินค้า
$date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
$time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
$date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
$time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
$date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
$time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
$second_emp         = $row['second_emp'];;//ค่าขับรถผู้ช่วย
$fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
$fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
$blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
$allowance          = $row['allowance'];//เบี้ยเลี้ยง
$kog_expense        = $row['kog_expense'];//หางคอก
$allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
$remark             = $row['remark'];
$shipment           = $row['shipment'];
$price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
$one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
$ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
$price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
$ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
$total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
$total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
$total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
$job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
$job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
$job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
$job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
$job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
$job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
$job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
$fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
$job_status_id            = $row['job_status_id'];
$job_order_profit         = $row['job_order_profit'];
$petrol_station           = $row['petrol_station'];//ปั้มน้ำมัน
$type_job                 = $row['type_job'];


if($type_job == 1)
{
  $type_job = "งานประจำ";
}else if($type_job == 2)
{
  $type_job = "งานเสริมประจำ";
}else if($type_job == 3)
{
  $type_job = "งานเสริมทั่วไป";
}

$employee_name2       = getEmpName($employee_id2);




  //echo  $nameCusInv;
  $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
  $fontDirs = $defaultConfig['fontDir'];

  $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
  $fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
      'mode' => 'utf-8',
      'format' => 'A4',
      'fontDir' => array_merge($fontDirs, [
           'fonts/CSChatThai',
      ]),
      'fontdata' => $fontData + [
          'kanit' => [
              'R' => 'CSChatThai.ttf'
          ]
      ],
      'default_font' => 'kanit'
  ]);
  ob_start();
  // print_r($result);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
    <link rel="stylesheet" href="css/pdf.css">
  </head>
  <body>
    <htmlpageheader name="firstpageheader">
      <div class="header">
        <table style="width:100%;margin-bottom:10px;">
          <tr>
            <td style="width:20%"></td>
            <td style="font-size:22px;padding:10px;" align="center">บริษัท โชควิรัตน์ จำกัด</td>
            <td style="width:20%;font-size:17px;text-align:right;" >
              <div align="right">Tel/Fax : 02-1151981</div>
              <div align="right">Tel/Fax : 02-1151982</div>
            </td>
          </tr>
          <tr>
            <td style="padding-top: 3px;padding-bottom:3px;font-size:18px;" colspan="3" align="center">24 หมู่ 1 ต.พระขาว อ.บางบาล จ.พระนครศรีอยุธยา เลขประจำตัวผู้เสียภาษี 0145566002853</td>
          </tr>
          <tr>
            <td style="padding-top: 3px;padding-bottom:3px;font-size:18px;" colspan="3" align="center">สำนักงาน : 1 ซอยแฮปปี้เพรซ 1 แขวงคลองสามประเวศ เขตลาดกระบัง กรุงเทพฯ 10520</td>
          </tr>
          <tr>
            <td style="border-bottom: 1px solid #0000" colspan="3" align="center"></td>
          </tr>
          <tr>
            <td style="padding:2px;" colspan="3" align="center"></td>
          </tr>
          <tr>
            <td></td>
            <td style="font-size:22px;" align="center">ข้อมูลใบสั่งงาน</td>
            <td></td>
          </tr>
          <tr>
            <td style="width:200px"></td>
            <td style="padding:1px;" align="center">&nbsp;</td>
            <td style="width:200px;" >
            </td>
          </tr>
        </table>
      </div>
    </htmlpageheader>
    <sethtmlpageheader name="firstpageheader" value="on" show-this-page="1" />
    <table style="width:100%;font-size:18px;" border="0">
      <tr>
        <td style="width:90px;text-align:right">เลขที่ใบสั่งงาน</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= $job_order_no ?></td>
        <td style="width:90px;text-align:right">ขนส่งแบบ</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" colspan="3"><?= $delivery_type_name ?></td>
        <td style="width:90px;text-align:right">วันที่</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" ><?= DateThai($job_order_date) ?></td>
      </tr>
      <tr>
        <td style="width:90px;text-align:right">DP</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= $cust_dp ?></td>
        <td style="width:90px;text-align:right">Shipment</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" colspan="3"><?= $shipment ?></td>
        <td style="width:90px;text-align:right">กำหนดส่ง</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= DateThai($job_delivery_date) ?></td>
      </tr>
      <tr>
        <td colspan="11" style="height:10px"></td>
      </tr>
      <tr>
        <td style="width:90px;text-align:right">ลูกค้า</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" colspan="6"><?= $cust_name ?></td>
        <td style="width:90px;text-align:right">ใบส่งของ</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= $delivery_doc ?></td>
      </tr>
      <tr>
        <td style="width:90px;text-align:right">สถานที่รับ</td>
        <td style="width:8px;text-align:center">:</td>
        <td colspan="3" style="border-bottom: 1px dotted #000;"><?= $source ?></td>
        <td style="width:90px;text-align:right">สถานที่ส่ง</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" colspan="4"><?= $destination ?></td>
      </tr>
      <tr>
        <td style="width:90px;text-align:right">ชื่อพนักงาน</td>
        <td style="width:8px;text-align:center">:</td>
        <td colspan="3" style="border-bottom: 1px dotted #000;"><?= $employee_name." ".$tel;  ?></td>
        <td style="width:90px;text-align:right">ชื่อพนักงานผู้ช่วย</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" colspan="4"><?= $employee_name2  ?></td>
      </tr>
      <tr>
        <td style="width:90px;text-align:right">หัว</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= $license_plate ?></td>
        <td style="width:90px;text-align:right">หาง</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" colspan="3"><?= $away_regi_no ?></td>
        <td style="width:90px;text-align:right">Spec หาง</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= $away_spec_no ?></td>
      </tr>
      <tr>
        <td style="width:90px;text-align:right">รถ(สังกัด)</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= $affiliation_name ?></td>
        <td style="width:90px;text-align:right">ประเภทงาน</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" colspan="3"><?= $type_job ?></td>
        <td style="width:90px;text-align:right">รหัสเส้นทาง</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= $route_id ?></td>
      </tr>
      <tr>
        <td style="width:90px;text-align:right">ระยะทาง</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= $distance ?></td>
        <td style="width:90px;text-align:right">หน่วยงาน</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" colspan="3"><?= $department ?></td>
        <td style="width:90px;text-align:right">สินค้า</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= $product_name ?></td>
      </tr>
      <tr>
        <td style="width:90px;text-align:right">จำนวนเที่ยว</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= $number_of_trips ?></td>
        <td style="width:90px;text-align:right">น้ำหนัก(ตัน)</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" colspan="3"><?= $weights ?></td>
        <td style="width:90px;text-align:right">ราคาค่าขนส่ง</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;"><?= $shipping_amount ?></td>
      </tr>

      <tr>
        <td style="width:90px;text-align:right">วันทีรับสินค้า</td>
        <td style="width:8px;text-align:center">:</td>
        <td colspan="3" style="border-bottom: 1px dotted #000;"><?= DateThai($date_product_in)." ".$time_product_in  ?></td>
        <td style="width:90px;text-align:right">วันที่ออก</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" colspan="4"><?= DateThai($date_product_out)." ".$time_product_out  ?></td>
      </tr>
      <tr>
        <td style="width:90px;text-align:right">วันที่ส่งสินค้า</td>
        <td style="width:8px;text-align:center">:</td>
        <td colspan="3" style="border-bottom: 1px dotted #000;"><?= DateThai($date_delivery_in)." ".$time_delivery_in  ?></td>
        <td style="width:90px;text-align:right">วันที่ออก</td>
        <td style="width:8px;text-align:center">:</td>
        <td style="border-bottom: 1px dotted #000;" colspan="4"><?= DateThai($date_delivery_out)." ".$time_delivery_out  ?></td>
      </tr>
      <tr>
        <td style="width:90px;text-align:right">หมายเหตุ</td>
        <td style="width:8px;text-align:center">:</td>
        <td colspan="9" style="border-bottom: 1px dotted #000;"><?= $remark;  ?></td>
      </tr>
    </table>
<?php
  // $footer = '<div class="foot">
  //             <table width = "100%">
  //               <tr class="text-center">
  //                   <td style="text-align:left;width:50%"><p>ค้นหาข้อมูล วันที่ '.date("d/m/Y H:i:s").'</td>
  //
  //                </tr>
  //              </table>
  //             </div>';
  //
  //  $header  = '<div class="header" >
  //               <table width = "100%">
  //                <tr class="text-center">
  //                    <td style="text-align:left;width:50px;"><img style="height:40px" src="../../image/logo.png"/></td>
  //                    <td style="text-align:left;"><p>ระบบสนับสุนนการให้บริการ (Queue)</p></td>
  //                    <td style="text-align:right;width:30%"><p>page {PAGENO} of {nb}</p></td>
  //                 </tr>
  //               </table>
  //              </div>';

    //echo $html;
    $html = ob_get_contents();
    ob_end_clean();
    ob_clean();
    $stylesheet = file_get_contents('css/pdf.css');
    $mpdf->SetTitle('ข้อมูลใบสั่งงาน');
    // $mpdf->StartProgressBarOutput(2);
    // $mpdf->AddPage('P','','','','',left,right,top,bottom,header,footer);
    // $mpdf->AddPage('P','','','','',10,10,12,10,10,10);
    $mpdf->autoPageBreak = true;
    $mpdf->AddPage('P','','','','',5,5,52,10,5,5);
    // $mpdf->SetHTMLHeader($header);
    // $mpdf->SetHTMLFooter($footer);
    // $mpdf->showWatermarkText = true;
    $mpdf->WriteHTML($stylesheet,\Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($html,\Mpdf\HTMLParserMode::HTML_BODY);

    $mpdf->Output('job.pdf', \Mpdf\Output\Destination::INLINE);
    // $mpdf->Output('rep01.pdf', \Mpdf\Output\Destination::FILE);
  ?>
