<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php
  include("inc/header.php");
  include("inc/utils.php");
?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>ข้อมูลเอกสารใบส่งของ</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>รายงาน</li>
        <li class="active">ข้อมูลเอกสารใบส่งของ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          $shipmentCode = "";
          $companyName  = "";
          $shipmentDate = "";
          $display = "";
          if(isset($_POST['shipmentCode'])){
             $shipmentCode  = $_POST['shipmentCode'];
             $companyName   = $_POST['companyName'];
             $shipmentDate  = $_POST['shipmentDate'];
             $display       = "display:none";
          }
          $date         = date('Y-m-d');
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));

      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black" style="<?= $display?>" >
          <div class="panel-heading">ค้นหาข้อมูลใบสั่งงาน</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">จากวันที่: </td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $startDate ?>" id="startDate">
                        <input type="hidden" value="<?= $startDate ?>" id="startDateTmp">
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ถึงวันที่ :</td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $endDate ?>" id="endDate">
                        <input type="hidden" value="<?= $endDate ?>" id="endDateTmp">
                        <input type="hidden" value="<?= $shipmentCode ?>" id="shipmentCode">
                        <input type="hidden" value="<?= $companyName ?>" id="companyName">
                        <input type="hidden" value="<?= $shipmentDate ?>" id="shipmentDate">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="searchShipment()" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="resetShipment()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
        <div class="panel panel-black">
          <div class="panel-heading">บันทึกรายการข้อมูลใบสั่งงาน</div>
          <form id="form-data" novalidate >
            <div class="box-body" >
              <div style="padding:5px" align="center">
              </div>
                <div style="width:100%;">
                  <div id="printableArea">
                    <div id="show-page" >
                      <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                    </div>
                </div>
                </div>
                <div style="margin-top:5px;" align="right">
                  <button type="submit" class="btn btn-primary btn-flat " style="width:100px">
                    <i class="fa fa-save"></i> บันทึก
                  </button>
                  <button type="button" onclick="gotoPage('shipment_list.php')" class="btn btn-success btn-flat " style="width:100px">
                    <i class="fa fa-mail-reply"></i> ย้อนกลับ
                  </button>
                </div>
            </div>
          </form>
      <!--  # coding -->
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/shipment.js" type="text/javascript"></script>
<script>
  $(document).ready(function() {
    searchShipment();
  });
</script>
</body>
</html>
