﻿<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php
  include("inc/header.php");
  include("inc/utils.php");
?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>ข้อมูลเอกสารใบแจ้งหนี้</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>การเงินและบัญชี</li>
        <li class="active">ข้อมูลเอกสารใบแจ้งหนี้</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          $cust_id = "";
          $invoiceCode = "";
          $invoiceDate = "";
          $display = "";
          $contract   = "";
          if(isset($_POST['invoiceCode'])){
             $invoiceCode = $_POST['invoiceCode'];
             $sql = "SELECT * FROM tb_invoice WHERE invoice_code = '$invoiceCode' and status_del not in ('Y')";
             $query = mysqli_query($conn,$sql);
             $row = mysqli_fetch_assoc($query);

             $cust_id       = $row['cust_id'];
             $invoiceDate   = $row['invoice_date'];
             $contract      = isset($row['contract'])?$row['contract']:"";

             $display       = "display:none";
          }

          $date         = date('Y-m-d');
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));
          $optionContract  = getoptionContract($contract);

          $startMonth    = date('m', strtotime($date." -1 month"));
          $startYear     = date('Y', strtotime($date));
          $Year2         = date('Y', strtotime($date." -1 year"));

          $selected1    = "";
          $selected2    = "selected";
          if($startMonth == 12)
          {
            $selected1   = "selected";
            $selected2    = "";
          }

      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black" style="<?= $display?>" >
          <div class="panel-heading">ค้นหาข้อมูลเอกสารใบแจ้งหนี้</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">ประจำเดือน : </td>
                      <td style="padding:5px;width: 150px;" align="left">
                        <select id="month"  class="form-control select2" style="width: 100%;" required >
                          <option value="01" <?= $startMonth=="01"?"selected":"" ?> >มกราคม</option>
                          <option value="02" <?= $startMonth=="02"?"selected":"" ?> >กุมภาพันธ์</option>
                          <option value="03" <?= $startMonth=="03"?"selected":"" ?> >มีนาคม</option>
                          <option value="04" <?= $startMonth=="04"?"selected":"" ?> >เมษายน</option>
                          <option value="05" <?= $startMonth=="05"?"selected":"" ?> >พฤษภาคม</option>
                          <option value="06" <?= $startMonth=="06"?"selected":"" ?> >มิถุนายน</option>
                          <option value="07" <?= $startMonth=="07"?"selected":"" ?> >กรกฎาคม</option>
                          <option value="08" <?= $startMonth=="08"?"selected":"" ?> >สิงหาคม</option>
                          <option value="09" <?= $startMonth=="09"?"selected":"" ?> >กันยายน</option>
                          <option value="10" <?= $startMonth=="10"?"selected":"" ?> >ตุลาคม</option>
                          <option value="11" <?= $startMonth=="11"?"selected":"" ?> >พฤศจิกายน</option>
                          <option value="12" <?= $startMonth=="12"?"selected":"" ?> >ธันวาคม</option>
                        </select>
                          <input type="hidden" value="<?= $startMonth ?>" id="month_tmp">
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ปี : </td>
                      <td style="padding:5px;" align="left">
                        <select id="year"  class="form-control select2" style="width: 100%;" required >
                          <option value="<?= $Year2 ?>" <?= $selected1 ?> ><?= $Year2 ?></option>
                          <option value="<?= $startYear ?>" <?= $selected2 ?> ><?= $startYear ?></option>
                        </select>
                        <input type="hidden" value="<?= $startYear ?>" id="year_tmp">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">เลขที่สัญญาจ้าง :</td>
                      <td style="padding:5px;" align="left" colspan="3">
                        <select id="contract" class="form-control select2" style="width: 100%;" required onchange="searchInvoice2();">
                          <?= $optionContract ?>
                        </select>
                        <input type="hidden" id="invoiceCode" value="<?= $invoiceCode ?>">
                        <input type="hidden" id="invoiceDate" value="<?= $invoiceDate ?>">

                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="searchInvoice2()" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="reset2()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
        <div class="panel panel-black">
          <div class="panel-heading">รายการข้อมูลใบสั่งงาน</div>
          <form id="form-data2" data-smk-icon="glyphicon-remove-sign" novalidate autocomplete="off">
          <!-- <form data-smk-icon="glyphicon-remove-sign" novalidate action="ajax/invoices/manage2.php" method="post"> -->
            <div class="box-body" >
              <div style="padding:5px" align="center">
              </div>
                <div style="width:100%;overflow-x: auto;">
                  <div id="printableArea">
                    <div id="show-page" >
                      <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                    </div>
                </div>
                </div>
                <div style="margin-top:5px;" align="right">
                  <button type="button" class="btn btn-flat " onclick="cal2()" style="width:100px;<?= $display?>">
                    <i class="fa fa-calculator"></i> คำนวณ
                  </button>
                  <button type="submit" class="btn btn-primary btn-flat " style="width:100px">
                    <i class="fa fa-save"></i> บันทึก
                  </button>
                  <button type="button" onclick="gotoPage('invoices_list_post.php')" class="btn btn-success btn-flat " style="width:100px">
                    <i class="fa fa-mail-reply"></i> ย้อนกลับ
                  </button>
                </div>
            </div>
          </form>
      <!--  # coding -->
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/invoices.js" type="text/javascript"></script>
<script>
  $(document).ready(function() {
    searchInvoice2();
    $('.select2').select2();
  });
</script>
</body>
</html>
