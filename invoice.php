<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php
  include("inc/header.php");
  include("inc/utils.php");
?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>ข้อมูลเอกสารใบแจ้งหนี้</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>การเงินและบัญชี</li>
        <li class="active">ข้อมูลเอกสารใบแจ้งหนี้</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          $cust_id = "";
          $invoiceCode = "";
          $invoiceDate = "";
          $display = "";
          if(isset($_POST['invoiceCode'])){
             $invoiceCode = $_POST['invoiceCode'];
             $sql = "SELECT * FROM tb_invoice WHERE invoice_code = '$invoiceCode'";
             $query = mysqli_query($conn,$sql);
             $row = mysqli_fetch_assoc($query);

             $cust_id       = $row['cust_id'];
             $invoiceDate   = $row['invoice_date'];

             $display       = "display:none";
          }
          $date         = date('Y-m-d');
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));
          $optionCustomer  = getoptionCustomer($cust_id);
          $optionRoute     = getoptionRoute();

      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black" style="<?= $display?>" >
          <div class="panel-heading">ค้นหาข้อมูลเอกสารใบแจ้งหนี้</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">ชื่อลูกค้า* :</td>
                      <td style="padding:5px;" align="left">
                        <select id="cust_id" class="form-control select2" required style="width: 350px;" required onchange="searchInvoice();" data-smk-msg="กรุณาระบุ ชื่อลูกค้า" >
                          <option value="" >&nbsp;</option>
                          <?= $optionCustomer ?>
                        </select>
                        <input type="hidden" id="invoiceCode" value="<?= $invoiceCode ?>">
                        <input type="hidden" id="invoiceDate" value="<?= $invoiceDate ?>">
                      </td>
                      <td style="padding:5px;" align="right">รหัสเส้นทาง :</td>
                      <td style="padding:5px;" align="left">
                        <select id="route_id" class="form-control select2"  style="width: 150px;" required onchange="searchInvoice();">
                          <option value="" >&nbsp;</option>
                          <?= $optionRoute ?>
                        </select>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="6" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="searchInvoice()" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="reset()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
        <div class="panel panel-black">
          <div class="panel-heading">รายการข้อมูลใบสั่งงาน</div>
          <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate >
          <!-- <form data-smk-icon="glyphicon-remove-sign" novalidate action="ajax/invoices/manage.php" method="post"> -->
            <div class="box-body" >
              <div style="padding:5px" align="center">
              </div>
                <div style="width:100%;overflow-x: auto;">
                  <div id="printableArea">
                    <div id="show-page" >
                      <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                    </div>
                </div>
                </div>
                <div style="margin-top:5px;" align="right">
                  <button type="button" class="btn btn-flat " onclick="cal()" style="width:100px;<?= $display?>">
                    <i class="fa fa-calculator"></i> คำนวณ
                  </button>
                  <button type="submit" class="btn btn-primary btn-flat " style="width:100px">
                    <i class="fa fa-save"></i> บันทึก
                  </button>
                  <button type="button" onclick="gotoPage('invoices_list.php')" class="btn btn-success btn-flat " style="width:100px">
                    <i class="fa fa-mail-reply"></i> ย้อนกลับ
                  </button>
                </div>
            </div>
          </form>
      <!--  # coding -->
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/invoices.js?v=2" type="text/javascript"></script>
<script>
  $(function () {
     //Initialize Select2 Elements
     $('.select2').select2();
  });
  $(document).ready(function() {
    searchInvoice();
  });
</script>
</body>
</html>
