<!DOCTYPE html>
<html style="height: auto; min-height: 100%;">

<?php
include("inc/head.php");
include('conf/connect.php');
?>
<style>
#sortable, #sortableCat{ list-style-type: none; margin-top: 10px; padding: 0; width: 100%; }
#sortable li, #sortableCat li { margin-bottom: 10px; padding: 10px; font-size: 1.2em; }
html>body #sortable li ,html>body #sortableCat li  { line-height: 1.2em; }
.ui-state-highlight { height:62px; line-height: 1.2em; }
</style>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper" >

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php");
        $typeCompany = $_SESSION['typeCompany'];
        $linkIndex = "po_list.php";
        $linkIndex2 = "job_list.php";
        $linkIndex3 = 'invoices_list.php';
        $txtInv = "ใบแจ้งหนี้";
        $typeCompanyName = "(เหล็ก)";
        if($typeCompany == 2){
          $typeCompanyName = "(ไปรษณีย์)";
          $linkIndex = "po_list2.php";
          $linkIndex2 = "job_list2.php";
          $linkIndex3 = 'invoices_list_post.php';
          $txtInv = "ใบแจ้งหนี้(เสริม)";
        }
        $role   = $_SESSION['ROLECODE'];
        $access = $_SESSION['ROLEACCESS'];
        $sql = "SELECT * FROM t_role where role_code in($role)";
        //echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>".$access;
        $query  = mysqli_query($conn,$sql);
        $num = mysqli_num_rows($query);

        $page_list = "";
        for ($i=1; $i <= $num ; $i++) {
          $row = mysqli_fetch_assoc($query);

          $page_list .= $row['page_list'];
        }
        //echo ">>>>>".$page_list;
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        หน้าหลัก TRAILER CONTROL SYSTEMS <?= $typeCompanyName ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>หน้าหลัก</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <?php //include("inc/head-main.php"); ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box boxBlack">
                <div class="box-header with-border">
                  <h3 class="box-title">เมนูลัด</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php if(strpos($page_list, ',12') !== false) {?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('<?= $linkIndex; ?>')">
                          <i class="fa fa-book" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                            ใบสั่งงาน
                          </p>
                      </div>
                    </div>
                  <?php } if(strpos($page_list, ',12') !== false){?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('<?= $linkIndex2; ?>')">
                          <i class="fa fa-clock-o" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                            บันทึกการปล่อยรถ
                          </p>
                      </div>
                    </div>
                  <?php } if(strpos($page_list, ',14') !== false){?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('job_approval.php')">
                          <i class="fa fa-check" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                            อนุมัติงาน
                          </p>
                      </div>
                    </div>
                  <?php } if(strpos($page_list, ',22') !== false){?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('allowance_list.php')">
                          <i class="fa fa-calculator" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                            ประมวลผลเบี้ยเลี้ยง
                          </p>
                      </div>
                    </div>
                  <?php } if(strpos($page_list, ',20') !== false){?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('invoices_list.php')">
                          <i class="fa fa-file-text-o" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                            <?= $txtInv ?>
                          </p>
                      </div>
                    </div>
                    <?php if($typeCompany == 2){?>
                      <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('<?= $linkIndex3; ?>')">
                            <i class="fa fa-file-text-o" style="font-size:40px;"></i>
                            <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                              ใบแจ้งหนี้(รายเดือน)
                            </p>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-6 col-xs-12" style="display:none;">
                        <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('purchase_list.php')">
                            <i class="fa fa-btc" style="font-size:40px;"></i>
                            <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                              ใบสำคัญจ่าย
                            </p>
                        </div>
                      </div>
                    <?php } ?>
                  <?php } if(strpos($page_list, ',21') !== false){?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('apinvoices_list.php')">
                          <i class="fa fa-truck" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                            ใบจ่ายรถร่วม
                          </p>
                      </div>
                    </div>
                  <?php } if(strpos($page_list, ',10') !== false){?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('shipment_list.php')">
                          <i class="fa fa-file-text" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                            ใบส่งของ
                          </p>
                      </div>
                    </div>
                  <?php } if(strpos($page_list, ',24') !== false){?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('doc_return_list.php')">
                          <i class="fa fa-ticket" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                            ใบคืนตั๋ว
                          </p>
                      </div>
                    </div>
                  <?php } if(strpos($page_list, ',23') !== false){?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('receipt_list.php')">
                          <i class="fa fa-list-alt" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                            ใบเสร็จรับเงิน
                          </p>
                      </div>
                    </div>
                  <?php } if(strpos($page_list, ',36') !== false){?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('income_list.php')">
                          <i class="fa fa-btc" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                             บันทึกรายจ่าย
                          </p>
                      </div>
                    </div>
                  <?php } if(strpos($page_list, ',15') !== false){?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('employee.php')">
                          <i class="fa fa-users" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                            ทะเบียนพนักงาน
                          </p>
                      </div>
                    </div>
                  <?php } if(strpos($access, 'ED') !== false){?>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                      <div class="info-box bg-green btn btnBg btn-flat" style="text-align: center;padding:10px;" onclick="gotoPage('rechk_apinv.php')">
                          <i class="fa fa-users" style="font-size:40px;"></i>
                          <p class="info-box-number" style="font-size:16px;padding-top:10px;font-weight:400;">
                            ตรวจสอบรายการวางบิล
                          </p>
                      </div>
                    </div>
                    <?php }?>
                </div>
          </div>
        </div>
      </div>
  <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php include("inc/foot.php"); ?>
 <?php include("inc/footer.php"); ?>
</div>
<!-- ./wrapper -->

</body>
</html>
