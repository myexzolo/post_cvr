<?php
include('conf/connect.php');
include('inc/utils.php');


$invoiceCode    = isset($_GET['invoiceCode'])?$_GET['invoiceCode']:"";
$strExcelFileName="export_invoice_".$invoiceCode.".xls";

header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

$numRow = 0;
$con = "";

// if($startDate != "" and  $endDate != "")
// {
//   $con .= " and jo.job_order_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
// }

if($invoiceCode != "")
{
  $con .= " and jo.invoice_code ='". $invoiceCode ."'";
}

$sql = "SELECT * FROM  tb_invoice i,  tb_customer_master c
where i.invoice_code ='$invoiceCode' and i.cust_id = c.cust_id";
//echo $sql;
$query  = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);

$row = mysqli_fetch_assoc($query);
$invoice_code       = $row['invoice_code'];
$invoice_date       = formatDate($row['invoice_date'],'d/m/Y');
$maturity_date      = formatDate($row['maturity_date'],'d/m/Y');
$cust_id            = $row['cust_id'];//รหัสลูกค้า
$credit             = $row['credit'];
$provision          = $row['provision'];
$delivery_cost      = $row['delivery_cost'];
$discount           = $row['discount'];
$ot                 = $row['ot'];
$total_cost         = $row['total_cost'];

$nameCus            = $row['prefix']." ".$row['cust_name'];
$address            = $row['address'];
$tax                = $row['tax'];
$tel                = $row['tel'];


?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">

  <table style="width:800px;font-size:12pt;" id="tableDisplayExp">
      <tr>
        <td colspan="8" style="font-size:16pt;padding:15px;" align="center">บริษัท โชควิรัตน์ จำกัด</td>
      </tr>
      <tr>
        <td colspan="8" style="padding-bottom:5px;font-size:12pt;" align="center">24 หมู่ 1 ต.พระขาว อ.บางบาล จ.พระนครศรีอยุธยา เลขประจำตัวผู้เสียภาษี 0145566002853  Tel/Fax : 035-950805</td>
      </tr>
      <tr>
        <td colspan="8" style="padding:5px;font-size:14pt;" align="center">ใบวางบิล/ใบแจ้งหนี้ - Customer Statement</td>
        </td>
      </tr>
      <tr>
        <td colspan="8" style="width:200px"></td>
      </tr>
      <tr>
        <td colspan="2" style="padding-top:5px;font-size:12pt;" align="left">ลูกค้า-Customer :</td>
        <td colspan="3" style="padding-top:5px;font-size:12pt;" align="left"><?= $nameCus ?></td>
        <td colspan="1" style="padding-top:5px;font-size:12pt;" align="right">เลขที่-No :</td>
        <td colspan="2" style="padding-top:5px;font-size:12pt;" align="left"><?= $invoice_code ?></td>
      </tr>
      <tr>
        <td colspan="2" style="padding-top:5px;font-size:12pt;" align="left">ที่อยู่-Address :</td>
        <td colspan="3" style="padding-top:5px;font-size:12pt;" align="left"><?= $address ?></td>
        <td colspan="1" style="padding-top:5px;font-size:12pt;" align="right">วันที่ :</td>
        <td colspan="2" style="padding:5px; padding-top:5px;font-size:12pt;" align="left"><?= $invoice_date ?></td>
  		</tr>
      <tr>
        <td colspan="2" style="padding-top:5px;font-size:12pt;"></td>
        <td colspan="3" style="padding-top:5px;font-size:12pt;" align="left">เลขประจำตัวผู้เสียภาษี <?= $tax ?></td>
        <td colspan="3" style="border-right: 1px solid;border-left:1px solid;border-top: 1px solid;padding:5px; font-size:14pt" align="center">ยอดเรียกชำระ-Total Amount</td>
      </tr>
      <tr>
        <td colspan="2" style="padding-top:5px;font-size:12pt;" align="left">เรียน-Attention :</td>
        <td colspan="3" style="padding-top:5px;font-size:12pt;" align="left">ฝ่ายบัญชี</td>
        <td colspan="3" style="border-right: 1px solid;border-left:1px solid;font-size:14pt;" align="center"><b><?= number_format($total_cost,2); ?></b></td>
      </tr>
      <tr>
  			<th style="border: 1px solid;padding:5px;vertical-align: middle;width:55px;">ลำดับ</th>
  			<th style="border: 1px solid;padding:5px;vertical-align: middle;width:100px;">วันที่</th>
  			<th style="border: 1px solid;padding:5px;vertical-align:middle;width:160px;">เลขที่ใบจ่าย(FM)</th>
  			<th style="border: 1px solid;padding:5px;vertical-align:middle;width:300px;">รายการ</th>
        <th style="border: 1px solid;padding:5px;vertical-align:middle;width:130px;">ทะเบียนรถ</th>
  			<th style="border: 1px solid;padding:5px;vertical-align:middle;width:100px;">จำนวน</th>
  			<th style="border: 1px solid;padding:5px;vertical-align:middle;width:100px;">ค่าขนส่ง</th>
  			<th style="border: 1px solid;padding:5px;vertical-align:middle;width:100px;s">จำนวนเงิน</th>
  		</tr>
    <tbody>
  <?php
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id  $con
  order by jo.shipment + 0,jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalAmont = 0;
  $sumWeigths = 0;
  $numDP  = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];

    $period_code              = $row['period_code'];


    $price = 0;
    $amount = 0;
    if($price_type_id == 1){
      $amount =  $weights;
      $price  =  $one_trip_ton;
      $total  =  ($weights * $one_trip_ton);
    }else{
      $amount = 1;
      $price  = $price_per_trip;
      $total  = $price_per_trip;
    }
    $sumWeigths += $amount;
    //$totalAmont += $total;

    $numDPArr  = explode(" ", $cust_dp);
    $numDP    += count($numDPArr);
    $sources  = $source." - ".$destination;

    if(strlen ($sources) > 110){
      $numDP++;
    }
  ?>
  <tr>
    <td style="border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;" class="text-center"><?= $i ?></td>
    <td style="border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;" class="text-center" ><?= $job_order_date ?></td>
    <td style="border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;" class="text-left" ><?= $cust_dp ?></td>
    <td style="border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;" class="text-left" ><?= $sources  ?></td>
    <td style="border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;" class="text-center" ><?= $license_plate ?></td>
    <td style="border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;" class="text-right" ><?= number_format($amount,3);?></td>
    <td style="border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;" class="text-right" ><?= number_format($price,2); ?></td>
    <td style="border-left: 1px solid;border-right: 1px solid;padding:5px;vertical-align:top;line-height:20px;" class="text-right" ><?= number_format($total,2); ?></td>
  </tr>
  <?php }
  //echo $numDP;
  $rowsp = 17;
  $dif   = 0;
  if($numDP > $num){
    $dif = ($numDP - $num);
    if($dif <= 2){
      $rowsp++;
    }else if($dif <= 4){
      $rowsp = ($rowsp + 2);
    }else if($dif <= 6){
      $rowsp = ($rowsp + 3);
    }else if($dif <= 8){
      $rowsp = ($rowsp + 3);
    }
  }

  //echo $num.",".$rowsp.",".$numDP.",".$dif;
  if($numDP < $rowsp){
    for($i = $numDP; $i< $rowsp; $i++){
      echo "<tr>";
        echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;'>&nbsp;</td>";
        echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;'>&nbsp;</td>";
        echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;'>&nbsp;</td>";
        echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;'>&nbsp;</td>";
        echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;'>&nbsp;</td>";
        echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;'>&nbsp;</td>";
        echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;'>&nbsp;</td>";
        echo "<td style='border-left: 1px solid;border-right: 1px solid;padding:5px;vertical-align:top;line-height:20px;'>&nbsp;</td>";
      echo "</tr>";
    }
  }
  $totalAmont =  ($delivery_cost - $discount);
  ?>
  <tr>
    <td colspan="5" style="border: 1px solid;padding:5px;vertical-align:top;"  align="center" ><b>รวม</b></td>
    <td style="padding:5px;border-right: 1px solid;border-top: 1px solid;" align="right"><b><?= number_format($sumWeigths,3); ?></b></td>
    <td style="border-right: 1px solid;padding:5px;border-top: 1px solid;" align="right">&nbsp;</td>
    <td style="border-right: 1px solid;padding:5px;border-top: 1px solid;" align="right"><b><?= number_format($delivery_cost,2); ?></b></td>
  </tr>
  <tr>
    <td colspan="4" rowspan="2" style="border: 1px solid;padding:5px;vertical-align:top;"  align="left" >หมายเหตุ :</td>
    <td colspan="2" style="padding:5px;border-top: 1px solid;" align="left">&nbsp;</td>
    <td colspan="2" style="border-right: 1px solid;padding:5px;border-top: 1px solid;" align="right"></td>
  </tr>
  <tr>
    <td colspan="2" style="padding:5px;"class="text-left">ส่วนลด</td>
    <td colspan="2" style="border-right: 1px solid;padding:5px;" class="text-right"><b><?= number_format($discount,2); ?></b></td>
  </tr>
  <tr>
    <td colspan="4" style="border: 1px solid;padding:5px;" class="text-center" ><b><?= convert(number_format($totalAmont,2));?></b></td>
    <td colspan="2" style="border-bottom: 1px solid;padding:5px;" class="text-left">รวมเงินสุทธิ</td>
    <td colspan="2" style="border-right: 1px solid;border-bottom: 1px solid;padding:5px;" class="text-right"><b><?= number_format($totalAmont,2); ?></b></td>
  </tr>
  <tr>
    <td colspan="3" style="border: 1px solid;padding:5px;" class="text-center"  align="center">
      <p>ผู้จัดทำเอกสาร-prepared by</p><br>
      <p>...................................................</p>
      <p>(.........../.........../...........)</p>
    </td>
    <td style="border: 1px solid;padding:5px;" class="text-center" align="center">
      <p>ผู้อนุมัติ-approved by</p><br>
      <p>...................................................</p>
      <p>(.........../.........../...........)</p></td>
    <td colspan="4" style="border: 1px solid;padding:5px;" class="text-center" align="center">
      <p>ผู้รับเอกสาร-received by</p><br>
      <p>...................................................</p>
      <p>(.........../.........../...........)</p></td>
    </td>
  </tr>
  </tbody>
  </table>
</div>
<script>
window.onbeforeunload = function(){return false;};
setTimeout(function(){window.close();}, 10000);
</script>
</body>
</html>
