<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>
<style>
@page {
  margin: 0;
  size: A4 landscape;
}
@media print {
  html, body {
    margin-left: 4mm;
    margin-right: 4mm;
  }
}
</style>
<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php
  include("inc/header.php");
  include("inc/utils.php");
?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>รายงานสรุปรายรับ-รายจ่าย</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>รายงาน</li>
        <li class="active">รายงานสรุปรายรับ-รายจ่าย</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          //$optionCustomer  = getoptionCustomer("");
          $date         = date('Y-m-d');
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));

          $TrailerNo    = getoptionTrailerNo("");
          $optionEmp    = getoptionEmp("","");

      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black">
          <div class="panel-heading">ค้นหาข้อมูล</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">จากวันที่ : </td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $startDate ?>" id="startDate">
                        <input type="hidden" value="<?= $startDate ?>" id="startDateTmp">
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ถึงวันที่ :</td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $endDate ?>" id="endDate">
                        <input type="hidden" value="<?= $endDate ?>" id="endDateTmp">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">ทะเบียนรถ : </td>
                      <td style="padding:5px;" align="left">
                        <select id="trailerId" class="form-control select2" style="width: 100%;" required>
                          <option value="" ></option>
                          <?= $TrailerNo ?>
                        </select>
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ชื่อคนขับ : </td>
                      <td style="padding:5px;" align="left">
                        <select id="employeeId" class="form-control select2" style="width: 100%;" required>
                          <option value="" ></option>
                          <?= $optionEmp ?>
                        </select></td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">ต้นทาง : </td>
                      <td style="padding:5px;" align="left">
                        <input type="text" class="form-control" value="" id="source">
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ปลายทาง : </td>
                      <td style="padding:5px;" align="left">
                        <input type="text" class="form-control" value="" id="destination">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="search()" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="reset()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
        <div class="panel panel-black">
          <div class="panel-heading">รายการรายรับ-รายจ่าย
            <button class="btn btn-success pull-right btn-flat" style="position: relative;top:-7px;right:-12px;display:none" onclick="postURL('income_history.php')">+ ประวัติการพิมพ์</button>

          </div>
            <div class="box-body" >
              <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
                <div style="width:100%;">
                  <div id="show-page" >
                    <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                  </div>
                </div>
              </form>
            </div>
      <!--  # coding -->
      </div>

      <div id="signature" style="display:none;">
        <div id="printableArea">
          <div id="show-page-exp" >
            <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
          </div>
      </div>
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/income.js" type="text/javascript"></script>
<script>
  search();
</script>
</body>
</html>
