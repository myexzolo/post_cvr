<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php
  include("inc/header.php");
  include("inc/utils.php");
?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>ข้อมูลเอกสารใบเสร็จรับเงิน</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>การเงินและบัญชี</li>
        <li class="active">ข้อมูลเอกสารใบเสร็จรับเงิน</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          $custId       = "";
          $receiptCode  = "";
          $display      = "";
          $customer_name  = "";
          $customer_address = "";
          if(isset($_POST['receiptCode'])){
             $receiptCode = $_POST['receiptCode'];
             $sql = "SELECT * FROM tb_receipt WHERE receipt_code = '$receiptCode'";
             $query = mysqli_query($conn,$sql);
             $row = mysqli_fetch_assoc($query);
             //echo $sql;
             $custId        = $row['cust_id'];
             $receiptCode   = $row['receipt_code'];
             $display       = "display:none";
             $customer_name      = $row['customer_name'];
             $customer_address   = $row['customer_address'];

          }
          $date         = date('Y-m-d');
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));
          $optionCustomer  = getoptionCustomer($custId);


      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black" style="<?= $display?>" >
          <div class="panel-heading">ค้นหาข้อมูลใบเสร็จรับเงิน</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">ชื่อลูกค้า :</td>
                      <td style="padding:5px;" align="left" colspan="2">
                        <select id="cust_id" class="form-control select2" style="width: 100%;" required onchange="searchReceipt();">
                          <option value="" ></option>
                          <?= $optionCustomer ?>
                        </select>
                        <input type="hidden" id="receiptCode" value="<?= $receiptCode ?>">
                        <input type="hidden" id="customerName" value="<?= $customer_name ?>">
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="searchReceipt()" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="reset()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
        <div class="panel panel-black">
          <div class="panel-heading">รายการข้อมูลใบแจ้งหนี้</div>
          <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate >
            <div class="box-body" >
              <div style="padding:5px" align="center">
              </div>
                <div style="width:100%;overflow-x: auto;">
                  <div id="printableArea">
                    <div id="show-page" >
                      <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                    </div>
                </div>
                </div>
                <div style="margin-top:5px;" >
                  <div class="row">
                      <div class="col-md-6">
                        <button type="button" onclick="gotoPage('receipt_list.php')" class="btn btn-success btn-flat " style="width:100px">
                          <i class="fa fa-mail-reply"></i> ย้อนกลับ
                        </button>
                      </div>
                      <div class="col-md-6" align="right">
                        <button type="submit" class="btn btn-primary btn-flat " style="width:100px" id="submitReceipt" disabled>
                          <i class="fa fa-save"></i> บันทึก
                        </button>
                      </div>
                    </div>
                </div>
            </div>
          </form>
      <!--  # coding -->
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/receipt.js" type="text/javascript"></script>
<script>
  $(document).ready(function() {
    searchReceipt();
  });
</script>
</body>
</html>
