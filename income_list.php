<!DOCTYPE html>
<html style="height: auto; min-height: 100%;">

<?php
include("inc/head.php");
include("inc/utils.php");

?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg" style="min-height: 500px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>ข้อมูลรายรับ-รายจ่าย</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>ข้อมูลรายวัน</li>
        <li class="active">ข้อมูลใบสั่งงาน</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          $date         = date('Y-m-d');
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));


          $month  = date("m",strtotime('-1 month'));
          $year   = date("Y");
          $year2  = date('Y', strtotime('-1 year'));
          $year3  = date('Y', strtotime('-2 year'));

          $TrailerNo    = getoptionTrailerNo("");
          $optionEmp    = getoptionEmp("","");
      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black">
          <div class="panel-heading">ค้นหารายการบันทึก</div>
          <div class="box-body">
            <div class="row" align="center">
                <table>
                  <tr>
                    <td style="padding:5px;" align="right">ประจำเดือน : </td>
                    <td style="padding:5px;" align="left" width="200px">
                      <select name="month" class="form-control" id="s_month" <?=$disabled ?> required>
                        <option value="01" <?= ($month == '01' ? 'selected="selected"':'') ?>>มกราคม</option>
                        <option value="02" <?= ($month == '02' ? 'selected="selected"':'') ?>>กุมภาพันธ์</option>
                        <option value="03" <?= ($month == '03' ? 'selected="selected"':'') ?>>มีนาคม</option>
                        <option value="04" <?= ($month == '04' ? 'selected="selected"':'') ?>>เมษายน</option>
                        <option value="05" <?= ($month == '05' ? 'selected="selected"':'') ?>>พฤษภาคม</option>
                        <option value="06" <?= ($month == '06' ? 'selected="selected"':'') ?>>มิถุนายน</option>
                        <option value="07" <?= ($month == '07' ? 'selected="selected"':'') ?>>กรกฎาคม</option>
                        <option value="08" <?= ($month == '08' ? 'selected="selected"':'') ?>>สิงหาคม</option>
                        <option value="09" <?= ($month == '09' ? 'selected="selected"':'') ?>>กันยายน</option>
                        <option value="10" <?= ($month == '10' ? 'selected="selected"':'') ?>>ตุลาคม</option>
                        <option value="11" <?= ($month == '11' ? 'selected="selected"':'') ?>>พฤศจิกายน</option>
                        <option value="12" <?= ($month == '12' ? 'selected="selected"':'') ?>>ธันวาคม</option>
                      </select>
                      <input type="hidden" value="<?= $month ?>" id="month">
                    </td>
                    <td style="padding:5px;width:80px;" align="right">ปี :</td>
                    <td style="padding:5px;" align="left">
                      <select name="year" class="form-control" id="s_year" required>
                        <option value="<?= $year ?>"><?= $year ?></option>
                        <option value="<?= $year2 ?>"><?= $year2 ?></option>
                        <option value="<?= $year3 ?>"><?= $year3 ?></option>
                      </select>
                      <input type="hidden" value="<?= $year ?>" id="year">
                    </td>
                  </tr>
                  <tr>
                    <td style="padding:5px;" align="right">ทะเบียนรถ : </td>
                    <td style="padding:5px;" align="left">
                      <select id="trailerId" class="form-control select2" style="width: 100%;" required>
                        <option value="" >&nbsp;</option>
                        <?= $TrailerNo ?>
                      </select>
                    </td>
                    <td style="padding:5px;" align="right"></td>
                    <td style="padding:5px;" align="left"></td>
                  </tr>
                  <tr>
                    <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                        <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="list()" style="width:100px">ค้นหา</button>
                        <button type="button" onclick="resetList()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                    </td>
                  </tr>
                </table>
              </div>
          </div>
        </div>
        <div class="panel panel-black">
          <div class="panel-heading">รายการข้อมูลใบสั่งงาน
            <button class="btn btn-success pull-right btn-flat" style="position: relative;top:-7px;right:-12px" onclick="postURL('income_up.php')">+ บันทึกรายจ่าย</button>
          </div>
            <div class="box-body" >
                <div id="printableArea">
                  <div id="show-page">
                    <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                  </div>
                </div>
            </div>
      <!--  # coding -->
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->
</div>
<?php include("inc/foot.php"); ?>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/income.js" type="text/javascript"></script>
<script>
  $('.select2').select2();
  $(document).ready(function() {
    list();
  });
</script>
</body>
</html>
