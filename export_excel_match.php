<?php
include('conf/connect.php');
include('inc/utils.php');


$month          = isset($_GET['month'])?$_GET['month']:"";
$year           = isset($_GET['year'])?$_GET['year']:"";
$trailerId      = isset($_GET['trailerId'])?$_GET['trailerId']:"";

$startDate      = $year."/".$month."/01";
$endDate        = date("Y/m/t", strtotime($startDate));


$strExcelFileName="export_income_".$year.$month.".xls";

header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

$fuel_litres = 0;
$fuel_costs = 0;
$blank_charges = 0;
$allowances = 0;
$kog_expenses = 0;
$job_ended_acc_expenses = 0;
$allowance_oths = 0;
$job_ended_expressways = 0;
$job_ended_recaps = 0;
$total_amount_allowances = 0;
$total_amount_receives = 0;
$withholdings = 0;
$job_order_profits = 0;
$salarys = 0;
$fuel_driver_bills = 0;
$job_ended_repairess = 0;
$diligences = 0;

$numRow = 0;
$con = "";

if($trailerId != "")
{
  $trailerIdArr = explode(":", $trailerId);
  $trailerId    = $trailerIdArr[0];

  $con .= " and jo.trailer_id ='". $trailerId ."'";
}
?>

<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
  <table class="table table-bordered table-striped tabf" id="tableDisplay" style="font-size:12px;width:100%;">
    <thead>
      <tr>
        <td id="hd1" style="font-size:16px;" align="center" colspan="21">
          <b>รายงานกำไร - ขาดทุน</b>
        </td>
      </tr>
      <tr>
        <td style="font-size:16px;" align="center" colspan="21">
          <b>
            ประจำวันที่ <?= formatDateTh($startDate) ?> ถึงวันที่ <?= formatDateTh($endDate) ?>
          </b>
        </td>
      </tr>
      <tr>
  			<th style="width:30px;border:1px solid black" class="tdb">No.</th>
        <th style="width:30px;border:1px solid black" class="tdb">เที่ยว</th>
        <th style="width:80px;border:1px solid black" class="tdb">ทะเบียน</th>
        <th style="width:80px;border:1px solid black" class="tdb">หาง</th>
        <th  class="tdb" style="border:1px solid black">ชื่อคนขับ</th>
  			<th style="width:55px;border:1px solid black" class="tdb">จำนวนลิตร</th>
  			<th  class="tdb" style="border:1px solid black">ค่าน้ำมัน</th>
        <th  class="tdb" style="border:1px solid black">ตีเปล่า</th>
        <th  class="tdb" style="border:1px solid black">เบี้ยเลี้ยง</th>
        <th  class="tdb" style="border:1px solid black">เบี้ยขยัน</th>
        <th  class="tdb" style="border:1px solid black">คอก</th>
        <th  class="tdb" style="border:1px solid black">อื่นๆ</th>
        <th style="width:100px;border:1px solid black" class="tdb">ค่าแรง/<br>ซ่อมหัว-หาง</th>
        <th style="width:80px;border:1px solid black" class="tdb">ทางด่วน</th>
        <th style="width:60px;border:1px solid black" class="tdb">ปะยาง</th>
        <th style="width:80px;border:1px solid black"class="tdb">เงินเดือน</th>
        <th style="width:80px;border:1px solid black" class="tdb">บิลน้ำมัน</th>
        <th style="width:100px;border:1px solid black" class="tdb">รวมค่าใช้จ่าย</th>
        <th style="width:100px;border:1px solid black" class="tdb">รายได้ค่าขนส่ง</th>
        <th style="width:70px;border:1px solid black" class="tdb">หัก 1%</th>
        <th style="width:100px;border:1px solid black" class="tdb">กำไรขั้นต้น</th>
  		</tr>
    </thead>
    <tbody>
  <?php
  $con .= " and jo.job_order_date between '". $startDate ."' and '". $endDate ."'";
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT income_h_id, count(jo.id) as sumjob ,SUM(jo.weights) as weights,SUM(jo.fuel_cost) as fuel_cost,SUM(jo.fuel_litre) as fuel_litre,
  SUM(jo.blank_charge) as blank_charge,SUM(jo.allowance) as allowance,SUM(jo.kog_expense) as kog_expense,SUM(jo.kog_expense) as kog_expense,
  SUM(jo.allowance_oth) as allowance_oth,SUM(jo.total_amount_allowance) as total_amount_allowance,SUM(jo.job_ended_clearance) as job_ended_clearance,
  SUM(jo.job_ended_recap) as job_ended_recap,SUM(jo.job_ended_expressway) as job_ended_expressway,SUM(jo.job_ended_passage_fee) as job_ended_passage_fee,
  SUM(jo.job_order_profit) as job_order_profit,SUM(jo.fuel_driver_bill) as fuel_driver_bill,SUM(jo.job_ended_repaires) as job_ended_repaires,
  SUM(jo.job_ended_other_expense) as job_ended_other_expense,SUM(jo.total_amount_receive) as total_amount_receive,
  SUM(jo.job_ended_acc_expense) as job_ended_acc_expense,
  GROUP_CONCAT(DISTINCT em.salary) as salary,
  GROUP_CONCAT(DISTINCT jo.id ORDER BY jo.id DESC SEPARATOR ',') as id,
  GROUP_CONCAT(DISTINCT trim(jo.away_regi_no) ORDER BY jo.away_regi_no DESC SEPARATOR '<br>') as away_regi_no,
  GROUP_CONCAT(DISTINCT em.employee_name ORDER BY em.employee_name DESC SEPARATOR '<br>') as employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id and  jo.job_status_id = '2' and invoice_code is not null
        and jo.income_h_id > 0
  group by jo.income_h_id, t.license_plate
  order by t.license_plate";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalTransfer = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    //$id                 = $row['id'];
    //$job_order_no       = $row['job_order_no'];
    //$delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    //$job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
    //$job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    //$cust_id            = $row['cust_id'];//รหัสลูกค้า
    //$employee_id        = $row['employee_id'];//รหัสพนักงาน
    //$trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    //$away_spec_no       = $row['away_spec_no'];//สเปคหาง
    //$affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    //$route_id           = $row['route_id'];//รหัสเส้นทาง
    //$source             = $row['source'];//ต้นทาง
    //$destination        = $row['destination'];//ปลายทาง
    //$distance           = $row['distance'];//ระยะทาง
    //$cust_dp            = $row['cust_dp'];//DP
    //$delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    //$department         = $row['department'];//หน่วยงาน
    //$product_name       = $row['product_name'];//สินค้า
    //$number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    //$shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    //$date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    //$time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    //$date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    //$time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    //$date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    //$time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    //$date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    //$time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    //$remark             = $row['remark'];
    //$shipment           = $row['shipment'];
    //$price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    //$one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    //$ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    //$price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    //$ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive     = $row['total_amount_receive'];//ราคาค่าขนส่ง
    //$total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = chkNum($row['fuel_driver_bill']);//บิลน้ำมันจากคนขับ
    //$job_status_id            = $row['job_status_id'];
    $job_order_profit         = $row['job_order_profit'];
    $sumjob                   = $row['sumjob'];

    //$invoice_code             = $row['invoice_code'];
    //$period_code              = $row['period_code'];
    $license_plate            = $row['license_plate'];

    $income_h_id              = $row['income_h_id'];

    $sql2 = "SELECT *FROM tb_income_his where income_h_id = $income_h_id ";
    //echo $sql;
    $query2   = mysqli_query($conn,$sql2);
    $row2     = mysqli_fetch_assoc($query2);

    $month            = $row2['month'];
    $year             = $row2['year'];
    $trailer_id       = $row2['trailer_id'];
    $other_txt1       = $row2['other_txt1'];
    $other_txt2       = $row2['other_txt2'];
    $other_txt3       = $row2['other_txt3'];
    $other_value1     = $row2['other_value1'];//
    $other_value2     = $row2['other_value2'];//
    $other_value3     = $row2['other_value3'];//
    $salary           = $row2['salary'];//
    $bill             = $row2['bill'];//
    $labor            = $row2['labor'];//
    $diligence        = $row2['diligence'];//
    $total            = $row2['total'];
    $job_profits      = $row2['job_profits'];

    $job_ended_repaires  = $labor;
    $fuel_driver_bill    = $bill;
    $other = ($other_value1 + $other_value2 + $other_value3);
    if(empty($allowance_oth)){
      $allowance_oth       = $other;
    }

    //$salary                   = chkNum($row['salary']);


    $transfer = ($fuel_cost + $blank_charge + $allowance + $kog_expense  + $allowance_oth + $job_ended_repaires + $job_ended_expressway + $job_ended_recap + $salary + $fuel_driver_bill + $diligence);
    $totalTransfer +=  $transfer;

    $job_order_profit = ($total_amount_receive - $transfer);

    $total_amount_allowance = $transfer;

    $withholding  =  ($total_amount_receive * 0.01);

    $job_order_profit -= $withholding;

    $fuel_cost_cal =  ($fuel_cost * 0.5);
    if($fuel_driver_bill >= $fuel_cost_cal){
      $fuel_driver_bill = 1000;
    }

    $fuel_litres            += $fuel_litre;
    $fuel_costs             += $fuel_cost;
    $blank_charges          += $blank_charge;
    $allowances             += $allowance;
    $kog_expenses           += $kog_expense;
    $job_ended_acc_expenses += $job_ended_acc_expense;
    $allowance_oths         += $allowance_oth;
    $job_ended_expressways  += $job_ended_expressway;
    $job_ended_recaps       += $job_ended_recap;
    //$total_amount_allowances  += $total_amount_allowance;
    $total_amount_allowances  += $total_amount_allowance;
    $total_amount_receives  += $total_amount_receive;
    $withholdings           += $withholding;
    $job_order_profits      += $job_order_profit;
    $salarys                += $salary;
    $fuel_driver_bills      += $fuel_driver_bill;
    $job_ended_repairess    += $job_ended_repaires;
    $diligences             += $diligence;

    ?>
  <tr>
    <td class="text-center tdb" style="border:1px solid black;"><?= $i ?></td>
    <td class="text-center tdb" style="border:1px solid black;"><?= $sumjob ?></td>
    <td class="text-left tdb" style="border:1px solid black;"><?= $license_plate ?></td>
    <td class="text-left tdb" style="border:1px solid black;"><?= $away_regi_no ?></td>
    <td class="text-left tdb" style="border:1px solid black;"><?= $Employee_Name ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($fuel_litre,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($fuel_cost,2);  ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($blank_charge,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($allowance,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($diligence,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($kog_expense,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($allowance_oth,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($job_ended_repaires,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($job_ended_expressway,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($job_ended_recap,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($salary,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($fuel_driver_bill,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($total_amount_allowance,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($total_amount_receive,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($withholding,2); ?></td>
    <td class="text-right tdb" style="border:1px solid black;"><?= number_format($job_order_profit,2); ?></td>
  </tr>
<?php
}
if($num == 0){
  ?>
  <tr>
    <td colspan="20" class="text-right tdb" >&nbsp;</td>
  </tr>
  <?php
}
//  echo "<div>ไม่พบข้อมูล</div>";
//}
?>
<tr>
  <td align="right" style="border:1px solid black;" colspan="5" class="text-right tdb" ><b>รวม</b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($fuel_litres,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($fuel_costs,2);  ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($blank_charges,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($allowances,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($diligences,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($kog_expenses,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($allowance_oths,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($job_ended_repairess,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($job_ended_expressways,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($job_ended_recaps,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($salarys,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($fuel_driver_bills,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($total_amount_allowances,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($total_amount_receives,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><?= number_format($withholdings,2); ?></b></td>
  <td align="right" style="border:1px solid black;" class="text-right tdb" ><b><div id="jobProfits"><?= number_format($job_order_profits,2); ?></div></b></td>
</tr>
</tbody>
</table>
</div>
<script>
window.onbeforeunload = function(){return false;};
setTimeout(function(){window.close();}, 10000);
</script>
</body>
</html>
