<!DOCTYPE html>
<html>
<body style="font-size:12px;">
<style>
@page {
  size: A4;
  margin: 0;
}

.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #333 !important;
}

@media print {
  html, body {
    size: portrait;
    margin-left: 20px;
    margin-right: 10px;
  }
  .textRed {
       color: red !important;
   }

   .break{
     page-break-after: always;
   }

  /* ... the rest of the rules ... */
}

page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}

page[size="A4"] {
  width: 23cm;
}

</style>
<?php
include("inc/head.php");
include('inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$startDate    = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate      = isset($_POST['endDate'])?$_POST['endDate']:"";
$periodCode   = isset($_POST['periodCode'])?$_POST['periodCode']:"";
$numRow = 0;
$con = "";

//echo ">>>>".$startDate;
$dateNow   = date("d/m/Y");

if($periodCode != "")
{
  $con .= " and jo.period_code ='". $periodCode ."'";
}
?>
<div>
  <?php
  $sql = "SELECT jo.*,em.employee_name,em.address,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id and  jo.job_status_id = '2'
  order by em.employee_name,jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalTransfer = 0;

  $EmployeeNameTmp = "";
  $idCopy = 0;
  $number = 0;

  $allowancOth = 0;
  $allowanceTotal = 0;
  $allowanceTotal2 = 0;
  $totalCost = 0;
  $totalCost2 = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $address            = $row['address'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];

    $period_code              = $row['period_code'];
    $transfer = ($fuel_cost + $allowance + $kog_expense + $blank_charge + $allowance_oth);
    $totalTransfer +=  $transfer;

    if($EmployeeNameTmp != $Employee_Name){
      if($EmployeeNameTmp != ""){
        echo "<tr class='trtt_$idCopy'>";
        echo "  <td colspan='10' class='text-center'><b>".convert(number_format($totalCost,2))."</b></td>";
        echo "  <td colspan='3' class='text-right'>เบี้ยเลี้ยงงวดนี้</td>";
        echo "  <td colspan='2' class='text-right'><b>".number_format($totalCost,2)."</b></td>";
        echo "</tr>";
        echo "<tr class='trtt2_$idCopy'>";
        echo "  <td colspan='11' class='text-center'><b>".convert(number_format($totalCost2,2))."</b></td>";
        echo "  <td colspan='3' class='text-right'>เบี้ยเลี้ยงงวดนี้</td>";
        echo "  <td colspan='2' class='text-right'><b>".number_format($totalCost2,2)."</b></td>";
        echo "</tr>";
        echo "</table>";
        echo "<table style='width:100%'>";
        echo "<tr >";
        echo "  <td style='padding:5px;width:50%' class='text-center' >";
        echo "    <p>ผู้จัดทำเอกสาร-prepared by</p><br>";
        echo "    <p>...................................................</p>";
        echo "    <p>(.........../.........../...........)</p>";
        echo "  </td>";
        echo "  <td style='padding:5px;width:50%' class='text-center'>";
        echo "    <p>ผู้อนุมัติ-approved by</p><br>";
        echo "    <p>...................................................</p>";
        echo "    <p>(.........../.........../...........)</p></td>";
        echo "</tr>";
        echo "</table>";
        echo "</div>";
        echo "</td>";
        echo "</tr>";
        echo "<tr>";
        echo "<td style='height:10px;'></td>";
        echo "</tr>";
        echo "<tr>";
        echo "  <td style='height:130mm'>";
        echo "    <div id='copyTax_$idCopy' style='padding:15px 20px 0px 10px;'></div>";
        echo "  </td>";
        echo "</tr>";
        echo "</table><br>";
        echo "<div class='break'></div>";
      }
      $number = 0;
      $allowancOth = 0;
      $allowanceTotal = 0;
      $allowanceTotal2 = 0;
      $totalCost = 0;
      $totalCost2 = 0;
      $idCopy++;

      $EmployeeNameTmp = $Employee_Name;
  ?>
<table border="0" align="center" style="width:100%;">
  <tr>
    <td style="height:175mm;" valign="top">
      <div id="taxData_<?= $idCopy ?>" class="allowance" style="padding:30px 20px 0px 10px;">
      <table style="width: 100%;" border="0">
        <tr>
          <td align="left"  style="width:150px;vertical-align: top;font-size:14px;">ใบสำรองจ่ายค่าขนส่ง</td>
          <td style="font-size:14px;font-weight:500;vertical-align: top;" align="center">
            <span style="font-size:16px;"><b>หจก.โชควิรัตน์ทรานสปอร์ต</b></span><br>
            24 หมู่ 1 ต.พระขาว อ.บางบาล จ.พระนครศรีอยุธยา
          </td>
          <td align="right" class="scriptID_<?= $idCopy ?>" style="width:150px;vertical-align: top;font-size:11px;">ต้นฉบับ</td>
        </tr>
      </table>
      <br>
      <table style="width: 100%;" border="0">
        <tr>
          <td align="left"  style="font-size:12px;"><b>ชื่อ - สกุล :</b>&nbsp;<?= $Employee_Name ?></td>
          <td align="right" style="width:80px;font-size:12px;"><b>เลขที่-No. :</b> &nbsp;</td>
          <td align="left" style="width:110px;font-size:12px;"><?=$periodCode;?></td>
        </tr>
        <tr>
          <td align="left"  style="font-size:12px;height:35px;"><b>ที่อยู่ :</b>&nbsp;<?= $address ?></td>
          <td align="right" style="font-size:12px;"><b>วันที่-Date :</b> &nbsp;</td>
          <td align="left" style="font-size:12px;"><?= $dateNow ?></td>
        </tr>
        <tr>
          <td align="left" colspan="3" style="font-size:12px;height:35px;"><i>เบี้ยเลี้ยงประจำวันที่ <?= $startDate;?></i></td>
        </tr>
      </table>
      <div style="height:5px;"></div>
      <table class="table table-bordered table-striped " id="tableDisplay" style="font-size:11px;">
          <tr class="text-center">
            <th style="width:20px;">No.</th>
            <th style="width:70px;" class="text-center" >ว/ด/ป</th>
            <th class="text-center" >ใบสั่งงาน</th>
            <th style="width:100px;" class="text-center" >เลข DP</th>
            <th class="text-center" >ต้นทาง</th>
            <th class="text-center" >ปลายทาง</th>
            <th style="width:70px;" class="text-center" >ทะเบียน</th>
            <th class="text-center" >น้ำมัน</th>
            <th class="text-center" >เบี้ยเลี้ยง</th>
            <th class="text-center" >คอก</th>
            <th class="text-center" >ตีเปล่า</th>
            <th class="text-center" >ปะยาง</th>
            <th class="text-center" >ทาง<br>ด่วน</th>
            <th class="text-center tr_<?= $idCopy ?>">คชจ.<br>อื่นๆ</th>
            <th class="text-center">อื่นๆ</th>
            <th style="width:100px;">จำนวนเงิน</th>
          </tr>
    <?php
       }
       $number++;

       $allowancOth       = $allowance_oth + $job_ended_clearance + $job_ended_passage_fee + $job_ended_repaires + $job_ended_other_expense;
       $allowanceTotal    = $fuel_cost + $allowance + $kog_expense + $blank_charge + $job_ended_recap + $job_ended_expressway +  $allowancOth;
       $allowanceTotal2   = $allowanceTotal + $job_ended_acc_expense;
       $totalCost         += $allowanceTotal;
       $totalCost2        += $allowanceTotal2;
    ?>
          <tr class="text-center">
            <td class="text-center"><?= $number ?></td>
            <td class="text-center" ><?= $job_order_date ?></td>
            <td class="text-center" ><?= $job_order_no ?></td>
            <td class="text-left" ><?= $cust_dp ?></td>
            <td class="text-left" ><?= $source ?></td>
            <td class="text-left" ><?= $destination ?></td>
            <td class="text-center" ><?= $license_plate ?></td>
            <td class="text-right" ><?= number_format($fuel_cost,2);  ?></td>
            <td class="text-right" ><?= number_format($allowance,2); ?></td>
            <td class="text-right" ><?= number_format($kog_expense,2); ?></td>
            <td class="text-right" ><?= number_format($blank_charge,2); ?></td>
            <td class="text-right" ><?= number_format($job_ended_recap,2); ?></td>
            <td class="text-right" ><?= number_format($job_ended_expressway,2); ?></td>
            <td class="text-right td_<?= $idCopy ?>" style="display:none;"><?= number_format($job_ended_acc_expense,2); ?></td>
            <td class="text-right"><?= number_format($allowancOth,2); ?></td>
            <td class="text-right" style="display:auto;" ><?= number_format($allowanceTotal,2); ?></td>
            <td class="text-right tt2_<?= $idCopy ?>" style="display:none;"><?= number_format($allowanceTotal2,2); ?></td>
          </tr>
<?php } ?>
    <tr class="trtt_<?= $idCopy?>">
      <td colspan='13' class='text-center'><b><?= convert(number_format($totalCost,2)) ?></b></td>
      <td colspan='2' class='text-right'><b><?= number_format($totalCost,2)?></b></td>
    </tr>
    <tr class="trtt2_<?= $idCopy?>">
      <td colspan='14' class='text-center'><b><?= convert(number_format($totalCost2,2))?></b></td>
      <td colspan='2' class='text-right'><b><?= number_format($totalCost2,2)?></b></td>
    </tr>
  </table>
  <table style='width:100%'>
  <tr >
    <td style='padding:5px;width:50%' class='text-center' >
      <p>ผู้จัดทำเอกสาร-prepared by</p><br>
      <p>...................................................</p>
      <p>(.........../.........../...........)</p>
    </td>
    <td style='padding:5px;width:50%' class='text-center'>
      <p>ผู้อนุมัติ-approved by</p><br>
      <p>...................................................</p>
      <p>(.........../.........../...........)</p></td>
  </tr>
  </table>
  </div>
  </td>
  </tr>
  <tr>
    <td style='height:10px;'></td>
  </tr>
  <tr>
    <td style='height:130mm'>
      <div id='copyTax_<?=$idCopy?>' style='padding:15px 20px 0px 10px;'></div>
    </td>
  </tr>
  </table>
</div>
<?php
include("inc/footer.php");
?>

<script type="text/javascript">
  $(document).ready(function(){
      var numItems = $('.allowance').length;
      for(var x= 1 ; x <= numItems ; x++){
        var id    = "taxData_"+ x;
        var copy  = "scriptID_" + x;
        var clone = "copyTax_" + x;
        var tr    = "tr_" + x;
        var td    = "td_" + x;
        var tt    = "tt_" + x;
        //var tt2   = "tt2_" + x;
        var trtt  = "trtt_" + x;
        var trtt2 = "trtt2_" + x;

        var data = $('#'+ id).html();
        data = data.replace(/display:none;/g, "");
        data = data.replace(/display:auto;/g, "display:none;");

        $('#'+ clone).html(data);

        var num = 0;
        $('.'+ copy).each(function() {
          num++;
          if(num == 2){
            $(this).html("<span>สำเนา</span>");
          }
       });

      //  num = 0;
      //  $('.'+ tt).each(function() {
      //    num++;
      //    if(num == 2){
      //      $(this).html($('.'+ tt2).html());
      //    }
      // });

       num = 0;
       $('.'+ tr).each(function() {
         num++;
         if(num == 1){
           $(this).css("display", "none");
         }
      });

     //  num = 0;
     //  $('.'+ td).each(function() {
     //    num++;
     //    if(num == 1){
     //      $(this).css("display", "none");
     //    }
     // });


   num = 0;
   $('.'+ trtt).each(function() {
     num++;
     if(num == 2){
       $(this).css("display", "none");
     }
  });
  num = 0;
  $('.'+ trtt2).each(function() {
    num++;
    if(num == 1){
      $(this).css("display", "none");
    }
 });
 setTimeout(function(){
   window.print();
   window.close();
  }, 1000);
    }
  });
</script>
</body>
<html>
