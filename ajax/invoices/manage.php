<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
$typeCompany = $_SESSION['typeCompany'];

$idJob          = isset($_POST['idJob'])?$_POST['idJob']:"";
$invoice_date   = isset($_POST['invoice_date'])?$_POST['invoice_date']:"";
$credit         = chkNum($_POST['credit']);
$maturity_date  = isset($_POST['maturity_date'])?$_POST['maturity_date']:"";
$provision      = chkNum($_POST['provision']);
$delivery_cost  = chkNum($_POST['delivery_cost']);
$discount       = chkNum($_POST['discount']);
$ot             = chkNum($_POST['ot']);
$total_cost     = chkNum($_POST['total_cost']);
$cust_id        = isset($_POST['cust_id'])?$_POST['cust_id']:"";

$invoice_code   = isset($_POST['invoice_code'])?$_POST['invoice_code']:"";


$arr['invoice_code']  = $invoice_code;
$arr['invoice_date']  = $invoice_date;
$arr['credit']        = $credit;
$arr['maturity_date'] = $maturity_date;
$arr['provision']     = $provision;
$arr['delivery_cost'] = $delivery_cost;
$arr['discount']      = $discount;
$arr['ot']            = $ot;
$arr['total_cost']    = $total_cost;
$arr['cust_id']       = $cust_id;



if(!empty($idJob)){
  if($invoice_code != ""){
      $sql = "UPDATE tb_job_order SET invoice_code = null WHERE invoice_code  = '$invoice_code'";
      mysqli_query($conn,$sql);

      // $sql = "UPDATE tb_invoice SET
      //        credit = '$credit',
      //        maturity_date ='$maturity_date',
      //        provision = '$provision',
      //        delivery_cost ='$delivery_cost',
      //        discount = '$discount',
      //        ot = '$ot',
      //        total_cost = '$total_cost'
      //        where invoice_code = '$invoice_code'";

      unset($_POST['invoice_date']);
      unset($_POST['cust_id']);

      $sql = DBUpdatePOST($arr,'tb_invoice','invoice_code');

      mysqli_query($conn,$sql);

      // echo $sql;
  }else{
      $dateYM       = date('ymd');

      if($typeCompany == 2){
        $ctiv         = "CVIV".$dateYM;
      }else{
        $ctiv         = "CTIV".$dateYM;
      }


      $sql = "SELECT max(invoice_code) as invoice_code FROM  tb_invoice where invoice_code LIKE '$ctiv%' and status_del not in ('Y')";
      $query  = mysqli_query($conn,$sql);
      $row    = mysqli_fetch_assoc($query);
      $invoiceCode  = $row['invoice_code'];

      if(!empty($invoiceCode)){
        $lastNum = substr($invoiceCode,strlen($ctiv));
        $lastNum = $lastNum + 1;
        $invoice_code = $ctiv.sprintf("%03d", $lastNum);
      }else{
        $invoice_code = $ctiv.sprintf("%03d", 1);
      }

      $arr['invoice_code']  = $invoice_code;

      // $sql = "INSERT INTO tb_invoice
      //        (invoice_code,invoice_date,credit,maturity_date,provision,delivery_cost,discount,ot,total_cost,cust_id)
      //        VALUES('$invoice_code','$invoice_date','$credit','$maturity_date','$provision','$delivery_cost','$discount','$ot','$total_cost','$cust_id')";

      $sql = DBInsertPOST($arr,'tb_invoice');

      mysqli_query($conn,$sql);

      //echo $sql;
  }

  foreach( $idJob as $key => $id ) {
    $sql = "UPDATE tb_job_order SET invoice_code = '$invoice_code' WHERE id  = '$id'";
    mysqli_query($conn,$sql);
  }
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
