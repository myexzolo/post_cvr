<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$contract      = isset($_POST['contract'])?$_POST['contract']:"";
$startDate    = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate      = isset($_POST['endDate'])?$_POST['endDate']:"";
$numRow = 0;
$con = "";

if($startDate != "" and  $endDate != "")
{
  $con .= " and i.invoice_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
}
if($contract != ""){

  $con .= " and i.contract  ='". $contract ."' ";
}

?>
<input type="hidden" id="typeCompany" value="<?= $_SESSION['typeCompany'] ?>">

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px;">ลำดับ</th>
      <th >เลขที่ใบสัญญาจ้าง</th>
      <th style="width:100px;">Invoice No</th>
      <th class="text-center" >วันออก Invoice</th>
      <th class="text-center" >ชื่อลูกค้า</th>
      <th class="text-center" >วันครบกำหนด</td>
      <th class="text-center;" style="width:70px;">สำรองจ่าย</th>
      <th class="text-center" >ค่าขนส่ง</th>
      <th class="text-center" >ส่วนลด</th>
      <th class="text-center;" style="width:70px;">ค่าบริการเพิ่ม(OT)</th>
      <th class="text-center" >Total</th>
      <th style="width:210px;"></th>
      <th style="width:50px;"></th>
    </tr>
  </thead>
  <tbody>
<?php

  $sql = "SELECT i.*,c.cust_name FROM  tb_invoice i,  tb_customer_master c
  where i.cust_id is not null and i.cust_id = c.cust_id and i.contract <> '' and status_del <> 'Y' $con order by invoice_code DESC";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num    = mysqli_num_rows($query);

  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $invoice_id         = $row['invoice_id'];
    $invoice_code       = $row['invoice_code'];
    $invoice_date       = formatDate($row['invoice_date'],'d/m/Y');
    $maturity_date      = formatDate($row['maturity_date'],'d/m/Y');
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $credit             = $row['credit'];
    $provision          = $row['provision'];
    $delivery_cost      = $row['delivery_cost'];
    $discount           = $row['discount'];
    $ot                 = $row['ot'];
    $total_cost         = $row['total_cost'];
    $cust_name          = $row['cust_name'];
    $contract           = $row['contract'];
    $receipt_code       = $row['receipt_code'];

    $disable = "";
    if($receipt_code != null && !$receipt_code == ""){
      $disable = "disabled";
    }
?>
  <tr class="text-center">
    <td class="text-center"><?= $i ?></td>
    <td class="text-center"><?= $contract ?></td>
    <td class="text-center"><?= $invoice_code ?></td>
    <td class="text-center"><?= $invoice_date ?></td>
    <td class="text-left" ><?= $cust_name ?></td>
    <td class="text-center" ><?= $maturity_date ?></td>
    <td class="text-right" ><?= number_format($provision,2); ?></td>
    <td class="text-right" ><?= number_format($delivery_cost,2); ?></td>
    <td class="text-right" ><?= number_format($discount,2); ?></td>
    <td class="text-right" ><?= number_format($ot,2); ?></td>
    <td class="text-right" ><?= number_format($total_cost,2);?></td>
    <td>
      <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="editInvoice2('<?= $invoice_code; ?>','<?= $contract; ?>')" ><i class="fa fa-edit"></i> แก้ไข</button>
      <button type="button" class="btn btn-primary btn-sm btn-flat" onclick="exportExcel2('<?= $invoice_code; ?>','<?= $invoice_date; ?>')"><i class="fa fa-file-excel-o"></i> Excel</button>
      <button type="button" class="btn btn-info btn-sm btn-flat" onclick="expInvoice2('<?= $invoice_code; ?>')"><i class="fa fa-print"></i> พิมพ์</button>
    </td>
    <td>
      <button type="button" class="btn btn-danger btn-sm btn-flat" <?= $disable ?> onclick="delInvoice2('<?= $invoice_code; ?>')" ><i class="fa fa-ban"></i> ยกเลิก</button>
    </td>
  </tr>

<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [50, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
