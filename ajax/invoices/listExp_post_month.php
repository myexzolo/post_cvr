<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$invoiceCode    = isset($_POST['invoiceCode'])?$_POST['invoiceCode']:"";

$con = "";

// if($startDate != "" and  $endDate != "")
// {
//   $con .= " and jo.job_order_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
// }

if($invoiceCode != "")
{
  $con .= " and jo.invoice_code ='". $invoiceCode ."'";
}

$sql = "SELECT * FROM  tb_invoice i,  tb_customer_master c, tb_route_price r
where i.invoice_code ='$invoiceCode' and i.cust_id = c.cust_id and r.contract_no = i.contract and i.status_del not in ('Y')";
//echo $sql;
$query  = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);

$row = mysqli_fetch_assoc($query);
$invoice_code       = $row['invoice_code'];
$invoice_date       = formatDate($row['invoice_date'],'d/m/Y');
$maturity_date      = formatDate($row['maturity_date'],'d/m/Y');
$cust_id            = $row['cust_id'];//รหัสลูกค้า
$credit             = $row['credit'];
$provision          = $row['provision'];
$delivery_cost      = $row['delivery_cost'];
$discount           = $row['discount'];
$ot                 = $row['ot'];
$total_cost         = $row['total_cost'];
$contract           = $row['contract'];
$installment        = $row['installment'];

$nameCus            = $row['prefix']." ".$row['cust_name'];
$address            = $row['address'];
$tax                = $row['tax'];
$tel                = $row['tel'];

$source             = $row['source'];
$destination        = $row['destination'];
$contract_detail    = $row['contract_detail'];

?>
<table style="width:92%;margin-bottom:10px;margin-left:4%;">
  <tr>
    <td style="width:150px"></td>
    <td style="font-size:18px;padding:15px;" align="center">ห้างหุ้นส่วนจำกัด โชควิรัตน์</td>
    <td style="width:150px;font-size:12px;" >
      <div align="right">Tel/Fax : 035-950805</div>
      <div align="right"></div>
    </td>
  </tr>
  <tr>
    <td style="padding-bottom:5px;font-size:14px;" colspan="3" align="center">24 หมู่ 1 ต.พระขาว อ.บางบาล จ.พระนครศรีอยุธยา เลขประจำตัวผู้เสียภาษี 0143539000495</td>
  </tr>
  <tr>
    <td></td>
    <td style="padding:5px;border: 1px solid #333;font-size:16px;" align="center">ใบวางบิล/ใบแจ้งหนี้ - Customer Statement</td>
    <td></td>
  </tr>
</table>
<table style="width:100%;font-size:16px;" id="tableDisplayExp">
    <thead>
      <tr>
  			<td style="height:10px;width:70px;">&nbsp;</td>
        <td style="height:10px;width:150px;">&nbsp;</td>
        <td style="height:10px;width:250px;">&nbsp;</td>
        <td style="height:10px;">&nbsp;</td>
        <td style="height:10px;width:100px;">&nbsp;</td>
  		</tr>
    </thead>
    <tr>
			<td colspan="5">
      <table style="width:100%;">
        <tr>
          <td style="width:90px;padding-top:5px;font-size:14px;" align="left">เรียน</td>
          <td style="padding:5px;width:2px;font-size:14px;" align="left">:</td>
          <td style="padding-top:5px;font-size:14px;" align="left"><?= $nameCus ?></td>
          <td style="width:60px;padding-top:5px;font-size:14px;" align="left">เลขที่-No</td>
          <td style="padding:5px;width:2px;font-size:14px;" align="left">:</td>
          <td style="padding-top:5px;font-size:14px;" align="left"><?= $invoice_code ?></td>
        </tr>
        <tr>
          <td style="padding-top:5px;font-size:14px;" align="left">ที่อยู่-Address</td>
          <td style="padding:5px;font-size:14px;" align="left">:</td>
          <td style="padding-top:5px;font-size:14px;" align="left"><div style="width: 300px;word-wrap: break-word;"><?= $address ?></div></td>
          <td style="padding-top:5px;font-size:14px;" align="left">วันที่</td>
          <td style="padding:5px;font-size:14px;" align="left">:</td>
          <td style="padding-top:5px;font-size:14px;" align="left"><?= $invoice_date ?></td>
        </tr>
        <tr>
          <td style="width:90px;padding-top:5px;font-size:14px;" align="left"></td>
          <td style="padding:5px;width:2px;font-size:14px;" align="left"></td>
          <td colspan="3" style="padding-top:5px;font-size:14px;" align="left">เลขประจำตัวผู้เสียภาษี <?= $tax ?></td>
        </tr>
      </table>
     </td>
		</tr>
    <tr>
			<th style="border: 1px solid;padding:5px;width:30px;vertical-align: middle;font-size:14px;">ลำดับ</th>
			<th colspan="3" style="border: 1px solid;padding:5px;vertical-align:middle;font-size:14px;">รายการ</th>
			<th style="border: 1px solid;padding:5px;width:40px;vertical-align:middle;font-size:14px;">จำนวนเงิน</th>
		</tr>
  <tbody>
  <tr>
    <td style="border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;font-size:14px;" class="text-center">1</td>
    <td colspan="3" style="border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;font-size:14px;" class="text-left" >
      ค่าขนส่งไปรษณีย์เส้นทาง <?= $source." - ".$destination ?><br>
      ประจำเดือน <?= $installment ?><br>
      &nbsp;&nbsp;&nbsp;เลขที่สัญญา <?= $contract ?><br>
      &nbsp;&nbsp;&nbsp;<?= $contract_detail ?>
    </td>
    <td style="border-left: 1px solid;border-right: 1px solid;padding:5px;vertical-align:top;line-height:20px;font-size:14px;" class="text-right" >
      <?= number_format($delivery_cost,2); ?>
    </td>
  </tr>
<?php
    for($i = 0; $i< 15; $i++){
      echo "<tr>";
        echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;font-size:14px;'>&nbsp;</td>";
        echo "<td colspan='3' style='border-left: 1px solid;padding:5px;vertical-align:top;line-height:20px;'>&nbsp;</td>";
        echo "<td style='border-left: 1px solid;border-right: 1px solid;padding:5px;vertical-align:top;line-height:20px;'>&nbsp;</td>";
      echo "</tr>";
    }
?>
<tr>
  <td colspan="4" style="border: 1px solid;padding:5px;vertical-align:top;font-size:14px;"  align="center" ><b>รวม</b></td>
  <td style="border-right: 1px solid;padding:5px;border-top: 1px solid;font-size:14px;" align="right"><b><?= number_format($delivery_cost,2); ?></b></td>
</tr>
<tr>
  <td colspan="3" rowspan="2" style="border: 1px solid;padding:5px;vertical-align:top;font-size:12px;"  align="left" >
    หมายเหตุ : กรุณาโอนเข้า หรือ ออกเช็คในนาม บริษัท โชควิรัตน์ จำกัด <br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    เลขที่บัญชี 452-1-01082-2 กระแสรายวัน<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    ธนาคารกสิกรไทย สาขาถนนโรจนะ
  </td>
  <td style="padding:5px;border-top: 1px solid;" align="left">&nbsp;</td>
  <td style="border-right: 1px solid;padding:5px;border-top: 1px solid;" align="right"></td>
</tr>
<tr>
  <td style="padding:5px;font-size:14px;"class="text-left">หัก ณ.ที่จ่าย</td>
  <td style="border-right: 1px solid;padding:5px;font-size:14px;" class="text-right"><b><?= number_format($discount,2); ?></b></td>
</tr>
<tr>
  <td colspan="3" style="border: 1px solid;padding:5px;font-size:14px;" class="text-center" ><b><?= convert(number_format($total_cost,2));?></b></td>
  <td style="border-bottom: 1px solid;padding:5px;width:80px;font-size:14px;" class="text-left">รวมเงินสุทธิ</td>
  <td style="border-right: 1px solid;border-bottom: 1px solid;padding:5px;font-size:14px;" class="text-right"><b><?= number_format($total_cost,2); ?></b></td>
</tr>
<tr>
  <td colspan="2" style="border: 1px solid;padding:5px;" class="text-center" >
    <p>ผู้จัดทำเอกสาร-prepared by</p><br>
    <p>...................................................</p>
    <p>(.........../.........../...........)</p>
  </td>
  <td style="border: 1px solid;padding:5px;" class="text-center">
    <p>ผู้อนุมัติ-approved by</p><br>
    <p>...................................................</p>
    <p>(.........../.........../...........)</p></td>
  <td colspan="2"  style="border: 1px solid;padding:5px;" class="text-center">
    <p>ผู้รับเอกสาร-received by</p><br>
    <p>...................................................</p>
    <p>(.........../.........../...........)</p></td>
  </td>
</tr>
</tbody>
</table>
