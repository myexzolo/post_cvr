<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$typeCompany = $_SESSION['typeCompany'];

$cust_id      = isset($_POST['cust_id'])?$_POST['cust_id']:"0";
$startDate    = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate      = isset($_POST['endDate'])?$_POST['endDate']:"";
$trailer_id   = isset($_POST['trailer_id'])?$_POST['trailer_id']:"";
$numRow = 0;
$con = "";

$linkPrint  = "expInvoice";

if($startDate != "" and  $endDate != "")
{
  $con .= " and i.invoice_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
}
if($cust_id != ""){
  $con .= " and i.cust_id  ='". $cust_id ."' ";
}

if($typeCompany == 2){
    $linkPrint = "expInvoice";
    $con .= " and i.contract  = '' ";
}

if($trailer_id != ""){
  $sql = "SELECT CONCAT(\"'\", GROUP_CONCAT(DISTINCT invoice_code SEPARATOR \"','\") , \"'\") as list
          FROM  tb_job_order where trailer_id = ".$trailer_id." and invoice_code is not null";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);

  $row      = mysqli_fetch_assoc($query);
  $list     = $row['list'];

  $con .= " and i.invoice_code in ($list) ";
}

?>
<input type="hidden" id="typeCompany" value="<?= $typeCompany ?>">

<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px;">ลำดับ</th>
      <th style="width:100px;">Invoice No</th>
      <th class="text-center" style="width:70px;">วันออก Invoice</th>
      <th class="text-center" >ชื่อลูกค้า</th>
      <th class="text-center" >วันครบกำหนด</td>
      <th class="text-center;" style="width:70px;">สำรองจ่าย</th>
      <th class="text-center" >ค่าขนส่ง</th>
      <th class="text-center" >ส่วนลด</th>
      <th class="text-center;" style="width:70px;">ค่าบริการเพิ่ม(OT)</th>
      <th class="text-center" >Total</th>
      <th style="width:210px;"></th>
      <th style="width:50px;"></th>
    </tr>
  </thead>
  <tbody>
<?php

  $sql = "SELECT i.*,c.cust_name FROM  tb_invoice i,  tb_customer_master c
  where i.cust_id is not null and i.cust_id = c.cust_id and i.status_del <> 'Y' $con order by i.invoice_code DESC";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num    = mysqli_num_rows($query);

  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $invoice_id         = $row['invoice_id'];
    $invoice_code       = $row['invoice_code'];
    $invoice_date       = formatDate($row['invoice_date'],'d/m/Y');
    $maturity_date      = formatDate($row['maturity_date'],'d/m/Y');
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $credit             = $row['credit'];
    $provision          = $row['provision'];
    $delivery_cost      = $row['delivery_cost'];
    $discount           = $row['discount'];
    $ot                 = $row['ot'];
    $total_cost         = $row['total_cost'];
    $cust_name          = $row['cust_name'];
    $receipt_code       = $row['receipt_code'];
    $disable = "";
    if($receipt_code != null && !$receipt_code == ""){
      $disable = "disabled";
    }
?>
  <tr class="text-center">
    <td class="text-center"><?= $i ?></td>
    <td class="text-center"><?= $invoice_code ?></td>
    <td class="text-center"><?= $invoice_date ?></td>
    <td class="text-left" ><?= $cust_name ?></td>
    <td class="text-center" ><?= $maturity_date ?></td>
    <td class="text-right" ><?= number_format($provision,2); ?></td>
    <td class="text-right" ><?= number_format($delivery_cost,2); ?></td>
    <td class="text-right" ><?= number_format($discount,2); ?></td>
    <td class="text-right" ><?= number_format($ot,2); ?></td>
    <td class="text-right" ><?= number_format($total_cost,2);?></td>
    <td>
      <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="editInvoice('<?= $invoice_code; ?>')" ><i class="fa fa-edit"></i> แก้ไข</button>
      <button type="button" class="btn btn-primary btn-sm btn-flat" onclick="exportExcel('<?= $invoice_code; ?>','<?= $invoice_date; ?>')"><i class="fa fa-file-excel-o"></i> Excel</button>
      <button type="button" class="btn btn-info btn-sm btn-flat" onclick="expInvoice('<?= $invoice_code; ?>')"><i class="fa fa-print"></i> พิมพ์</button>
    </td>
    <td>
      <button type="button" class="btn btn-danger btn-sm btn-flat" <?= $disable ?> onclick="delInvoice('<?= $invoice_code; ?>')" ><i class="fa fa-ban"></i> ยกเลิก</button>
    </td>
  </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [50, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
