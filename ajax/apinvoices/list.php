<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$department_id  = isset($_POST['department_id'])?$_POST['department_id']:"0";
$startDate      = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate        = isset($_POST['endDate'])?$_POST['endDate']:"";
$numRow = 0;
$con = "";

$typeCompany = $_SESSION['typeCompany'];

if($startDate != "" and  $endDate != "")
{
  $con .= " and a.apinvoice_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
}
if($department_id != ""){
  $con .= " and a.department_id  ='". $department_id ."' ";
}

?>
<input type="hidden" id="typeCompany" value="<?= $typeCompany ?>">
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px;">ลำดับ</th>
      <th style="width:150px;">เลขที่ใบจ่าย</th>
      <th class="text-center" >วันออกใบจ่าย</th>
      <th class="text-center" >ชื่อรถร่วม</th>
      <th class="text-center" >วันครบกำหนด</td>
      <th class="text-center" style="width:70px;">สำรองจ่าย</th>
      <th class="text-center" >ค่าขนส่ง</th>
      <th class="text-center" >หัก ณ ที่จ่าย</th>
      <th class="text-center" style="width:70px;">ค่าบริการเพิ่ม(OT)</th>
      <th class="text-center" >Total</th>
      <th style="width:220px;"></th>
    </tr>
  </thead>
  <tbody>
<?php

  $sql = "SELECT a.*, d.department_name FROM tb_apinvoice a, tb_department_master d
  where a.department_id is not null and a.department_id  = d.department_id  $con
  order by apinvoice_no DESC";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num    = mysqli_num_rows($query);

  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $apinvoice_id         = $row['apinvoice_id'];
    $apinvoice_no       = $row['apinvoice_no'];
    $apinvoice_date     = formatDate($row['apinvoice_date'],'d/m/Y');
    $maturity_date      = formatDate($row['maturity_date'],'d/m/Y');
    $department_id      = $row['department_id'];//รหัสลูกค้า
    $credit             = $row['credit'];
    $provision          = $row['provision'];
    $delivery_cost      = $row['delivery_cost'];
    $discount           = $row['discount'];
    $ot                 = $row['ot'];
    $total_cost         = $row['total_cost'];

    $department_name    = $row['department_name'];
?>
  <tr class="text-center">
    <td class="text-center"><?= $i ?></td>
    <td class="text-center"><?= $apinvoice_no ?></td>
    <td class="text-center"><?= $apinvoice_date ?></td>
    <td class="text-left" ><?= $department_name ?></td>
    <td class="text-center" ><?= $maturity_date ?></td>
    <td class="text-right" ><?= number_format($provision,2); ?></td>
    <td class="text-right" ><?= number_format($delivery_cost,2); ?></td>
    <td class="text-right" ><?= number_format($discount,2); ?></td>
    <td class="text-right" ><?= number_format($ot,2); ?></td>
    <td class="text-right" ><?= number_format($total_cost,2);?></td>
    <td>
      <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="editApInvoice('<?= $apinvoice_no; ?>','<?=$department_id?>')" ><i class="fa fa-edit"></i> แก้ไข</button>
      <button type="button" class="btn btn-primary btn-sm btn-flat" onclick="exportExcel('<?= $apinvoice_no; ?>','<?= $apinvoice_date; ?>')"><i class="fa fa-file-excel-o"></i> Excel</button>
      <button type="button" class="btn btn-info btn-sm btn-flat" onclick="expApInvoice('<?= $apinvoice_no; ?>')"><i class="fa fa-print"></i> พิมพ์</button>
    </td>
  </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [50, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
