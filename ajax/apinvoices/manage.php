<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');

// print_r($_POST);
$idJob          = isset($_POST['idJob'])?$_POST['idJob']:"";
$apinvoice_date = isset($_POST['apinvoice_date'])?$_POST['apinvoice_date']:"";
$credit         = isset($_POST['credit'])?$_POST['credit']:"0";
$maturity_date  = isset($_POST['maturity_date'])?$_POST['maturity_date']:"";
$provision      = isset($_POST['provision'])?$_POST['provision']:"";
$delivery_cost  = isset($_POST['delivery_cost'])?$_POST['delivery_cost']:"";
$discount       = isset($_POST['discount'])?$_POST['discount']:"";
$ot             = isset($_POST['ot'])?$_POST['ot']:"";
$total_cost     = isset($_POST['total_cost'])?$_POST['total_cost']:"";
$department_id  = isset($_POST['department_id'])?$_POST['department_id']:"";
$remark         = isset($_POST['remark'])?$_POST['remark']:"";

$apinvoice_no   = isset($_POST['apinvoice_no'])?$_POST['apinvoice_no']:"";

$installment   = isset($_POST['installment'])?$_POST['installment']:"";//ค่างวดรถ
$oil_price     = isset($_POST['oil_price'])?$_POST['oil_price']:"";//ค่าน้ำมัน
$gps           = isset($_POST['gps'])?$_POST['gps']:"";
$sso           = isset($_POST['sso'])?$_POST['sso']:"";//ประกันสังคม
$warranty      = isset($_POST['warranty'])?$_POST['warranty']:"";//ประกันสินค้า
$insurance     = isset($_POST['insurance'])?$_POST['insurance']:"";//ประกันภัยรถ
$register_head = isset($_POST['register_head'])?$_POST['register_head']:"";//ต่อทะเบียนหัว
$register_footter = isset($_POST['register_footter'])?$_POST['register_footter']:"";//ต่อทะเบียนหาง
$act           = isset($_POST['act'])?$_POST['act']:"";//ต่อพรบ.
$tire          = isset($_POST['tire'])?$_POST['tire']:"";//ค่ายาง
$other         = isset($_POST['other'])?$_POST['other']:"";//อื่นๆ

$typeCompany = $_SESSION['typeCompany'];

$_POST['credit']      = chkNum2($credit);
$_POST['provision']   = chkNum2($provision);
$_POST['ot']          = chkNum2($ot);
$_POST['installment'] = chkNum2($installment);
$_POST['oil_price']   = chkNum2($oil_price);
$_POST['gps']         = chkNum2($gps);
$_POST['sso']         = chkNum2($sso);
$_POST['warranty']    = chkNum2($warranty);
$_POST['insurance']     = chkNum2($insurance);
$_POST['register_head'] = chkNum2($register_head);
$_POST['register_footter'] = chkNum2($register_footter);
$_POST['act']         = chkNum2($act);
$_POST['tire']        = chkNum2($tire);
$_POST['other']       = chkNum2($other);


// print_r($_POST);

unset($_POST['idJob']);

if(!empty($idJob)){
  if($apinvoice_no != ""){
      $sql = "UPDATE tb_job_order SET apinvoice_no = null WHERE apinvoice_no  = '$apinvoice_no'";
      //echo $sql;
      mysqli_query($conn,$sql);

      // $sql = "UPDATE tb_apinvoice SET
      //        credit = '$credit',
      //        maturity_date ='$maturity_date',
      //        provision = '$provision',
      //        delivery_cost ='$delivery_cost',
      //        discount = '$discount',
      //        ot = '$ot',
      //        total_cost = '$total_cost',
      //        remark = '$remark',
      //        installment   = '$installment',
      //        oil_price     = '$oil_price',
      //        gps           = '$gps',
      //        sso           = '$sso',
      //        warranty      = '$warranty',
      //        insurance     = '$insurance',
      //        register_head = '$register_head',
      //        register_footter = '$register_footter',
      //        act           = '$act',
      //        tire          = '$tire',
      //        other         = '$other'
      //        where apinvoice_no = '$apinvoice_no'";

       $_POST['apinvoice_no'] = $apinvoice_no;
       $sql = DBUpdatePOST($_POST,'tb_apinvoice','apinvoice_no');
      //mysqli_query($conn,$sql);

      // echo $sql;
  }else{
      $dateYM       = date('y-m-d');
      if($typeCompany == '1'){
        $ct           = "CT".$dateYM."-";
      }else{
        $ct           = "CV".$dateYM."-";
      }


      $sql = "SELECT max(apinvoice_no) as apinvoice_no FROM  tb_apinvoice where apinvoice_no LIKE '$ct%'";
      $query  = mysqli_query($conn,$sql);
      $row    = mysqli_fetch_assoc($query);
      $apinvoiceNo  = $row['apinvoice_no'];

      if(!empty($apinvoiceNo)){
        $lastNum = substr($apinvoiceNo,strlen($ct));
        $lastNum = $lastNum + 1;
        $apinvoice_no = $ct.sprintf("%03d", $lastNum);
      }else{
        $apinvoice_no = $ct.sprintf("%03d", 1);
      }

      $_POST['apinvoice_no'] = $apinvoice_no;

      $sql = DBInsertPOST($_POST,'tb_apinvoice');

      // $sql = "INSERT INTO tb_apinvoice
      //        (apinvoice_no,
      //         apinvoice_date,
      //         credit,
      //         maturity_date,
      //         provision,
      //         delivery_cost,
      //         discount,
      //         ot,
      //         total_cost,
      //         department_id,
      //         remark,
      //         installment,
      //         oil_price,
      //         gps,
      //         sso,
      //         warranty,
      //         insurance,
      //         register_head,
      //         register_footter,
      //         act,
      //         tire,
      //         other)
      //        VALUES(
      //          '$apinvoice_no',
      //          '$apinvoice_date',
      //          '$credit',
      //          '$maturity_date',
      //          '$provision',
      //          '$delivery_cost',
      //          '$discount',
      //          '$ot',
      //          '$total_cost',
      //          '$department_id',
      //          '$remark',
      //          '$installment',
      //          '$oil_price',
      //          '$gps',
      //          '$sso',
      //          '$warranty',
      //          '$insurance',
      //          '$register_head',
      //          '$register_footter',
      //          '$act',
      //          '$tire',
      //          '$other'
      //        )";


      mysqli_query($conn,$sql);
      // echo $sql;
  }


  foreach( $idJob as $key => $id ) {
    $sql = "UPDATE tb_job_order SET apinvoice_no = '$apinvoice_no' WHERE id  = '$id'";
    mysqli_query($conn,$sql);
    //echo $sql;
  }
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
