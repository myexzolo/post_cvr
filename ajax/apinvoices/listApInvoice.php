<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$department_id    = isset($_POST['department_id'])?$_POST['department_id']:"0";
$apInvoiceNo      = isset($_POST['apInvoiceNo'])?$_POST['apInvoiceNo']:"";
$apinvoiceDate    = isset($_POST['apinvoiceDate'])?$_POST['apinvoiceDate']:"";
$remark           = isset($_POST['remark'])?$_POST['remark']:"";
$startDate        = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate          = isset($_POST['endDate'])?$_POST['endDate']:"";
$contract         = isset($_POST['contr'])?$_POST['contr']:"";

$trailer_id       = isset($_POST['trailer_id'])?$_POST['trailer_id']:"";

// print_r($_POST);

$credit = "";
$maturity_date = "";
$provision = "";
$delivery_cost = "";
$discount = "";
$ot = "";

$total_cost = "";
$installment = "";
$oil_price = "";
$gps = "";
$sso = "";
$warranty = "";
$insurance = "";
$register_head = "";
$register_footter = "";
$act = "";
$tire = "";
$other = "";

$numRow = 0;
$con = "";

if(empty($department_id)){
  $department_id = 0;
}



if($trailer_id != ""){
  $con .= " and jo.trailer_id  in ($trailer_id) " ;
}

$dateNow = date('Y-m-d');
if($apinvoiceDate != ""){
  $dateNow = $apinvoiceDate;
}


if($contract != ""){
  $con .= " and jo.route_id  in (SELECT route_id FROM tb_route_price where contract_no = '". $contract ."') ";
}

if($department_id != "")
{
  $con .= " and jo.employee_id in (SELECT employee_id FROM tb_employee_master where  department_id ='". $department_id ."') ";
}

if($con == ""){
    $con .= " and jo.employee_id in (0) ";
}

if($apInvoiceNo == ""){
  $con .= " and jo.apinvoice_no is null ";

  if($startDate != "" and  $endDate != "")
  {
    $con .= " and jo.job_order_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
  }
}else{
  $conDate = "";
  if($startDate != "" and  $endDate != "")
  {
    $conDate .= " and jo.job_order_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
  }
  $con .= " and (jo.apinvoice_no = '$apInvoiceNo')";
}

// if($con == ""){
//     $con .= " and jo.employee_id in (0) ";
// }
?>
<table style="width:100%">
  <td style="width:100px;padding-bottom:5px" align="left">
    <input type="checkbox" id="checkApp" onclick="checkAll(this)"><label for="checkApp">&nbsp;เลือกทั้งหมด</label>
  <td>
</table>
<table class="table table-bordered table-striped " id="tableDisplay">
  <thead>
    <tr>
			<th style="width:50px;">ลำดับ</th>
      <th style="width:30px;"></th>
			<th>เลขที่ใบสั่ง</th>
			<th>วันที่</th>
			<th>ShipMent</th>
			<th style="width:100px;">เลขที่ DP</th>
			<th>กำหนดส่ง</th>
      <th>ทะเบียนรถ</th>
      <th>ชื่อเจ้ารถร่วม</th>
			<th>ต้นทาง</th>
			<th>ปลายทาง</th>
      <th>Spec</th>
      <th>จำนวน(ตัน/เที่ยว)</th>
      <th>ค่าขนส่ง</th>
      <th>จำนวนเงิน</th>
		</tr>
  </thead>
  <tbody>
<?php
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate,d.department_name,d.department_id
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t, tb_department_master d
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id
  and jo.job_status_id <> 3 and jo.cust_dp is not null and jo.cust_dp <> '' and  em.department_id = d.department_id
  order by jo.shipment + 0,jo.job_order_date";
  // echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalTransfer = 0;

  for ($i=1; $i <= $num ; $i++) {
    $checked = "";

    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $department_name    = $row['department_name'];
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    //$remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];

    $period_code              = $row['period_code'];
    $apinvoice_no             = $row['apinvoice_no'];


    if($department_id == "")
    {
      $department_id = $row['department_id'];
    }else if ($department_id != $row['department_id'])
    {
      $department_id = "0";
    }





    $transfer = ($fuel_cost + $allowance + $kog_expense + $blank_charge + $allowance_oth);
    $total =  ($weights * $one_trip_ton);

    if($price_type_id == 1){

    }else{
      $total  = $ext_price_per_trip;
    }

    if($price_type_id == 1){
      $amount =  $weights;
      $price  =  $ext_one_trip_ton;
      $total  =  ($weights * $ext_one_trip_ton);
    }else{
      if($price_type_id == 0){
        $amount = 0;
      }else{
        $amount = 1;
      }

      $price  = $ext_price_per_trip;
      $total  = $ext_price_per_trip;
    }

    if($apInvoiceNo == $apinvoice_no && $apinvoice_no!=""){
      $checked = "checked";
    }


?>
  <tr>
    <td class="text-center"><?= $i ?></td>
    <td class="text-center" style="width:50px;"><input type="checkbox" <?= $checked ?> onchange="chkList(this);cal();" class="checkJobId" name="idJob[]" value="<?= $id ?>"></td>
    <td class="text-center" ><?= $job_order_no ?></td>
    <td class="text-center" ><?= $job_order_date ?></td>
    <td class="text-left" ><?= $shipment ?></td>
    <td class="text-left" ><?= $cust_dp ?></td>
    <td class="text-center" ><?= $job_delivery_date ?></td>
    <td class="text-left" ><?= $license_plate ?></td>
    <td class="text-left" ><?= $department_name ?></td>
    <td class="text-left" ><?= $source ?></td>
    <td class="text-left" ><?= $destination ?></td>
    <td class="text-left" ><?= $away_spec_no ?></td>
    <td class="text-right" ><?= number_format($amount,3); ?></td>
    <td class="text-right" ><?= number_format($price,2); ?></td>
    <td class="text-right" ><div id="net<?=$id?>"><?= number_format($total,2); ?></div></td>
  </tr>
<?php
 }

 if($num > 0){
   if($apInvoiceNo != ""){
     $sql2 = "SELECT * FROM  tb_apinvoice where apinvoice_no = '$apInvoiceNo'";
     //echo $sql;
     $query2  = mysqli_query($conn,$sql2);
     $row2 = mysqli_fetch_assoc($query2);

     $total_cost    = $row2['total_cost'];//
     $installment   = $row2['installment'];//ค่างวดรถ
     $oil_price     = $row2['oil_price'];//ค่าน้ำมัน
     $gps           = $row2['gps'];
     $sso           = $row2['sso'];//ประกันสังคม
     $warranty      = $row2['warranty'];//ประกันสินค้า
     $insurance     = $row2['insurance'];//ประกันภัยรถ
     $register_head = $row2['register_head'];//ต่อทะเบียนหัว
     $register_footter = $row2['register_footter'];//ต่อทะเบียนหาง
     $act           = $row2['act'];//ต่อพรบ.
     $tire          = $row2['tire'];//ค่ายาง
     $other         = $row2['other'];//อื่นๆ


     $credit        = $row2['credit'];
     $maturity_date = $row2['maturity_date'];
     $provision     = $row2['provision'];//สำรองจ่าย
     $delivery_cost = $row2['delivery_cost'];//ค่าขนส่ง
     $discount      = $row2['discount'];//หัก ณ ที่จ่าย 1%
     $ot            = $row2['ot'];//ค่าบริการเพิ่ม(OT)
   }

  ?>
  <tr>
    <td colspan="5" class="text-right" style="line-height:30px"><label>ค่างวดรถ</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $installment ?>" name="installment" id="installment" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
    <td colspan="3" class="text-right" style="line-height:30px"><label>วันที่ออกใบจ่าย</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $dateNow ?>" name="apinvoice_date" type="date" class="form-control" placeholder="" required readonly>
    </td>
  </tr>
  <tr>
    <td colspan="5" class="text-right" style="line-height:30px"><label>ค่าน้ำมัน</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $oil_price ?>" name="oil_price" id="oil_price" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
    <td colspan="3" class="text-right" style="line-height:30px"><label>ค่าบริการเพิ่ม(OT)</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $ot ?>" name="ot" id="ot" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
  </tr>
  <tr>
    <td colspan="5" class="text-right" style="line-height:30px"><label>GPS</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $gps ?>" name="gps" id="gps" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
    <td colspan="3" class="text-right" style="line-height:30px"><label>ค่าขนส่ง</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $delivery_cost ?>" name="delivery_cost" id="delivery_cost" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
  </tr>
  <tr>
    <td colspan="5" class="text-right" style="line-height:30px"><label>ประกันสังคม</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $sso ?>" name="sso" id="sso" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
    <td colspan="3" class="text-right" style="line-height:30px"><label>หัก ณ ที่จ่าย 1%</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $discount ?>" name="discount" id="discount" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
  </tr>
  <tr>
    <td colspan="5" class="text-right" style="line-height:30px"><label>ประกันสินค้า</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $warranty ?>" name="warranty" id="warranty" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
    <td colspan="3" class="text-right" style="line-height:30px"><label>สำรองจ่าย</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $provision ?>" name="provision" id="provision" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
  </tr>
  <tr>
    <td colspan="5" class="text-right" style="line-height:30px"><label>ประกันภัยรถ</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $insurance ?>" name="insurance" id="insurance" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
    <td colspan="3" class="text-right" style="line-height:30px"><label>ต่อพรบ.</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $act ?>" name="act" id="act" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
  </tr>
  <tr>
    <td colspan="5" class="text-right" style="line-height:30px"><label>ต่อทะเบียนหัว</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $register_head ?>" name="register_head" id="register_head" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
    <td colspan="3" class="text-right" style="line-height:30px"><label>ต่อทะเบียนหาง</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $register_footter ?>" name="register_footter" id="register_footter" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
  </tr>
  <tr>
    <td colspan="5" class="text-right" style="line-height:30px"><label>ค่ายาง</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $tire ?>" name="tire" id="tire" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
    <td colspan="3" class="text-right" style="line-height:30px"><label>อื่นๆ</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $other ?>" name="other" id="other" type="text" data-smk-type="decimal" class="form-control" placeholder="">
    </td>
  </tr>
  <tr>
    <td colspan="11" class="text-right" style="line-height:30px"><label>หมายเหตุ</label></td>
    <td colspan="3" class="text-left">
      <textarea class="form-control" name="remark" id="remark" rows="2" placeholder="Remark ..." ><?= $remark ?></textarea>
    </td>
  </tr>
  <tr>
    <td colspan="11" class="text-right" style="line-height:30px"><label>Total</label></td>
    <td colspan="3" class="text-left">
      <input value="" name="total_cost" id="total_cost" type="text" data-smk-type="decimal" class="form-control" placeholder="">
      <input name="apinvoice_no" type="hidden" value="<?= $apInvoiceNo ?>">
    </td>
  </tr>
  </tbody>
  </table>
  <?php
}else{
  ?>
  <tr>
    <td colspan="14" class="text-center" >ไม่พบข้อมูล</td>
  </tr>
  </tbody>
  </table>
  <?php
}

 ?>
<input type="hidden" id="department_id" name="department_id" value="<?= $department_id?>">
