<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$startDate    = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate      = isset($_POST['endDate'])?$_POST['endDate']:"";
$numRow = 0;
$con = "";

if($startDate != "" and  $endDate != "")
{
  $con .= " and shipment_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
}

?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;">ลำดับ</th>
      <th style="width:150px;">เลขที่ใบส่งของ</th>
      <th class="text-center" >ชื่อลูกค้า</td>
      <th style="width:150px;" class="text-center" >วันออกใบส่งของ</th>
      <th style="width:100px;" class="text-center" >จำนวนรายการ</th>
      <th style="width:220px;"></th>
    </tr>
  </thead>
  <tbody>
<?php

  $sql = "SELECT * FROM tb_shipment where shipment_id is not null $con order by shipment_code DESC";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num    = mysqli_num_rows($query);

  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $shipment_id        = $row['shipment_id'];
    $shipment_code      = $row['shipment_code'];
    $shipment_date      = formatDate($row['shipment_date'],'d/m/Y');
    $company_name       = $row['company_name'];
    $num_list           = $row['num_list'];

?>
  <tr class="text-center">
    <td class="text-center"><?= $i ?></td>
    <td class="text-center"><?= $shipment_code ?></td>
    <td class="text-center"><?= $company_name ?></td>
    <td class="text-right" ><?= $shipment_date ?></td>
    <td class="text-right" ><?= number_format($num_list,2); ?></td>
    <td>
      <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="edit('<?= $shipment_code; ?>','<?= $company_name; ?>','<?= $row['shipment_date'] ?>')" ><i class="fa fa-edit"></i> แก้ไข</button>
      <button type="button" class="btn btn-info btn-sm btn-flat" onclick="printShipMent('<?= $shipment_code; ?>')"><i class="fa fa-print"></i> พิมพ์</button>
    </td>
  </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [50, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
