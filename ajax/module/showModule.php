<?php

include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td>Num</td>
      <td>Code</td>
      <td>Name</td>
      <td>ประเภทการแสดง</td>
      <td>ลำดับการแสดง</td>
      <td>สถานะ</td>
      <td>Edit/Del</td>
    </tr>
  </thead>
  <tbody>
<?php
  $sql = "SELECT * FROM t_module ORDER BY module_id DESC";
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td><?= $i ?></td>
      <td><?= $row['module_code']; ?></td>
      <td><?= $row['module_name']; ?></td>
      <td><?= ($row['module_type'] == '1' ? 'Back Office' : 'Front Office') ; ?></td>
      <td><?= $row['module_order']; ?></td>
      <td><?= ($row['is_active'] == 'Y' ? 'ใช้งาน' : 'ไม่ใช้งาน') ; ?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm" onclick="showForm(<?= $row['module_id']; ?>)">Edit</button>
        <button type="button" class="btn btn-danger btn-sm" onclick="removeModule(<?= $row['module_id']; ?>)">Del</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable();
  })
</script>
