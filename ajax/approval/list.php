<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate      = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate        = isset($_POST['endDate'])?$_POST['endDate']:"";
$status         = isset($_POST['status'])?$_POST['status']:"";
$employeeId     = isset($_POST['employeeId'])?$_POST['employeeId']:"";
$affiliationId  = isset($_POST['affiliationId'])?$_POST['affiliationId']:"";
$trailer_id   = isset($_POST['trailer_id'])?$_POST['trailer_id']:"";
$cust_id      = isset($_POST['cust_id'])?$_POST['cust_id']:"";
$numRow = 0;
//$con = " and invoice_code is null ";
$con = "";
// if($startDate != "" and  $startDate != "")
// {
//   $con .= " and DATE_FORMAT(pc.Date_Po, '%Y/%m/%d') ='". $startDate ."' ";
// }

if($startDate != "" and  $endDate != "")
{
  $con .= " and jo.job_order_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
}

if($status != "")
{
  $con .= " and jo.job_status_id ='". $status ."' ";
}

if($employeeId != "")
{
  $con .= " and jo.employee_id  ='". $employeeId ."' ";
}

if($affiliationId != "")
{
  $con .= " and jo.affiliation_id  ='". $affiliationId ."' ";
}

if($trailer_id != "" )
{
  $con .= " and jo.trailer_id ='". $trailer_id ."' ";
}

if($cust_id != "")
{
  $con .= " and jo.cust_id = '". $cust_id ."' ";
}

?>
<table class="table table-bordered table-striped " id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;">ลำดับ</th>
      <th style="width:50px;">อนุมัติ</th>
      <th>เลขที่ใบสั่ง</th>
      <th>วันที่สั่งงาน</th>
      <th>ทะเบียนรถ</td>
      <th>ชื่อพนักงานขับรถ</th>
      <th>ShipMent NO</th>
      <th>ต้นทาง</th>
      <th>ปลายทาง</th>
      <th>สังกัด</th>
      <th>เบี้ยเลี้ยง</th>
      <th >ค่าขนส่ง</th>
      <th style="width:50px;"></th>
    </tr>
  </thead>
  <tbody>
<?php
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id
  order by jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];

    $color = "";


    if($affiliation_id == 1){
      $color = 'green';
    }else if($affiliation_id == 2){
      $color = 'yellow';
    }else if($affiliation_id == 3){
      $color = 'red';
    }
      $typeAffiliation = '<span class="fa fa-circle" style="color:'.$color.';font-size:20px;"></span>';

    $checked = "";
    if($job_status_id == "2"){
      $checked = "checked";
    }

?>
  <tr class="text-center">
    <td class="text-center"><?= $i ?></td>
    <td class="text-center" style="width:50px;"><input type="checkbox" class="checkApprove" name="approveId[]" value="<?= $id ?>" <?= $checked ?>></td>
    <td class="text-left" ><?= $job_order_no ?></td>
    <td class="text-left" ><?= $job_delivery_date ?></td>
    <td class="text-left" ><?= $license_plate ?></td>
    <td class="text-left" ><?= $Employee_Name ?></td>
    <td class="text-left" ><?= $shipment ?></td>
    <td class="text-left" ><?= $source ?></td>
    <td class="text-left" ><?= $destination ?></td>
    <td class="text-left" ><?= getAffiliationName($affiliation_id) ?></td>
    <td class="text-right" ><?= number_format($allowance,2);?></td>
    <td class="text-right"><?= number_format($total_amount_allowance,2);?></td>
    <td class="text-center"><?= $typeAffiliation;?></td>
  </tr>
<?php } ?>
</tbody>
</table>

<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [50, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : true
   })
  })
</script>
