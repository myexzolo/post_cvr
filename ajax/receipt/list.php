<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$cust_id      = isset($_POST['cust_id'])?$_POST['cust_id']:"0";
$startDate    = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate      = isset($_POST['endDate'])?$_POST['endDate']:"";
$numRow = 0;
$con = "";


$typeCompany = $_SESSION['typeCompany'];


if($startDate != "" and  $endDate != "")
{
  $con .= " and r.receipt_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
}
if($cust_id != ""){
  $con .= " and c.cust_id  ='". $cust_id ."' ";
}

if($typeCompany == 2)
{
  $url = "printReceiptPost";
}else{
  $url = "printReceipt";
}

?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;">ลำดับ</th>
      <th style="width:200px;">เลขที่ใบเสร็จ</th>
      <th style="width:150px;" class="text-center" >วันออกใบเสร็จ</th>
      <th class="text-center" >ชื่อลูกค้า</th>
      <th style="width:150px;" class="text-center" >Total</th>
      <th style="width:220px;"></th>
    </tr>
  </thead>
  <tbody>
<?php

  $sql = "SELECT * FROM tb_receipt r, tb_customer_master c where r.cust_id = c.cust_id $con order by r.receipt_code DESC";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num    = mysqli_num_rows($query);

  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $receipt_id         = $row['receipt_id'];
    $receipt_code       = $row['receipt_code'];
    $receipt_date       = formatDate($row['receipt_date'],'d/m/Y');
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $total              = $row['total'];
    $cust_name          = $row['cust_name'];

    $customer_name      = $row['customer_name'];
    $customer_address   = $row['customer_address'];

    if($customer_name != "")
    {
      $cust_name = $customer_name;
    }


?>
  <tr class="text-center">
    <td class="text-center"><?= $i ?></td>
    <td class="text-center"><?= $receipt_code ?></td>
    <td class="text-center"><?= $receipt_date ?></td>
    <td class="text-left" ><?= $cust_name ?></td>
    <td class="text-right" ><?= number_format($total,2);?></td>
    <td>
      <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="editReceipt('<?= $receipt_code; ?>','<?= $cust_id; ?>','<?= $cust_name; ?>')" ><i class="fa fa-edit"></i> แก้ไข</button>
      <button type="button" class="btn btn-info btn-sm btn-flat" onclick="<?=$url ?>('<?= $receipt_code; ?>')"><i class="fa fa-print"></i> พิมพ์</button>
    </td>
  </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [50, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
