<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$receiptCode    = isset($_POST['receiptCode'])?$_POST['receiptCode']:"";

$sql = "SELECT * FROM  tb_receipt r,  tb_customer_master c where r.receipt_code ='$receiptCode' and r.cust_id = c.cust_id";
//echo $sql;
$query  = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);

$row = mysqli_fetch_assoc($query);
$receipt_code       = $row['receipt_code'];
$receipt_date       = formatDate($row['receipt_date'],'d/m/Y');
$cust_id            = $row['cust_id'];//รหัสลูกค้า
$total              = $row['total'];
$invoice_codes      = $row['invoice_codes'];

$nameCus            = $row['prefix']." ".$row['cust_name'];
$address            = $row['address'];
$tax                = $row['tax'];
$tel                = $row['tel'];

$customer_name      = $row['customer_name'];
$customer_address   = $row['customer_address'];

?>

<table style="width:215mm;" border="0" id="tableDisplayExp">
  <tr><td colspan="3" style="height:39mm"></td></tr>
  <tr>
    <td colspan="2" style="width:90mm"></td>
    <td align="left" style="padding:5px;font-size:14px"><?= $receipt_code ?></td>
  </tr>
  <tr>
    <td colspan="2" style="width:90mm"></td>
    <td align="left" style="padding:5px;font-size:14px"><?= $receipt_date ?></td>
  </tr>
  <tr>
    <td colspan="3" align="left" style="padding-left:19mm;padding-top:2mm ;font-size:14px"><div style="width:90mm;"><?= $nameCus ?></div></td>
  </tr>
  <tr>
    <td colspan="3" align="left" style="padding-left:19mm;padding-top:4mm;font-size:14px"><div style="width:90mm;"><?= $address ?></div></td>
  </tr>
  <tr>
    <td colspan="3" align="left" style="padding-left:19mm;font-size:14px">เลขประจำตัวผู้เสียภาษี <?= $tax ?></td>
  </tr>
</table>
<table style="width:215mm;" border="0">
  <tr><td colspan="3" style="height:18mm"></td></tr>
  <tr>
    <td style="width:19mm;" align="center">1</td>
    <td style="width:164mm;padding:5px;" align="left">ค่าขนส่ง</td>
    <td align="right" style="padding:10px;padding-right:6mm;"><?= number_format($total,2); ?></td>
  </tr>
  <tr>
    <td></td>
    <td style="width:164mm;padding:5px;" align="left">(<?= $invoice_codes ?>)</td>
    <td align="right" style="padding:10px;vertical-align: top;padding-right:5mm;"></td>
  </tr>
  <tr><td colspan="3" style="height:30mm"></td></tr>
  <tr>
    <td colspan="2"></td>
    <td align="right" style="padding:10px;padding-right:6mm;"><?= number_format($total,2); ?></td>
  </tr>
  <tr><td colspan="3" style="height:7mm"></td></tr>
  <tr>
    <td style="width:19mm" align="center"></td>
    <td style="width:164mm;padding:5px"><?= convert(number_format($total,2)) ?></td>
    <td align="right" style="padding:10px;padding-right:6mm;"><?= number_format($total,2); ?></td>
  </tr>
</table>
