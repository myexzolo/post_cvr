<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$receiptCode    = isset($_POST['receiptCode'])?$_POST['receiptCode']:"";

$sql = "SELECT * FROM  tb_receipt r,  tb_customer_master c where r.receipt_code ='$receiptCode' and r.cust_id = c.cust_id";
//echo $sql;
$query  = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);

$row = mysqli_fetch_assoc($query);
$receipt_code       = $row['receipt_code'];
$receipt_date       = formatDate($row['receipt_date'],'d/m/Y');
$cust_id            = $row['cust_id'];//รหัสลูกค้า
$total              = $row['total'];
$invoice_codes      = $row['invoice_codes'];
$customer_name      = $row['customer_name'];
$customer_address   = $row['customer_address'];

$invoice_codesArr   =  explode(",", $invoice_codes);

$cn = count($invoice_codesArr);

$source = "";
$destination = "";
if($cn == 1){

  // $sqlinv = "SELECT * FROM  tb_invoice i, tb_route_price r
  // where i.invoice_code ='$invoice_codes' and r.contract_no = i.contract";

  $sqlinv = "SELECT source, destination
             FROM tb_job_order WHERE invoice_code = '$invoice_codes'
             GROUP BY source,destination";
  //echo $sqlinv;
  $queryinv   = mysqli_query($conn,$sqlinv);
  $numinv     = mysqli_num_rows($queryinv);

  $rowinv = mysqli_fetch_assoc($queryinv);

  //$contract           = $rowinv['contract'];
  //$installment        = $rowinv['installment'];

  $source             = $rowinv['source'];
  $destination        = $rowinv['destination'];
  //$contract_detail    = $rowinv['contract_detail'];
}

if($customer_name != "")
{
  $nameCus            = $customer_name;
}else{
  $nameCus            = $row['prefix']." ".$row['cust_name'];
}

if($customer_address != "")
{
  $address            = $customer_address;
}else{
  $address            = $row['address'];
}

$tax                = $row['tax'];
$tel                = $row['tel'];

?>

<table style="width:215mm;" border="0" id="tableDisplayExp">
  <tr><td colspan="3" style="height:39mm"></td></tr>
  <tr>
    <td colspan="2" style="width:90mm"></td>
    <td align="left" style="padding:5px;font-size:14px"><?= $receipt_code ?></td>
  </tr>
  <tr>
    <td colspan="2" style="width:90mm"></td>
    <td align="left" style="padding:5px;font-size:14px"><?= $receipt_date ?></td>
  </tr>
  <tr>
    <td colspan="3" align="left" style="padding-left:19mm;padding-top:2mm ;font-size:14px"><div style="width:90mm;"><?= $nameCus ?></div></td>
  </tr>
  <tr>
    <td colspan="3" align="left" style="padding-left:19mm;padding-top:4mm;font-size:14px"><div style="width:90mm;word-wrap: break-word;"><?= $address ?></div></td>
  </tr>
  <tr>
    <td colspan="3" align="left" style="padding-left:19mm;font-size:14px">เลขประจำตัวผู้เสียภาษี <?= $tax ?></td>
  </tr>
</table>
<table style="width:215mm;" border="0">
  <tr><td colspan="3" style="height:18mm"></td></tr>
  <tr>
    <td style="width:19mm;" align="center">1</td>
    <td style="width:164mm;padding:5px;" align="left">
      ค่าบริการขนส่งถุงไปรษณีย์<br>
      <div style="width: 300px;word-wrap: break-word;">อ้างถึง (<?= $invoice_codes ?>)</div>
      <?php
      if($source != "")
      {
        echo "<div style='width: 300px;word-wrap: break-word;'>".$source." - ".$destination."</div>";
      }
      ?>
    </td>
    <td align="right" style="padding:10px;padding-right:6mm;"><?= number_format($total,2); ?></td>
  </tr>
  <tr>
    <td></td>
    <td style="width:164mm;padding:5px;" align="left">
      <!-- (<?php //echo $invoice_codes ?>) -->
    </td>
    <td align="right" style="padding:10px;vertical-align: top;padding-right:5mm;"></td>
  </tr>
  <tr><td colspan="3" style="height:30mm"></td></tr>
  <tr>
    <td colspan="2"></td>
    <td align="right" style="padding:10px;padding-right:6mm;"><?= number_format($total,2); ?></td>
  </tr>
  <tr><td colspan="3" style="height:7mm"></td></tr>
  <tr>
    <td style="width:19mm" align="center"></td>
    <td style="width:164mm;padding:5px"><?= convert(number_format($total,2)) ?></td>
    <td align="right" style="padding:10px;padding-right:6mm;"><?= number_format($total,2); ?></td>
  </tr>
</table>
