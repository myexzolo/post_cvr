<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$receiptCode    = isset($_POST['receiptCode'])?$_POST['receiptCode']:"";

//$receiptCode    = "RV1902002";


$sql = "SELECT * FROM  tb_receipt r,  tb_customer_master c where r.receipt_code ='$receiptCode' and r.cust_id = c.cust_id";
//echo $sql;
$query  = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);

$row = mysqli_fetch_assoc($query);
$receipt_code       = $row['receipt_code'];
$receipt_date       = formatDate($row['receipt_date'],'d/m/Y');
$cust_id            = $row['cust_id'];//รหัสลูกค้า
$total              = $row['total'];
$invoice_codes      = $row['invoice_codes'];
$customer_name      = $row['customer_name'];
$customer_address   = $row['customer_address'];

$invoice_codesArr   =  explode(",", $invoice_codes);

$cn = count($invoice_codesArr);

if($cn > 0){
  $n = 0;
  $strInv = "";
  for($c =0 ; $c < $cn ; $c++){
    $n++;
    $strInv .= $invoice_codesArr[$c].",";
    if($n == 2 && $c != ($cn -1) ){
      $strInv .= "<br>";
      $n = 0;
    }

  }
  $invoice_codes = substr($strInv,0,-1);
}
if($customer_name != "")
{
  $nameCus            = $customer_name;
}else{
  $nameCus            = $row['prefix']." ".$row['cust_name'];
}

if($customer_address != "")
{
  $address            = $customer_address;
}else{
  $address            = $row['address'];
}

$tax                = $row['tax'];
$tel                = $row['tel'];

?>
<div id="master">
<table style="width:100%;" border="0" id="tableDisplayExp">
  <tr>
    <td colspan="3">
      <table style="width:100%;" border="0">
        <tr>
          <td style="width:110px;" align="left"><img src="images/logo_cvr2.jpg" style="height:80px;"></td>
          <td style="vertical-align: text-top;" >
            <span style="font-size:20px;line-height:30px;"><b>ห้างหุ้นส่วนจำกัด โชควิรัตน์ทรานสปอร์ต</b></span><br>
            <span style="font-size:14px;line-height:20px;">
              24 หมู่ที่ 1 ตำบลพระขาว อำเภอบางบาล จังหวัดพระนครศรีอยุธยา 13250<br>
              โทร. 02-115-1981 โทรสาร. 02-115-1981, 02-115-1982<br>
              เลขที่ประจำตัวผู้เสียภาษี 0143546001302
            </span>
          </td>
          <td align="center" style="width:180px; border:1px solid black;vertical-align: text-top;">
            <div style="font-size:16px;padding:5px;"><b>ใบเสร็จรับเงิน<br>Receipt</b></div>
            <div class="receipt" style="font-size:14px;line-height:40px;padding:5px;">ต้นฉบับ</div>
          </td>
        </tr>
      </table>
  </td>
  </tr>
  <tr>
    <td colspan="3">
      <div style="height:30px;"></div>
      <table style="width:100%;">
        <tr>
          <td style="vertical-align: text-top;width:110px;padding:5px;border-top:1px solid black;border-left:1px solid black;">
            <span style="font-size:14px;">ลูกค้า</span>/<span style="font-size:10px;">Customer
          </td>
          <td align="left" style="vertical-align: text-top;font-size:14px;border-top:1px solid black;border-right:1px solid black;"><?= $nameCus ?></td>
          <td align="left" style="width:10px;">&nbsp;</td>
          <td style="vertical-align: text-top;width:70px;padding:5px;border-top:1px solid black;border-left:1px solid black;">
            <span style="font-size:14px;">เลขที่</span>/<span style="font-size:10px;">No.
          </td>
          <td align="left" style="vertical-align: text-top;padding:5px;font-size:14px;width:140px;border-top:1px solid black;border-right:1px solid black;">
            <?= $receipt_code ?>
          </td>
        </tr>
        <tr>
          <td style="vertical-align: text-top;padding:5px;border-left:1px solid black;">
            <span style="font-size:14px;">ที่อยู่</span>/<span style="font-size:10px;">Address
          </td>
          <td align="left" style="padding-top:5px;font-size:14px;border-right:1px solid black;">
            <div style="width: 400px;height: 40px;word-wrap: break-word;">
              <?= $address ?>
            </div>
            </td>
          <td align="left" style="width:10px;">&nbsp;</td>
          <td style="vertical-align: text-top;padding:5px;border-left:1px solid black;">
            <span style="font-size:14px;vertical-align: text-top;">วันที่</span>/<span style="font-size:10px;">Date
          </td>
          <td align="left" style="vertical-align: text-top;padding:5px;font-size:14px;width:100px;border-right:1px solid black;"><?= $receipt_date ?></td>
        </tr>
        <tr>
          <td style="vertical-align: text-top;padding:5px;border-left:1px solid black;border-bottom:1px solid black;">
            <span style="font-size:14px;line-height:20px;">&nbsp;</span>
          </td>
          <td align="left" style="font-size:14px;border-right:1px solid black;border-bottom:1px solid black;">
            เลขประจำตัวผู้เสียภาษี <?= $tax ?>
          </td>
          <td align="left" style="width:10px;">&nbsp;</td>
          <td style="border-left:1px solid black;border-bottom:1px solid black;">
            &nbsp;
          </td>
          <td align="left" style="border-right:1px solid black;border-bottom:1px solid black;">
            &nbsp;
          </td>
        </tr>
      </table>
  </td>
  </tr>
</table>
<table style="width:100%;" border="0">
  <tr><td colspan="5" style="height:10px"></td></tr>
  <tr>
    <td style="font-size:14px;border:1px solid black;width:90px;padding:5px;" align="center">ลำดับ<br>Item</td>
    <td style="font-size:14px;border:1px solid black;padding:5px;" align="center">รายการ<br>Description</td>
    <td style="font-size:14px;border:1px solid black;width:170px;padding:5px;" align="center">จำนวน<br>Quantity</td>
    <td style="font-size:14px;border:1px solid black;width:150px;padding:5px;" align="center">ราคาต่อหน่วย<br>Unit Price</td>
    <td style="font-size:14px;border:1px solid black;width:170px;padding:5px;" align="center">จำนวน<br>Unit Price</td>
  </tr>
  <tr>
    <td style="vertical-align: text-top;border:1px solid black;font-size:14px;height:425px;" align="center">1</td>
    <td style="border:1px solid black;font-size:14px;padding:5px;vertical-align: text-top;" align="left">
      ค่าขนส่ง<br>
      <div style="width: 300px;word-wrap: break-word;">(<?= $invoice_codes ?>)</div>
    </td>
    <td align="right" style="border:1px solid black;vertical-align: text-top;padding:5px;font-size:14px;"><?= number_format($total,2); ?></td>
    <td align="center" style="border:1px solid black;vertical-align: text-top;padding:5px;font-size:14px;">1</td>
    <td align="right" style="border:1px solid black;vertical-align: text-top;padding:5px;font-size:14px;"><?= number_format($total,2); ?></td>
  </tr>
  <tr>
    <td rowspan="2" colspan="2" style="border:1px solid black;padding:5px;font-size:14px;">หมายเหตุ<br>Remarks</td>
    <td colspan="2" style="padding:5px;border:1px solid black;font-size:14px;" align="center">รวมเงิน Sub Total</td>
    <td align="right" style="padding:5px;border:1px solid black;padding:5px;font-size:14px;"><?= number_format($total,2); ?></td>
  </tr>
  <tr>
    <td colspan="2" style="padding:5px;border:1px solid black;" align="center">จำนวนภาษีมูลค่าเพิ่ม Vat</td>
    <td align="right" style="padding:5px;border:1px solid black;padding:5px;">-</td>
  </tr>
  <tr>
    <td style="border-top:1px solid black;border-left:1px solid black;border-bottom:1px solid black;padding:5px;">
      บาท<br>Baht
    </td>
    <td style="border-top:1px solid black;border-right:1px solid black;border-bottom:1px solid black;padding:5px;"><?= convert(number_format($total,2)) ?></td>
    <td colspan="2" style="border:1px solid black;padding:5px;" align="center">จำนวนเงินรวมทั้งสิ้น<br>Net Total Amount</td>
    <td align="right" style="border:1px solid black;padding:5px;"><?= number_format($total,2); ?></td>
  </tr>
</table>
<table style="width:100%;" border="0">
  <tr><td colspan="2" style="height:10px"></td></tr>
  <tr>
    <td style="border:1px solid black;width:50%">
        <table style="width:100%;" border="0">
          <tr>
            <td  style="padding:5px;width:30%">ชำระโดย<br>Payment By</td>
            <td  style="padding:5px;width:5%" align="right;font-size:20px;">
              <div style="border:1px solid black;width:15px;height:15px;"></div>
            </td>
            <td  style="padding:5px;width:15%;font-size:12px;">
              เงินสด<br>Cash
            </td>
            <td  style="padding:5px;width:5%" align="right">
              <div style="border:1px solid black;width:15px;height:15px;"></div>
            </td>
            <td  style="padding:5px;font-size:12px;">
                เช็ค<br>Cheque
            </td>
          </tr>
            <tr>
              <td colspan="3" style="padding:5px;width:50%">
                <div style="font-size:12px;">ธนาคาร</div>
                <div style="font-size:11px;">Bank ................................................................</div>
              </td>
              <td colspan="2" style="padding:5px">
                <div style="font-size:12px;">สาขา</div>
                <div style="font-size:11px;">Branch ...............................................................</div>
              </td>
            </tr>
            <tr>
              <td colspan="3" style="padding:5px">
                <div style="font-size:12px;">ลงวันที่</div>
                <div style="font-size:11px;">Date ...................................................................</div>
              </td>
              <td colspan="2" style="padding:5px">
                <div style="font-size:12px;">เช็คเลขที่</div>
                <div style="font-size:11px;">Cheque No .....................................................</div>
              </td>
            </tr>
        </table>
    </td>
    <td style="border:1px solid black;">
      <table style="width:100%;" border="0">
        <tr>
          <td  colspan="2" style="padding:5px;">ในนาม : <b>ห้างหุ้นส่วนจำกัด โชควิรัตน์ทรานสปอร์ต</b></td>
        </tr>
        <tr>
          <td colspan="2" style="padding:5px;">
            <div style="font-size:12px;">&nbsp;</div>
            <div style="font-size:11px;" align="right">.................................................................................... ผู้มีอำนาจลงนาม/Authorized Signature</div>
          </td>
        </tr>
        <tr>
          <td style="padding:5px;width:55%;border-top:1px solid black;">
            <div style="font-size:12px;padding-top:19px;">ผู้รับเงิน</div>
            <div style="font-size:11px;">Collector .....................................................................</div>
          </td>
          <td style="padding:5px;border-top:1px solid black;">
            <div style="font-size:12px;padding-top:19px;">วันที่</div>
            <div style="font-size:11px;">Date ................................................................</div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<div style="font-size:11px;padding-top:5px;" align="center">ใบเสร็จรับเงินฉบับนี้จะสมบูรณ์ต่อเมื่อมีลายเซ็นของผู้รับเงินและผู้มีอำนาจลงนามกำกับอย่างครบถ้วน และต้องเรียกเก็บเงินตามเช็คได้ครบถ้วนเรียบร้อยแล้ว</div>
<div style="font-size:11px;" align="center">The receipt is vaild when signed by the collector and the authorized persons. In case the payment is made by cheque this receipt is not valid until the cheque is cleared.</div>
<div class="break"></div>
<div id="copy"></div>
