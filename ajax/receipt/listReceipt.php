<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$cust_id      = isset($_POST['cust_id'])?$_POST['cust_id']:"0";
$receiptCode  = isset($_POST['receiptCode'])?$_POST['receiptCode']:"";
$custName     = isset($_POST['custName'])?$_POST['custName']:"";


$numRow = 0;
$con = "";
$con = " and cust_id = '". $cust_id ."'";

$fee           = "";
$bank          = "";
$account       = "";
$transfer      = "";

$numCheck       = 0;

$dateNow = date('Y-m-d');

$receipt_date  = $dateNow;

if($receiptCode == ""){
  $con .= " and receipt_code is null ";
}else{
  $con .= " and (receipt_code = '$receiptCode' or receipt_code is null )";

  $sql = "SELECT * FROM  tb_receipt where receipt_code ='".$receiptCode."'";
  $query  = mysqli_query($conn,$sql);
  $rowr = mysqli_fetch_assoc($query);

  $fee            = $rowr['fee'];
  $bank           = $rowr['bank'];
  $account        = $rowr['account'];
  $transfer       = $rowr['transfer'];
  $receipt_date   = $rowr['receipt_date'];
}

?>
<table style="width:100%">
  <td style="width:100px;padding-bottom:5px" align="left">
    <input type="checkbox" id="checkApp" onclick="checkAll(this)"><label for="checkApp">&nbsp;เลือกทั้งหมด</label>
    <input type="hidden" name="cust_id" value="<?= $cust_id?>">
  <td>
</table>
<table class="table table-bordered table-striped " id="tableDisplay">
  <thead>
    <tr>
			<th style="width:50px;">ลำดับ</th>
      <th style="width:30px;"></th>
      <th style="width:150px;">Invoice No</th>
      <th class="text-center" >วันออก Invoice</th>
      <th class="text-center" >เครดิต(วัน)</th>
      <th class="text-center" >วันครบกำหนด</td>
      <th class="text-center" >สำรองจ่าย</th>
      <th class="text-center" >ค่าขนส่ง</th>
      <th class="text-center" >ส่วนลด</th>
      <th style="width:130px;" class="text-center" >ค่าบริการเพิ่ม (OT)</th>
      <th style="width:130px;" class="text-center" >Total</th>
		</tr>
  </thead>
  <tbody>
<?php
  $sql = "SELECT * FROM  tb_invoice where cust_id is not null and status_del <> 'Y' $con order by invoice_code DESC";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalTransfer = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);

    $checked = "";
    $invoice_id         = $row['invoice_id'];
    $invoice_code       = $row['invoice_code'];
    $invoice_date       = formatDate($row['invoice_date'],'d/m/Y');
    $maturity_date      = formatDate($row['maturity_date'],'d/m/Y');
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $credit             = $row['credit'];
    $provision          = $row['provision'];
    $delivery_cost      = $row['delivery_cost'];
    $discount           = $row['discount'];
    $ot                 = $row['ot'];
    $total_cost         = $row['total_cost'];
    $receipt_code       = $row['receipt_code'];


    if($receiptCode == $receipt_code && $receipt_code!=""){
      $checked = "checked";
      $numCheck++;
    }

?>
  <tr>
    <td class="text-center"><?= $i ?></td>
    <td class="text-center" style="width:50px;"><input type="checkbox" <?= $checked ?> onchange="chkList(this);" class="checkJobId" name="idJob[]" value="<?= $invoice_id ?>"></td>
    <td class="text-center"><?= $invoice_code ?></td>
    <td class="text-center"><?= $invoice_date ?></td>
    <td class="text-right" ><?= $credit ?></td>
    <td class="text-center" ><?= $maturity_date ?></td>
    <td class="text-right" ><?= number_format($provision,2); ?></td>
    <td class="text-right" ><?= number_format($delivery_cost,2); ?></td>
    <td class="text-right" ><?= number_format($discount,2); ?></td>
    <td class="text-right" ><?= number_format($ot,2); ?></td>
    <td class="text-right" ><?= number_format($total_cost,2);?></td>
  </tr>
<?php }
if($num > 0){ ?>
  <tr>
    <td colspan="2" class="text-right" style="line-height:30px"><label>* ชื่อลูกค้า :</label></td>
    <td colspan="4" class="text-left">
      <input value="<?= $custName ?>" name="customer_name" type="text" class="form-control" placeholder="" required>
    </td>
    <td colspan="3" class="text-right" style="line-height:30px"><label>* วันที่ออกใบเสร็จ :</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $receipt_date ?>" name="receipt_date" type="date" class="form-control" placeholder="" required>
      <input value="<?= $numCheck ?>" id="numRecipt" type="hidden">
    </td>
  </tr>
  <tr>
    <td colspan="9" class="text-right" style="line-height:30px"><label>* ยอดเงินโอน :</label></td>
    <td colspan="3" class="text-left">
      <input value="<?= $transfer ?>" name="transfer" type="text" class="form-control" placeholder="" required>
    </td>
  </tr>
<tr>
  <td colspan="9" class="text-right" style="line-height:30px"><label> ค่าธรรมเนียม :</label></td>
  <td colspan="3" class="text-left">
    <input value="<?= $fee ?>" name="fee" type="text" class="form-control" placeholder="">
  </td>
</tr>
<tr>
  <td colspan="9" class="text-right" style="line-height:30px"><label>* ธนาคาร :</label></td>
  <td colspan="3" class="text-left">
    <input value="<?= $bank ?>" name="bank" type="text" class="form-control" placeholder="" required>
  </td>
</tr>
<tr>
  <td colspan="9" class="text-right" style="line-height:30px"><label>* เลขที่บัญชี :</label></td>
  <td colspan="3" class="text-left">
    <input value="<?= $account ?>" name="account" type="text" class="form-control" placeholder="" required>
  </td>
</tr>
<?php } ?>
</tbody>
</table>
<input name="receipt_code" type="hidden" value="<?= $receiptCode ?>">
