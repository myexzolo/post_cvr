<?php

include('../../conf/connect.php');
include('../../inc/utils.php');

$idJob          = isset($_POST['idJob'])?$_POST['idJob']:"";
$cust_id        = isset($_POST['cust_id'])?$_POST['cust_id']:"";
$receipt_code   = isset($_POST['receipt_code'])?$_POST['receipt_code']:"";
$fee           = isset($_POST['fee'])?$_POST['fee']:"";
$bank          = isset($_POST['bank'])?$_POST['bank']:"";
$account       = isset($_POST['account'])?$_POST['account']:"";
$transfer       = isset($_POST['transfer'])?$_POST['transfer']:"";
$receipt_date  = isset($_POST['receipt_date'])?$_POST['receipt_date']:"";
$customer_name  = isset($_POST['customer_name'])?$_POST['customer_name']:"";

if(!empty($idJob)){

  $totalAmount = 0;
  $invoice_codes = "";
  foreach( $idJob as $key => $id ) {
    $sql = "SELECT * from tb_invoice WHERE invoice_id  = '$id'";
    $query  = mysqli_query($conn,$sql);
    $row    = mysqli_fetch_assoc($query);
    $total_cost    = $row['total_cost'];
    $invoice_code  = $row['invoice_code'];

    if($invoice_codes == ""){
      $invoice_codes = $invoice_code;
    }else{
      $invoice_codes .= ",".$invoice_code;
    }
    $totalAmount += $total_cost;
  }

  if($receipt_code != ""){
      $sql = "UPDATE tb_invoice SET receipt_code = null WHERE receipt_code  = '$receipt_code'";
      mysqli_query($conn,$sql);
      //echo $sql;

      $sql = "UPDATE tb_receipt SET
      total = '$totalAmount',
      invoice_codes ='$invoice_codes',
      fee           = '$fee',
      bank          = '$bank',
      account       = '$account',
      transfer      = '$transfer',
      customer_name = '$customer_name',
      receipt_date  = '$receipt_date'
      where receipt_code = '$receipt_code'";
      mysqli_query($conn,$sql);

  }else{
      $date     = date_create($receipt_date);
      $dateYM   = date_format($date, 'ym');

      //$dateYM     = date('ymd');
      $RV         = "RV".$dateYM;

      $sql = "SELECT max(receipt_code) as receipt_code FROM tb_receipt where receipt_code LIKE '$RV%'";
      $query  = mysqli_query($conn,$sql);
      $row    = mysqli_fetch_assoc($query);
      $receiptCode  = $row['receipt_code'];

      if(!empty($receiptCode)){
        $lastNum = substr($receiptCode,strlen($RV));
        $lastNum = $lastNum + 1;
        $receipt_code = $RV.sprintf("%03d", $lastNum);
      }else{
        $receipt_code = $RV.sprintf("%03d", 1);
      }

      $sql = "INSERT INTO tb_receipt
             (receipt_code,receipt_date,total,cust_id,invoice_codes,fee,bank,account,transfer,customer_name)
             VALUES('$receipt_code','$receipt_date','$totalAmount','$cust_id','$invoice_codes','$fee','$bank','$account','$transfer','$customer_name')";
      mysqli_query($conn,$sql);
      //echo $sql;
  }

  foreach( $idJob as $key => $id ) {
    $sql = "UPDATE tb_invoice SET receipt_code = '$receipt_code' WHERE invoice_id  = '$id'";
    mysqli_query($conn,$sql);
  }


  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>
