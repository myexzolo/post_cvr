<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate   = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate     = isset($_POST['endDate'])?$_POST['endDate']:"";
$status        = isset($_POST['status'])?$_POST['status']:"";
$employeeId    = isset($_POST['employeeId'])?$_POST['employeeId']:"";
$jobOrderNo    = isset($_POST['jobOrderNo'])?$_POST['jobOrderNo']:"";
$dp            = isset($_POST['dp'])?$_POST['dp']:"";
$trailerId     = isset($_POST['trailerId'])?$_POST['trailerId']:"";
$awaySpecNo    = isset($_POST['awaySpecNo'])?$_POST['awaySpecNo']:"";
$cust_id       = isset($_POST['cust_id'])?$_POST['cust_id']:"";
$type_job      = isset($_POST['type_job'])?$_POST['type_job']:"";

$con = "";
$cusName = "";
if($startDate != "")
{
  $con .= " and jo.job_order_date between '". $startDate ."' and '". $endDate ."' ";
}

if($status != "")
{
  $con .= " and jo.job_status_id ='". $status ."' ";
}
if($employeeId != "")
{
  $con .= " and jo.employee_id = '". $employeeId ."' ";
}
if($jobOrderNo != "")
{
  $con .= " and jo.job_order_no LIKE '". $jobOrderNo ."%' ";
}
if($dp != "")
{
  $con .= " and jo.job_order_no in (select job_order_no FROM tb_dp where dp_code LIKE '". $dp ."%')";
}

if($trailerId != "")
{
  $con .= " and jo.trailer_id = '". $trailerId ."' ";
}

if($awaySpecNo != "")
{
  $con .= " and jo.away_spec_no LIKE '". $awaySpecNo ."%' ";
}

if($type_job != "")
{
  $con .= " and jo.type_job = '". $type_job ."' ";
}

if($cust_id != "")
{
  $con .= " and jo.cust_id = '". $cust_id ."' ";
  $cusName = getCustomerName($cust_id);
}
?>
<table style="width:100%">
  <tr>
    <td id="hd0" ></td>
  </tr>
  <tr>
    <td id="hd1" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>รายงานสรุปยอดรถร่วม <?= $cusName ?></b>
    </td>
  </tr>
  <tr>
    <td id="hd2" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>
        ประจำวันที่ <?= formatDateTh($startDate) ?> ถึงวันที่ <?= formatDateTh($endDate) ?>
      </b>
    </td>
  </tr>
</table>
<?php

  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name, c.cust_name, t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_customer_master c, tb_trailer t
  where jo.employee_id = em.employee_id $con and jo.cust_id = c.cust_id and job_status_id <> 3 and jo.trailer_id = t.trailer_id and jo.affiliation_id = '3'
  order by t.license_plate,jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $time = 0;
  $license_plate_tmp = "";

  $weightss = 0;
  $total_amount_ap_pays = 0;
  $total_amount_receives = 0;
  $job_order_profits = 0;

  $weightsss = 0;
  $total_amount_ap_payss = 0;
  $total_amount_receivess = 0;
  $job_order_profitss = 0;
  if($num > 0){
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $job_order_no         = $row['job_order_no'];
    $job_order_date       = date('d/m/Y', strtotime($row['job_order_date']));
    $job_delivery_date    = date('d/m/Y', strtotime($row['job_delivery_date']));
    $cust_dp              = $row['cust_dp'];
    $source               = $row['source'];
    $destination          = $row['destination'];
    $Employee_id          = $row['employee_id'];
    $Employee_Name        = $row['employee_name'];
    $affiliation_id       = $row['affiliation_id'];
    $job_status_id        = $row['job_status_id'];
    $shipment             = $row['shipment'];
    $cust_name            = $row['cust_name'];
    $away_spec_no         = $row['away_spec_no'];//สเปคหาง
    $license_plate        = $row['license_plate'];//หมายเลขทะเบียนรถ
    $product_name         = $row['product_name'];//สินค้า
    $weights              = $row['weights'];//น้ำหนัก(ตัน)
    $fuel_cost            = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre           = $row['fuel_litre'];//จำนวนลิตร
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ 1=ราคาต่อตัน  2=ราคาต่อเที่ยว
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_order_profit         = $row['job_order_profit'];
    $invoice_code         = $row['invoice_code'];//เลขที่ใบแจ้งหนี้ ลูกหนี้

    if($price_type_id == 2){
      $one_trip_ton     = $price_per_trip;
      $ext_one_trip_ton = $ext_price_per_trip;
    }

    if($license_plate_tmp != $license_plate)
    {
      if($license_plate_tmp != ""){
    ?>
        <tr class="text-center tdb">
          <td colspan="7" class="text-right tdb" ><b>รวม</b></td>
          <td align="right"  class="tdb"><?= number_format($weightss,3); ?></td>
          <td align="right"  class="tdb"></td>
          <td align="right"  class="tdb"><?= number_format($total_amount_ap_pays,2); ?></td>
          <td align="right"  class="tdb"></td>
          <td align="right"  class="tdb"><?= number_format($total_amount_receives,2); ?></td>
          <td align="right"  class="tdb"><?= number_format($job_order_profits,2); ?></td>
        </tr>
            </tbody>
            </table>
            <div class="break"></div>
    <?php
          $time = 0;
          $weightss = 0;
          $total_amount_ap_pays = 0;
          $total_amount_receives = 0;
          $job_order_profits = 0;
        }
        $license_plate_tmp = $license_plate;
     ?>
     <table class="table table-bordered table-striped table-hover tabf" style="width:100%">
       <thead>
        <tr>
          <td class="tdherder" style="border-style: none;" colspan="12" align="left"><br><b><?= "ทะเบียนรถ ". $license_plate."  ".$Employee_Name?></b></td>
        </tr>
         <tr class="text-center">
           <th style="width:3%;" class="tdb">เที่ยว</th>
           <th style="width:8%;" class="text-center tdb" >วันที่</th>
           <th style="width:9%;" class="text-center tdb" >SH.</th>
           <th style="width:9%;" class="text-center tdb" >DP.</td>
           <th style="width:9%;" class="text-center tdb" >เลขที่วางบิล</td>
           <th style="width:20%;" class="text-center tdb" >รายการ</th>
           <th class="text-center tdb" style="width:6%;">หาง</th>
           <th class="text-center tdb" style="width:7%;">ตัน/เที่ยว</th>
           <th class="text-center tdb" style="width:7%;">ราคาจ่าย</th>
           <th class="text-center tdb" style="width:8%;">ราคารวม</th>
           <th class="text-center tdb" style="width:7%;">วางบิล</th>
           <th class="text-center tdb" style="width:8%;">ราคารวม</th>
           <th class="text-center tdb" style="width:8%;">กำไร</th>
         </tr>
       </thead>
       <tbody>
     <?php
      }
      $time++;

      $weightss += $weights;
      $total_amount_ap_pays  += $total_amount_ap_pay;
      $total_amount_receives += $total_amount_receive;
      $job_order_profits += $job_order_profit;

      $weightsss += $weights;
      $total_amount_ap_payss  += $total_amount_ap_pay;
      $total_amount_receivess += $total_amount_receive;
      $job_order_profitss += $job_order_profit;

    ?>


    <tr class="text-center">
      <td align="center"  class="tdb"><?= $time ?></td>
      <td align="center"  class="tdb"><?= $job_order_date ?></td>
      <td  class="tdb"><?= $shipment ?></td>
      <td  class="tdb"><?= $cust_dp ?></td>
      <td  class="tdb"><?= $invoice_code ?></td>
      <td align="left"  class="tdb"><?= $source." - ".$destination ?></td>
      <td align="left"  class="tdb"><?= $away_spec_no ?></td>
      <td align="right"  class="tdb"><?= number_format($weights,3); ?></td>
      <td align="right"  class="tdb"><?= number_format($ext_one_trip_ton,2); ?></td>
      <td align="right"  class="tdb"><?= number_format($total_amount_ap_pay,2); ?></td>
      <td align="right"  class="tdb"><?= number_format($one_trip_ton,2); ?></td>
      <td align="right"  class="tdb"><?= number_format($total_amount_receive,2); ?></td>
      <td align="right"  class="tdb"><?= number_format($job_order_profit,2); ?></td>
    </tr>
<?php }
?>
<tr class="text-center">
  <td colspan="7" class="text-right tdb" ><b>รวม</b></td>
  <td align="right"  class="tdb"><?= number_format($weightss,3); ?></td>
  <td align="right"  class="tdb"></td>
  <td align="right"  class="tdb"><?= number_format($total_amount_ap_pays,2); ?></td>
  <td align="right"  class="tdb"></td>
  <td align="right"  class="tdb"><?= number_format($total_amount_receives,2); ?></td>
  <td align="right"  class="tdb"><?= number_format($job_order_profits,2); ?></td>
</tr>
</tbody>
</table>

<table class="table table-bordered table-striped tabf" width="100%">
<tr class="text-center">
  <td colspan="7"   style="width:55%;" class="text-right tdb" ><b>รวมทั้งสิ้น</b></td>
  <td align="right" class="tdb" style="width:7%;"><?= number_format($weightsss,3); ?></td>
  <td align="right" class="tdb" style="width:7%;"></td>
  <td align="right" class="tdb" style="width:8%;"><?= number_format($total_amount_ap_payss,2); ?></td>
  <td align="right" class="tdb" style="width:7%;"></td>
  <td align="right" class="tdb" style="width:8%;"><?= number_format($total_amount_receivess,2); ?></td>
  <td align="right" class="tdb" style="width:8%;"><?= number_format($job_order_profitss,2); ?></td>
</tr>
</tbody>
</table>
<?php
}else{
  echo "<div style='padding-top:5px;'>ไม่พบข้อมูล</div>";
}
?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [2, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
