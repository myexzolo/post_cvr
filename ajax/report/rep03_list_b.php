<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate      = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate        = isset($_POST['endDate'])?$_POST['endDate']:"";
$petrol_group   = isset($_POST['petrol_group'])?$_POST['petrol_group']:"";
$trailer_id     = isset($_POST['trailer_id'])?$_POST['trailer_id']:"";
$employeeId     = isset($_POST['employeeId'])?$_POST['employeeId']:"";
$route_id       = isset($_POST['route_id'])?$_POST['route_id']:"";
$cust_id        = isset($_POST['cust_id'])?$_POST['cust_id']:"";
$petrol_code    = isset($_POST['petrol_code'])?$_POST['petrol_code']:"";
$fuelDate       = isset($_POST['fuelDate'])?$_POST['fuelDate']:"";

$con = "";
$cusName = "";
$nump = 0;
if($startDate != "")
{
  $con = " and jo.job_order_date between '". $startDate ."' and '". $endDate ."' ";
}

if($trailer_id != "")
{
  $con .= " and jo.trailer_id = '". $trailer_id ."'";
}

if($employeeId != "")
{
  $con .= " and jo.employee_id = '". $employeeId ."'";
}

if($route_id != "")
{
  $con .= " and jo.route_id = '". $route_id ."'";
}

if($cust_id != "")
{
  $con .= " and jo.cust_id = '". $cust_id ."'";
}
$conp = "";
if($petrol_code != ""){
  $conp  = " and petrol_code = '".$petrol_code."' ";
}

if($fuelDate != ""){
  $con  .= " and fuel_date = '".$fuelDate."' ";
}

$petrolCodeList = "";
$jobOrderNolist = "";

$petrolCode = array();
$petrolArr = array();

if($petrol_group != "")
{
  $sql = "SELECT * FROM tb_petrol_master  where  petrol_group ='". $petrol_group ."' $conp";
  $queryp   = mysqli_query($conn,$sql);
  $nump     = mysqli_num_rows($queryp);

  for ($i=0; $i < $nump ; $i++) {
    $row = mysqli_fetch_assoc($queryp);

    $petrolCode[$i] = $row;
    $petrolArr[$i]['tl']  = 0;
    $petrolArr[$i]['p']   = 0;
    $petrolArr[$i]['tp']  = 0;
    if($petrolCodeList == ""){
      $petrolCodeList = "'".$row['petrol_code']."'";
    }else{
      $petrolCodeList .= ",'".$row['petrol_code']."'";
    }

    if($jobOrderNolist == ""){
      $jobOrderNolist = "'".$row['petrol_code']."'";
    }else{
      $jobOrderNolist .= ",'".$row['petrol_code']."'";
    }
  }
}else{
  $sql = "SELECT p.petrol_code , p.petrol_station, jo.job_order_no as petrol_name
  FROM tb_job_order jo ,tb_petrol p
  where jo.job_order_no = p.job_order_no  $con  $conp
  GROUP BY petrol_code";
  //echo $sql;
  $queryp   = mysqli_query($conn,$sql);
  $nump     = mysqli_num_rows($queryp);

  for ($i=0; $i < $nump ; $i++) {
    $row = mysqli_fetch_assoc($queryp);
    $petrol_code = $row['petrol_code'];
    if($petrol_code == ""){

    }
    $petrolCode[$i] = $row;
    $petrolArr[$i]['tl']  = 0;
    $petrolArr[$i]['p']   = 0;
    $petrolArr[$i]['tp']  = 0;
    if($petrolCodeList == ""){
      $petrolCodeList = "'".$row['petrol_code']."'";
    }else{
      $petrolCodeList .= ",'".$row['petrol_code']."'";
    }
  }
}

$petroName = "";
if($petrol_group == "SH")
{
  $petroName = "shell";
}
else if($petrol_group == "BG")
{
  $petroName = "บางจาก";
}
else if($petrol_group == "PT")
{
  $petroName = "PT";
}
else if($petrol_group == "BB")
{
  $petroName = "อู่บางบาล";
}
else if($petrol_group == "PTT")
{
  $petroName = "ปตท.";
}
else if($petrol_group == "ES")
{
  $petroName = "ESSO";
}
?>
<table style="width:100%">
  <tr>
    <td id="hd0" ></td>
  </tr>
  <tr>
    <td id="hd1" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>รายงานสรุปการใช้น้ำมัน <?= $petroName ?></b>
    </td>
  </tr>
  <tr>
    <td id="hd2" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>
        ประจำวันที่ <?= formatDateTh($startDate) ?> ถึงวันที่ <?= formatDateTh($endDate) ?>
      </b>
    </td>
  </tr>
</table>
<br>
<?php

  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  // $sql = "SELECT jo.*,em.employee_name, c.cust_name, t.license_plate
  // FROM tb_job_order jo, tb_employee_master em, tb_customer_master c, tb_trailer t
  // where jo.employee_id = em.employee_id $con and jo.cust_id = c.cust_id and job_status_id <> 3 and jo.trailer_id = t.trailer_id and jo.affiliation_id = '3'
  // order by t.license_plate,jo.job_order_date";
?>
<div style=" overflow-x: auto;width:100%" id="divOverFlow">
<table class="table table-bordered table-striped table-hover tabf" id="tableDisplay">
  <thead>
    <tr>
      <td class="tdherder" style="border-style: none;" colspan="<?= ($nump * 3) + 7 ?>" align="left"></td>
    </tr>
    <tr class="text-center">
      <th rowspan="2" class="text-center tdb" style="vertical-align:middle;" >วันที่</th>
      <th rowspan="2" class="text-center tdb" style="vertical-align:middle;">ทะเบียน</th>
      <th rowspan="2" class="text-center tdb" style="vertical-align:middle;">ประเภทรถ</td>
      <th rowspan="2" class="text-center tdb" style="vertical-align:middle;">ชื่อพนักงานขับรถ</td>
      <th rowspan="2" class="text-center tdb" style="vertical-align:middle;">หน่วยงาน</th>
      <th rowspan="2" class="text-center tdb" style="vertical-align:middle;">ต้นทาง</th>
      <th rowspan="2" class="text-center tdb" style="vertical-align:middle;">ปลายทาง</th>
      <?php
        for ($i=0; $i < $nump ; $i++) {
            echo "<th colspan='3' class='text-center tdb' >".$petrolCode[$i]['petrol_name']."</th>";
        }
      ?>
    </tr>

      <?php
      if($nump > 0){
        echo "<tr class='text-center tdb'>";
        for ($i=0; $i < $nump ; $i++) {
            echo "<th class='text-center tdb' >จำนวน<br>ลิตรที่เติม</th>";
            echo "<th class='text-center tdb' >ราคา/<br>ลิตร</th>";
            echo "<th class='text-center tdb' >จำนวนเงิน<br>ตามปั้ม</th>";
        }
        echo "</tr>";
      }
      ?>
  </thead>
  <tbody>
    <?php
    if($nump > 0){
        $sql = "SELECT jo.job_order_no,jo.job_order_date ,jo.source,jo.department,jo.destination,
        em.employee_name, p.*, t.license_plate, t.nature
        FROM tb_petrol p, tb_job_order jo, tb_employee_master em, tb_trailer t
        where p.petrol_code in (".$petrolCodeList.") and p.job_order_no = jo.job_order_no
        and jo.employee_id = em.employee_id $con and jo.trailer_id = t.trailer_id $con
        order by jo.job_order_date , p.job_order_no";
        //echo $sql;
        $query   = mysqli_query($conn,$sql);
        $num     = mysqli_num_rows($query);
        //echo   $num;
        for ($i=1; $i <= $num ; $i++) {
          $row = mysqli_fetch_assoc($query);
          $job_order_no         = $row['job_order_no'];
          $job_order_date       = date('d/m/Y', strtotime($row['job_order_date']));
          $source               = $row['source'];
          $destination          = $row['destination'];
          $Employee_Name        = $row['employee_name'];
          $department           = $row['department'];
          $license_plate        = $row['license_plate'];//หมายเลขทะเบียนรถ
          $nature = $row['nature'];//ประเภทรถ

          $petrol_code   = $row['petrol_code'];//ปั้ม
          $fuel_cost     = $row['fuel_cost'];//ค่าน้ำมัน
          $fuel_litre    = $row['fuel_litre'];//จำนวนลิตร
    ?>
        <tr class="text-center tdb">
          <td align="center"  class="tdb"><?= $job_order_date; ?></td>
          <td align="center"  class="tdb"><?= $license_plate; ?></td>
          <td align="center"  class="tdb"><?= $nature; ?></td>
          <td align="left"  class="tdb"><?= $Employee_Name; ?></td>
          <td align="center"  class="tdb"><?= $department; ?></td>
          <td align="left"  class="tdb"><?= $source; ?></td>
          <td align="left"  class="tdb"><?= $destination; ?></td>
    <?php
    for ($x=0; $x < $nump ; $x++) {
        $tl = '';
        $p = '';
        $tp = '';
        //echo $Employee_Name.">>".$petrol_code."=". $petrolCode[$x]['petrol_code']."<br>";
        if($petrol_code == $petrolCode[$x]['petrol_code'] && $petrol_code != ""){
          $tl = $fuel_litre;
          $p  = number_format($fuel_cost,2);
          $tp = number_format(($fuel_litre * $fuel_cost),2);

          $petrolArr[$x]['tl']  += $tl;
          $petrolArr[$x]['p']   += $fuel_cost;
          $petrolArr[$x]['tp']  += ($fuel_litre * $fuel_cost);
        }
        echo "<td class='text-right tdb' >$tl</td>";
        echo "<td class='text-right tdb' >$p</td>";
        echo "<td class='text-right tdb' >$tp</td>";
    }
     ?>
        </tr>
    <?php
        }
    ?>
    <tr class="text-center tdb">
      <td colspan="7" align="center"  class="tdb"><b>รวม</b></td>
   <?php
       $ttl = 0;
       $ttp = 0;

       for ($x=0; $x < $nump  ; $x++) {
           $tl = number_format($petrolArr[$x]['tl'],2);
           $p = number_format($petrolArr[$x]['p'],2);
           $tp = number_format($petrolArr[$x]['tp'],2);

           $ttl += $petrolArr[$x]['tl'];
           $ttp += $petrolArr[$x]['tp'];

           echo "<td class='text-right tdb' >$tl</td>";
           echo "<td class='text-right tdb' >$p</td>";
           echo "<td class='text-right tdb' >$tp</td>";
       }

       $numTD = ($nump * 3) + 4;
   ?>

    </tr>
    <tr class="text-center tdb">
      <td colspan="<?= $numTD ?>" align="right"  class="tdb"><b>รวมจำนวนลิตร</b></td>
      <td colspan="3" align="right"  class="tdb"><b><?= number_format($ttl,2) ?></b></td>
    </tr>
    <tr class="text-center tdb">
      <td colspan="<?= $numTD ?>" align="right"  class="tdb"><b>รวมจำนวนเงิน</b></td>
      <td colspan="3" align="right"  class="tdb"><b><?= number_format($ttp,2) ?></b></td>
    </tr>
    <?php
      }
    ?>
  </tbody>
</table>
</div>
