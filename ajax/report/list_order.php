<?php
include('../../conf/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate   = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate     = isset($_POST['endDate'])?$_POST['endDate']:"";
$status        = isset($_POST['status'])?$_POST['status']:"";
$employeeId    = isset($_POST['employeeId'])?$_POST['employeeId']:"";
$jobOrderNo    = isset($_POST['jobOrderNo'])?$_POST['jobOrderNo']:"";
$dp            = isset($_POST['dp'])?$_POST['dp']:"";
$trailerId     = isset($_POST['trailerId'])?$_POST['trailerId']:"";
$awaySpecNo    = isset($_POST['awaySpecNo'])?$_POST['awaySpecNo']:"";
$cust_id       = isset($_POST['cust_id'])?$_POST['cust_id']:"";
$statusInv     = isset($_POST['statusInv'])?$_POST['statusInv']:"";
$productName   = isset($_POST['productName'])?$_POST['productName']:"";

$con = "";
if($startDate != "")
{
  $con .= " and jo.job_order_date between '". $startDate ."' and '". $endDate ."' ";
}

if($status != "")
{
  $con .= " and jo.job_status_id ='". $status ."' ";
}
if($employeeId != "")
{
  $con .= " and jo.employee_id = '". $employeeId ."' ";
}
if($jobOrderNo != "")
{
  $con .= " and jo.job_order_no LIKE '". $jobOrderNo ."%' ";
}
if($dp != "")
{
  $con .= " and jo.job_order_no in (select job_order_no FROM tb_dp where dp_code LIKE '". $dp ."%')";
}

if($trailerId != "")
{
  $con .= " and jo.trailer_id = '". $trailerId ."' ";
}

if($awaySpecNo != "")
{
  $con .= " and jo.away_spec_no LIKE '". $awaySpecNo ."%' ";
}

if($cust_id != "")
{
  $con .= " and jo.cust_id = '". $cust_id ."' ";
}

if($statusInv != "")
{
  if($statusInv == "1"){
    $con .= " and jo.invoice_code is null ";
  }else if($statusInv == "2"){
    $con .= " and jo.invoice_code is not null ";
  }
}

if($productName != "")
{
  $con .= " and jo.product_name like '%". $productName ."%' ";
}

?>
<table class="table table-bordered table-striped " id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;">No</th>
      <th class="text-center" >วันที่</th>
      <th class="text-center" >เลขที่ใบสั่ง</th>
      <th class="text-center" >กำหนดส่ง</td>
      <th class="text-center" >ต้นทาง</th>
      <th class="text-center" >ปลายทาง</th>
      <th class="text-center" style="width:70px;">ทะเบียนรถ</th>
      <th class="text-center" style="width:70px;">ทะเบียนหาง</th>
      <th class="text-center" >Spec หาง</th>
      <th class="text-center" >ชื่อพนักงานขับรถ</th>
      <th class="text-center" style="width:90px;">DP</th>
      <th class="text-center" >สินค้า</th>
      <th class="text-center" >ระยะทาง</th>
      <th class="text-center" >น้ำหนัก(ตัน)</th>
      <th class="text-center" >ค่าน้ำมัน</th>
      <th class="text-center" >จำนวนลิตร</th>
    </tr>
  </thead>
  <tbody>
<?php
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name, c.cust_name, t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_customer_master c, tb_trailer t
  where jo.employee_id = em.employee_id $con and jo.cust_id = c.cust_id and job_status_id <> 3 and jo.trailer_id = t.trailer_id
  order by jo.job_order_no DESC";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalDistance  = 0;
  $totalWeights   = 0;
  $totalFuelCost  = 0;
  $totalfuelLitre = 0;

  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $job_order_no         = $row['job_order_no'];
    $job_order_date       = date('d/m/Y', strtotime($row['job_order_date']));
    $job_delivery_date    = date('d/m/Y', strtotime($row['job_delivery_date']));
    $cust_dp              = $row['cust_dp'];
    $source               = $row['source'];
    $destination          = $row['destination'];
    $Employee_id          = $row['employee_id'];
    $Employee_Name        = $row['employee_name'];
    $affiliation_id       = $row['affiliation_id'];
    $job_status_id        = $row['job_status_id'];
    $away_regi_no         = $row['away_regi_no'];//หาง
    $shipment             = $row['shipment'];
    $cust_name            = $row['cust_name'];
    $away_spec_no         = $row['away_spec_no'];//สเปคหาง
    $license_plate        = $row['license_plate'];//หมายเลขทะเบียนรถ
    $product_name         = $row['product_name'];//สินค้า
    $weights              = $row['weights'];//น้ำหนัก(ตัน)
    $fuel_cost            = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre           = $row['fuel_litre'];//จำนวนลิตร
    $distance             = $row['distance'];//ระยะทาง

    $totalDistance        += $distance;
    $totalWeights         += $weights;
    $totalFuelCost        += $fuel_cost;
    $totalfuelLitre       += $fuel_litre;

?>
    <tr class="text-center">
      <td align="center"><?= $i ?></td>
      <td align="center"><?= $job_order_date ?></td>
      <td><?= $job_order_no ?></td>
      <td align="center"><?= $job_delivery_date ?></td>
      <td align="left"><?= $source ?></td>
      <td align="left"><?= $destination ?></td>
      <td align="center"><?= $license_plate ?></td>
      <td align="center"><?= $away_regi_no ?></td>
      <td align="left"><?= $away_spec_no ?></td>
      <td align="left"><?= $Employee_Name ?></td>
      <td align="left"><?= $cust_dp ?></td>
      <td align="left"><?= $product_name ?></td>
      <td align="right"><?= number_format($distance,2); ?></td>
      <td align="right"><?= number_format($weights,3); ?></td>
      <td align="right"><?= number_format($fuel_cost,2); ?></td>
      <td align="right"><?= number_format($fuel_litre,2); ?></td>
    </tr>
<?php } ?>
<tr class="text-center">
  <td align="center" colspan="12" style="text-align:right;">รวม</td>
  <td align="right"><?= number_format($totalDistance,2); ?></td>
  <td align="right"><?= number_format($totalWeights,3); ?></td>
  <td align="right"><?= number_format($totalFuelCost,2); ?></td>
  <td align="right"><?= number_format($totalfuelLitre,2); ?></td>
</tr>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [2, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
