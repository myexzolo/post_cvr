<style type="text/css">
     .right { text-align: right; }
</style>
<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate      = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate        = isset($_POST['endDate'])?$_POST['endDate']:"";
$employeeId     = isset($_POST['employeeId'])?$_POST['employeeId']:"";

$fuel_litres = 0;
$fuel_costs = 0;
$blank_charges = 0;
$allowances = 0;
$kog_expenses = 0;
$job_ended_acc_expenses = 0;
$allowance_oths = 0;
$job_ended_expressways = 0;
$job_ended_recaps = 0;
$total_amount_allowances = 0;
$total_amount_receives = 0;
$withholdings = 0;
$job_order_profits = 0;
$fuel_driver_bills =  0;

$numRow = 0;
$con = "";

$iNum = 0;

$employeeIdTmp = "";

if($employeeId != "")
{
  $con .= " and jo.employee_id ='". $employeeId ."'";
}

?>
<table style="width:100%">
  <tr>
    <td id="hd0"></td>
  </tr>
  <tr>
    <td id="hd1" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>รายงานรายได้พนักงานขับรถ</b>
    </td>
  </tr>
  <tr>
    <td id="hd2" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>
        ประจำวันที่ <?= formatDateTh($startDate) ?> ถึงวันที่ <?= formatDateTh($endDate) ?>
      </b>
    </td>
  </tr>
</table>
<?php

  $con .= " and jo.job_order_date between '". $startDate ."' and '". $endDate ."'";
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id and  jo.job_status_id = '2' and invoice_code is not null and jo.affiliation_id = '1' 
  order by jo.employee_id,jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalTransfer = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = chkNum($row['fuel_driver_bill']);//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];
    $job_order_profit         = $row['job_order_profit'];

    $invoice_code             = $row['invoice_code'];
    $period_code              = $row['period_code'];
    $license_plate            = $row['license_plate'];


    $transfer = ($fuel_cost + $allowance + $kog_expense + $blank_charge + $allowance_oth);
    $totalTransfer +=  $transfer;

    $total_amount_allowance -= $job_ended_acc_expense;

    $job_order_profit +=  $job_ended_acc_expense;

    $withholding  =  round(($total_amount_receive * 0.01),2);

    $job_order_profit -= $withholding;



    //echo  $employee_id." <> ".$employeeIdTmp."<br>";
    if($employee_id != $employeeIdTmp){
      if($employeeIdTmp != ""){
      ?>
        <tr>
          <td colspan="8" class="text-right tdb" ><b>รวม</b></td>
          <td class="text-right tdb" ><b><?= number_format($fuel_litres,2); ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($fuel_costs,2);  ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($blank_charges,2); ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($allowances,2); ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($kog_expenses,2); ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($allowance_oths,2); ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($job_ended_expressways,2); ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($job_ended_recaps,2); ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($total_amount_allowances,2); ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($total_amount_receives,2); ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($withholdings,2); ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($job_order_profits,2); ?></b></td>
          <td class="text-right tdb" ><b><?= number_format($fuel_driver_bills,2); ?></b></td>
        </tr>
        </tbody>
        </table>
        <div class="break"></div>
      </div>
<?php
        $fuel_litres            = 0;
        $fuel_costs             = 0;
        $blank_charges          = 0;
        $allowances             = 0;
        $kog_expenses           = 0;
        $job_ended_acc_expenses = 0;
        $allowance_oths         = 0;
        $job_ended_expressways  = 0;
        $job_ended_recaps       = 0;
        $total_amount_allowances  = 0;
        $total_amount_receives  = 0;
        $withholdings           = 0;
        $job_order_profits      = 0;
        $fuel_driver_bills      = 0;

      }



      $employeeIdTmp = $employee_id;
      $iNum = 0;



      //echo  ">>>>".$employee_id." <<<<>>>> ".$employeeIdTmp."<br>";

?>
<br>
<div class="divf" style="font-size:14px;"><b><?= $Employee_Name ?></b></div>
<div class="divo" style="width:100%;overflow-x: auto">
<table class="table table-bordered table-striped tabf"  style="font-size:14px;width:2000px;">
  <thead>
    <tr>
			<th style="width:30px;" class="tdb">No.</th>
      <th style="width:90px;" class="tdb">วันที่สั่งจ้าง</th>
      <th style="width:98px;" class="tdb">เลขที่เอกสาร</th>
      <th style="width:100px;" class="tdb">Shipment</th>
      <th style="width:80px;" class="tdb">ทะเบียน</th>
      <th style="width:115px;" class="tdb">เลขวางบิล</th>
			<th style="width:180px;" class="tdb">ต้นทาง</th>
			<th class="tdb">ปลายทาง</th>
			<th class="tdb" style="width:80px;">จำนวนลิตร</th>
			<th class="tdb">ค่าน้ำมัน</th>
      <th class="tdb">ตีเปล่า</th>
      <th class="tdb">เบี้ยเลี้ยง</th>
      <th class="tdb">คอก</th>
      <th class="tdb" >อื่นๆ</th>
      <th class="tdb" >ทางด่วน</th>
      <th class="tdb" style="width:80px;">ปะยาง</th>
      <th class="tdb" style="width:85px;">รวมค่าใช้จ่าย</th>
      <th class="tdb" style="width:100px;">รายได้ค่าขนส่ง</th>
      <th class="tdb" style="width:60px;">หัก 1%</th>
      <th class="tdb" style="width:80px;">กำไรขั้นต้น</th>
      <th class="tdb" style="width:80px;">บิลน้ำมัน</th>
		</tr>
  </thead>
  <tbody>
<?php
}
$fuel_litres            += $fuel_litre;
$fuel_costs             += $fuel_cost;
$blank_charges          += $blank_charge;
$allowances             += $allowance;
$kog_expenses           += $kog_expense;
//$job_ended_acc_expenses += $job_ended_acc_expense;
$allowance_oths         += $allowance_oth;
$job_ended_expressways  += $job_ended_expressway;
$job_ended_recaps       += $job_ended_recap;
$total_amount_allowances  += $total_amount_allowance;
$total_amount_receives  += $total_amount_receive;
$withholdings           += $withholding;
$job_order_profits      += $job_order_profit;
$fuel_driver_bills      += $fuel_driver_bill;
$iNum++;
?>
  <tr>
    <td class="text-center tdb" ><?= $iNum ?></td>
    <td class="text-center tdb" ><?= $job_order_date ?></td>
    <td class="text-left tdb" ><?= $job_order_no ?></td>
    <td class="text-center tdb" ><?= $shipment ?></td>
    <td class="text-center tdb" ><?= $license_plate ?></td>
    <td class="text-center tdb" ><?= $invoice_code ?></td>
    <td class="text-left tdb" ><?= $source ?></td>
    <td class="text-left tdb" ><?= $destination ?></td>
    <td class="text-right tdb" ><?= number_format($fuel_litre,2); ?></td>
    <td class="text-right tdb" ><?= number_format($fuel_cost,2);  ?></td>
    <td class="text-right tdb" ><?= number_format($blank_charge,2); ?></td>
    <td class="text-right tdb" ><?= number_format($allowance,2); ?></td>
    <td class="text-right tdb" ><?= number_format($kog_expense,2); ?></td>
    <td class="text-right tdb" ><?= number_format($allowance_oth,2); ?></td>
    <td class="text-right tdb" ><?= number_format($job_ended_expressway,2); ?></td>
    <td class="text-right tdb" ><?= number_format($job_ended_recap,2); ?></td>
    <td class="text-right tdb" ><?= number_format($total_amount_allowance,2); ?></td>
    <td class="text-right tdb" ><?= number_format($total_amount_receive,2); ?></td>
    <td class="text-right tdb" ><?= number_format($withholding,2); ?></td>
    <td class="text-right tdb" ><?= number_format($job_order_profit,2); ?></td>
    <td class="text-right tdb" ><?= number_format($fuel_driver_bill,2); ?></td>

  </tr>
<?php
}

if($num == 0){
  ?>
  <br>
  <div class="divo" style="width:100%;overflow-x: auto">
  <table class="table table-bordered table-striped tabf"  style="font-size:13px;width:2000px;">
    <thead>
      <tr>
  			<th class="tdb" style="width:30px;">No.</th>
        <th class="tdb" style="width:90px;">วันที่สั่งจ้าง</th>
        <th class="tdb" style="width:98px;">เลขที่เอกสาร</th>
        <th class="tdb" style="width:100px;">Shipment</th>
        <th class="tdb" style="width:80px;">ทะเบียน</th>
        <th class="tdb" style="width:115px;">เลขวางบิล</th>
  			<th class="tdb" style="width:180px;">ต้นทาง</th>
  			<th class="tdb" >ปลายทาง</th>
  			<th class="tdb" style="width:80px;">จำนวนลิตร</th>
  			<th class="tdb" >ค่าน้ำมัน</th>
        <th class="tdb" >ตีเปล่า</th>
        <th class="tdb" >เบี้ยเลี้ยง</th>
        <th class="tdb" >คอก</th>
        <th class="tdb" style="width:50px;">บัญชี</th>
        <th class="tdb" >อื่นๆ</th>
        <th class="tdb" >ทางด่วน</th>
        <th class="tdb" style="width:80px;">ปะยาง</th>
        <th class="tdb" style="width:85px;">รวมค่าใช้จ่าย</th>
        <th class="tdb" style="width:100px;">รายได้ค่าขนส่ง</th>
        <th class="tdb" style="width:60px;">หัก 1%</th>
        <th class="tdb" style="width:80px;">กำไรขั้นต้น</th>
        <th class="tdb" style="width:80px;">บิลน้ำมัน</th>
  		</tr>
    </thead>
    <tbody>
  <tr>
    <td colspan="22" class="text-center" ><div>ไม่พบข้อมูล</div></td>
  </tr>
</tbody>
</table>
</div>
  <?php
}else{
  ?>
  <tr>
    <td colspan="8" class="text-right tdb" ><b>รวม</b></td>
    <td class="text-right tdb" ><b><?= number_format($fuel_litres,2); ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($fuel_costs,2);  ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($blank_charges,2); ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($allowances,2); ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($kog_expenses,2); ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($allowance_oths,2); ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($job_ended_expressways,2); ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($job_ended_recaps,2); ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($total_amount_allowances,2); ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($total_amount_receives,2); ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($withholdings,2); ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($job_order_profits,2); ?></b></td>
    <td class="text-right tdb" ><b><?= number_format($fuel_driver_bills,2); ?></b></td>
  </tr>
  </tbody>
  </table>
</div>
  <?php
}
//}
?>

</tbody>
</table>
