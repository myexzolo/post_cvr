<?php

include('../../conf/connect.php');
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php

$cust_id = isset($_POST['value'])?$_POST['value']:"";
$prefix  = "";
$cust_code = "";
$cust_name = "";
$tax = "";
$address = "";
$contact = "";
$tel = "";
$fax = "";
$email = "";
$employee_no  = "";
$payment_type = "";
$credit_line  = "";
$out_debt     = "";
$iv_type      = "";
$cust_status  = "";
$last_contact_date = "";
$last_payment_date = "";
$trade_discount = "";
$cr_day = "";
$remark = "";
$monthly_job_order = "";
$monthly_invoice = "";
$monthly_invoice_yet = "";
$monthly_receipt = "";
$monthly_invoice_no_receipt = "";
$company_id = "";

if(!empty($cust_id))
{
    $sql = "SELECT * FROM tb_customer_master WHERE cust_id = '$cust_id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $cust_id      = $row['cust_id'];
    $cust_code    = $row['cust_code'];
    $prefix       = $row['prefix'];
    $cust_name    = $row['cust_name'];
    $tax          = $row['tax'];
    $address      = $row['address'];
    $contact      = $row['contact'];
    $tel          = $row['tel'];
    $fax          = $row['fax'];
    $email        = $row['email'];
    $employee_no  = $row['employee_no'];
    $payment_type = $row['payment_type'];
    $credit_line  = $row['credit_line'];
    $out_debt     = $row['out_debt'];
    $iv_type      = $row['iv_type'];
    $cust_status  = $row['cust_status'];
    $last_contact_date = $row['last_contact_date'];
    $last_payment_date = $row['last_payment_date'];
    $trade_discount     = $row['trade_discount'];
    $cr_day       = $row['cr_day'];
    $remark       = $row['remark'];
    $monthly_job_order    = $row['monthly_job_order'];
    $monthly_invoice      = $row['monthly_invoice'];
    $monthly_invoice_yet  = $row['monthly_invoice_yet'];
    $monthly_receipt      = $row['monthly_receipt'];
    $monthly_invoice_no_receipt = $row['monthly_invoice_no_receipt'];
    $company_id           = $row['company_id'];
}
?>
<div class="row">
  <div class="col-md-2">
    <div class="form-group">
      <label>รหัสลูกค้า</label>
      <input value="<?= $cust_code ?>" name="cust_code" type="text" class="form-control" placeholder="รหัสลูกค้า" required>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>คำนำหน้า</label>
      <select name="prefix"  class="form-control select2" style="width: 100%;" >
        <option value="" ></option>
        <option value="บริษัท" <?= ($prefix == 'บริษัท' ? 'selected="selected"':'') ?>>บริษัท</option>
        <option value="หจก." <?= ($prefix == 'หจก.' ? 'selected="selected"':'') ?>>หจก.</option>
        <option value="ร้าน" <?= ($prefix == 'ร้าน' ? 'selected="selected"':'') ?>>ร้าน</option>
        <option value="นาย" <?= ($prefix == 'นาย' ? 'selected="selected"':'') ?>>นาย</option>
        <option value="นาง" <?= ($prefix == 'นาง' ? 'selected="selected"':'') ?>>นาง</option>
        <option value="น.ส." <?= ($prefix == 'น.ส.' ? 'selected="selected"':'') ?>>น.ส.</option>
      </select>
    </div>
  </div>
  <div class="col-md-5">
    <div class="form-group">
      <label>ชื่อลูกค้า</label>
      <input value="<?= $cust_name ?>" name="cust_name" type="text" class="form-control" placeholder="ชื่อลูกค้า" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เลขที่ประจำตัวผู้เสียภาษี</label>
      <input value="<?= $tax ?>" name="tax" type="text" class="form-control" placeholder="tax" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ที่อยู่</label>
      <textarea class="form-control" name="address" id="Address" rows="2" placeholder="Address ..." required><?= $address ?></textarea>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>หมายเหตุ</label>
      <textarea class="form-control" name="remark" id="Remark" rows="2" placeholder="Remark ..." ><?= $remark ?></textarea>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ผู้ติดต่อ</label>
      <input value="<?= $contact ?>" name="contact" type="text" class="form-control" placeholder="" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>โทรศัพท์</label>
      <input value="<?= $tel ?>" name="tel" type="text" class="form-control" placeholder="Phone" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Fax</label>
      <input value="<?= $fax ?>" name="fax" type="text" class="form-control" placeholder="Fax" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Email</label>
      <input value="<?= $email ?>" name="email" type="email" class="form-control" placeholder="Email" >
    </div>
  </div>
</div>

<input  type="hidden" value="<?= $cust_id ?>" name="cust_id" type="text" >
