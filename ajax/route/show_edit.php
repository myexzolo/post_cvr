<?php

include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
.txtNum {
    text-align: right;
    font-size:18px;
}
</style>
<table class="table table-bordered table-striped table-hover fontChatThai" style="font-size:18px;">
  <thead>
    <tr class="text-center">
      <th style="width:40px" class="text-center">No.</th>
      <th class="text-center" style="width:90px">รหัสเส้นทาง </th>
      <th class="text-center" >ต้นทาง</th>
      <th class="text-center" >ปลายทาง</th>
      <th class="text-center" style="width:100px">ระยะทาง</th>
      <th class="text-center" style="width:100px">ตันละ</th>
      <th class="text-center" style="width:100px">เที่ยวละ</th>
      <th class="text-center" style="width:100px">เบี้ยเลี้ยง</th>
      <th class="text-center" style="width:100px">อื่นๆ</th>
      <th class="text-center" style="width:100px">คอก</th>
      <th class="text-center" style="width:100px">คชจ.บัญชี</th>
      <th class="text-center" style="width:100px">รถร่วม ตันละ</th>
      <th class="text-center" style="width:110px">รถร่วม เที่ยวละ</th>
    </tr>
  </thead>
  <tbody>
<?php
  $con = "";
  $source       = isset($_POST['source'])?$_POST['source']:"";
  $destination  = isset($_POST['destination'])?$_POST['destination']:"";

  if($source != ""){
    $con .= " and source LIKE '%".$source."%' ";
  }

  if($destination != ""){
    $con .= " and destination LIKE '%".$destination."%' ";
  }
  $num = 0;
  if($con != ""){

  $sql = "SELECT * FROM  tb_route_price where id > 0 $con order by route_id ,source , destination";

  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

 //echo $sql;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);

    $id                 = $row['id'];
    $route_id           = $row['route_id'];
    $source             = $row['source'];
    $destination        = $row['destination'];
    $distance           = intval(chkNum($row['distance']));
    $one_trip_ton       = intval(chkNum($row['one_trip_ton']));
    $price_per_trip     = intval(chkNum($row['price_per_trip']));
    $allowance          = intval(chkNum($row['allowance']));
    $allowance_oth      = intval(chkNum($row['allowance_oth']));
    $kog_expense        = intval(chkNum($row['kog_expense']));
    $acc_expense        = intval(chkNum($row['acc_expense']));
    $ext_one_trip_ton   = intval(chkNum($row['ext_one_trip_ton']));
    $ext_price_per_trip = intval(chkNum($row['ext_price_per_trip']));

?>
    <tr class="text-center" >
      <td style="font-size:18px;"><?= $i ?></td>
      <td class="text-center">
        <input value="<?= $id; ?>" name="id[]" type="hidden">
        <input value="<?= $route_id; ?>" style="font-size:18px;" name="route_id[]" autocomplete="off" type="text" class="form-control inputNum" placeholder="" required>
      </td>
      <td class="text-center">
        <input value="<?= $source; ?>" style="font-size:18px;" name="source[]" type="text" class="form-control" autocomplete="off" placeholder="" required>
      </td>
      <td class="text-center" >
        <input value="<?= $destination; ?>" style="font-size:18px;" name="destination[]" type="text" class="form-control" autocomplete="off" placeholder="" required>
      </td>
      <td class="text-center">
        <input value="<?= $distance; ?>" name="distance[]" type="text"  onkeypress="return chkNumber(this)"class="form-control txtNum" autocomplete="off" placeholder="" >
      </td>
      </td>
      <td class="text-center">
        <input value="<?= $one_trip_ton; ?>" name="one_trip_ton[]" type="text" onkeypress="return chkNumber(this)" class="form-control txtNum" autocomplete="off" placeholder="" >
      </td>
      <td class="text-center">
        <input value="<?= $price_per_trip; ?>" name="price_per_trip[]" type="text" onkeypress="return chkNumber(this)" class="form-control txtNum" autocomplete="off" placeholder="" >
      </td>
      <td class="text-center">
        <input value="<?= $allowance; ?>" name="allowance[]" type="text" onkeypress="return chkNumber(this)" class="form-control txtNum" autocomplete="off" placeholder="" >
      </td>
      <td class="text-center">
        <input value="<?= $allowance_oth; ?>" name="allowance_oth[]" type="text" onkeypress="return chkNumber(this)" class="form-control txtNum" autocomplete="off" placeholder="" >
      </td>
      <td class="text-center">
        <input value="<?= $kog_expense; ?>" name="kog_expense[]" type="text" onkeypress="return chkNumber(this)" class="form-control txtNum" autocomplete="off" placeholder="" >
      </td>
      <td class="text-center">
        <input value="<?= $acc_expense; ?>" name="acc_expense[]" type="text" onkeypress="return chkNumber(this)" class="form-control txtNum" autocomplete="off" placeholder="" >
      </td>
      <td class="text-center">
        <input value="<?= $ext_one_trip_ton; ?>" name="ext_one_trip_ton[]" type="text" onkeypress="return chkNumber(this)" class="form-control txtNum" autocomplete="off" placeholder="" >
      </td>
      <td class="text-center">
        <input value="<?= $ext_price_per_trip; ?>" name="ext_price_per_trip[]" type="text" onkeypress="return chkNumber(this)" class="form-control txtNum" autocomplete="off"  placeholder="" >
      </td>
    </tr>
<?php
}
}?>
</tbody>
</table>
<?php
if($num > 0){
?>
  <div class="modal-footer">
    <button type="reset" class="btn btn-default  btn-flat" style="width:100px">ยกเลิก</button>
    <button type="submit" class="btn btn-primary btn-flat" style="width:100px">บันทึก</button>
  </div>
<?php
}
?>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : false,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : true
    })
  })

</script>
