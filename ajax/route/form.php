<?php

include('../../conf/connect.php');
include('../../inc/utils.php');
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$typeCompany =  $_SESSION['typeCompany'];
?>

<?php

$id                  = isset($_POST['value'])?$_POST['value']:"";
$route_id            = "";
$cust_id             = "";
$source              = "";
$destination         = "";
$distance            = "";
$one_trip_ton        = "";
$round_trip_ton      = "";
$price_per_trip      = "";
$allowance           = "";
$allowance_oth       = "";
$kog_expense         = "";
$acc_expense         = "";
$ext_one_trip_ton    = "";
$ext_round_trip_ton  = "";
$ext_price_per_trip  = "";
$contract_no         = "";
$price_contract      = "";
$fuel_litre_total    = "";
$contract_detail     = "";

if(!empty($id))
{
    $sql = "SELECT * FROM tb_route_price WHERE id = '$id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $id                  = $row['id'];
    $route_id            = $row['route_id'];
    $cust_id             = $row['cust_id'];
    $source              = $row['source'];
    $destination         = $row['destination'];
    $distance            = $row['distance'];
    $one_trip_ton        = $row['one_trip_ton'];
    //$round_trip_ton      = $row['round_trip_ton'];
    $price_per_trip      = $row['price_per_trip'];
    $allowance           = $row['allowance'];
    $allowance_oth       = $row['allowance_oth'];
    $kog_expense         = $row['kog_expense'];
    $acc_expense         = $row['acc_expense'];
    $ext_one_trip_ton    = $row['ext_one_trip_ton'];
    //$ext_round_trip_ton  = $row['ext_round_trip_ton'];
    $ext_price_per_trip   = $row['ext_price_per_trip'];
    $contract_no          = isset($row['contract_no'])?$row['contract_no']:"";
    $price_contract       = isset($row['price_contract'])?$row['price_contract']:"";
    $fuel_litre_total     = isset($row['fuel_litre_total'])?$row['fuel_litre_total']:"";

    $contract_detail      = isset($row['contract_detail'])?$row['contract_detail']:"";
}
$optionCustomer         = getoptionCustomer($cust_id);

?>
<div class="row">
  <div class="col-md-2">
    <div class="form-group">
      <label>รหัสเส้นทาง</label>
      <input value="<?= $route_id ?>" name="route_id" type="text" class="form-control" placeholder="รหัสเส้นทาง" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>ชื่อลูกค้า</label>
      <select name="cust_id"  id="cust_id" class="form-control select2" style="width: 100%;" >
        <option value="" ></option>
        <?= $optionCustomer ?>
      </select>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>ต้นทาง</label>
      <input value="<?= $source ?>" name="source" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>ปลายทาง</label>
      <input value="<?= $destination ?>" name="destination" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>ระยะทาง</label>
      <input value="<?= $distance ?>" name="distance" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>ตันละ</label>
      <input value="<?= $one_trip_ton ?>" name="one_trip_ton" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>เที่ยวละ</label>
      <input value="<?= $price_per_trip ?>" name="price_per_trip" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>เบี้ยเลี้ยง</label>
      <input value="<?= $allowance ?>" name="allowance" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>อื่นๆ</label>
      <input value="<?= $allowance_oth ?>" name="allowance_oth" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>คอก</label>
      <input value="<?= $kog_expense ?>" name="kog_expense" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>คชจ.บัญชี</label>
      <input value="<?= $acc_expense ?>" name="acc_expense" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>รถร่วม ตันละ</label>
      <input value="<?= $ext_one_trip_ton ?>" name="ext_one_trip_ton" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>รถร่วม เที่ยวละ</label>
      <input value="<?= $ext_price_per_trip ?>" name="ext_price_per_trip" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <?php
  if($typeCompany == 2){
  ?>

  <div class="col-md-3">
    <div class="form-group">
      <label>ยอดค่าจ้าง/เดือน</label>
      <input value="<?= $price_contract ?>" name="price_contract" type="text" class="form-control" placeholder="" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>จำนวนลิตร/เที่ยว</label>
      <input value="<?= $fuel_litre_total ?>" name="fuel_litre_total" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เลขที่สัญญา</label>
      <input value="<?= $contract_no ?>" name="contract_no" type="text" class="form-control" placeholder="" >
    </div>
  </div>
  <div class="col-md-9">
    <div class="form-group">
      <label>รายละเอียดสัญญา</label>
      <input value="<?= $contract_detail ?>" name="contract_detail" type="text" class="form-control" placeholder="" >
    </div>
  </div>
<?php } ?>
</div>

<input  type="hidden" value="<?= $id ?>" name="id" type="text" >
