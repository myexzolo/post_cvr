<?php

include('../../conf/connect.php');
session_start();

$id                  = $_POST['id'];
$route_id            = $_POST['route_id'];
//$cust_id             = $_POST['cust_id'];
$source              = $_POST['source'];
$destination         = $_POST['destination'];
$distance            = $_POST['distance'];
$one_trip_ton        = $_POST['one_trip_ton'];
//$round_trip_ton      = $_POST['round_trip_ton'];
$round_trip_ton      = 0;
$price_per_trip      = $_POST['price_per_trip'];
$allowance           = $_POST['allowance'];
$allowance_oth       = $_POST['allowance_oth'];
$kog_expense         = $_POST['kog_expense'];
$acc_expense         = $_POST['acc_expense'];
$ext_one_trip_ton    = $_POST['ext_one_trip_ton'];
//$ext_round_trip_ton  = $_POST['ext_round_trip_ton'];
$ext_round_trip_ton  = 0;
$ext_price_per_trip  = $_POST['ext_price_per_trip'];

$flag = true;
if(isset($id) && count($id) > 0){
  for($x= 0; $x < count($id); $x++){
    $ID                 = $id[$x];
    $routeId            = $route_id[$x];
    //$custId             = $cust_id[$x];
    $sourceT            = isset($source[$x])?$source[$x]:"0";
    $destinationT       = isset($destination[$x])?$destination[$x]:"0";
    $distanceT          = isset($distance[$x])?$distance[$x]:"0";
    $oneTripTon         = isset($one_trip_ton[$x])?$one_trip_ton[$x]:"0";
    //$round_trip_ton      = $round_trip_ton[$x];
    $pricePerTrip       = isset($price_per_trip[$x])?$price_per_trip[$x]:"0";
    $allowanceT         = isset($allowance[$x])?$allowance[$x]:"0";
    $allowanceOth       = isset($allowance_oth[$x])?$allowance_oth[$x]:"0";
    $kogExpense         = isset($kog_expense[$x])?$kog_expense[$x]:"0";
    $accExpense         = isset($acc_expense[$x])?$acc_expense[$x]:"0";
    $extOneTripTon      = isset($ext_one_trip_ton[$x])?$ext_one_trip_ton[$x]:"0";
    //$ext_round_trip_ton  = $ext_round_trip_ton[$x];
    $extPricePerTrip    = isset($ext_price_per_trip[$x])?$ext_price_per_trip[$x]:"0";

    $sql = "UPDATE tb_route_price SET
            route_id            = '$routeId',
            source              = '$sourceT',
            destination         = '$destinationT',
            distance            = '$distanceT',
            one_trip_ton        = '$oneTripTon',
            round_trip_ton      = '$round_trip_ton',
            price_per_trip      = '$pricePerTrip',
            allowance           = '$allowanceT',
            allowance_oth       = '$allowanceOth',
            kog_expense         = '$kogExpense',
            acc_expense         = '$accExpense',
            ext_one_trip_ton    = '$extOneTripTon',
            ext_round_trip_ton  = '$ext_round_trip_ton',
            ext_price_per_trip  = '$extPricePerTrip'
            WHERE id  = '$ID'";

    $result = mysqli_query($conn,$sql);
    if (!$result) {
        // break;
        // $flag = false;
    }
  }
}

if($flag){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail :')));
}
?>
