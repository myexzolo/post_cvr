<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$custId         = isset($_POST['custId'])?$_POST['custId']:"";
$docReturnCode  = isset($_POST['docReturnCode'])?$_POST['docReturnCode']:"";

$numRow = 0;
$con = "";



if($docReturnCode == ""){
  $con .= " and doc_return_code is null ";
}else{
  $con .= " and (doc_return_code = '$docReturnCode' or doc_return_code is null )";
}

if($custId != "")
{
  $con .= " and cust_id =". $custId;
}else{
  $con .= " and cust_id is null ";
}


$dateNow = date('Y-m-d');

?>
<table style="width:100%">
  <td style="width:100px;padding-bottom:5px" align="left">
    <input type="checkbox" id="checkApp" onclick="checkAll(this)"><label for="checkApp">&nbsp;เลือกทั้งหมด</label>
    <input type="hidden" name="docReturnCode" value="<?= $docReturnCode?>">
    <input type="hidden" name="custId" value="<?= $custId?>">
  <td>
</table>
<table class="table table-bordered table-striped " id="tableDisplay">
  <thead>
    <tr>
			<th style="width:50px;">ลำดับ</th>
      <th style="width:30px;"></th>
			<th>เลขที่ใบสั่ง</th>
			<th>วันที่</th>
			<th>ShipMent</th>
			<th>เลขที่เอกสาร (DP)</th>
      <th>เลขที่ใบส่งของ</th>
      <th>ต้นทาง</th>
      <th>ปลายทาง</th>
      <th>ทะเบียนรถ</th>
      <th>จำนวน(ตัน/เที่ยว)</th>
		</tr>
  </thead>
  <tbody>
<?php
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id  and jo.shipment is not null and jo.shipment <> ''
  order by jo.shipment + 0,jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalTransfer = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);

    $checked = "";

    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];

    $period_code              = $row['period_code'];
    $doc_return_code          = $row['doc_return_code'];



    $transfer = ($fuel_cost + $allowance + $kog_expense + $blank_charge + $allowance_oth);

    if($price_type_id == 1){
      $amount =  $weights;
      $price  =  $one_trip_ton;
      $total  =  ($weights * $one_trip_ton);
    }else{
      $amount = 1;
      $price  = $price_per_trip;
      $total  = $price_per_trip;
    }

    if($docReturnCode == $doc_return_code && $doc_return_code!=""){
      $checked = "checked";
    }

?>
  <tr>
    <td class="text-center"><?= $i ?></td>
    <td class="text-center" style="width:50px;"><input type="checkbox" <?= $checked ?> onchange="chkList(this);cal();" class="checkJobId" name="idJob[]" value="<?= $id ?>"></td>
    <td class="text-center" ><?= $job_order_no ?></td>
    <td class="text-center" ><?= $job_order_date ?></td>
    <td class="text-left" ><?= $shipment ?></td>
    <td class="text-left" ><?= $cust_dp ?></td>
    <td class="text-left" ><?= $delivery_doc ?></td>
    <td class="text-left" ><?= $source ?></td>
    <td class="text-left" ><?= $destination ?></td>
    <td class="text-center" ><?= $license_plate ?></td>
    <td class="text-right" ><?= number_format($amount,3); ?></td>
  </tr>
<?php } ?>
</tbody>
</table>
