<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$startDate    = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate      = isset($_POST['endDate'])?$_POST['endDate']:"";
$numRow = 0;
$con = "";

if($startDate != "" and  $endDate != "")
{
  $con .= " and doc_return_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
}

?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;">ลำดับ</th>
      <th style="width:150px;">เลขทีใบจ่ายสินค้า</th>
      <th class="text-center" >ชื่อลูกค้า</td>
      <th style="width:150px;" class="text-center" >วันออกใบสั่งจ่ายสินค้า</th>
      <th style="width:220px;"></th>
    </tr>
  </thead>
  <tbody>
<?php

  $sql = "SELECT * FROM tb_doc_return d, tb_customer_master c
  where d.cust_id = c.cust_id $con order by doc_return_code DESC";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num    = mysqli_num_rows($query);

  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $doc_return_id      = $row['doc_return_id'];
    $doc_return_code    = $row['doc_return_code'];
    $doc_return_date    = formatDate($row['doc_return_date'],'d/m/Y');
    $cust_name          = $row['cust_name'];
    $cust_id            = $row['cust_id'];

?>
  <tr class="text-center">
    <td class="text-center"><?= $i ?></td>
    <td class="text-center"><?= $doc_return_code ?></td>
    <td class="text-left"><?= $cust_name ?></td>
    <td class="text-center" ><?= $doc_return_date ?></td>
    <td>
      <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="edit('<?= $doc_return_code; ?>','<?= $cust_id ?>')" ><i class="fa fa-edit"></i> แก้ไข</button>
      <button type="button" class="btn btn-info btn-sm btn-flat" onclick="printDoc('<?= $doc_return_code; ?>')"><i class="fa fa-print"></i> พิมพ์</button>
    </td>
  </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [50, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
