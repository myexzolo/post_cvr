<style type="text/css">
     .right { text-align: right; }
     .tf { background-color: #E1E0E0; }
</style>
<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate      = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate        = isset($_POST['endDate'])?$_POST['endDate']:"";
$custId         = isset($_POST['custId'])?$_POST['custId']:"";
$receiptCode    = isset($_POST['receiptCode'])?$_POST['receiptCode']:"";
$invoiceCode    = isset($_POST['invoiceCode'])?$_POST['invoiceCode']:"";



$invoice_code = "";
$invoice_date = "";
$credit = "";
$maturity_date = "";
$provision = "";
$delivery_cost = "";
$discount = "";
$ot = "";
$total_cost = "";
$cust_id = "";
$receipt_code = "";

$numRow = 0;
$con = "";

if($custId != "")
{
  $con .= " and i.cust_id ='". $custId ."'";
}
if($receiptCode != "")
{
  $con .= " and i.receipt_code LIKE '". $receiptCode ."%'";
}
if($invoiceCode != "")
{
  $con .= " and i.invoice_code LIKE' ". $invoiceCode ."%'";
}

?>
<table style="width:100%">
  <tr>
    <td id="hd0"></td>
  </tr>
  <tr>
    <td id="hd1" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>รายงานตัดรับชำระหนี้</b>
    </td>
  </tr>
  <tr>
    <td id="hd2" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>
        ประจำวันที่ <?= formatDateTh($startDate) ?> ถึงวันที่ <?= formatDateTh($endDate) ?>
      </b>
    </td>
  </tr>
</table>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr>
      <th colspan="13" id="trh" style="border:0px solid;visibility: hidden;"></th>
		</tr>
    <tr>
      <th class="tdb" style="width:30px;">ลำดับ</th>
			<th class="tdb" >เลขที่ใบเสร็จ</th>
      <th class="tdb" >วันที่ออกใบเสร็จ</th>
      <th class="tdb" >เลขที่ใบวางบิล</th>
      <th class="tdb" >รายชื่อลูกค้า</th>
			<th class="tdb" >ยอดเงิน</th>
      <th class="tdb" >หัก 1%</th>
			<th class="tdb" >ยอดรับ</th>
      <th class="tdb" >ค่าธรรมเนียม</th>
      <th class="tdb" >ยอดเงินโอน</th>
      <th class="tdb" >ธนาคาร</th>
      <th class="tdb" >เลขที่บัญชี</th>
		</tr>
  </thead>
  <tbody>
<?php
$con .= " and r.receipt_date between '". $startDate ."' and '". $endDate ."'";
//$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
$sql = "SELECT *
FROM tb_invoice i, tb_customer_master c, tb_receipt r
where i.cust_id = c.cust_id $con and i.receipt_code = r.receipt_code
order by r.account,c.cust_id,r.receipt_date,r.receipt_code";
//echo $sql;
$query  = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);

$custIdTmp = "";
$numInvoiceCodes  = 0;
$total_costs = 0;
$total_rcosts = 0;
$owe = 0;
$owes = 0;

$numInvoiceCodess  = 0;
$total_costss = 0;
$total_rcostss = 0;
$taxs = 0;
$transferss = 0;

$tts = 0;
$ttsum = 0;
$ttsums = 0;
$fees = 0;

$invoiceNum = 0;
$invoiceTmp = 0;

$numDis = 0;
$receipt_code_tmp = "";
$cust_id_tmp = "";
$transferst = 0;
if($num > 0){
for ($i=1; $i <= $num ; $i++)
{
  $row = mysqli_fetch_assoc($query);

  $invoice_code   = $row['invoice_code'];
  $invoice_date   =  date('d/m/Y', strtotime($row['invoice_date']));
  $credit         = $row['credit'];
  $maturity_date  = $row['maturity_date'];
  $provision      = $row['provision'];
  $delivery_cost  = $row['delivery_cost'];
  $discount       = $row['discount'];
  $ot             = $row['ot'];
  $total_cost     = $row['total_cost'];
  $cust_id        = $row['cust_id'];
  $receipt_code   = $row['receipt_code'];
  $prefix         = $row['prefix'];
	$cust_name      = $row['cust_name'];
  $receipt_code   = $row['receipt_code'];
  $receipt_date   = date('d/m/Y', strtotime($row['receipt_date']));
  $invoice_codes  = $row['invoice_codes'];
  $transfer       = $row['transfer'];
  $fee            = $row['fee'];
  $bank           = $row['bank'];
  $account        = $row['account'];

  if($invoiceTmp == 0){
    $invoiceArr = explode(",", $invoice_codes);
    $invoiceNum = count($invoiceArr);
  }

  $invoiceTmp++;
  //echo $invoiceTmp.":".$invoiceNum.">>";
  // if(!is_numeric($fee)){
  //   $fee = "0";
  // }

  $numdiss = "";
  //echo $transfer;
  if(!is_numeric($transfer)){
    $transfer = "0";
  }

  $tax = ($total_cost * 0.01);
  $tt  = ($total_cost - $tax);

  $total_costs += $total_cost;
  $taxs += $tax;
  $tts  += $tt;

  $ttsum += $tt;
  $ttsums += $tt;

  if($invoiceTmp == $invoiceNum){
    //$tts = number_format($ttsum,2);
    $transfers = number_format($transfer,2);
    $bankSh = $bank;
    $accountSh = $account;
    $feeSh = $fee;

    if(is_numeric($fee)){
       $fees += $fee;
    }

    $transferss += $transfer;
    $transferst += $transfer;
  }else{
    //echo "<br>==>invoice_code :".$invoice_code." , fee :".$fee." , i :".$i." , num :".$num;
    $transfers = "";
    $bankSh = "";
    $accountSh = "";
    $feeSh = "";
  }
  $fl = true;
  if($i == $num && $receipt_code != $receipt_code_tmp)
  {
    $transfers = number_format($transfer,2);
    $bankSh = $bank;
    $accountSh = $account;
    $feeSh = $fee;

    if(is_numeric($fee)){
       $fees += $fee;
    }

    // $transferss += $transfer;
    // $transferst += $transfer;
    // echo "<br>xx";
  }

  if($receipt_code != $receipt_code_tmp){
    $receipt_code_tmp = $receipt_code;
    $numDis++;
    $numdiss = $numDis;
  }
  //echo $cust_id.",".$cust_id_tmp.$cust_name."  >>>>";
  if($cust_id_tmp != $cust_id && $cust_id_tmp != ""){
    //echo "<br>===========".$cust_id_tmp.">>".$cust_id;
    $cust_id_tmp = $cust_id;
    if($transfers != "" && $fl){
      $transferst -= $transfer;
    }
?>
<tr>
  <td colspan="5" class="text-right tdb tf" ><b>รวมยอดโอน</b></td>
  <td class="text-right tdb tf" ></td>
  <td class="text-right tdb tf" ></td>
  <td class="text-right tdb tf" ></td>
  <td class="text-right tdb tf" ></td>
  <td class="text-right tdb tf" ><b><?= number_format($transferst,2); ?></b></td>
  <td class="text-right tdb tf" ></td>
  <td class="text-right tdb tf" ></td>
</tr>
<?php
  if($transfers != ""){
    $transferst = $transfer;
  }else{
    $transferst = 0;
  }
  //$transferst = 0;
  $numDis  = 1;
  $numdiss = $numDis;
}

if($cust_id_tmp == ""){
  $cust_id_tmp = $cust_id;
  //echo ",tmp:".$cust_id_tmp;
}
?>
<tr>
  <td class="text-center tdb" ><?= $numdiss ?></td>
  <td class="text-center tdb" ><?= $receipt_code ?></td>
  <td class="text-center tdb" ><?= $receipt_date ?></td>
  <td class="text-center tdb" ><?= $invoice_code ?></td>
  <td class="text-center tdb" ><?= $prefix.$cust_name  ?></td>
  <td class="text-right tdb" ><?= number_format($total_cost,2); ?></td>
  <td class="text-right tdb" ><?= number_format($tax,2); ?></td>
  <td class="text-right tdb" ><?= number_format($tt,2); ?></td>
  <td class="text-right tdb" ><?= $feeSh ?></td>
  <td class="text-right tdb" ><?= $transfers; ?></td>
  <td class="text-left tdb"  ><?= $bankSh ?></td>
  <td class="text-right tdb" ><?= $accountSh ?></td>
</tr>

<?php
  if($invoiceTmp == $invoiceNum){
    $ttsum = 0;
    $invoiceTmp = 0;
  }
}
?>
</tbody>
<tfoot>
<tr>
  <td colspan="5" class="text-right tdb tf" ><b>รวมยอดโอน</b></td>
  <td class="text-right tdb tf" ></td>
  <td class="text-right tdb tf" ></td>
  <td class="text-right tdb tf" ></td>
  <td class="text-right tdb tf" ></td>
  <td class="text-right tdb tf" ><b><?= number_format($transferst,2); ?></b></td>
  <td class="text-right tdb tf" ></td>
  <td class="text-right tdb tf" ></td>
</tr>
<tr>
  <td colspan="5" class="text-right tdb" ><b>รวม</b></td>
  <td class="text-right tdb" ><b><?= number_format($total_costs,2); ?></b></td>
  <td class="text-right tdb" ><b><?= number_format($taxs,2); ?></b></td>
  <td class="text-right tdb" ><b><?= number_format($tts,2); ?></b></td>
  <td class="text-right tdb" ><b><?= number_format($fees,2); ?></b></td>
  <td class="text-right tdb" ><b><?= number_format($transferss,2); ?></b></td>
  <td class="text-right tdb" ></td>
  <td class="text-right tdb" ></td>
</tr>
</tfoot>
</table>
<?php
}else{
?>
</tbody>
<tfoot>
  <tr>
    <td colspan="13" class="text-center tdb" >ไม่พบข้อมูล</td>
  </tr>
</tfoot>
</table>
<?php
}
?>
