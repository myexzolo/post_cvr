<style type="text/css">
     .right { text-align: right; }
</style>
<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate      = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate        = isset($_POST['endDate'])?$_POST['endDate']:"";
$custId         = isset($_POST['custId'])?$_POST['custId']:"";


$total_cost = "";
$cust_id = "";
$receipt_code = "";

$numRow = 0;
$con = "";

$numInvoiceCodes = 0;
$total_costs = 0;

if($custId != "")
{
  $con .= " and i.cust_id ='". $custId ."'";
}

?>
<table style="width:100%">
  <tr>
    <td id="hd0" ></td>
  </tr>
  <tr>
    <td id="hd1" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>รายงานสรุปลูกหนี้</b>
    </td>
  </tr>
  <tr>
    <td id="hd2" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>
        ประจำวันที่ <?= formatDateTh($startDate) ?> ถึงวันที่ <?= formatDateTh($endDate) ?>
      </b>
    </td>
  </tr>
</table>
<br>
<table class="table table-bordered table-striped tabf" >
  <thead>
    <tr>
      <th colspan="4" id="trh" style="border:0px solid;visibility: hidden;"></th>
		</tr>
    <tr>
      <th style="width:5%" class="tdb">ลำดับ</th>
			<th class="tdb">รายชื่อลูกค้า</th>
      <th class="tdb">จำนวนเที่ยว</th>
			<th class="tdb">จำนวนเงิน</th>
		</tr>
  </thead>
  <tbody>
<?php
$con .= " and i.invoice_date between '". $startDate ."' and '". $endDate ."'";
//$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
$sql = "SELECT DISTINCT c.cust_name, SUM(total_cost) as total_cost, group_concat(concat('"."\\'"."',i.invoice_code,'"."\\'"."') separator ',') as inv
FROM tb_invoice i, tb_customer_master c
where i.cust_id = c.cust_id and receipt_code is null $con and i.status_del not in ('Y')
group by i.cust_id
order by i.cust_id,i.invoice_code";
//echo $sql;
$query  = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);


if($num > 0){
for ($i=1; $i <= $num ; $i++)
{
  $row = mysqli_fetch_assoc($query);

  $cust_name    = $row['cust_name'];
  $total_cost   = $row['total_cost'];
  $inv          = $row['inv'];


  $sqli = "SELECT COUNT(id) AS num FROM tb_job_order where invoice_code in ($inv)";
  $queryi  = mysqli_query($conn,$sqli);
  $rowi = mysqli_fetch_assoc($queryi);

  $numInvoiceCode = $rowi['num'];

  $numInvoiceCodes += $numInvoiceCode;
  $total_costs    += $total_cost;
?>
  <tr>
    <td class="text-center tdb" ><?= $i ?></td>
    <td class="text-left tdb" ><?= $cust_name ?></td>
    <td class="text-right tdb" ><?= $numInvoiceCode ?></td>
    <td class="text-right tdb" ><?= number_format($total_cost,2); ?></td>
  </tr>
<?php
}
?>
<tr>
  <td colspan="2" class="text-right tdb" ><b>รวม</b></td>
  <td class="text-right tdb" ><b><?= $numInvoiceCodes ?></b></td>
  <td class="text-right tdb" ><b><?= number_format($total_costs,2); ?></b></td>
</tr>
</tbody>
</table>
<?php
}else{
  echo "<div style='padding-top:5px;'>ไม่พบข้อมูล</div>";
}
?>
