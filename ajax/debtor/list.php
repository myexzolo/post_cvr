<style type="text/css">
     .right { text-align: right; }
</style>
<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate      = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate        = isset($_POST['endDate'])?$_POST['endDate']:"";
$custId         = isset($_POST['custId'])?$_POST['custId']:"";
$statusInv      = isset($_POST['statusInv'])?$_POST['statusInv']:"";

$invoice_code = "";
$invoice_date = "";
$credit = "";
$maturity_date = "";
$provision = "";
$delivery_cost = "";
$discount = "";
$ot = "";
$total_cost = "";
$cust_id = "";
$receipt_code = "";

$numRow = 0;
$con = "";

if($custId != "")
{
  $con .= " and i.cust_id ='". $custId ."'";
}
if($statusInv != "")
{
  if($statusInv == "1"){
    $con .= " and i.receipt_code is null ";
  }else if($statusInv == "2"){
    $con .= " and i.receipt_code is not null ";
  }
}
?>
<table style="width:100%">
  <tr>
    <td id="hd0" ></td>
  </tr>
  <tr>
    <td id="hd1" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>รายงานสรุปวางบิล/ยอดค้างชำระ</b>
    </td>
  </tr>
  <tr>
    <td id="hd2" style="font-size:16px;padding:5px;width:100%" align="center">
      <b>
        ประจำวันที่ <?= formatDateTh($startDate) ?> ถึงวันที่ <?= formatDateTh($endDate) ?>
      </b>
    </td>
  </tr>
</table>
<?php
$con .= " and i.invoice_date between '". $startDate ."' and '". $endDate ."'";
//$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
$sql = "SELECT *
FROM tb_invoice i, tb_customer_master c
where i.cust_id = c.cust_id $con and i.status_del not in ('Y') order by i.cust_id,i.invoice_code";
//echo $sql;
$query  = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);

$custIdTmp = "";
$numInvoiceCodes  = 0;
$total_costs = 0;
$total_rcosts = 0;
$owe = 0;
$owes = 0;

$numInvoiceCodess  = 0;
$total_costss = 0;
$total_rcostss = 0;
$owess = 0;

if($num > 0){
for ($i=1; $i <= $num ; $i++)
{
  $row = mysqli_fetch_assoc($query);

  $invoice_code   = $row['invoice_code'];
  $invoice_date   = $row['invoice_date'];
  $credit         = $row['credit'];
  $maturity_date  = $row['maturity_date'];
  $provision      = $row['provision'];
  $delivery_cost  = $row['delivery_cost'];
  $discount       = $row['discount'];
  $ot             = $row['ot'];
  $total_cost     = $row['total_cost'];
  $cust_id        = $row['cust_id'];
  $receipt_code   = $row['receipt_code'];
  $prefix         = $row['prefix'];
	$cust_name      = $row['cust_name'];

  $sqli = "SELECT COUNT(id) AS num FROM tb_job_order where invoice_code = '$invoice_code'";
  $queryi  = mysqli_query($conn,$sqli);
  $rowi = mysqli_fetch_assoc($queryi);

  $numInvoiceCode = $rowi['num'];

  if ($numInvoiceCode == 0){ // skip even members
        continue;
  }

  if($custIdTmp != $cust_id){

    if($custIdTmp != ""){
  ?>
      <tr>
        <td colspan="2" class="text-right tdb" ><b>รวม</b></td>
        <td class="text-right tdb" ><b><?= $numInvoiceCodes ?></b></td>
        <td class="text-right tdb" ><b><?= number_format($total_costs,2); ?></b></td>
        <td class="text-right tdb" ><b><?= "" ?></b></td>
        <td class="text-right tdb" ><b><?= number_format($total_rcosts,2); ?></b></td>
        <td class="text-right tdb" ><b><?= number_format($owes,2); ?></b></td>
      </tr>
      </tbody>
      </table>
      <div class="break"></div>
<?php
      $numInvoiceCodes  = 0;
      $total_costs = 0;
      $owes = 0;
      $total_rcosts = 0;
    }
    $custIdTmp = $cust_id;
?>

<div class="divf" align="left" style="font-size:14px;padding-top:5px;">
  <b><?= $prefix.$cust_name?></b>
</div>
<table class="table table-bordered table-striped tabf" >
  <thead>
    <tr>
			<th style="width:13%" class="tdb">เลขที่เอกสาร</th>
      <th style="width:13%" class="tdb">วันที่เอกสาร</th>
      <th style="width:13%" class="tdb">จำนวนเที่ยว</th>
      <th style="width:13%" class="tdb">รายได้ค่าขนส่ง</th>
			<th style="width:13%" class="tdb">เลขที่ใบเสร็จ</th>
      <th style="width:13%" class="tdb">จำนวนเงิน</th>
			<th style="" class="tdb">ยอดค้างชำระ</th>
		</tr>
  </thead>
  <tbody>
<?php
  }
  $numInvoiceCodes += $numInvoiceCode;
  $total_costs += $total_cost;

  if($receipt_code != ""){
    $owe = 0;
    $total_rcost = $total_cost;
  }else{
    $owe = $total_cost;
    $total_rcost = 0;
  }
  $owes += $owe;
  $total_rcosts += $total_rcost;

  $numInvoiceCodess  += $numInvoiceCode;
  $total_costss += $total_cost;
  $owess += $owe;
  $total_rcostss += $total_cost;
?>
  <tr>
    <td class="text-center tdb" ><?= $invoice_code ?></td>
    <td class="text-center tdb" ><?= $invoice_date ?></td>
    <td class="text-right tdb" ><?= $numInvoiceCode ?></td>
    <td class="text-right tdb" ><?= number_format($total_cost,2); ?></td>
    <td class="text-center tdb" ><?= $receipt_code  ?></td>
    <td class="text-right tdb" ><?= number_format($total_rcost,2); ?></td>
    <td class="text-right tdb" ><?= number_format($owe,2); ?></td>
  </tr>
<?php
}
$total_rcostss = ($total_costss - $owess);
?>
<tr>
  <td colspan="2" class="text-right tdb" ><b>รวม</b></td>
  <td class="text-right tdb" ><b><?= $numInvoiceCodes ?></b></td>
  <td class="text-right tdb" ><b><?= number_format($total_costs,2); ?></b></td>
  <td class="text-right tdb" ><b><?= "" ?></b></td>
  <td class="text-right tdb" ><b><?= number_format($total_rcosts,2); ?></b></td>
  <td class="text-right tdb" ><b><?= number_format($owes,2); ?></b></td>
</tr>
</tbody>
</table>

<table class="table table-bordered table-striped tabf">
<tr>
  <td style="width:26%" colspan="2" class="text-right tdb" ><b>รวมสุทธิ</b></td>
  <td style="width:13%" class="text-right tdb" ><b><?= $numInvoiceCodess ?></b></td>
  <td style="width:13%" class="text-right tdb" ><b><?= number_format($total_costss,2); ?></b></td>
  <td style="width:13%" class="text-right tdb" ><b><?= "" ?></b></td>
  <td style="width:13%" class="text-right tdb" ><b><?= number_format($total_rcostss,2); ?></b></td>
  <td class="text-right tdb" ><b><?= number_format($owess,2); ?></b></td>
</tr>
</table>
<?php
}else{
  echo "<div style='padding-top:5px;'>ไม่พบข้อมูล</div>";
}
?>
