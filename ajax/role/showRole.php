<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td>Num</td>
      <td>Code</td>
      <td>Name</td>
      <td>รายละเอียด</td>
      <td>สถานะ</td>
      <td>Edit/Del</td>
    </tr>
  </thead>
  <tbody>
<?php
  $sql = "SELECT * FROM t_role ORDER BY role_id DESC";
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td><?= $i ?></td>
      <td><?= $row['role_code']; ?></td>
      <td><?= $row['role_name']; ?></td>
      <td><?= $row['role_desc']; ?></td>
      <td><?= ($row['is_active'] == 'Y' ? 'ใช้งาน' : 'ไม่ใช้งาน') ; ?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm" onclick="showForm(<?= $row['role_id']; ?>)">Edit</button>
        <button type="button" class="btn btn-danger btn-sm" onclick="removeRole(<?= $row['role_id']; ?>)">Del</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable();
  })
</script>
