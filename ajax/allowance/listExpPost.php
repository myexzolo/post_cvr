<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate    = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate      = isset($_POST['endDate'])?$_POST['endDate']:"";
$periodCode   = isset($_POST['periodCode'])?$_POST['periodCode']:"";

$numRow = 0;
$con = "";

// if($startDate != "" and  $endDate != "")
// {
//   $con .= " and jo.job_order_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
// }
?>
<div style="height:4mm"></div>
<div align="center" style="font-size:14px;padding:5px;"><b>รายงานโอนเงินเบี้ยเลี้ยง</b></div>
<div align="center" style="font-size:14px;padding:5px;">
  <b>
    งวดที่ <?=$periodCode;?>&nbsp;
    ประจำวันที่ <?= $startDate;?>
  </b>
</div>
<table class="table" id="tableDisplay" style="border: 1px solid #000000;font-size:11px;">
  <thead>
    <tr>
			<th colspan="14" style="height:10px;border-top: 1px solid #fff;border-left: 1px solid #fff;border-right: 1px solid #fff;border-bottom: 1px solid #000000;"></th>
		</tr>
    <tr>
			<th rowspan="2" style="width:45px;vertical-align: middle;border: 1px solid #000000;">ลำดับ</th>
			<th rowspan="2" style="vertical-align: middle;border: 1px solid #000000;">ชื่อพนักงานขับรถ</th>
			<th rowspan="2" style="width:70px;vertical-align:middle;border: 1px solid #000000;">ทะเบียนรถ</th>
      <th rowspan="2" style="width:70px;vertical-align:middle;border: 1px solid #000000;">วันที่</th>
			<th rowspan="2" style="vertical-align:middle;border: 1px solid #000000;">ระยะทาง<br>(กม.)</th>
			<th rowspan="2" style="vertical-align:middle;border: 1px solid #000000;">จำนวน<br>ลิตร</th>
			<th colspan="5" style="border: 1px solid #000000;">รายได้สุทธิ</th>
      <th rowspan="2" style="vertical-align:middle;border: 1px solid #000000;">ยอดเงินโอน</th>
			<th rowspan="2" style="width:100px;vertical-align:middle;border: 1px solid #000000;">เลขที่เอกสาร<br>(FM)</th>
			<th rowspan="2" style="vertical-align:middle;border: 1px solid #000000;">ต้นทาง</th>
			<th rowspan="2" style="vertical-align:middle;border: 1px solid #000000;">ปลายทาง</th>
		</tr>
    <tr>
			<th style="border: 1px solid #000000;">น้ำมัน</th>
			<th style="border: 1px solid #000000;">เบี้ยเลี้ยง</th>
			<th style="border: 1px solid #000000;">คอก</th>
			<th style="border: 1px solid #000000;">ตีเปล่า</th>
			<th style="border: 1px solid #000000;">อื่นๆ</th>
		</tr>
  </thead>
  <tbody>
<?php
  if($periodCode != "")
  {
    $con .= " and jo.period_code ='". $periodCode ."'";
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id and  jo.job_status_id = '2'
  order by jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  $j = 0;
  $totalTransfer = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $employee_id2       = isset($row['employee_id2'])?$row['employee_id2']:"";//รหัสพนักงาน
    $second_emp         = isset($row['second_emp'])?$row['second_emp']:"";//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];

    $period_code              = $row['period_code'];



    $transfer = ($allowance + $kog_expense + $blank_charge + $allowance_oth);
    $totalTransfer +=  $transfer;

    $sqlf = "SELECT * FROM tb_petrol where job_order_no  ='$job_order_no'";
    $queryf  = mysqli_query($conn,$sqlf);
    $numf    = mysqli_num_rows($queryf);
    if($numf > 0){

    for ($x=0; $x < $numf; $x++) {
      $rowf               = mysqli_fetch_assoc($queryf);
      $petrol_station_s   = $rowf['petrol_station'];
      $petrol_code_s      = $rowf['petrol_code'];
      $fuel_cost_s        = $rowf['fuel_cost'];
      $fuel_litre_s       = $rowf['fuel_litre'];
      $fuel_total_s       = $rowf['fuel_total'];
      $fuel_receipt_s     = $rowf['fuel_receipt'];

      if($x == 0)
      {
        $fuel_litre =  number_format($fuel_litre_s,2);
        $fuel_cost  =  number_format($fuel_cost_s,2);
      }else{
        $fuel_litre .=  "<br>".number_format($fuel_litre_s,2);
        $fuel_cost  .=  "<br>".number_format($fuel_cost_s,2);
      }
    }
  }
    $j++;
?>
  <tr>
    <td class="text-center" style="border: 1px solid #000000;"><?= $j ?></td>
    <td class="text-left" style="border: 1px solid #000000;"><?= $Employee_Name ?></td>
    <td class="text-right" style="border: 1px solid #000000;"><?= $license_plate ?></td>
    <td class="text-right" style="border: 1px solid #000000;"><?= $job_order_date ?></td>
    <td class="text-right" style="border: 1px solid #000000;"><?= $distance ?></td>
    <td class="text-right" style="border: 1px solid #000000;"><?= $fuel_litre ?></td>
    <td class="text-right" style="border: 1px solid #000000;"><?= $fuel_cost  ?></td>
    <td class="text-right" style="border: 1px solid #000000;"><?= number_format($allowance,2); ?></td>
    <td class="text-right" style="border: 1px solid #000000;"><?= number_format($kog_expense,2); ?></td>
    <td class="text-right" style="border: 1px solid #000000;"><?= number_format($blank_charge,2); ?></td>
    <td class="text-right" style="border: 1px solid #000000;"><?= number_format($allowance_oth,2); ?></td>
    <td class="text-right" style="border: 1px solid #000000;"><div id="transfer<?=$id?>"><?= number_format($transfer,2); ?></div></td>
    <td class="text-left" style="border: 1px solid #000000;"><?= $cust_dp ?></td>
    <td class="text-left" style="border: 1px solid #000000;"><?= $source ?></td>
    <td class="text-left" style="border: 1px solid #000000;"><?= $destination ?></td>
  </tr>
  <?php
    if($employee_id2 != "" && $employee_id2 != "0")
    {
      //$employee_id2       = $row['employee_id2'];//รหัสพนักงาน
      //$second_emp         = $row['$second_emp'];//รหัสพนักงาน
      $allowance_sec       = ($second_emp + $allowance_oth);
      $totalTransfer      +=  $allowance_sec;
      $j++;
    ?>
    <tr>
      <td class="text-center" style="border: 1px solid #000000;"><?= $j ?></td>
      <td class="text-left" style="border: 1px solid #000000;"><?= getEmpName($employee_id2);?></td>
      <td class="text-right" style="border: 1px solid #000000;"><?= $license_plate ?></td>
      <td class="text-right" style="border: 1px solid #000000;"><?= $job_order_date ?></td>
      <td class="text-right" style="border: 1px solid #000000;"><?= $distance ?></td>
      <td class="text-right" style="border: 1px solid #000000;">0.00</td>
      <td class="text-right" style="border: 1px solid #000000;">0.00</td>
      <td class="text-right" style="border: 1px solid #000000;"><?= number_format($second_emp,2); ?></td>
      <td class="text-right" style="border: 1px solid #000000;">0.00</td>
      <td class="text-right" style="border: 1px solid #000000;">0.00</td>
      <td class="text-right" style="border: 1px solid #000000;"><?= number_format($allowance_oth,2); ?></td>
      <td class="text-right" style="border: 1px solid #000000;"><div id="transfer2<?=$id?>"><?= number_format($allowance_sec,2); ?></div></td>
      <td class="text-left" style="border: 1px solid #000000;"><?= $cust_dp ?></td>
      <td class="text-left" style="border: 1px solid #000000;"><?= $source ?></td>
      <td class="text-left" style="border: 1px solid #000000;"><?= $destination ?></td>
    </tr>
    <?php
    }
  }
  }?>
<tr>
  <td colspan="11" class="text-right" style="border: 1px solid #000000;"><b>รวมเงินที่ต้องโอนทั้งสิ้น</b></td>
  <td class="text-right" style="border: 1px solid #000000;"><b><?= number_format($totalTransfer,2); ?></b></td>
  <td colspan="3" class="text-left" style="border: 1px solid #000000;"><b><?= convert(number_format($totalTransfer,2));?></b></td>
</tr>
</tbody>
</table>
