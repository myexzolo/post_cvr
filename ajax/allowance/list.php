<?php
session_start();
include('../../conf/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate    = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate      = isset($_POST['endDate'])?$_POST['endDate']:"";
$trailer_id   = isset($_POST['trailer_id'])?$_POST['trailer_id']:"";
$cust_id      = isset($_POST['cust_id'])?$_POST['cust_id']:"";
$numRow = 0;
//$con = " and invoice_code is null ";
$con = "";
// if($startDate != "" and  $startDate != "")
// {
//   $con .= " and DATE_FORMAT(pc.Date_Po, '%Y/%m/%d') ='". $startDate ."' ";
// }
$display = "";

if($startDate != "" and  $endDate != "")
{
  $con = " where period_date BETWEEN  '". $startDate ." 00:00:00' and '". $endDate ." 23:59:59'";
}
$typeCompany = $_SESSION['typeCompany'];

?>

<input type="hidden" id="typeCompany" value="<?= $typeCompany ?>">
<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;"></th>
      <th class="text-center" >เลขที่งวด</th>
      <th class="text-center" >วันที่ทำรายการ</th>
      <th class="text-center" >วันที่ออกเบี้ยเลี้ยง</td>
      <th class="text-center" >จำนวนเงินโอน</th>
      <th style="width:150px;">เงินเบี้ยเลี้ยง</th>
      <th style="width:100px;">ใบสำรองจ่าย</th>
    </tr>
  </thead>
  <tbody>
<?php

  $sql = "SELECT * FROM tb_period  $con order by period_code DESC";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $period_code          = $row['period_code'];
    $period_date          = date('d/m/Y', strtotime($row['period_date']));
    $start_date           = date('d/m/Y', strtotime($row['start_date']));
    $end_date             = date('d/m/Y', strtotime($row['end_date']));
    $total_transfer       = $row['total_transfer'];
?>
    <tr class="text-center">
      <td><?= $i ?></td>
      <td align="center"><?= $period_code ?></td>
      <td align="center"><?= $period_date ?></td>
      <td align="center"><?= $start_date ?></td>
      <td align="right"><?= number_format($total_transfer,2); ?></td>
      <td>
        <button type="button" class="btn btn-primary btn-sm btn-flat" onclick="exportExcelV2('<?= $period_code; ?>','<?= $start_date; ?>','<?= $end_date; ?>')"><i class="fa fa-file-excel-o"></i> Excel</button>
        <button type="button" class="btn btn-info btn-sm btn-flat" onclick="expPeriod('<?= $period_code; ?>','<?= $start_date; ?>','<?= $end_date; ?>')"><i class="fa fa-print"></i> พิมพ์</button>
      </td>
      <td>
        <button type="button" class="btn btn-info btn-sm btn-flat" onclick="expPeriodV2('<?= $period_code; ?>','<?= $start_date; ?>','<?= $end_date; ?>')"><i class="fa fa-print"></i> พิมพ์</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [2, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
