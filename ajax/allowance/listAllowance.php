<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate    = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate      = isset($_POST['endDate'])?$_POST['endDate']:"";
$dateApp      = isset($_POST['dateApp'])?$_POST['dateApp']:"";
$trailer_id   = isset($_POST['trailer_id'])?$_POST['trailer_id']:"";
$employeeId   = isset($_POST['employeeId'])?$_POST['employeeId']:"";

$numRow = 0;
$con = "";

if($startDate != "" and  $endDate != "")
{
  $con .= " and jo.job_order_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
}

if($trailer_id != "" )
{
  $con .= " and jo.trailer_id ='". $trailer_id ."' ";
}

if($employeeId != "")
{
  $con .= " and jo.employee_id  ='". $employeeId ."' ";
}

$dateYM       = date('ym',strtotime($dateApp));
//echo $dateApp."".$dateYM;

$sql = "SELECT max(period_code) as period_code FROM tb_period where period_code LIKE '$dateYM%'";
$query  = mysqli_query($conn,$sql);
$row    = mysqli_fetch_assoc($query);
$periodCode  = $row['period_code'];

if(!empty($periodCode)){
  $lastNum = substr($periodCode,strlen($dateYM));
  $lastNum = $lastNum + 1;
  $period_code = $dateYM.sprintf("%03d", $lastNum);
}else{
  $period_code = $dateYM.sprintf("%03d", 1);
}

?>
<div align="center" style="font-size:16px;padding:5px;"><b>รายงานโอนเงินเบี้ยเลี้ยง</b></div>
<div align="center" style="font-size:16px;padding:5px;">
  <b>
    งวดที่ <?=$period_code;?>&nbsp;
    ประจำวันที่ <?=formatDate($dateApp,'d/m/Y');?>
  </b>
</div>
<table style="width:100%">
  <td style="width:100px;padding-bottom:5px" align="left">
    <input type="checkbox" id="checkApp" onclick="checkAll(this)"><label for="checkApp">&nbsp;เลือกทั้งหมด</label>
    <input type="hidden" name="period_code" value="<?= $period_code?>">
    <input type="hidden" name="startDate" value="<?= $dateApp?>" >
    <input type="hidden" name="endDate" value="<?= $endDate?>" >
    <input type="hidden" id="total_transfer" name="total_transfer">

  <td>
</table>
<table class="table table-bordered table-striped " id="tableDisplay">
  <thead>
    <tr>
			<th rowspan="2" style="width:50px;vertical-align: middle;">ลำดับ</th>
      <th rowspan="2" style="width:30px;"></th>
			<th rowspan="2" style="width:11%;vertical-align: middle;">ชื่อพนักงานขับรถ</th>
			<th rowspan="2" style="vertical-align:middle;">ทะเบียนรถ</th>
			<th rowspan="2" style="vertical-align:middle;">ระยะทาง<br>(กม.)</th>
			<th rowspan="2" style="vertical-align:middle;">จำนวน<br>ลิตร</th>
			<th colspan="5">รายได้สุทธิ</th>
      <th rowspan="2" style="vertical-align:middle;">ยอดเงินโอน</th>
			<th rowspan="2" style="width:8%;vertical-align:middle;">เลขที่เอกสาร<br>(DP)</th>
      <th rowspan="2" style="width:50px;vertical-align:middle;">วันที่</th>
			<th rowspan="2" style="vertical-align:middle;">ต้นทาง</th>
			<th rowspan="2" style="vertical-align:middle;">ปลายทาง</th>
		</tr>
    <tr>
			<th>น้ำมัน</th>
			<th>เบี้ยเลี้ยง</th>
			<th>คอก</th>
			<th>ตีเปล่า</th>
			<th>อื่นๆ</th>
		</tr>
  </thead>
  <tbody>
<?php
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id and  jo.job_status_id = '2' and jo.period_code is null $con and jo.affiliation_id = '1'
  order by jo.job_order_date";

  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalTransfer = 0;
  $fuel_costs    = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $employee_id2       = @$row['employee_id2'];//รหัสพนักงาน
    $second_emp         = @$row['second_emp'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];

    $period_code              = $row['period_code'];



    $transfer = ($fuel_cost + $allowance + $kog_expense + $blank_charge + $allowance_oth);
    $totalTransfer +=  $transfer;
    $fuel_costs += $fuel_cost;

?>
  <tr>
    <td class="text-center"><?= $i ?></td>
    <td class="text-center" style="width:50px;"><input type="checkbox" onchange="calAllowance()" class="checkJobId" name="idJob[]" value="<?= $id ?>"></td>
    <td class="text-left" ><?= $Employee_Name ?></td>
    <td class="text-right" ><?= $license_plate ?></td>
    <td class="text-right" ><?= $distance ?></td>
    <td class="text-right" ><?= number_format($fuel_litre,2); ?></td>
    <td class="text-right" ><?= number_format($fuel_cost,2);  ?></td>
    <td class="text-right" ><?= number_format($allowance,2); ?></td>
    <td class="text-right" ><?= number_format($kog_expense,2); ?></td>
    <td class="text-right" ><?= number_format($blank_charge,2); ?></td>
    <td class="text-right" ><?= number_format($allowance_oth,2); ?></td>
    <td class="text-right" ><div id="transfer<?=$id?>"><?= number_format($transfer,2); ?></div></td>
    <td class="text-left" ><?= $cust_dp ?></td>
    <td class="text-center" ><?= $job_order_date ?></td>
    <td class="text-left" ><?= $source ?></td>
    <td class="text-left" ><?= $destination ?></td>
  </tr>

<?php
  if($employee_id2 != "" && $employee_id2 != "0")
  {
    //$employee_id2       = $row['employee_id2'];//รหัสพนักงาน
    //$second_emp         = $row['$second_emp'];//รหัสพนักงาน
    $allowance_sec       = ($second_emp + $allowance_oth);
    $totalTransfer      +=  $allowance_sec;
  ?>
    <tr>
      <td class="text-center"></td>
      <td class="text-center" style="width:50px;"></td>
      <td class="text-left" ><?= getEmpName($employee_id2); ?></td>
      <td class="text-right" ><?= $license_plate ?></td>
      <td class="text-right" ><?= $distance ?></td>
      <td class="text-right" >0.00</td>
      <td class="text-right" >0.00</td>
      <td class="text-right" ><?= number_format($second_emp,2); ?></td>
      <td class="text-right" >0.00</td>
      <td class="text-right" >0.00</td>
      <td class="text-right" ><?= number_format($allowance_oth,2); ?></td>
      <td class="text-right" ><div id="transfer2<?=$id?>"><?= number_format($allowance_sec,2); ?></div></td>
      <td class="text-left" ><?= $cust_dp ?></td>
      <td class="text-center" ><?= $job_order_date ?></td>
      <td class="text-left" ><?= $source ?></td>
      <td class="text-left" ><?= $destination ?></td>
    </tr>
  <?php
  }
} ?>
<tr>
  <td colspan="11" class="text-right" ><b>รวมเงินที่ต้องโอนทั้งสิ้น</b></td>
  <td class="text-right"><b><div id="totalTransfer">0.00</div></b></td>
  <td colspan="5" class="text-left"></td>
</tr>
</tbody>
</table>
