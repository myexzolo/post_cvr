<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
session_start();
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php
//echo $_SESSION['ROLECODE'];
$index = strpos($_SESSION['ROLECODE'],"SADM");
$con = "";
//echo ">>>>".$index;
if($index <= 0 || empty($index) ){
  $con = " and role_code not in ('SADM') ";
}

$sql = "SELECT * FROM t_role WHERE is_active = 'Y' ".$con." ORDER BY role_id";
$queryRole  = mysqli_query($conn,$sql);
$numRole    = mysqli_num_rows($queryRole);

$user_id = "";
$user_login = "";
$user_password = "";
$user_name = "";
$user_img  = "";
$is_active = "";
$option    = "";

$note1     = "";
$note2     = "";

$display    = "block";
$display2   = "block";
$required    = "required";
$required2   = "required";
$type       = "";

if(isset($_POST['typeEdit'])){
  $type = $_POST['typeEdit'];

  if($type == "edit"){
    $display    = "none";
    $required    = "";
  }

  if($type == "resetPw"){
    $display2     = "none";
    $required2    = "";
  }
}


if(!empty($_POST['value'])){
  $sql = "SELECT * FROM t_user WHERE user_id = '{$_POST['value']}'";
  $query = mysqli_query($conn,$sql);
  $row = mysqli_fetch_assoc($query);

  $user_id        = $row['user_id'];
  $user_login     = $row['user_login'];
  $user_password  = $row['user_password'];
  $user_name      = $row['user_name'];
  $is_active      = $row['is_active'];
  $role_list      = $row['role_list'];
  $user_img       = $row['user_img'];
  $note1          = isset($row['note1'])?$row['note1']:"";
  $note2          = isset($row['note2'])?$row['note2']:"";

  if($type == "resetPw"){
    $user_password = "";
  }

  $mapRole = explode(",", $role_list);

  for ($i=1; $i <= $numRole ; $i++) {
    $rowRole   = mysqli_fetch_assoc($queryRole);
    $role_id   = $rowRole['role_id'];
    $role_name = $rowRole['role_name'];

    $checked = ' ';
    if(array_search($role_id,$mapRole) != ""){
      $checked =  "checked";
    }
    $option .= '<div class="form-group col-md-6"><input class="minimal" name="role_list[]" type="checkbox" value="'.$role_id.'" '.$checked.'>'.'  '.$role_name.'</div>';
  }
}else{
  for ($i=1; $i <= $numRole ; $i++) {
    $rowRole   = mysqli_fetch_assoc($queryRole);
    $role_id   = $rowRole['role_id'];
    $role_name = $rowRole['role_name'];

    $option .= '<div class="form-group col-md-6"><input class="minimal" name="role_list[]" type="checkbox" value="'.$role_id.'" >'.'  '.$role_name.'</div>';
  }
}
?>
<div class="row">
  <div class="col-md-3" style="display: <?= $display2 ?>" >
    <div class="form-group">
        <label>User Login</label>
        <input value="<?= $user_login ?>" autocomplete="off" name="user_login" type="text" class="form-control" placeholder="UserLogin" <?= $required2 ?>>
    </div>
  </div>
  <div class="col-md-5" style="display: <?= $display2 ?>" >
    <div class="form-group">
        <label>Name</label>
        <input value="<?= $user_name ?>" name="user_name" type="text" class="form-control" placeholder="Name" <?= $required2 ?>>
    </div>
  </div>
  <div class="col-md-4" style="display: <?= $display2 ?>" >
    <div class="form-group">
        <label>Personnel Id</label>
        <input value="<?= $note1 ?>" name="note1" type="text" class="form-control" placeholder="ID" >
    </div>
  </div>
  <div class="col-md-6" style="display: <?= $display ?>" >
      <div class="form-group">
        <label>Password</label>
        <input value="<?= $user_password ?>" id="user_password" name="user_password" type="password" class="form-control" placeholder="Password" <?= $required ?>>
      </div>
  </div>
  <div class="col-md-6" style="display: <?= $display ?>" >
    <div class="form-group">
      <label>Comfirm Password</label>
      <input value="<?= $user_password ?>" id="user_password_comfirm" name="user_password_comfirm" type="password" class="form-control" placeholder="Password" <?= $required ?>>
    </div>
  </div>
  <div class="col-md-4" style="display: <?= $display2 ?>">
    <div class="form-group">
      <label>Status</label>
      <select name="is_active" class="form-control select2" style="width: 100%;" <?= $required2 ?>>
        <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>
  <div class="col-md-6" style="display: <?= $display2 ?>">
    <div class="form-group">
      <label>Image</label>
      <input name="user_img" type="file" class="form-control" >
    </div>
  </div>
  <div class="col-md-2" style="display: <?= $display2 ?>">
    <div class="form-group">
      <label>
        <img src="<?= $user_img ?>" onerror="this.onerror='';this.src='image/man.png'" style="height: 60px;">
      </label>
    </div>
  </div>
  <div class="col-md-12" style="display: <?= $display2 ?>" >
    <div class="form-group">
      <label>Select Role</label>
        <div class="box-body">
          <?=$option ?>
        </div>
    </div>
  </div>
</div>
<input type="hidden" name="user_id" value="<?= $user_id ?>">
<input type="hidden" name="type" value="<?= $type ?>">
