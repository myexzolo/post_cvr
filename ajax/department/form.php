<?php

include('../../conf/connect.php');
include('../../inc/utils.php');
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php

$department_id      = isset($_POST['value'])?$_POST['value']:"";
$department_code    = "";
$department_name    = "";
$contact_name       = "";
$tax                = "";
$tel                = "";
$address            = "";
$fax                = "";
$email              = "";
$cust_id            = "";

if(!empty($department_id))
{
    $sql = "SELECT * FROM tb_department_master WHERE department_id = '$department_id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $department_id      = $row['department_id'];
    $department_code    = $row['department_code'];
    $department_name    = $row['department_name'];
    $contact_name       = $row['contact_name'];
    $address            = $row['address'];
    $tax                = $row['tax'];
    $tel                = $row['tel'];
    $fax                = $row['fax'];
    $email              = $row['email'];
    $cust_id            = $row['cust_id'];
}
$optionCustomer  = getoptionCustomer($cust_id);
?>
<div class="row">
  <div class="col-md-2">
    <div class="form-group">
      <label>รหัสหน่วยงาน</label>
      <input value="<?= $department_code ?>" name="department_code" type="text" class="form-control" placeholder="รหัสตำแหน่ง" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>ชื่อหน่วยงาน</label>
      <input value="<?= $department_name ?>" name="department_name" type="text" class="form-control" placeholder="ชื่อลูกค้า" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>โทรศัพท์</label>
      <input value="<?= $tel ?>" name="tel" type="text" class="form-control" placeholder="Phone" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เลขที่ประจำตัวผู้เสียภาษี</label>
      <input value="<?= $tax ?>" name="tax" type="text" class="form-control" placeholder="tax" >
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>ที่อยู่</label>
      <textarea class="form-control" name="address" id="Address" rows="2" placeholder="Address ..." ><?= $address ?></textarea>
    </div>
  </div>

  <div class="col-md-2">
    <div class="form-group">
      <label>Fax</label>
      <input value="<?= $fax ?>" name="fax" type="text" class="form-control" placeholder="Fax" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Email</label>
      <input value="<?= $email ?>" name="email" type="email" class="form-control" placeholder="Email" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>ชื่อผู้จัดการ</label>
      <input value="<?= $contact_name ?>" name="contact_name" type="text" class="form-control" placeholder="Contact" >
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>ลูกค้า</label>
      <select name="cust_id"  id="cust_id" class="form-control select2" style="width: 100%;" required>
        <option value="" >เลือกลูกค้า</option>
        <?= $optionCustomer ?>
      </select>
    </div>
  </div>
<input  type="hidden" value="<?= $department_id ?>" name="department_id" type="text" >
