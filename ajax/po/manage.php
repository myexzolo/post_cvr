<?php

include('../../conf/connect.php');
include('../../inc/utils.php');
session_start();

$ID  = isset($_POST['id'])?$_POST['id']:"";


$job_order_no       = chkNull($_POST['job_order_no']);
$delivery_type_id   = chkNull($_POST['delivery_type_id']);//รูปแบบการขนส่ง
$job_order_date     = chkNull($_POST['job_order_date']);//วันที่ออกใบสั่งงาน
$job_delivery_date  = chkNull($_POST['job_delivery_date']);//วันที่กำหนดส่งสินค้า
$cust_id            = chkNull($_POST['cust_id']);//รหัสลูกค้า
$employee_id        = chkNull($_POST['employee_id']);//รหัสพนักงาน
$trailer_id         = chkNull($_POST['trailer_id']);//หัว หมายเลขทะเบียนรถ
$away_regi_no       = chkNull($_POST['away_regi_no']);//หาง
$away_spec_no       = chkNull($_POST['away_spec_no']);//สเปคหาง
$affiliation_id     = chkNull($_POST['affiliation_id']);//รหัส รถ(สังกัด)
$route_id           = chkNull($_POST['route_id']);//รหัสเส้นทาง
$source             = chkNull($_POST['source']);//ต้นทาง
$destination        = chkNull($_POST['destination']);//ปลายทาง
$distance           = chkNull($_POST['distance']);//ระยะทาง
$cust_dp            = chkNull($_POST['cust_dp']);//DP
$delivery_doc       = chkNull($_POST['delivery_doc']);//ใบส่งของ
$department         = chkNull($_POST['department']);//หน่วยงาน
$product_name       = chkNull($_POST['product_name']);//สินค้า
$number_of_trips    = chkNull($_POST['number_of_trips']);//จำนวนเที่ยว
$weights            = chkNull($_POST['weights']);//น้ำหนัก(ตัน)
$shipping_amount    = chkNull($_POST['shipping_amount']);//ราคาค่าขนส่ง
$date_product_in    = chkNull($_POST['date_product_in']);//วันทีรับสินค้า
$time_product_in    = chkNull($_POST['time_product_in']);//เวลารับสินค้า
$date_product_out   = chkNull($_POST['date_product_out']);//วันที่ออกสินค้า
$time_product_out   = chkNull($_POST['time_product_out']);//เวลาออกสินค้า
$date_delivery_in   = chkNull($_POST['date_delivery_in']);//วันทีส่งสินค้า
$time_delivery_in   = chkNull($_POST['time_delivery_in']);//เวลาเข้าส่งสินค้า
$date_delivery_out  = chkNull($_POST['date_delivery_out']);//วันที่ออกส่งสินค้า
$time_delivery_out  = chkNull($_POST['time_delivery_out']);//เวลาออกส่งสินค้า
$fuel_cost          = chkNull($_POST['fuel_cost']);//ค่าน้ำมัน
$fuel_litre         = chkNull($_POST['fuel_litre']);//จำนวนลิตร
$blank_charge       = chkNull($_POST['blank_charge']);//ค่าตีเปล่า
$allowance          = chkNull($_POST['allowance']);//เบี้ยเลี้ยง
$kog_expense        = chkNull($_POST['kog_expense']);//หางคอก
$allowance_oth      = chkNull($_POST['allowance_oth']);//เบี้ยงเลี้ยงอื่นๆ
$remark             = chkNull($_POST['remark']);
$shipment           = chkNull($_POST['shipment']);
$price_type_id      = chkNull($_POST['price_type_id']);//รหัสการคำนวณ
$one_trip_ton       = chkNull($_POST['one_trip_ton']);//ราคาต่อตัน
$ext_one_trip_ton   = chkNull($_POST['ext_one_trip_ton']);//ราคารถร่วมต่อตัน
$price_per_trip     = chkNull($_POST['price_per_trip']);//ราคาต่อเที่ยว
$ext_price_per_trip = chkNull($_POST['ext_price_per_trip']);//ราคารถร่วมต่อเที่ยว
$total_amount_receive = chkNull($_POST['total_amount_receive']);//ราคาค่าขนส่ง
$total_amount_ap_pay  = chkNull($_POST['total_amount_ap_pay']);//ราคาจ่ายรถร่วม
$total_amount_allowance   = chkNull($_POST['total_amount_allowance']);//รวมค่าใช้จ่าย
$job_ended_clearance      = chkNull($_POST['job_ended_clearance']);//ค่าเคลียร์ค่าปรับ
$job_ended_recap          = chkNull($_POST['job_ended_recap']);//ค่าปะยาง
$job_ended_expressway     = chkNull($_POST['job_ended_expressway']);//ค่าทางด่วน
$job_ended_passage_fee    = chkNull($_POST['job_ended_passage_fee']);//ค่าธรรมเนียมผ่านท่าเรือ
$job_ended_repaires       = chkNull($_POST['job_ended_repaires']);//ค่าซ่อม
$job_ended_acc_expense    = chkNull($_POST['job_ended_acc_expense']);//ค่าทำบัญชี
$job_ended_other_expense  = chkNull($_POST['job_ended_other_expense']);//คชจ.อื่นๆ
$fuel_driver_bill         = chkNull($_POST['fuel_driver_bill']);//บิลน้ำมันจากคนขับ
$job_status_id            = chkNull($_POST['job_status_id']);
$job_order_profit         = chkNull($_POST['job_order_profit']);//กำไรเบื้องต้น
$petrol_station           = chkNull($_POST['petrol_station']);//ปั้มน้ำมัน

if($trailer_id != ""){
  $arr = explode(":", $trailer_id);
  $trailer_id = $arr[0];
}

if($cust_dp != ""){
  $cust_dps = explode(" ", $cust_dp);
  $sql = "DELETE FROM tb_dp WHERE job_order_no = '$job_order_no'";
  $query  = mysqli_query($conn,$sql);
  for($i=0 ; $i < count($cust_dps); $i++){
    $sqldp = "select * FROM tb_dp WHERE dp_code = '$cust_dps[$i]'";
    $querydp  = mysqli_query($conn,$sqldp);
    $numdp    = mysqli_num_rows($querydp);

    if($numdp > 0){
      $sql = "UPDATE tb_dp SET job_order_no = '$job_order_no' where dp_code = '$cust_dps[$i]'";
      $query  = mysqli_query($conn,$sql);
    }else{
      $sql = "INSERT INTO tb_dp(dp_code,job_order_no)VALUES('$cust_dps[$i]','$job_order_no')";
      $query  = mysqli_query($conn,$sql);
    }
  }
}

if($affiliation_id == 1){
  $shipping_amount = $total_amount_receive;
}else{
  $shipping_amount = $total_amount_ap_pay;
}

if($ID != ""){
  $sql = "UPDATE tb_job_order SET
              job_order_no       = '$job_order_no',
              delivery_type_id   = '$delivery_type_id',
              job_order_date     = '$job_order_date',
              job_delivery_date  = '$job_delivery_date',
              cust_id            = '$cust_id',
              employee_id        = '$employee_id',
              trailer_id         = '$trailer_id',
              away_regi_no       = '$away_regi_no',
              away_spec_no       = '$away_spec_no',
              affiliation_id     = '$affiliation_id',
              route_id           = '$route_id',
              source             = '$source',
              destination        = '$destination',
              distance           = '$distance',
              cust_dp            = '$cust_dp',
              delivery_doc       = '$delivery_doc',
              department         = '$department',
              product_name       = '$product_name',
              number_of_trips    = '$number_of_trips',
              weights            = '$weights',
              shipping_amount    = '$shipping_amount',
              date_product_in    = '$date_product_in',
              time_product_in    = '$time_product_in',
              date_product_out   = '$date_product_out',
              time_product_out   = '$time_product_out',
              date_delivery_in   = '$date_delivery_in',
              time_delivery_in   = '$time_delivery_in',
              date_delivery_out  = '$date_delivery_out',
              time_delivery_out  = '$time_delivery_out',
              fuel_cost          = '$fuel_cost',
              fuel_litre         = '$fuel_litre',
              blank_charge       = '$blank_charge',
              allowance          = '$allowance',
              kog_expense        = '$kog_expense',
              allowance_oth      = '$allowance_oth',
              remark             = '$remark',
              shipment           = '$shipment',
              price_type_id      = '$price_type_id',
              one_trip_ton       = '$one_trip_ton',
              ext_one_trip_ton   = '$ext_one_trip_ton',
              price_per_trip     = '$price_per_trip',
              ext_price_per_trip = '$ext_price_per_trip',
              total_amount_receive = '$total_amount_receive',
              total_amount_ap_pay  = '$total_amount_ap_pay',
              total_amount_allowance   = '$total_amount_allowance',
              job_ended_clearance      = '$job_ended_clearance',
              job_ended_recap          = '$job_ended_recap',
              job_ended_expressway     = '$job_ended_expressway',
              job_ended_passage_fee    = '$job_ended_passage_fee',
              job_ended_repaires       = '$job_ended_repaires',
              job_ended_acc_expense    = '$job_ended_acc_expense',
              job_ended_other_expense  = '$job_ended_other_expense',
              fuel_driver_bill         = '$fuel_driver_bill',
              job_status_id            = '$job_status_id',
              job_order_profit         = '$job_order_profit',
              petrol_station           = '$petrol_station'
          WHERE id = '$ID'";
}else{

  $d        = date('Y-m-d',strtotime($job_order_date));
  $strDay   = date('d',strtotime($d));
  $strMonth = date('m',strtotime($d));
  $strYear  = date("Y",strtotime($d));
  if($strYear < 2500){
    $strYear += 543;
  }

  $strYear  = substr($strYear,2,2);
  $Code = "TT".$strYear.$strMonth.$strDay;
  $sql = "SELECT max(job_order_no) as job_order_no FROM tb_job_order where job_order_no LIKE '$Code%'";
  $query  = mysqli_query($conn,$sql);
  $row    = mysqli_fetch_assoc($query);
  $jobOrderNo  = $row['job_order_no'];

  if(!empty($jobOrderNo)){
    $lastNum = substr($jobOrderNo,strlen($Code));
    $lastNum = $lastNum + 1;
    $job_order_no = $Code.sprintf("%03d", $lastNum);
  }else{
    $job_order_no = $Code.sprintf("%03d", 1);
  }

  $sql = "INSERT INTO tb_job_order
         (job_order_no,
         delivery_type_id,
         job_order_date,
         job_delivery_date,
         cust_id,
         employee_id,
         trailer_id,
         away_regi_no,
         away_spec_no,
         affiliation_id,
         route_id,
         source,
         destination,
         distance,
         cust_dp,
         delivery_doc,
         department,
         product_name,
         number_of_trips,
         weights,
         shipping_amount,
         date_product_in,
         time_product_in,
         date_product_out,
         time_product_out,
         date_delivery_in,
         time_delivery_in,
         date_delivery_out,
         time_delivery_out,
         fuel_cost,
         fuel_litre,
         blank_charge,
         allowance,
         kog_expense,
         allowance_oth,
         remark,
         shipment,
         price_type_id,
         one_trip_ton,
         ext_one_trip_ton,
         price_per_trip,
         ext_price_per_trip,
         total_amount_receive,
         total_amount_ap_pay,
         total_amount_allowance,
         job_ended_clearance,
         job_ended_recap,
         job_ended_expressway,
         job_ended_passage_fee,
         job_ended_repaires,
         job_ended_acc_expense,
         job_ended_other_expense ,
         fuel_driver_bill,
         job_status_id,
         job_order_profit,
         petrol_station
         )
         VALUES(
           '$job_order_no',
           '$delivery_type_id',
           '$job_order_date',
           '$job_delivery_date',
           '$cust_id',
           '$employee_id',
           '$trailer_id',
           '$away_regi_no',
           '$away_spec_no',
           '$affiliation_id',
           '$route_id',
           '$source',
           '$destination',
           '$distance',
           '$cust_dp',
           '$delivery_doc',
           '$department',
           '$product_name',
           '$number_of_trips',
           '$weights',
           '$shipping_amount',
           '$date_product_in',
           '$time_product_in',
           '$date_product_out',
           '$time_product_out',
           '$date_delivery_in',
           '$time_delivery_in',
           '$date_delivery_out',
           '$time_delivery_out',
           '$fuel_cost',
           '$fuel_litre',
           '$blank_charge',
           '$allowance',
           '$kog_expense',
           '$allowance_oth',
           '$remark',
           '$shipment',
           '$price_type_id',
           '$one_trip_ton',
           '$ext_one_trip_ton',
           '$price_per_trip',
           '$ext_price_per_trip',
           '$total_amount_receive',
           '$total_amount_ap_pay',
           '$total_amount_allowance',
           '$job_ended_clearance',
           '$job_ended_recap',
           '$job_ended_expressway',
           '$job_ended_passage_fee',
           '$job_ended_repaires',
           '$job_ended_acc_expense',
           '$job_ended_other_expense ',
           '$fuel_driver_bill',
           '$job_status_id',
           '$job_order_profit',
           '$petrol_station'
       )";
}

if(mysqli_query($conn,$sql)){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail'.$sql)));
}
?>
