<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate    = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate      = isset($_POST['endDate'])?$_POST['endDate']:"";
$status       = isset($_POST['status'])?$_POST['status']:"";
$employeeId   = isset($_POST['employeeId'])?$_POST['employeeId']:"";
$page         = isset($_POST['page'])?$_POST['page']:"";
$dp           = isset($_POST['dp'])?$_POST['dp']:"";
$jobOrderNo   = isset($_POST['jobOrderNo'])?$_POST['jobOrderNo']:"";
$trailer_id   = isset($_POST['trailer_id'])?$_POST['trailer_id']:"";
$cust_id      = isset($_POST['cust_id'])?$_POST['cust_id']:"";

$numRow = 0;
//$con = " and invoice_code is null ";
$con = "";
// if($startDate != "" and  $startDate != "")
// {
//   $con .= " and DATE_FORMAT(pc.Date_Po, '%Y/%m/%d') ='". $startDate ."' ";
// }
$display = "";
$displayPO = "display:none";
if($page == "JOB"){
  $display = 'display:none';
  $displayPO =  'display:block';
  $con .= " and jo.job_status_id != '3' ";
}

if($startDate != "" and  $endDate != "")
{
  $con .= " and jo.job_order_date BETWEEN  '". $startDate ."' and '". $endDate ."'";
}

if($trailer_id != "" )
{
  $con .= " and jo.trailer_id ='". $trailer_id ."' ";
}

if($jobOrderNo != "")
{
  $con .= " and jo.job_order_no LIKE  '". $jobOrderNo ."%' ";
}

if($cust_id != "")
{
  $con .= " and jo.cust_id = '". $cust_id ."' ";
}



//echo ">>>>>".$page;
if($status != "" && $page == "PO")
{
  $con .= " and jo.job_status_id ='". $status ."' ";
}

if($dp != "")
{
  $con .= " and jo.job_order_no in (select job_order_no FROM tb_dp where dp_code LIKE '". $dp ."%')";
}


if($employeeId != "")
{
  $con .= " and jo.employee_id  ='". $employeeId ."' ";
}



?>
<table class="table table-bordered table-striped " id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px;"></th>
      <th style="width:50px;"></th>
      <th class="text-center" >เลขที่ใบสั่ง</th>
      <th class="text-center" >วันที่</th>
      <th class="text-center" >กำหนดส่ง</td>
      <th style="width:110px;" class="text-center" >DP</th>
      <th class="text-center" >ลูกค้า</th>
      <?php
        if($page == "JOB"){
          echo "<th class='text-center' >Shipment</th>";
        }
      ?>
      <th class="text-center" >ชื่อพนักงานขับรถ</th>
      <th style="width:65px;" class="text-center" >ทะเบียน</th>
      <th class="text-center" >ต้นทาง</th>
      <th class="text-center" >ปลายทาง</th>
      <th style="width:50px;<?= $display?>"></th>
    </tr>
  </thead>
  <tbody>
<?php
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name, t.license_plate, c.cust_name
  FROM tb_job_order jo, tb_employee_master em , tb_trailer t, tb_customer_master c
  where jo.employee_id = em.employee_id  and jo.trailer_id = t.trailer_id and jo.cust_id = c.cust_id $con
  order by jo.job_order_date DESC,jo.job_order_no DESC";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $job_order_no         = $row['job_order_no'];
    $job_order_date       = date('d/m/Y', strtotime($row['job_order_date']));
    $job_delivery_date    = date('d/m/Y', strtotime($row['job_delivery_date']));
    $cust_dp              = $row['cust_dp'];
    $source               = $row['source'];
    $destination          = $row['destination'];
    $Employee_id          = $row['employee_id'];
    $Employee_id2         = $row['employee_id2'];
    $Employee_Name        = $row['employee_name'];
    $affiliation_id       = $row['affiliation_id'];
    $job_status_id        = $row['job_status_id'];
    $shipment             = $row['shipment'];
    $cust_name            = $row['cust_name'];

    $license_plate        = $row['license_plate'];

    if($affiliation_id == "1" &&  $job_status_id == 1 && $page == "JOB"){
      continue;
    }
    $disabled = "";
    if($job_status_id == 3){
      $disabled = 'disabled';
    }

    if($Employee_id2 > 0){
      $Employee_Name .= "/".getEmpName($Employee_id2);
    }

?>
    <tr class="text-center">
      <td>
        <button type="button" class="btn btn-primary btn-sm btn-flat" onclick="showFormJobOrder2(<?= $row['id']; ?>,'<?= $page ?>')">ข้อมูล</button>
      </td>
      <td>
        <button type="button" class="btn btn-info btn-sm btn-flat" onclick="printPO(<?= $row['id']; ?>)">พิมพ์</button>
      </td>
      <td><?= $job_order_no ?></td>
      <td align="center"><?= $job_order_date ?></td>
      <td align="center"><?= $job_delivery_date ?></td>
      <td align="right"><?= $cust_dp ?></td>
      <td align="left" ><?= $cust_name ?></td>
      <td align="right" style="<?= $displayPO ?>"><?= $shipment ?></td>
      <td align="left" ><?= $Employee_Name ?></td>
      <td align="left" ><?= $license_plate ?></td>
      <td align="left"><?= $source ?></td>
      <td align="left"><?= $destination ?></td>
      <td style="<?= $display?>">
        <button type="button" <?= $disabled ?> class="btn btn-danger btn-sm btn-flat" onclick="cancelPo(<?= $row['id']; ?>,'<?= $job_order_no ?>')">ยกเลิก</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
     'paging'      : false,
     'lengthMenu'  : [2, 100, 150,200],
     'lengthChange': false,
     'searching'   : false,
     'ordering'    : false,
     'info'        : false,
     'autoWidth'   : false
   })
  })
</script>
