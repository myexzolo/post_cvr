<?php

include('../../conf/connect.php');
include("../../inc/utils.php");
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php

$department_id = isset($_POST['value'])?$_POST['value']:"";
$department_code = "";
$department_name = "";
$contact_name = "";
$tax = "";
$address = "";
$tel = "";
$fax = "";
$email = "";
$cust_id  = "";
$remark = "";
$affiliation_id = "";

if(!empty($department_id))
{
    $sql = "SELECT * FROM tb_department_master WHERE department_id = '$department_id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $department_id    = $row['department_id'];
    $department_code  = $row['department_code'];
    $department_name  = $row['department_name'];
    $contact_name     = $row['contact_name'];
    $tax              = $row['tax'];
    $address          = $row['address'];
    $tel              = $row['tel'];
    $fax              = $row['fax'];
    $email            = $row['email'];
    $remark           = $row['remark'];
    $affiliation_id   = $row['affiliation_id'];
}
$optionAffiliation    = getoptionAffiliation($affiliation_id);
?>
<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      <label>รหัสสังกัด</label>
      <input value="<?= $department_code ?>" name="department_code" type="text" class="form-control" placeholder="รหัสสังกัด" required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ชื่อสังกัด</label>
      <input value="<?= $department_name ?>" name="department_name" type="text" class="form-control" placeholder="ชื่อสังกัด" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เลขที่ประจำตัวผู้เสียภาษี</label>
      <input value="<?= $tax ?>" name="tax" type="text" class="form-control" placeholder="tax" required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ที่อยู่</label>
      <textarea class="form-control" name="address" id="Address" rows="2" placeholder="Address ..." required><?= $address ?></textarea>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>หมายเหตุ</label>
      <textarea class="form-control" name="remark" id="Remark" rows="2" placeholder="Remark ..." ><?= $remark ?></textarea>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ชื่อผู้จัดการ</label>
      <input value="<?= $contact_name ?>" name="contact_name" type="text" class="form-control" placeholder="" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>โทรศัพท์</label>
      <input value="<?= $tel ?>" name="tel" type="text" class="form-control" placeholder="Phone" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Fax</label>
      <input value="<?= $fax ?>" name="fax" type="text" class="form-control" placeholder="Fax" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>Email</label>
      <input value="<?= $email ?>" name="email" type="email" class="form-control" placeholder="Email" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>รถ(สังกัด)</label>
      <select name="affiliation_id"  id="affiliation_id" class="form-control select2" style="width: 100%;" required >
        <option value="" >เลือกสังกัด</option>
        <?= $optionAffiliation ?>
      </select>
    </div>
  </div>
</div>

<input  type="hidden" value="<?= $department_id ?>" name="department_id" type="text" >
