<?php

include('../../conf/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px" class="text-center">No.</th>
      <th class="text-center">รหัส</th>
      <th class="text-center">ชื่อสังกัด</th>
      <th class="text-center">ชื่อผู้จัดการ</th>
      <th class="text-center">โทรศัพท์</th>
      <th class="text-center">เลขที่ประจำตัวผู้เสียภาษี</th>
      <th class="text-center">สังกัด</th>
      <th style="width:100px;"></th>
    </tr>
  </thead>
  <tbody>
<?php
  $sql = "SELECT d.*,t.affiliation_name FROM tb_department_master d, tb_trailer_affiliation t where d.affiliation_id = t.affiliation_id";

  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td><?= $i ?></td>
      <td class="text-left"><?= $row['department_code']; ?></td>
      <td class="text-left"><?= $row['department_name']; ?></td>
      <td class="text-left"><?= $row['contact_name']; ?></td>
      <td class="text-left"><?= $row['tel']; ?></td>
      <td class="text-left"><?= $row['tax']; ?></td>
      <td class="text-left"><?= $row['affiliation_name']; ?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm(<?= $row['department_id']; ?>)">แก้ไข</button>
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="removeRow(<?= $row['department_id']; ?>)">ลบ</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })

</script>
