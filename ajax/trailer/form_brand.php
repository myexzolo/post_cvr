<?php

include('../../conf/connect.php');
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php
$trailer_code   = "";
$trailer_brand  = "";


$trailer_brand_id = isset($_POST['value'])?$_POST['value']:"";

if(!empty($trailer_brand_id))
{
    $sql = "SELECT * FROM tb_trailer_brand WHERE trailer_brand_id = '$trailer_brand_id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $trailer_code     = $row['trailer_code'];
    $trailer_brand    = $row['trailer_brand'];
}
?>
<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      <label>รหัส</label>
      <input value="<?= $trailer_code ?>" name="trailer_code" type="number" class="form-control" placeholder="รหัส" required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ชื่อยี่ห้อ</label>
      <input value="<?= $trailer_brand ?>" name="trailer_brand" type="text" class="form-control" placeholder="ชื่อยี่ห้อ" required>
    </div>
  </div>
</div>

<input  type="hidden" value="<?= $trailer_brand_id ?>" name="trailer_brand_id" type="text" >
