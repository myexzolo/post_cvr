<?php

include('../../conf/connect.php');
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php
$trailer_category_code   = "";
$trailer_category_name  = "";


$trailer_category_id = isset($_POST['value'])?$_POST['value']:"";

if(!empty($trailer_category_id))
{
    $sql = "SELECT * FROM tb_trailer_category WHERE trailer_category_id = '$trailer_category_id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $trailer_category_code    = $row['trailer_category_code'];
    $trailer_category_name    = $row['trailer_category_name'];
}
?>
<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      <label>รหัส</label>
      <input value="<?= $trailer_category_code ?>" name="trailer_category_code" type="text"  maxlength="2" class="form-control" placeholder="รหัส" required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ชื่อประเภทรถ</label>
      <input value="<?= $trailer_category_name ?>" name="trailer_category_name" type="text" class="form-control" placeholder="ชื่อประเภทรถ" required>
    </div>
  </div>
</div>

<input  type="hidden" value="<?= $trailer_category_id ?>" name="trailer_category_id" type="text" >
