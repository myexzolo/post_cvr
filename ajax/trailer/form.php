<?php

include('../../conf/connect.php');
include('../../inc/utils.php');
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php

$trailer_id         = isset($_POST['value'])?$_POST['value']:"";
$trailer_no         = "";
$license_plate      = "";
$province_id        = "";
$trailer_brand_id   = "";
$model              = "";
$car_serial_no      = "";
$registration_date  = "";
$tax_expiration     = "";
$check_status       = "";
$vin                = "";//เลขเครื่องยนต์
$drive_shaft        = "";
$weight             = "";
$color              = "";
$type               = "";
$nature             = "";
$long_car           = "";
$car_type_id        = "";
$gps                = "";
$employee_id        = "";
$total_name         = "";
$employee_id        = "";
$affiliation_id     = "";
$department_id      = "";
$trailer_category_id = "";
$trailer_type_id     = "";
$vendors_id          = "";

if(!empty($trailer_id))
{
    $sql = "SELECT * FROM tb_trailer WHERE trailer_id = '$trailer_id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $trailer_no         = $row['trailer_no'];
    $license_plate      = $row['license_plate'];
    $province_id        = $row['province_id'];
    $trailer_brand_id   = $row['trailer_brand_id'];
    $model              = $row['model'];
    $car_serial_no      = $row['car_serial_no'];
    $registration_date  = $row['registration_date'];
    $tax_expiration     = $row['tax_expiration'];
    $check_status       = $row['check_status'];
    $vin                = $row['vin'];
    $drive_shaft        = $row['drive_shaft'];
    $weight             = $row['weight'];
    $color              = $row['color'];
    $type               = $row['type'];
    $nature             = $row['nature'];
    $long_car           = $row['long_car'];
    $car_type_id        = $row['car_type_id'];
    $gps                = $row['gps'];
    $employee_id        = $row['employee_id'];
    $total_name         = $row['total_name'];
    $employee_id        = $row['employee_id'];
    $affiliation_id     = $row['affiliation_id'];
    $department_id      = $row['department_id'];
    $trailer_category_id = $row['trailer_category_id'];
    $trailer_type_id     = $row['trailer_type_id'];
    $vendors_id          = $row['vendors_id'];
}
$optionEmp              = getoptionEmp($employee_id,"");
$optionVendors          = getoptionVendors($vendors_id);
$optionTrailerCategory  = getoptionTrailerCategory($trailer_category_id);
$optionAffiliation      = getoptionAffiliation($affiliation_id);
$optionProvince         = getoptionProvince($province_id);
$optionTrailerBrand     = getoptionTrailerBrand($trailer_brand_id);
$optionTrailerType      = getoptionTrailerType($trailer_type_id);
$optionDep              = getoptionDep($department_id);
?>
<div class="row">
  <div class="col-md-2">
    <div class="form-group">
      <label>รหัสรถ</label>
      <input value="<?= $trailer_no ?>" name="trailer_no" type="text" class="form-control" placeholder="รหัสรถ" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>สังกัดรถ</label>
      <select name="affiliation_id"  id="affiliation_id" class="form-control select2" style="width: 100%;" required>
        <option value="" >เลือกสังกัดรถ</option>
        <?= $optionAffiliation ?>
      </select>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>ทะเบียนรถ</label>
      <input value="<?= $license_plate ?>" name="license_plate" type="text" class="form-control" placeholder="" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>จังหวัด</label>
      <select name="province_id"  id="province_id" class="form-control select2" style="width: 100%;" required>
        <option value="" >เลือกจังหวัด</option>
        <?= $optionProvince ?>
      </select>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>ยี่ห้อรถ</label>
      <select name="trailer_brand_id"  id="trailer_brand_id" class="form-control select2" style="width: 100%;">
        <option value="" >เลือกยี่ห้อรถ</option>
        <?= $optionTrailerBrand ?>
      </select>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>รุ่น (Model)</label>
      <input value="<?= $model ?>" name="model" type="text" class="form-control" placeholder="Model" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เลขตัวถังรถ</label>
      <input value="<?= $car_serial_no ?>" name="car_serial_no" type="text" class="form-control" placeholder="เลขตัวถัง" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เลขเครื่องยนต์</label>
      <input value="<?= $vin ?>" name="vin" type="text" class="form-control" placeholder="เลขเครื่อง" >
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>ชนิดรถ</label>
      <select name="trailer_type_id"  id="trailer_type_id" class="form-control select2" style="width: 100%;" >
        <option value="" >เลือกชนิดรถ</option>
        <?= $optionTrailerType ?>
      </select>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>ประเภทรถ</label>
      <select name="trailer_category_id"  id="trailer_category_id" class="form-control select2" style="width: 100%;" >
        <option value="" >เลือกประเภทรถ</option>
        <?= $optionTrailerCategory ?>
      </select>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เพลา</label>
      <input value="<?= $drive_shaft ?>" name="drive_shaft" type="text" class="form-control" placeholder="เพลา" >
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>สีรถ</label>
      <input value="<?= $color ?>" name="color" type="text" class="form-control" placeholder="สีรถ" >
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>น้ำหนัก</label>
      <input value="<?= $weight ?>" name="weight" type="text" class="form-control" placeholder="weight" >
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>ความยาวตัวรถ</label>
      <input value="<?= $long_car ?>" name="long_car" type="text" class="form-control" placeholder="ความยาว" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ลักษณะ</label>
      <input value="<?= $nature ?>" name="nature" type="text" class="form-control" placeholder="ลักษณะ" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>วันจดทะเบียน</label>
      <input value="<?= $registration_date ?>" name="registration_date" type="date" class="form-control" placeholder="วันจดทะเบียน" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>วันหมดอายุภาษี</label>
      <input value="<?= $tax_expiration ?>" name="tax_expiration" type="date" class="form-control" placeholder="วันหมดอายุภาษี" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>รหัสตรวจสภาพรถ</label>
      <input value="<?= $check_status ?>" name="check_status" type="text" class="form-control" placeholder="รหัสตรวจสภาพรถ" >
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>GPS</label>
      <input value="<?= $gps ?>" name="gps" type="text" class="form-control" placeholder="GPS" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>ชื่อคนขับ</label>
      <select name="employee_id"  id="employee_id" class="form-control select2" style="width: 100%;" required>
        <option value="" >เลือกคนขับ</option>
        <?= $optionEmp ?>
      </select>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>เจ้าของรถ</label>
      <select name="department_id"  id="department_id" class="form-control select2" style="width: 100%;" required>
        <option value="" >เลือกสังกัด</option>
        <?= $optionDep ?>
      </select>
    </div>
  </div>
</div>

<input  type="hidden" value="<?= $trailer_id ?>" name="trailer_id" type="text" >
