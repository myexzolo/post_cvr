<?php

include('../../conf/connect.php');
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php
$trailer_type_code  = "";
$trailer_type_name  = "";


$trailer_type_id = isset($_POST['value'])?$_POST['value']:"";

if(!empty($trailer_type_id))
{
    $sql = "SELECT * FROM tb_trailer_type WHERE trailer_type_id = '$trailer_type_id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $trailer_type_code    = $row['trailer_type_code'];
    $trailer_type_name    = $row['trailer_type_name'];
}
?>
<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      <label>รหัส</label>
      <input value="<?= $trailer_type_code ?>" name="trailer_type_code" type="text"  maxlength="2" class="form-control" placeholder="รหัส" required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>ชื่อชนิดรถ</label>
      <input value="<?= $trailer_type_name ?>" name="trailer_type_name" type="text" class="form-control" placeholder="ชื่อชนิดรถ" required>
    </div>
  </div>
</div>

<input  type="hidden" value="<?= $trailer_type_id ?>" name="trailer_type_id" type="text" >
