<?php

include('../../conf/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$strExcelFileName="export_trailer.xls";

header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
<table x:str border=1 cellpadding=0 cellspacing=1 width=100% style="border-collapse:collapse">
  <thead>
    <tr class="text-center">
      <th colspan="8"  class="text-center">รายการทะเบียนรถบรรทุก</th>
    </tr>
    <tr class="text-center">
      <th style="width:40px" class="text-center">No.</th>
      <th class="text-center">รหัส </th>
      <th class="text-center">ทะเบียนรถ</th>
      <th class="text-center">จังหวัด</th>
      <th class="text-center">Model</th>
      <th class="text-center">พนักงานขับรถ</th>
      <th class="text-center">สังกัด</th>
      <th class="text-center">เจ้าของรถ</th>
    </tr>
  </thead>
  <tbody>
<?php
  $sql = "SELECT t.*, p.province_name_th, a.affiliation_name, e.employee_name, d.department_name
  FROM  tb_trailer t , tb_province_master p , tb_trailer_affiliation a,  tb_employee_master e,  tb_department_master d
  where t.province_id = p.province_id  and t.affiliation_id  = a.affiliation_id  and t.employee_id = e.employee_id and t.department_id = d.department_id
  order by t.license_plate";

  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
?>
    <tr class="text-center">
      <td><?= $i ?></td>
      <td class="text-center"><?= $row['trailer_no']; ?></td>
      <td class="text-left"><?= $row['license_plate'] ?></td>
      <td class="text-left"><?= $row['province_name_th']; ?></td>
      <td class="text-left"><?= $row['model']; ?></td>
      <td class="text-left"><?= $row['employee_name']; ?></td>
      <td class="text-left"><?= $row['affiliation_name']; ?></td>
      <td class="text-left"><?= $row['department_name']; ?></td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
window.onbeforeunload = function(){return false;};
setTimeout(function(){window.close();}, 10000);
</script>
</body>
</html>
