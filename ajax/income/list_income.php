<style type="text/css">
     .right { text-align: right; }
</style>
<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);



$month          = isset($_POST['month'])?$_POST['month']:"";
$year           = isset($_POST['year'])?$_POST['year']:"";
$trailerId      = isset($_POST['trailerId'])?$_POST['trailerId']:"";


$con = "";
if($trailerId != "")
{
  $trailerIdArr = explode(":", $trailerId);
  $trailerId    = $trailerIdArr[0];

  $con .= " and i.trailer_id ='". $trailerId ."'";
}



?>
<table class="table table-bordered table-striped" id="tableDisplay" style="font-size:12px;">
  <thead>
    <tr>
			<th style="width:30px;">ลำดับ</th>
      <th >ประจำเดือน</th>
      <th >ทะเบียน</th>
			<th >รายรับ</th>
			<th >เงินเดือน</th>
			<th >บิลน้ำมัน</th>
      <th >เบี้ยขยัน</th>
      <th >ค่าแรง/่ค่าซ่อมหัว - หาง</th>
      <th >อื่นๆ</th>
      <th >รวม</th>
      <th style="width:30px;"></th>
		</tr>
  </thead>
  <tbody>
<?php

  $sql = "SELECT i.*,t.license_plate
  FROM tb_income_his i, tb_trailer t
  where i.trailer_id = t.trailer_id $con and i.year = '$year' and  i.month = '$month'
  order by t.license_plate";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalTransfer = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $income_h_id      = $row['income_h_id'];
    $month            = $row['month'];
    $year             = $row['year'];
    $trailer_id       = $row['trailer_id'];
    $other_txt1       = $row['other_txt1'];
    $other_txt2       = $row['other_txt2'];
    $other_txt3       = $row['other_txt3'];
    $other_value1     = $row['other_value1'];
    $other_value2     = $row['other_value2'];
    $other_value3     = $row['other_value3'];
    $salary           = $row['salary'];
    $bill             = $row['bill'];
    $labor            = $row['labor'];
    $diligence        = $row['diligence'];
    $total            = $row['total'];
    $job_profits      = $row['job_profits'];
    $license_plate    = $row['license_plate'];
    $trailer_id       = $row['trailer_id'];

    $other = ($other_value1 + $other_value2 + $other_value3);

?>
  <tr>
    <td class="text-center" ><?= $i ?></td>
    <td class="text-center" ><?= $month."/".$year ?></td>
    <td class="text-center" ><?= $license_plate ?></td>
    <td class="text-right"><?= number_format($job_profits,2);  ?></td>
    <td class="text-right" ><?= number_format($salary,2); ?></td>
    <td class="text-right" ><?= number_format($bill,2);  ?></td>
    <td class="text-right" ><?= number_format($diligence,2); ?></td>
    <td class="text-right" ><?= number_format($labor,2); ?></td>
    <td class="text-right" ><?= number_format($other,2); ?></td>
    <td class="text-right" ><?= number_format($total,2); ?></td>
    <td><button type="button" class="btn btn-warning btn-sm btn-flat" onclick="editIncome(<?= $income_h_id; ?>,'<?=$month ?>','<?=$year ?>','<?=$trailer_id ?>')">แก้ไข</button></td>
  </tr>
<?php
}
if($num == 0){
  ?>
  <tr>
    <td colspan="10">&nbsp;</td>
  </tr>
  <?php
}
?>
</tbody>
</table>
