<style type="text/css">
     .right { text-align: right; }
</style>
<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);



$month          = isset($_POST['month'])?$_POST['month']:"";
$year           = isset($_POST['year'])?$_POST['year']:"";
$trailer_id     = isset($_POST['trailer_id'])?$_POST['trailer_id']:"";
$income_h_id    = isset($_POST['income_h_id'])?$_POST['income_h_id']:"";
$employeeId     = isset($_POST['employeeId'])?$_POST['employeeId']:"";

$startDate      = $year."/".$month."/01";
$endDate        = date("Y/m/t", strtotime($startDate));

$readonly = "";


$other_txt1   ="";
$other_txt2   ="";
$other_txt3   ="";
$other_value1 ="";
$other_value2 ="";
$other_value3 ="";
$salary       ="";
$bill         ="";
$labor        ="";
$diligence    ="";
$total        ="";



$fuel_litres = 0;
$fuel_costs = 0;
$blank_charges = 0;
$allowances = 0;
$kog_expenses = 0;
$job_ended_acc_expenses = 0;
$allowance_oths = 0;
$job_ended_expressways = 0;
$job_ended_recaps = 0;
$total_amount_allowances = 0;
$total_amount_receives = 0;
$withholdings = 0;
$job_order_profits = 0;

$numRow = 0;
$con = "";

$trailerName = "";
if($trailer_id != "")
{
  $trailerIdArr = explode(":", $trailer_id);
  $trailerId    = $trailerIdArr[0];
  $trailerName   = getLicensePlate($trailerId);

  $con .= " and jo.trailer_id ='". $trailerId ."'";
}else{
  //$con .= " and jo.trailer_id ='0'";
}

if($employeeId != "")
{
  $con .= " and jo.employee_id ='". $employeeId ."'";
}

if($income_h_id != ""){

  $sql    = "SELECT * FROM tb_income_his where income_h_id = $income_h_id";
  $query  = mysqli_query($conn,$sql);
  $row    = mysqli_fetch_assoc($query);

  $other_txt1   = $row['other_txt1'];
  $other_txt2   = $row['other_txt2'];
  $other_txt3   = $row['other_txt3'];
  $other_value1 = $row['other_value1'];
  $other_value2 = $row['other_value2'];
  $other_value3 = $row['other_value3'];
  $salary       = $row['salary'];
  $bill         = $row['bill'];
  $labor        = $row['labor'];
  $diligence    = $row['diligence'];
  $total        = $row['total'];

  //$readonly = 'readonly';
  $con .= " and jo.income_h_id = $income_h_id ";
}else{

  $con .= " and jo.income_h_id = 0";
}


?>
<div align="left" style="font-size:14px;padding:5px;"><b>ทะเบียน <?= $trailerName ?></b></div>
<input type="hidden" value="<?= $month ?>" name="month">
<input type="hidden" value="<?= $year ?>" name="year">
<input type="hidden" value="<?= $trailerId ?>" name="trailer_id">
<input type="hidden" value="<?= $income_h_id ?>" name="income_h_id">
<table class="table table-bordered table-striped" id="tableDisplay" style="font-size:12px;">
  <thead>
    <tr>
			<th style="width:30px;">ลำดับ</th>
      <th style="width:75px;">วันที่สั่งจ้าง</th>
      <th >ชื่อคนขับ</th>
			<th >ต้นทาง</th>
			<th >ปลายทาง</th>
			<th style="width:80px;">จำนวน/ลิตร</th>
			<th style="width:65px;">ค่าน้ำมัน</th>
      <th style="width:65px;">ตีเปล่า</th>
      <th style="width:65px;">เบี้ยเลี้ยง</th>
      <th style="width:65px;">คอก</th>
      <th style="width:65px;">อื่นๆ</th>
      <th style="width:65px;">ทางด่วน</th>
      <th style="width:65px;ุ">ปะยาง</th>
      <th style="width:75px;">รวมค่าใช้จ่าย</th>
      <th style="width:75px;">รายได้ค่าขนส่ง</th>
      <th style="width:60px;">หัก 1%</th>
      <th style="width:80px;">กำไรขั้นต้น</th>
		</tr>
  </thead>
  <tbody>
<?php


  $con .= " and jo.job_order_date between '". $startDate ."' and '". $endDate ."'";
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id and jo.job_status_id = '2'  and invoice_code is not null
  order by jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalTransfer = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];
    $job_order_profit         = $row['job_order_profit'];

    $invoice_code             = $row['invoice_code'];
    $period_code              = $row['period_code'];
    $license_plate            = $row['license_plate'];


    // $transfer = ($fuel_cost + $allowance + $kog_expense + $blank_charge + $allowance_oth);
    // $totalTransfer +=  $transfer;
    //
    // $withholding  =  ($total_amount_receive * 0.01);
    //
    // $job_order_profit -= $withholding;


    $transfer = ($fuel_cost + $blank_charge + $allowance + $kog_expense  + $allowance_oth + $job_ended_expressway+ $job_ended_recap);
    $totalTransfer +=  $transfer;

    $job_order_profit = ($total_amount_receive - $transfer);

    $total_amount_allowance = $transfer;

    $withholding  =  ($total_amount_receive * 0.01);

    $job_order_profit -= $withholding;




    $fuel_litres            += $fuel_litre;
    $fuel_costs             += $fuel_cost;
    $blank_charges          += $blank_charge;
    $allowances             += $allowance;
    $kog_expenses           += $kog_expense;
    $job_ended_acc_expenses += $job_ended_acc_expense;
    $allowance_oths         += $allowance_oth;
    $job_ended_expressways  += $job_ended_expressway;
    $job_ended_recaps       += $job_ended_recap;
    $total_amount_allowances  += $total_amount_allowance;
    $total_amount_receives  += $total_amount_receive;
    $withholdings           += $withholding;
    $job_order_profits      += $job_order_profit;
?>
  <tr>
    <td class="text-center" ><?= $i ?><input type="hidden" value="<?= $id ?>" name="id_job[]"></td>
    <td class="text-center" ><?= $job_order_date ?></td>
    <td class="text-left" ><?= $Employee_Name ?></td>
    <td class="text-left" ><?= $source ?></td>
    <td class="text-left" ><?= $destination ?></td>
    <td class="text-right" ><?= number_format($fuel_litre,2); ?></td>
    <td class="text-right" ><?= number_format($fuel_cost,2);  ?></td>
    <td class="text-right" ><?= number_format($blank_charge,2); ?></td>
    <td class="text-right" ><?= number_format($allowance,2); ?></td>
    <td class="text-right" ><?= number_format($kog_expense,2); ?></td>
    <td class="text-right" ><?= number_format($allowance_oth,2); ?></td>
    <td class="text-right" ><?= number_format($job_ended_expressway,2); ?></td>
    <td class="text-right" ><?= number_format($job_ended_recap,2); ?></td>
    <td class="text-right" ><?= number_format($total_amount_allowance,2); ?></td>
    <td class="text-right" ><?= number_format($total_amount_receive,2); ?></td>
    <td class="text-right" ><?= number_format($withholding,2); ?></td>
    <td class="text-right" ><?= number_format($job_order_profit,2); ?></td>
  </tr>
<?php
}
if($num == 0){
  ?>
  <tr>
    <td colspan="17" class="text-right" >&nbsp;</td>
  </tr>
  <?php
}
//  echo "<div>ไม่พบข้อมูล</div>";
//}
?>
<tr>
  <td colspan="5" class="text-right" ><b>รวม</b></td>
  <td class="text-right" ><b><?= number_format($fuel_litres,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($fuel_costs,2);  ?></b></td>
  <td class="text-right" ><b><?= number_format($blank_charges,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($allowances,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($kog_expenses,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($allowance_oths,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($job_ended_expressways,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($job_ended_recaps,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($total_amount_allowances,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($total_amount_receives,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($withholdings,2); ?></b></td>
  <td class="text-right" >
    <b><div id="jobProfits"><?= number_format($job_order_profits,2); ?></div></b>
    <input name="job_profits" value="<?= $job_order_profits ?>" type="hidden">
  </td>
</tr>
<tr>
  <td colspan="10" align="right" style="vertical-align:middle;">
    <input type="text" class="form-control" name="other_txt1" value="<?= $other_txt1 ?>" style="width:150px;font-size:11px;" id="other_txt1" placeholder="รายการอื่นๆ" <?= $readonly ?>>
  </td>
  <td colspan="3" class="text-right" >
    <input type="text" style="font-size:11px;" name="other_value1" class="form-control right" value="<?= $other_value1 ?>" id="other_value1" placeholder="จำนวนเงิน" <?= $readonly ?>>
  </td>
  <td colspan="2" class="text-right" style="vertical-align:middle;"><b>เงินเดือน : </b></td>
  <td colspan="2" class="text-right" >
    <input type="text" style="font-size:11px;" class="form-control right" value="<?= $salary ?>" id="salary" name="salary" <?= $readonly ?>>
  </td>
</tr>
<tr>
  <td colspan="10" align="right" style="vertical-align:middle;">
    <input type="text" class="form-control" value="<?= $other_txt2 ?>" style="width:150px;font-size:11px;" id="other_txt2"  name="other_txt2" placeholder="รายการอื่นๆ" <?= $readonly ?>>
  </td>
  <td colspan="3" class="text-right" >
    <input type="text" style="font-size:11px;" class="form-control right" value="<?= $other_value2 ?>" id="other_value2" name="other_value2" placeholder="จำนวนเงิน" <?= $readonly ?>></td>
  <td colspan="2" class="text-right" style="vertical-align:middle;"><b>บิลน้ำมัน : </b></td>
  <td colspan="2" class="text-right" ><input type="text" style="font-size:11px;" class="form-control right" value="<?= $bill ?>" id="bill" name="bill" <?= $readonly ?>></td>
</tr>
<tr>
  <td colspan="10" align="right" style="vertical-align:middle;">
    <input type="text" class="form-control" value="<?= $other_txt3 ?>" style="width:150px;font-size:11px;" id="other_txt3" name="other_txt3" placeholder="รายการอื่นๆ" <?= $readonly ?>>
  </td>
  <td colspan="3" class="text-right" >
    <input type="text" style="font-size:11px;" class="form-control right" value="<?= $other_value3 ?>" id="other_value3" name="other_value3" placeholder="จำนวนเงิน" <?= $readonly ?>></td>
  <td colspan="2" class="text-right" style="vertical-align:middle;"><b>เบี้ยขยัน : </b></td>
  <td colspan="2" class="text-right" >
    <input type="text" style="font-size:11px;" class="form-control right" value="<?= $diligence ?>" id="diligence" name="diligence" <?= $readonly ?>></td>
</tr>
<tr>
  <td colspan="10" align="right" style="vertical-align:middle;"></td>
  <td colspan="3" class="text-right" ></td>
  <td colspan="2" class="text-right" style="vertical-align:middle;"><b>ค่าแรง/่ค่าซ่อมหัว - หาง : </b></td>
  <td colspan="2" class="text-right" ><input type="text" style="font-size:11px;" class="form-control right" value="<?= $labor ?>" id="labor" name="labor" <?= $readonly ?>></td>
</tr>
<tr>
  <td colspan="15" class="text-right" style="vertical-align:middle;"><b>รวม : </b></td>
  <td colspan="2" class="text-right" ><input type="text" style="font-size:11px;" class="form-control right" value="<?= $total ?>" id="total" name="total" readonly></td>
</tr>
</tbody>
</table>
<div align="center">
  <button type="button" class="btn btn-warning btn-flat" style="width:100px;" onclick="postURL('income_list.php')">
  <i class="fa fa-arrow-left"></i> กลับ</button>
  <button type="button" class="btn btn-flat " onclick="calAmount()" style="width:100px;">
    <i class="fa fa-calculator"></i> คำนวณ</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">
  <i class="fa fa-save"></i> บันทึก</button>
</div>
