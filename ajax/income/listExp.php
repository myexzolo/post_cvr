
<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate      = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate        = isset($_POST['endDate'])?$_POST['endDate']:"";
$trailerId      = isset($_POST['trailerId'])?$_POST['trailerId']:"";
$employeeId     = isset($_POST['employeeId'])?$_POST['employeeId']:"";
$source         = isset($_POST['source'])?$_POST['source']:"";
$destination    = isset($_POST['destination'])?$_POST['destination']:"";

$other1          = isset($_POST['other1'])?$_POST['other1']:"";
$other2          = isset($_POST['other2'])?$_POST['other2']:"";
$other3          = isset($_POST['other3'])?$_POST['other3']:"";
$txtOther1       = isset($_POST['txtOther1'])?$_POST['txtOther1']:"รายการอื่นๆ";
$txtOther2       = isset($_POST['txtOther2'])?$_POST['txtOther2']:"รายการอื่นๆ";
$txtOther3       = isset($_POST['txtOther3'])?$_POST['txtOther3']:"รายการอื่นๆ";
$salary          = !empty($_POST['salary'])?$_POST['salary']:"0";
$bill            = !empty($_POST['bill'])?$_POST['bill']:"0";
$diligence       = !empty($_POST['diligence'])?$_POST['diligence']:"0";
$total           = !empty($_POST['total'])?$_POST['total']:"0";

$id_h            = isset($_POST['id_h'])?$_POST['id_h']:"";


$numRow = 0;
$con = "";

$fuel_litres = 0;
$fuel_costs = 0;
$blank_charges = 0;
$allowances = 0;
$kog_expenses = 0;
$job_ended_acc_expenses = 0;
$allowance_oths = 0;
$job_ended_expressways = 0;
$job_ended_recaps = 0;
$total_amount_allowances = 0;
$total_amount_receives = 0;
$withholdings = 0;
$job_order_profits = 0;

$numRow = 0;
$con = "";


if($id_h == ""){
  $sql = "INSERT INTO tb_income_his
         (start_date,end_date,
           trailer_id,employee_id,
           source,destination,
           salary,bill,
           diligence,total,
           other_txt1,other_value1,
           other_txt2,other_value2,
           other_txt3,other_value3
        )
         VALUES
         (
           '$startDate','$endDate',
           '$trailerId','$employeeId',
           '$source','$destination',
           '$salary','$bill',
           '$diligence','$total',
           '$txtOther1','$other1',
           '$txtOther2','$other2',
           '$txtOther3','$other3'
         )";
  //echo $sql;
  mysqli_query($conn,$sql);
}



if($trailerId != "")
{
  $trailerIdArr = explode(":", $trailerId);
  $trailerId    = $trailerIdArr[0];

  $con .= " and jo.trailer_id ='". $trailerId ."'";
}
if($employeeId != "")
{
  $con .= " and jo.employee_id ='". $employeeId ."'";
}
if($source != "")
{
  $con .= " and jo.source LIKE '". $source ."%'";
}
if($destination != "")
{
  $con .= " and jo.destination LIKE '". $destination ."%'";
}
?>
<table style="width:100%">
    <tr class="text-center">
      <th style="padding:5px;border:0px"><b>รายงานสรุปรายรับ - รายจ่าย</b></th>
    </tr>
    <tr class="text-center">
      <th style="padding:5px;border:0px"><b>
        <b>
        ประจำวันที่ <?= formatDateTh($startDate) ?> ถึงวันที่ <?= formatDateTh($endDate) ?>
      </b>
     </th>
    </tr>
</table>
<table class="table" id="tableDisplay" style="font-size:8px;" >
  <thead>
    <tr>
			<th colspan="18" style="height:30px;border:0px"></th>
		</tr>
    <tr>
			<th style="width:30px;border:1px solid black">ลำดับ</th>
      <th style="width:75px;border:1px solid black">วันที่สั่งจ้าง</th>
      <th style="width:120px;border:1px solid black">ชื่อคนขับ</th>
      <th style="width:65px;border:1px solid black">ทะเบียน</th>
			<th style="border:1px solid black">ต้นทาง</th>
			<th style="border:1px solid black">ปลายทาง</th>
			<th style="width:55px;border:1px solid black">จำนวน<br>ลิตร</th>
			<th style="border:1px solid black">ค่าน้ำมัน</th>
      <th style="border:1px solid black">ตีเปล่า</th>
      <th style="border:1px solid black">เบี้ยเลี้ยง</th>
      <th style="border:1px solid black">คอก</th>
      <th style="border:1px solid black">อื่นๆ</th>
      <th style="border:1px solid black">ทางด่วน</th>
      <th style="width:50px;border:1px solid black">ปะยาง</th>
      <th style="width:75px;border:1px solid black">รวมค่าใช้จ่าย</th>
      <th style="width:75px;border:1px solid black">รายได้ค่าขนส่ง</th>
      <th style="width:60px;border:1px solid black">หัก 1%</th>
      <th style="width:80px;border:1px solid black">กำไรขั้นต้น</th>
		</tr>
  </thead>
  <tbody>
<?php

$con .= " and jo.job_order_date between '". $startDate ."' and '". $endDate ."'";
//$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
$sql = "SELECT jo.*,em.employee_name,t.license_plate
FROM tb_job_order jo, tb_employee_master em, tb_trailer t
where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id and  jo.job_status_id = '2' and invoice_code is not null
order by jo.job_order_date";
//echo $sql;
$query  = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);

$totalTransfer = 0;
for ($i=1; $i <= $num ; $i++) {
  $row = mysqli_fetch_assoc($query);
  $Employee_Name      = $row['employee_name'];
  $license_plate      = $row['license_plate'];
  $id                 = $row['id'];
  $job_order_no       = $row['job_order_no'];
  $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
  $job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
  $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
  $cust_id            = $row['cust_id'];//รหัสลูกค้า
  $employee_id        = $row['employee_id'];//รหัสพนักงาน
  $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
  $away_regi_no       = $row['away_regi_no'];//หาง
  $away_spec_no       = $row['away_spec_no'];//สเปคหาง
  $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
  $route_id           = $row['route_id'];//รหัสเส้นทาง
  $source             = $row['source'];//ต้นทาง
  $destination        = $row['destination'];//ปลายทาง
  $distance           = $row['distance'];//ระยะทาง
  $cust_dp            = $row['cust_dp'];//DP
  $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
  $department         = $row['department'];//หน่วยงาน
  $product_name       = $row['product_name'];//สินค้า
  $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
  $weights            = $row['weights'];//น้ำหนัก(ตัน)
  $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
  $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
  $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
  $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
  $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
  $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
  $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
  $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
  $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
  $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
  $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
  $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
  $allowance          = $row['allowance'];//เบี้ยเลี้ยง
  $kog_expense        = $row['kog_expense'];//หางคอก
  $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
  $remark             = $row['remark'];
  $shipment           = $row['shipment'];
  $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
  $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
  $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
  $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
  $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
  $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
  $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
  $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
  $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
  $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
  $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
  $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
  $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
  $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
  $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
  $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
  $job_status_id            = $row['job_status_id'];
  $job_order_profit         = $row['job_order_profit'];

  $invoice_code             = $row['invoice_code'];
  $period_code              = $row['period_code'];
  $license_plate            = $row['license_plate'];


  $transfer = ($fuel_cost + $allowance + $kog_expense + $blank_charge + $allowance_oth);
  $totalTransfer +=  $transfer;

  $withholding  =  ($total_amount_receive * 0.01);

  $job_order_profit -= $withholding;

  $fuel_litres            += $fuel_litre;
  $fuel_costs             += $fuel_cost;
  $blank_charges          += $blank_charge;
  $allowances             += $allowance;
  $kog_expenses           += $kog_expense;
  $job_ended_acc_expenses += $job_ended_acc_expense;
  $allowance_oths         += $allowance_oth;
  $job_ended_expressways  += $job_ended_expressway;
  $job_ended_recaps       += $job_ended_recap;
  $total_amount_allowances  += $total_amount_allowance;
  $total_amount_receives  += $total_amount_receive;
  $withholdings           += $withholding;
  $job_order_profits      += $job_order_profit;
?>
<tr>
  <td style="border:1px solid black" class="text-center" ><?= $i ?></td>
  <td style="border:1px solid black" class="text-center" ><?= $job_order_date ?></td>
  <td style="border:1px solid black" class="text-left" ><?= $Employee_Name ?></td>
  <td style="border:1px solid black" class="text-center" ><?= $license_plate ?></td>
  <td style="border:1px solid black" class="text-left" ><?= $source ?></td>
  <td style="border:1px solid black" class="text-left" ><?= $destination ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($fuel_litre,2); ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($fuel_cost,2);  ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($blank_charge,2); ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($allowance,2); ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($kog_expense,2); ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($allowance_oth,2); ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($job_ended_expressway,2); ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($job_ended_recap,2); ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($total_amount_allowance,2); ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($total_amount_receive,2); ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($withholding,2); ?></td>
  <td style="border:1px solid black" class="text-right" ><?= number_format($job_order_profit,2); ?></td>
</tr>
<?php
}
if($num == 0){
?>
<tr>
  <td colspan="19" class="text-right" >&nbsp;</td>
</tr>
<?php
}
//  echo "<div>ไม่พบข้อมูล</div>";
//}
?>
<tr>
<td colspan="6" class="text-right" style="border:1px solid black"><b>รวม</b></td>
<td class="text-right" style="border:1px solid black"><b><?= number_format($fuel_litres,2); ?></b></td>
<td class="text-right" style="border:1px solid black"><b><?= number_format($fuel_costs,2);  ?></b></td>
<td class="text-right" style="border:1px solid black"><b><?= number_format($blank_charges,2); ?></b></td>
<td class="text-right" style="border:1px solid black"><b><?= number_format($allowances,2); ?></b></td>
<td class="text-right" style="border:1px solid black"><b><?= number_format($kog_expenses,2); ?></b></td>
<td class="text-right" style="border:1px solid black"><b><?= number_format($allowance_oths,2); ?></b></td>
<td class="text-right" style="border:1px solid black"><b><?= number_format($job_ended_expressways,2); ?></b></td>
<td class="text-right" style="border:1px solid black"><b><?= number_format($job_ended_recaps,2); ?></b></td>
<td class="text-right" style="border:1px solid black"><b><?= number_format($total_amount_allowances,2); ?></b></td>
<td class="text-right" style="border:1px solid black"><b><?= number_format($total_amount_receives,2); ?></b></td>
<td class="text-right" style="border:1px solid black"><b><?= number_format($withholdings,2); ?></b></td>
<td class="text-right" style="border:1px solid black"><b><div id="jobProfits"><?= number_format($job_order_profits,2); ?></div></b></td>
</tr>
<tr>
  <td colspan="14" style="border:0px"></td>
  <td colspan="2" class="text-right" style="vertical-align:middle;border:1px solid black;"><b>เงินเดือน : </b></td>
  <td colspan="2" class="text-right" style="border:1px solid black;" ><b><?= number_format($salary,2) ?></b></td>
</tr>
<tr>
  <td colspan="14" style="border:0px"></td>
  <td colspan="2" class="text-right" style="vertical-align:middle;border:1px solid black;"><b>บิลน้ำมัน : </b></td>
  <td colspan="2" class="text-right" style="border:1px solid black" ><b><?= number_format($bill,2); ?></b></td>
</tr>
<tr>
  <td colspan="14" style="border:0px"></td>
  <td colspan="2" class="text-right" style="vertical-align:middle;border:1px solid black;"><b>เบี้ยขยัน : </b></td>
  <td colspan="2" class="text-right" style="border:1px solid black"><b><?= number_format($diligence,2); ?></b></td>
</tr>
<?php
if($other1 !="")
{
?>
<tr>
  <td colspan="14" style="border:0px"></td>
  <td colspan="2" align="right" style="vertical-align:middle;border:1px solid black;"><b><?= $txtOther1." :" ?></b></td>
  <td colspan="2" class="text-right" style="border:1px solid black"><b><?= number_format($other1,2) ?></b></td>
</tr>
<?php
}
if($other2 !=""){
?>
<tr>
  <td colspan="14" style="border:0px"></td>
  <td colspan="2"  align="right" style="vertical-align:middle;border:1px solid black;"><b><?= $txtOther2." :" ?></b></td>
  <td colspan="2" class="text-right" style="border:1px solid black"><b><?= number_format($other2,2) ?></b></td>
</tr>
<?php
}
if($other3 !=""){
?>
<tr>
  <td colspan="14" style="border:0px"></td>
  <td colspan="2"  align="right" style="vertical-align:middle;border:1px solid black;"><b><?= $txtOther3." :"; ?></b></td>
  <td colspan="2" class="text-right" style="border:1px solid black"><b><?= number_format($other3,2) ?></b></td>
</tr>
<?php
}
?>
<tr>
  <td colspan="14" style="border:0px"></td>
  <td colspan="2"  class="text-right" style="vertical-align:middle;border:1px solid black;"><b>รวมกำไรขั้นต้น : </b></td>
<td colspan="2" class="text-right" style="border:1px solid black"><b><?= number_format($total,2); ?></b></td>
</tr>
<tr>
</tbody>
</table>
