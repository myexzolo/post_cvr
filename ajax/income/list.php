<style type="text/css">
     .right { text-align: right; }
</style>
<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$startDate      = isset($_POST['startDate'])?$_POST['startDate']:"";
$endDate        = isset($_POST['endDate'])?$_POST['endDate']:"";
$trailerId      = isset($_POST['trailerId'])?$_POST['trailerId']:"";
$employeeId     = isset($_POST['employeeId'])?$_POST['employeeId']:"";
$source         = isset($_POST['source'])?$_POST['source']:"";
$destination    = isset($_POST['destination'])?$_POST['destination']:"";


$id_h            = isset($_POST['id_h'])?$_POST['id_h']:"";
$other1          = isset($_POST['other1'])?$_POST['other1']:"";
$other2          = isset($_POST['other2'])?$_POST['other2']:"";
$other3          = isset($_POST['other3'])?$_POST['other3']:"";
$txtOther1       = isset($_POST['txtOther1'])?$_POST['txtOther1']:"";
$txtOther2       = isset($_POST['txtOther2'])?$_POST['txtOther2']:"";
$txtOther3       = isset($_POST['txtOther3'])?$_POST['txtOther3']:"";
$salary          = !empty($_POST['salary'])?$_POST['salary']:"";
$bill            = !empty($_POST['bill'])?$_POST['bill']:"";
$diligence       = !empty($_POST['diligence'])?$_POST['diligence']:"";
$total           = !empty($_POST['total'])?$_POST['total']:"";

$fuel_litres = 0;
$fuel_costs = 0;
$blank_charges = 0;
$allowances = 0;
$kog_expenses = 0;
$job_ended_acc_expenses = 0;
$allowance_oths = 0;
$job_ended_expressways = 0;
$job_ended_recaps = 0;
$total_amount_allowances = 0;
$total_amount_receives = 0;
$withholdings = 0;
$job_order_profits = 0;

$numRow = 0;
$con = "";
$readonly = "";
if($id_h != ""){
  $readonly = 'readonly';
}

if($trailerId != "")
{
  $trailerIdArr = explode(":", $trailerId);
  $trailerId    = $trailerIdArr[0];

  $con .= " and jo.trailer_id ='". $trailerId ."'";
}
if($employeeId != "")
{
  $con .= " and jo.employee_id ='". $employeeId ."'";
}
if($source != "")
{
  $con .= " and jo.source LIKE '". $source ."%'";
}
if($destination != "")
{
  $con .= " and jo.destination LIKE '". $destination ."%'";
}
?>
<div align="center" style="font-size:16px;padding:5px;"><b>รายงานสรุปรายรับ - รายจ่าย</b></div>
<div align="center" style="font-size:14px;padding:5px;">
  <b>
    ประจำวันที่ <?= formatDateTh($startDate) ?> ถึงวันที่ <?= formatDateTh($endDate) ?>
  </b>
</div>
<table class="table table-bordered table-striped" id="tableDisplay" style="font-size:11px;">
  <thead>
    <tr>
			<th style="width:30px;">ลำดับ</th>
      <th style="width:75px;">วันที่สั่งจ้าง</th>
      <th style="width:120px;">ชื่อคนขับ</th>
      <th style="width:65px;">ทะเบียน</th>
			<th >ต้นทาง</th>
			<th >ปลายทาง</th>
      <th >จำนวนลิตร</th>
			<th >ค่าน้ำมัน</th>
      <th >ค่าตีเปล่า</th>
      <th >เบี้ยเลี้ยง</th>
      <th >หางคอก</th>
      <th >อื่นๆ</th>
      <th >ทางด่วน</th>
      <th >ปะยาง</th>
      <th >รวมค่าใช้จ่าย</th>
      <th >ราคาค่าขนส่ง</th>
      <th >หัก ณ ที่จ่าย</th>
      <th >กำไรเบื้องต้น</th>
		</tr>
  </thead>
  <tbody>
<?php


  $con .= " and jo.job_order_date between '". $startDate ."' and '". $endDate ."'";
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id and  jo.job_status_id = '2' and invoice_code is not null
  order by jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalTransfer = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/Y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $remark             = $row['remark'];
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];
    $job_order_profit         = $row['job_order_profit'];

    $invoice_code             = $row['invoice_code'];
    $period_code              = $row['period_code'];
    $license_plate            = $row['license_plate'];


    // $transfer = ($fuel_cost + $allowance + $kog_expense + $blank_charge + $allowance_oth);
    // $totalTransfer +=  $transfer;
    //
    // $withholding  =  ($total_amount_receive * 0.01);
    //
    // $job_order_profit -= $withholding;


    $transfer = ($fuel_cost + $blank_charge + $allowance + $kog_expense  + $allowance_oth + $job_ended_expressway+ $job_ended_recap);
    $totalTransfer +=  $transfer;

    $job_order_profit = ($total_amount_receive - $transfer);

    $total_amount_allowance = $transfer;

    $withholding  =  ($total_amount_receive * 0.01);

    $job_order_profit -= $withholding;




    $fuel_litres            += $fuel_litre;
    $fuel_costs             += $fuel_cost;
    $blank_charges          += $blank_charge;
    $allowances             += $allowance;
    $kog_expenses           += $kog_expense;
    $job_ended_acc_expenses += $job_ended_acc_expense;
    $allowance_oths         += $allowance_oth;
    $job_ended_expressways  += $job_ended_expressway;
    $job_ended_recaps       += $job_ended_recap;
    $total_amount_allowances  += $total_amount_allowance;
    $total_amount_receives  += $total_amount_receive;
    $withholdings           += $withholding;
    $job_order_profits      += $job_order_profit;
?>
  <tr>
    <td class="text-center" ><?= $i ?></td>
    <td class="text-center" ><?= $job_order_date ?></td>
    <td class="text-left" ><?= $Employee_Name ?></td>
    <td class="text-center" ><?= $license_plate ?></td>
    <td class="text-left" ><?= $source ?></td>
    <td class="text-left" ><?= $destination ?></td>
    <td class="text-right" ><?= number_format($fuel_litre,2); ?></td>
    <td class="text-right" ><?= number_format($fuel_cost,2);  ?></td>
    <td class="text-right" ><?= number_format($blank_charge,2); ?></td>
    <td class="text-right" ><?= number_format($allowance,2); ?></td>
    <td class="text-right" ><?= number_format($kog_expense,2); ?></td>
    <td class="text-right" ><?= number_format($allowance_oth,2); ?></td>
    <td class="text-right" ><?= number_format($job_ended_expressway,2); ?></td>
    <td class="text-right" ><?= number_format($job_ended_recap,2); ?></td>
    <td class="text-right" ><?= number_format($total_amount_allowance,2); ?></td>
    <td class="text-right" ><?= number_format($total_amount_receive,2); ?></td>
    <td class="text-right" ><?= number_format($withholding,2); ?></td>
    <td class="text-right" ><?= number_format($job_order_profit,2); ?></td>
  </tr>
<?php
}
if($num == 0){
  ?>
  <tr>
    <td colspan="19" class="text-right" >&nbsp;</td>
  </tr>
  <?php
}
//  echo "<div>ไม่พบข้อมูล</div>";
//}
?>
<tr>
  <td colspan="6" class="text-right" ><b>รวม</b></td>
  <td class="text-right" ><b><?= number_format($fuel_litres,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($fuel_costs,2);  ?></b></td>
  <td class="text-right" ><b><?= number_format($blank_charges,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($allowances,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($kog_expenses,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($allowance_oths,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($job_ended_expressways,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($job_ended_recaps,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($total_amount_allowances,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($total_amount_receives,2); ?></b></td>
  <td class="text-right" ><b><?= number_format($withholdings,2); ?></b></td>
  <td class="text-right" ><b><div id="jobProfits"><?= number_format($job_order_profits,2); ?></div></b></td>
</tr>
<tr>
  <td colspan="11" align="right" style="vertical-align:middle;">
    <input type="text" class="form-control" value="<?= $txtOther1 ?>" style="width:150px;font-size:11px;" id="txtOther1" placeholder="รายการอื่นๆ" <?= $readonly ?>>
  </td>
  <td colspan="3" class="text-right" ><input type="text" style="font-size:11px;" class="form-control right" value="<?= $txtOther1 ?>" id="other1" placeholder="จำนวนเงิน" <?= $readonly ?>></td>
  <td colspan="2" class="text-right" style="vertical-align:middle;"><b>เงินเดือน : </b></td>
  <td colspan="2" class="text-right" ><input type="text" style="font-size:11px;" class="form-control right" value="<?= $txtOther1 ?>" id="salary" <?= $readonly ?>></td>
</tr>
<tr>
  <td colspan="11" align="right" style="vertical-align:middle;">
    <input type="text" class="form-control" value="<?= $txtOther2 ?>" style="width:150px;font-size:11px;" id="txtOther2" placeholder="รายการอื่นๆ" <?= $readonly ?>>
  </td>
  <td colspan="3" class="text-right" ><input type="text" style="font-size:11px;" class="form-control right" value="<?= $txtOther1 ?>" id="other2" placeholder="จำนวนเงิน" <?= $readonly ?>></td>
  <td colspan="2" class="text-right" style="vertical-align:middle;"><b>บิลน้ำมัน : </b></td>
  <td colspan="2" class="text-right" ><input type="text" style="font-size:11px;" class="form-control right" value="<?= $txtOther1 ?>" id="bill" <?= $readonly ?>></td>
</tr>
<tr>
  <td colspan="11" align="right" style="vertical-align:middle;">
    <input type="text" class="form-control" value="<?= $txtOther3 ?>" style="width:150px;font-size:11px;" id="txtOther3" placeholder="รายการอื่นๆ" <?= $readonly ?>>
  </td>
  <td colspan="3" class="text-right" ><input type="text" style="font-size:11px;" class="form-control right" value="<?= $txtOther1 ?>" id="other3" placeholder="จำนวนเงิน" <?= $readonly ?>></td>
  <td colspan="2" class="text-right" style="vertical-align:middle;"><b>เบี้ยขยัน : </b></td>
  <td colspan="2" class="text-right" ><input type="text" style="font-size:11px;" class="form-control right" value="<?= $txtOther1 ?>" id="diligence" <?= $readonly ?>></td>
</tr>
<tr>
  <td colspan="16" class="text-right" style="vertical-align:middle;"><b>รวม : </b></td>
  <td colspan="2" class="text-right" ><input type="text" style="font-size:11px;" class="form-control right" value="<?= $txtOther1 ?>" id="total" readonly></td>
</tr>
<tr>
  <td colspan="19" class="text-center" >
    <button type="button" class="btn btn-flat " onclick="calAmount()" style="width:100px;">
      <i class="fa fa-calculator"></i> คำนวณ</button>
    <button type="button" class="btn btn-primary btn-flat" style="width:100px;" onclick="printOrder()">
    <i class="fa fa-print"></i> Print</button>
    <input type="hidden" value="<?= $id_h ?>" id="id_h" >
  </td>
</tr>
</tbody>
</table>
