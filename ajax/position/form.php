<?php

include('../../conf/connect.php');
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php

$position_id    = isset($_POST['value'])?$_POST['value']:"";
$position_no    = "";
$position_name  = "";

if(!empty($position_id))
{
    $sql = "SELECT * FROM tb_position_master WHERE position_id = '$position_id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $position_id    = $row['position_id'];
    $position_no    = $row['position_no'];
    $position_name  = $row['position_name'];
}
?>
<div class="row">
  <div class="col-md-4">
    <div class="form-group">
      <label>รหัสตำแหน่ง</label>
      <input value="<?= $position_no ?>" name="position_no" type="text" class="form-control" placeholder="รหัสตำแหน่ง" required>
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      <label>ตำแหน่ง</label>
      <input value="<?= $position_name ?>" name="position_name" type="text" class="form-control" placeholder="ชื่อลูกค้า" required>
    </div>
  </div>
<input  type="hidden" value="<?= $position_id ?>" name="position_id" type="text" >
