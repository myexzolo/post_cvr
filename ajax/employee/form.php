<?php

include('../../conf/connect.php');
include('../../inc/utils.php');
session_start();

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php

$employee_id    = isset($_POST['value'])?$_POST['value']:"";
$employee_no    = "";
$employee_name  = "";
$position_id    = "";
$address        = "";
$tax            = "";
$address        = "";
$account_no     = "";
$tel            = "";
$department_id  = "";
$hire_date      = "";
$status         = "";

if(!empty($employee_id))
{
    $sql = "SELECT * FROM tb_employee_master WHERE employee_id = '$employee_id'";
    $query = mysqli_query($conn,$sql);
    $row = mysqli_fetch_assoc($query);

    $employee_id    = $row['employee_id'];
    $employee_no    = $row['employee_no'];
    $employee_name  = $row['employee_name'];
    $position_id    = $row['position_id'];
    $address        = $row['address'];
    $tax            = $row['tax'];
    $address        = $row['address'];
    $account_no     = $row['account_no'];
    $tel            = $row['tel'];
    $department_id  = $row['department_id'];
    $hire_date      = $row['hire_date'];
    $status         = $row['status'];
}


  $optionDep  = getoptionDep($department_id);
  $optionPos  = getoptionPos($position_id);
?>
<div class="row">
  <div class="col-md-2">
    <div class="form-group">
      <label>รหัสพนักงาน</label>
      <input value="<?= $employee_no ?>" name="employee_no" type="text" class="form-control" placeholder="รหัสพนักงาน" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>ชื่อพนักงาน</label>
      <input value="<?= $employee_name ?>" name="employee_name" type="text" class="form-control" placeholder="ชื่อพนักงาน" required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>ตำแหน่ง</label>
      <select name="position_id" class="form-control select2" style="width: 100%;" required >
        <option value="" ></option>
        <?= $optionPos ?>
      </select>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เลขที่ประจำตัวผู้เสียภาษี</label>
      <input value="<?= $tax ?>" name="tax" type="text" class="form-control" placeholder="tax" >
    </div>
  </div>
  <div class="col-md-9">
    <div class="form-group">
      <label>ที่อยู่</label>
      <input value="<?= $address ?>" name="address" type="text" class="form-control" placeholder="Address .." >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>วันที่รับทำงาน</label>
      <input value="<?= $hire_date ?>" name="hire_date" type="date" class="form-control" placeholder="วันที่รับทำงาน" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เลขที่บัญชี</label>
      <input value="<?= $account_no ?>" name="account_no" type="text" class="form-control" placeholder="" >
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>โทรศัพท์</label>
      <input value="<?= $tel ?>" name="tel" type="text" class="form-control" placeholder="Phone" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>สังกัด</label>
      <select name="department_id"  id="department_id" class="form-control select2" style="width: 100%;" required >
        <option value="" ></option>
        <?= $optionDep ?>
      </select>
    </div>
  </div>
  <div class="col-md-2">
    <div class="form-group">
      <label>สถานะ</label>
      <select name="status"  class="form-control select2" style="width: 100%;" required >
        <option value="1" <?= ($status == '1' ? 'selected="selected"':'') ?>>ทำงาน</option>
        <option value="2" <?= ($status == '2' ? 'selected="selected"':'') ?>>ลาออก</option>
      </select>
    </div>
  </div>
</div>

<input  type="hidden" value="<?= $employee_id ?>" name="employee_id" type="text" >
