<?php

include('../../conf/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px" class="text-center">No.</th>
      <th class="text-center">รหัสพนักงาน</th>
      <th class="text-center">ชื่อพนักงาน</th>
      <th class="text-center">ตำแหน่ง</th>
      <th class="text-center">โทรศัพท์</th>
      <th class="text-center">เงินเดือน</th>
      <th class="text-center">สถานะ</th>
      <th style="width:100px;"></th>
    </tr>
  </thead>
  <tbody>
<?php
  $sql = "SELECT e.*,p.position_name
  FROM  tb_employee_master e , tb_position_master p
  where e.position_id = p.position_id";
  $query = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);

?>
    <tr class="text-center">
      <td><?= $i ?></td>
      <td class="text-left"><?= $row['employee_no']; ?></td>
      <td class="text-left"><?= $row['employee_name']; ?></td>
      <td class="text-left"><?= $row['position_name']; ?></td>
      <td class="text-left" ><?= $row['tel']; ?></td>
      <td class="text-right"><?= number_format($row['salary'],2); ?></td>
      <td class="text-center"><?= ($row['status'] == '1' ? 'ทำงาน':'ลาออก') ?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm btn-flat" onclick="showForm(<?= $row['employee_id']; ?>)">แก้ไข</button>
        <button type="button" class="btn btn-danger btn-sm btn-flat" onclick="removeRow(<?= $row['employee_id']; ?>)">ลบ</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'aLengthMenu' : [[50, 100, -1], [50, 100,"All"]],
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })

</script>
