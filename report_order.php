<!DOCTYPE html>
<html>

<?php include("inc/head.php");
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<style>

@page {
  margin : 0mm;
  size: A4 landscape;
}
@media print {
  html, body {
    margin-top : 0mm;
    margin-right: 4mm;
    margin-left: 4mm;
    margin-bottom: 0mm;
  }
}
</style>
<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php
  include("inc/header.php");
  include("inc/utils.php");
?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>รายงานใบสั่งจ้าง (PO) รายวัน</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>รายงาน</li>
        <li class="active">รายงานใบสั่งจ้าง (PO)</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          $optionCustomer   = getoptionCustomer("");
          $TrailerNo        = getoptionTrailerLicense("");
          $optionEmp        = getoptionEmp("","");

          $date         = date('Y-m-d');
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));
      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black">
          <div class="panel-heading">ค้นหาใบสั่งจ้าง (PO)</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">จากวันที่ : </td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $startDate ?>" id="startDate">
                        <input type="hidden" value="<?= $startDate ?>" id="startDateTmp">
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ถึงวันที่ :</td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $endDate ?>" id="endDate">
                        <input type="hidden" value="<?= $endDate ?>" id="endDateTmp">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">สถานะ :</td>
                      <td style="padding:5px;" align="left">
                        <select id="status" class="form-control select2" required >
                          <option value="1" >รอการอนุมัติ</option>
                          <option value="2" selected>อนุมัติเรียบร้อย</option>
                          <option value="3" >ยกเลิกสั่งงาน</option>
                          <option value="" >สถานะทั้งหมด</option>
                        </select>
                      </td>
                      <td style="padding:5px;" align="right">ชื่อพนักงานขับรถ :</td>
                      <td style="padding:5px;" align="left">
                        <select id="employeeId" class="form-control select2" style="width: 100%;" required >
                          <option value="" ></option>
                          <?= $optionEmp ?>
                        </select>
                    </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">เลขที่ใบสั่ง :</td>
                      <td style="padding:5px;" align="left">
                        <input type="text" class="form-control" value="" id="jobOrderNo">
                      </td>
                      <td style="padding:5px;" align="right">DP : </td>
                      <td style="padding:5px;" align="left">
                          <input type="text" class="form-control" value="" id="dp">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">ทะเบียนรถ : </td>
                      <td style="padding:5px;" align="left">
                        <select id="trailerId" class="form-control select2" style="width: 100%;" required>
                          <option value="" ></option>
                          <?= $TrailerNo ?>
                        </select>
                      </td>
                      <td style="padding:5px;width:150px;" align="right">SPAC : </td>
                      <td style="padding:5px;" align="left">
                        <input type="text" class="form-control" value="" id="awaySpecNo">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">สถานะวางบิล : </td>
                      <td style="padding:5px;" align="left">
                        <select id="statusInv" class="form-control select2" style="width: 100%;" required >
                          <option value="" >ทั้งหมด</option>
                          <option value="1" >ค้างวางบิล</option>
                          <option value="2" >วางบิลแล้ว</option>
                        </select>
                      </td>
                      <td style="padding:5px;width:150px;" align="right">สินค้า : </td>
                      <td style="padding:5px;" align="left">
                        <input type="text" class="form-control" value="" id="productName">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">ชื่อลูกค้า :</td>
                      <td style="padding:5px;" align="left" colspan="2">
                        <select id="cust_id" class="form-control select2" style="width: 100%;" required >
                          <option value="" >ทั้งหมด</option>
                          <?= $optionCustomer ?>
                        </select>
                      </td>
                      <td></td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="search()" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="reset()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
        <div class="panel panel-black">
          <div class="panel-heading">รายงานใบสั่งจ้าง (PO) รายวัน

            <button class="btn btn-success pull-right btn-flat" style="position: relative;top:-7px;right:-12px;width:100px;" onclick="exportExcel()">
              <i class="fa fa-file-excel-o"></i> Excel</button>
              <button class="btn btn-primary pull-right btn-flat" style="position: relative;top:-7px;width:100px;" onclick="printOrder()">
                <i class="fa fa-print"></i> Print</button>
          </div>
            <div class="box-body" >
              <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
                  <div id="show-page" >
                    <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                  </div>
              </form>
            </div>
      <!--  # coding -->
      </div>

      <div style="display:none;">
        <div id="printableArea">
          <div id="show-page-exp" >
            <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
          </div>
      </div>
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/report_order.js" type="text/javascript"></script>
<script>
  $(document).ready(function() {
    search();
  });
</script>
</body>
</html>
