<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>
<style>
@page {
  margin-top: 0mm;
  margin-left: 4mm;
  margin-right: 4mm;
  margin-bottom: 0mm;
  size: landscape;
}
@media print {
  html, body {
    background-color:#FFFFFF;
    margin: 0mm;
  }
  .textRed {
       color: red !important;
   }
  /* ... the rest of the rules ... */
}

</style>
<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php
  include("inc/header.php");
  include("inc/utils.php");
?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>ประมวลผลเบี้ยเลี้ยง</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>ข้อมูลรายวัน</li>
        <li class="active">ประมวลผลเบี้ยเลี้ยง</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          $date         = date('Y-m-d');
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));

          $optionEmp    = getoptionEmp("","");
      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black">
          <div class="panel-heading">ค้นหาข้อมูลใบสั่งงาน</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">จากวันที่: </td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $startDate ?>" id="startDate">
                        <input type="hidden" value="<?= $startDate ?>" id="startDateTmp">
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ถึงวันที่ :</td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $endDate ?>" id="endDate">
                        <input type="hidden" value="<?= $endDate ?>" id="endDateTmp">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="search()" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="reset()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
        <div class="panel panel-black">
          <div class="panel-heading">รายการข้อมูลใบสั่งงาน
              <button class="btn btn-success pull-right btn-flat" style="position: relative;top:-7px;right:-12px" onclick="postURL('allowance.php')">+ ออกเบี้ยเลี้ยง</button>
          </div>
            <div class="box-body" >
              <div style="padding:5px" align="center">
              </div>
                <div style="width:100%;">
                  <div id="show-page" >
                    <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                  </div>
                </div>
                <div style="margin-top:5px;" align="right">
            </div>
      <!--  # coding -->
      </div>
      <div style="width:100%;display:none;">
        <div id="printableArea">
          <div id="show-page-exp" >
            <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
          </div>
          <div id="signature">
              <table class="table" style="width:100%;border: 1px solid #000000;font-size:11px;">
                  <tr>
                    <td style="width:16.66%;border: 1px solid #000000;" class="text-center">
                      <p>ผู้จัดทำ</p><br>
                      <p>...................................................</p>
                      <p>(.........../.........../...........)</p>
                    </td>
                    <td style="width:16.66%;border: 1px solid #000000;" class="text-center">
                      <p>ผู้ตรวจสอบคนที่ 1</p><br>
                      <p>...................................................</p>
                      <p>(.........../.........../...........)</p>
                    </td>
                    <td style="width:16.66%;border: 1px solid #000000;" class="text-center">
                      <p>ผู้ตรวจสอบคนที่ 2</p><br>
                      <p>...................................................</p>
                      <p>(.........../.........../...........)</p>
                    </td>
                    <td style="width:16.66%;border: 1px solid #000000;" class="text-center">
                      <p>ผู้อนุมัติ</p><br>
                      <p>...................................................</p>
                      <p>(.........../.........../...........)</p>
                    </td>
                    <td style="width:16.66%;border: 1px solid #000000;" class="text-center">
                      <p>ผู้โอนเงิน</p><br>
                      <p>...................................................</p>
                      <p>(.........../.........../...........)</p>
                    </td>
                    <td style="width:16.66%;border: 1px solid #000000;" class="text-center">
                      <p>บัญชี ตรวจสอบ</p><br>
                      <p>...................................................</p>
                      <p>(.........../.........../...........)</p>
                    </td>
                  </tr>
              </table>
          </div>
      </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/allowance.js?v=1" type="text/javascript"></script>
<script>
  $(document).ready(function() {
    search();
  });
</script>
</body>
</html>
