<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>

        <small>ข้อมูลยี่ห้อรถ</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li class="active">ข้อมูลยี่ห้อรถ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black" >
          <div class="panel-heading">รายการข้อมูลยี่ห้อรถ
              <button class="btn btn-success btnAdd" onclick="showForm()">+ เพิ่มยี่ห้อรถ</button>
          </div>
            <div class="box-body">
              <div id="show-page"><div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i><div></div>
          </div>
        </div>
      </div>
      <!--  # coding -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">ข้อมูลชื่อยี่ห้อ</h4>
        </div>
        <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
		    <div class="modal-body" id="show-form"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
  </div>

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->
<?php include("inc/footer.php"); ?>
<script src="js/trailer_brand.js" type="text/javascript"></script>
</body>
</html>
