<?php
session_start();
include('conf/connect.php');
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ระบบบริหารจัดการขนส่ง</title>

    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap/css/main.css">
    <!-- Smoke -->
    <link rel="stylesheet" href="css/smoke.css">

    <!-- lightbox -->
    <link rel="stylesheet" href="css/lightbox.css" >
    <link rel="stylesheet" href="css/select2.min.css" >


    <!-- Font Awesome -->
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="Ionicons/css/ionicons.min.css">

    <!-- fullCalendar -->
    <!-- <link rel="stylesheet" href="fullcalendar/dist/fullcalendar.min.css"> -->
    <link rel="stylesheet" href="fullcalendar/dist/fullcalendar.print.min.css" media="print">

    <!-- daterange picker -->
    <link rel="stylesheet" href="bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="iCheck/all.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

	  <link rel="stylesheet" href="css/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="dist/css/skins/_all-skins.css">

    <link rel="stylesheet" href="css/utils.css" >


  </head>
  <style>
  body {
      font-family: "Kanit","Segoe UI";
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      font-variant-numeric: tabular-nums;
  }
  html, h1, h2, h3, h4, p ,span{
    font-family: "Kanit","Segoe UI";
    font-variant-numeric: tabular-nums;
  }

  @font-face {
      font-family: "Kanit";
      font-variant-numeric: tabular-nums;
      src: url("fonts/Kanit/Kanit-Light.ttf");
  }

  @font-face {
      font-family: "THSarabun";
      font-variant-numeric: tabular-nums;
      src: url("fonts/THSarabun/THSarabun.ttf");
  }

  .fontTHSarabun
  {
      font-size: 18px;
      font-family: "THSarabun";
      src: url("fonts/THSarabun/THSarabun.ttf");
  }

  @font-face {
      font-family: "CSChatThai";
      src: url("fonts/CSChatThai/CSChatThai.ttf");
  }

  .fontChatThai {
    font-size: 20px;
    font-family: "CSChatThai";
    src: url("fonts/CSChatThai/CSChatThai.ttf");
  }

  .fontKanit {
      font-family: "Kanit";
      src: url("fonts/Kanit/Kanit-Light.ttf");
  }
  .btn_point{
    cursor: pointer;
  }

  .btn_point:hover
  {
    opacity: 0.5;
  }
  </style>
