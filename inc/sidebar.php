<?php
include('auth_admin.php');

  $role_List = "";
  $page_List = "";

  $menu = "";

  $admin_user = $_SESSION['user_id'];
  $sql = "SELECT * FROM t_user WHERE user_id = '$user_id'";
  $query = mysqli_query($conn,$sql);
  $row = mysqli_fetch_assoc($query);


  if(isset($row['role_list'])){
    $role_List = substr($row['role_list'] , 1);
  }

  $sql = "SELECT * FROM t_role WHERE role_id in ($role_List) and is_active = 'Y' ";
  $queryRole  = mysqli_query($conn,$sql);
  //echo $sql;
  $numRole    = mysqli_num_rows($queryRole);

  for ($i=1; $i <= $numRole ; $i++) {
    $rowRole  = mysqli_fetch_assoc($queryRole);
    $pl       = $rowRole['page_list'];

    $page_List .=  $pl;
  }

  if(!empty($page_List)){
    $page_List = substr($page_List , 1);
  }

  $sql = "SELECT count(p.page_id) as num,p.module_id,m.*  FROM t_page p
          INNER JOIN t_module m ON p.module_id = m.module_id
          where p.page_id in ($page_List) and p.is_active = 'Y' and m.is_active = 'Y' and m.module_type = '1'
          GROUP BY p.module_id
          order by m.module_order asc;";

  $queryModule  = mysqli_query($conn,$sql);
  $numModule    = mysqli_num_rows($queryModule);


  $typeCompany = $_SESSION['typeCompany'];

  for ($i=1; $i <= $numModule ; $i++) {
    $rowModule  = mysqli_fetch_assoc($queryModule);
    $numMod          = $rowModule['num'];
    $module_id       = $rowModule['module_id'];
    $module_name     = $rowModule['module_name'];
    $module_icon     = $rowModule['module_icon'];


    $sql = "SELECT * FROM t_page WHERE module_id = $module_id and page_id in ($page_List) and is_active = 'Y' order by page_seq;";
    $queryPage  = mysqli_query($conn,$sql);
    $numPage    = mysqli_num_rows($queryPage);





    if($numPage == 1){
      $rowPage  = mysqli_fetch_assoc($queryPage);
      $page_id       = $rowPage['page_id'];
      $page_name     = $rowPage['page_name'];
      $page_path     = $rowPage['page_path'];
      $active = "";
      if(strstr($_SERVER['SCRIPT_NAME'], '$page_path')){
          $active =  'active';
      }


      if($typeCompany == 2){

        if($page_path == "po_list"){
          $page_path = "po_list2";
        }else if($page_path == "po_list"){
          $page_path = "job_list";
        }else if($page_path == "invoices_list"){
          $page_path = "invoices_list_post";
        }
      }

      $menu  .= "<li class='$active'>";
      $menu  .= "<a href='$page_path.php'>";
      $menu  .= "<i class='fa $module_icon'></i>";
      $menu  .= "<span>$module_name</span></a></li>";
    }else{

      $active = '';
      if(strstr ( $_SERVER['SCRIPT_NAME'], '$page_path' )){
          $active =  'active';
      }

      $menu  .= "<li class='treeview $active' >";
      $menu  .= "<a href='#'>";
      $menu  .= "<i class='fa $module_icon'></i><span>$module_name</span>";
      $menu  .= "<span class='pull-right-container'>";
      $menu  .= "<i class='fa fa-angle-left pull-right'></i></span></a>";
      $menu  .= "<ul class='treeview-menu'>";

      for ($x=1; $x <= $numPage ; $x++) {
        $rowPage  = mysqli_fetch_assoc($queryPage);
        $page_id       = $rowPage['page_id'];
        $page_name     = $rowPage['page_name'];
        $page_path     = $rowPage['page_path'];

        if($typeCompany == 2){

          if($page_path == "po_list"){
            $page_path = "po_list2";
          }else if($page_path == "po_list"){
            $page_path = "job_list";
          }else if($page_path == "invoices_list"){
            $page_path = "invoices_list_post";
          }
        }

        $menu  .= "<li><a href='$page_path.php'><i class='fa fa-circle-o'></i>$page_name</a></li>";
      }
      $menu  .= "</ul></li>";
    }
  }
?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img width="150" src="<?= $row['user_img']; ?>" class="img-circle" onerror="this.onerror='';this.src='image/man.png'" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $row['user_name']; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
          <?= $menu ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
