<!-- jQuery 2.1.4 -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>

    <script src="js/jquery-confirm.min.js"></script>
    <script src='js/jquery.base64.js'></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="bootstrap/js/app.min.js"></script>
    <script src="bootstrap/js/smoke.js"></script>
    <script src="bootstrap/js/spin.min.js"></script>
    <script src="bootstrap/js/lightbox.js"></script>
    <script src="bootstrap/js/bootstrap-table.js"></script>

    <!-- bootstrap time picker -->
    <script src="timepicker/bootstrap-timepicker.min.js"></script>
    <!-- fullCalendar -->
    <script src="js/moment.js"></script>
    <script src="js/utils.js"></script>
    <script src="js/bootbox.all.min.js"></script>
    <script src="js/select2.full.min.js"></script>


    <script src="fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="iCheck/icheck.min.js"></script>
    <!-- date-range-picker -->
    <script src="bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>




    <script type="text/javascript">
      // document.addEventListener('DOMContentLoaded', function () {
      // if (!Notification) {
      //   alert('Desktop notifications not available in your browser. Try Chromium.');
      //   return;
      // }
      //
      // if (Notification.permission !== "granted")
      //   Notification.requestPermission();
      // });
      //
      // function notifyMe(body,link) {
      //   if (Notification.permission !== "granted")
      //     Notification.requestPermission();
      //   else {
      //     var notification = new Notification('Notification title', {
      //       icon: 'image/logo.jpg',
      //       body: "Hey there! You've been notified!",
      //     });
      //
      //     notification.onclick = function () {
      //       window.open("http://stackoverflow.com/a/13328397/1269037");
      //     };
      //   }
      // }


      function logout(){
        $.smkConfirm({
          text:'ARE YOU SURE?',
          accept:'Yes',
          cancel:'No'
        },function(res){
          // Code here
          if (res) {
            $.post( "ajax/logout/logout.php")
            .done(function( data ) {
              if(data.status == 'success'){
                $.smkProgressBar({
                  element:'body',
                  status:'start',
                  bgColor: '#000',
                  barColor: '#fff',
                  content: 'Loading...'
                });
                setTimeout(function(){
                  $.smkProgressBar({
                    status:'end'
                  });
                  window.location='login.php';
                }, 1000);
              }
            });
          }
        });
      }
    </script>

    <style>
    .loading {
      width: 100%;
      height: 100vh;
      background-color: #000000c2;
      position: fixed;
      top: 0;
      z-index: 10000;
      cursor: progress;
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .loading>img {
      width: 200px;
    }

    </style>
    <div id="loading" class="loading">
      <img src="images/logo2.png">
    </div>
