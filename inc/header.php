<style>
body {
    font-family: "Kanit";
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%
}
html{
  font-family: "Kanit";
}

.fontKanit{
  font-family: "Kanit";
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%
}

@font-face {
    font-family: "Kanit";
    src: url("../fonts/Kanit/Kanit-Light.ttf");
}


</style>

<header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>T</b>C</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>TRAILER</b> CONTROL</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" >
        <span class="sr-only fontKanit">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->

          <li class="dropdown user user-menu">
            <?php
              $user_id = "";
              if(isset($_SESSION['user_id'])){
                $user_id = $_SESSION['user_id'];
              }
              $sql = "SELECT * FROM t_user WHERE user_id = '$user_id'";
              $query = mysqli_query($conn,$sql);
              $row = mysqli_fetch_assoc($query);
            ?>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img width="160" src="<?= $row['user_img']; ?>" class="user-image" onerror="this.onerror='';this.src='image/man.png'" alt="User Image">
              <span class="hidden-xs"><?php echo $row['user_name']; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img width="160" src="<?= $row['user_img']; ?>" class="img-circle" onerror="this.onerror='';this.src='image/man.png'" alt="User Image">
                <p>
                  Name - <?php echo $row['user_name']; ?>
                  <small>Member ID. <?php echo $row['user_login']; ?></small>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a onclick="logout()" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>

    </nav>
  </header>
