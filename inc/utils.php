<?php

function monthThai($month){
  $arr = array('','ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');
  return $arr[$month];
}

function monthThaiFull($month){
  $arr = array('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
  return $arr[$month];
}

function getCustomerName($cusID){
    global $conn;

    $sql    = "SELECT * FROM tb_customer_master where cust_id = $cusID";
    $query  = mysqli_query($conn,$sql);

    $row         = mysqli_fetch_assoc($query);
    $cust_id     = $row['cust_id'];
    $Cust_Name   = $row['cust_name'];

    return $Cust_Name;
}

function getEmpName($id){
  global $conn;

  if($id != "")
  {
    $sql    = "SELECT * FROM tb_employee_master where employee_id = $id";
    $query  = mysqli_query($conn,$sql);

    $rowEmp         = mysqli_fetch_assoc($query);
    $Employee_id    = $rowEmp['employee_id'];
    $Employee_Name  = $rowEmp['employee_name'];

    return $Employee_Name;
  }else{
    return "";
  }

}

function getoptionEmp($EmployeeID,$con){
    global $conn;

    $optionEmp = "";
    $sqlEmp   = "SELECT * FROM tb_employee_master where status = 1 $con order by convert(employee_name using tis620)";
    $queryEmp = mysqli_query($conn,$sqlEmp);
    $numEmp   = mysqli_num_rows($queryEmp);

    for ($i=1; $i <= $numEmp; $i++) {
      $rowEmp           = mysqli_fetch_assoc($queryEmp);
      $Employee_id      = $rowEmp['employee_id'];
      $Employee_Name    = $rowEmp['employee_name'];

      $selected =  ' ';
      if($Employee_id == $EmployeeID && $EmployeeID != ""){
        $selected =  'selected="selected"';
      }
      $optionEmp .= '<option value="'.$Employee_id.'" '.$selected.'>'.$Employee_Name.'</option>';
    }
    return $optionEmp;
}


function getoptionPetrol($petrolId){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM tb_petrol_master order by convert(petrol_code using tis620)";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row           = mysqli_fetch_assoc($query);
      $petrol_id     = $row['petrol_id'];
      $petrol_code   = $row['petrol_code'];
      $petrol_name   = $row['petrol_name'];

      $value = $petrol_code."|".$petrol_name;

      $selected =  ' ';
      if($petrol_id == $petrolId && $petrolId != ""){
        $selected = 'selected="selected"';
      }

      $option .= '<option value="'.$value.'" '.$selected.'>'.$petrol_name.'</option>';
    }
    return $option;
}


function getoptionContract($contract){
    global $conn;

    $option = "";
    $sql  = "SELECT contract_no FROM  tb_route_price where contract_no is not null group by contract_no";
    $query = mysqli_query($conn,$sql);
    $num  = mysqli_num_rows($query);


    for ($i=0; $i < $num; $i++) {
      $row          = mysqli_fetch_assoc($query);
      $contract_no     = $row['contract_no'];

      $selected =  ' ';
      if($contract_no == $contract && $contract != ""){
        $selected = 'selected="selected"';
      }
      if($contract_no != ""){
        $option .= '<option value="'.$contract_no.'" '.$selected.'>'.$contract_no.'</option>';
      }

    }
    return $option;
}

function getoptionRoute(){
    global $conn;

    $option = "";
    $sql  = "SELECT route_id,source,destination FROM tb_route_price";
    $query = mysqli_query($conn,$sql);
    $num  = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row          = mysqli_fetch_assoc($query);
      $route_id     = $row['route_id'];
      $source       = $row['source'];
      $destination  = $row['destination'];

      $val   = $route_id.":".$source."-".$destination;

      $option .= '<option value="'.$route_id.'">'.$route_id.'</option>';
    }
    return $option;
}


function getoptionProvince($provinceId){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_province_master order by convert(province_name_th using tis620)";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row              = mysqli_fetch_assoc($query);
      $province_id        = $row['province_id'];
      $province_name_th   = $row['province_name_th'];

      $selected =  ' ';
      if($province_id == $provinceId && $provinceId != ""){
        $selected = 'selected="selected"';
      }

      $option .= '<option value="'.$province_id.'" '.$selected.'>'.$province_name_th.'</option>';
    }
    return $option;
}



function getoptionVendors($vendorsId){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_vendors order by convert(vendors_name using tis620)";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row              = mysqli_fetch_assoc($query);
      $vendors_id       = $row['vendors_id'];
      $vendors_name    = $row['vendors_name'];

      $selected =  ' ';
      if($vendors_id == $vendorsId && $vendorsId != ""){
        $selected = 'selected="selected"';
      }

      $option .= '<option value="'.$vendors_id.'" '.$selected.'>'.$vendors_name.'</option>';
    }
    return $option;
}


function getoptionTrailerCategory($trailerCategoryId){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_trailer_category order by convert(trailer_category_name using tis620)";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row              = mysqli_fetch_assoc($query);
      $trailer_category_id   = $row['trailer_category_id'];
      $trailer_category_name = $row['trailer_category_name'];

      $selected =  ' ';
      if($trailer_category_id == $trailerCategoryId && $trailerCategoryId != ""){
        $selected = 'selected="selected"';
      }

      $option .= '<option value="'.$trailer_category_id.'" '.$selected.'>'.$trailer_category_name.'</option>';
    }
    return $option;
}

function getoptionTrailerBrand($trailerBrandId){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_trailer_brand order by trailer_brand";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row              = mysqli_fetch_assoc($query);
      $trailer_brand_id   = $row['trailer_brand_id'];
      $trailer_brand      = $row['trailer_brand'];

      $selected =  ' ';
      if($trailer_brand_id == $trailerBrandId && $trailerBrandId != ""){
        $selected = 'selected="selected"';
      }

      $option .= '<option value="'.$trailer_brand_id.'" '.$selected.'>'.$trailer_brand.'</option>';
    }
    return $option;
}

function getoptionTrailerType($trailerTypeId){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_trailer_type order by  convert(trailer_type_name using tis620)";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row              = mysqli_fetch_assoc($query);
      $trailer_type_id    = $row['trailer_type_id'];
      $trailer_type_name  = $row['trailer_type_name'];

      $selected =  ' ';
      if($trailer_type_id == $trailerTypeId && $trailerTypeId != ""){
        $selected = 'selected="selected"';
      }

      $option .= '<option value="'.$trailer_type_id.'" '.$selected.'>'.$trailer_type_name.'</option>';
    }
    return $option;
}

function getoptionAffiliation($affiliationId){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_trailer_affiliation";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row              = mysqli_fetch_assoc($query);
      $affiliation_id     = $row['affiliation_id'];
      $affiliation_name   = $row['affiliation_name'];

      $selected =  ' ';
      if($affiliation_id == $affiliationId && $affiliationId != ""){
        $selected = 'selected="selected"';
      }

      $option .= '<option value="'.$affiliation_id.'" '.$selected.'>'.$affiliation_name.'</option>';
    }
    return $option;
}

function getoptionPos($positionId){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_position_master";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row              = mysqli_fetch_assoc($query);
      $position_id      = $row['position_id'];
      $position_name    = $row['position_name'];

      $selected =  ' ';
      if($position_id == $positionId && $positionId != ""){
        $selected =  'selected="selected"';
      }

      $option .= '<option value="'.$position_id.'" '.$selected.'>'.$position_name.'</option>';
    }
    return $option;
}

function getoptionDep($departmentId){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_department_master order by convert(department_name using tis620)";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row              = mysqli_fetch_assoc($query);
      $department_id      = $row['department_id'];
      $department_name    = $row['department_name'];

      $selected =  ' ';
      if($department_id == $departmentId && $departmentId != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$department_id.'" '.$selected.'>'.$department_name.'</option>';
    }
    return $option;
}

function getoptionDepByAffiliation($Id,$departmentId){
    //1=รถบริษัท  2=รถร่วมบริษัท  3=รถร่วม
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_department_master where affiliation_id =".$Id. " order by convert(department_name using tis620)";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row              = mysqli_fetch_assoc($query);
      $department_id      = $row['department_id'];
      $department_name    = $row['department_name'];

      $selected =  ' ';
      if($department_id == $departmentId && $departmentId != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$department_id.'" '.$selected.'>'.$department_name.'</option>';
    }
    return $option;
}

function getoptionDeliveryType($deliveryTypeId){
    global $conn;

    $option = "";
    $sql   = "SELECT * FROM tb_delivery_type order by convert(delivery_type_name using tis620)";
    $query = mysqli_query($conn,$sql);
    $num   = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row           = mysqli_fetch_assoc($query);
      $delivery_type_id   = $row['delivery_type_id'];
      $delivery_type_name = $row['delivery_type_name'];

      $selected =  ' ';
      if($delivery_type_id == $deliveryTypeId && $deliveryTypeId != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$delivery_type_id.'" '.$selected.'>'.$delivery_type_name.'</option>';
    }
    return $option;
}


function getoptionCustomer($CustID){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM tb_customer_master order by convert(cust_name using tis620)";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row         = mysqli_fetch_assoc($query);
      $cust_id     = $row['cust_id'];
      $Cust_Name   = $row['cust_name'];

      $selected =  ' ';
      if($cust_id == $CustID && $CustID != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$cust_id.'" '.$selected.'>'.$Cust_Name.'</option>';
    }
    return $option;
}


function getLicensePlate($trailerID){
  $License_Plate = "";
  if($trailerID != ""){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM tb_trailer where trailer_id = $trailerID";
    $query  = mysqli_query($conn,$sql);
    $row         = mysqli_fetch_assoc($query);
    $License_Plate  = $row['license_plate'];
  }
    return $License_Plate;
}

function getoptionTrailerNo($trailerID){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_trailer order by license_plate";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row         = mysqli_fetch_assoc($query);
      $trailer_id     = $row['trailer_id'];
      $License_Plate  = $row['license_plate'];
      $affiliation_id = $row['affiliation_id'];

      $selected =  ' ';
      if($trailer_id == $trailerID && $trailerID != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$trailer_id.':'.$affiliation_id.'" '.$selected.'>'.$License_Plate.'</option>';
    }
    return $option;
}


function getoptionTrailerLicense($trailerID){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_trailer order by license_plate";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row         = mysqli_fetch_assoc($query);
      $trailer_id     = $row['trailer_id'];
      $License_Plate  = $row['license_plate'];
      $affiliation_id = $row['affiliation_id'];

      $selected =  ' ';
      if($trailer_id == $trailerID && $trailerID != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$trailer_id.'" '.$selected.'>'.$License_Plate.'</option>';
    }
    return $option;
}


function getoptionTrailerNoByAffiliation($trailerID,$affiliationId){
    global $conn;

    $option = "";
    $sql    = "SELECT * FROM  tb_trailer where affiliation_id = '$affiliationId' order by license_plate";
    $query  = mysqli_query($conn,$sql);
    $num    = mysqli_num_rows($query);

    for ($i=1; $i <= $num; $i++) {
      $row         = mysqli_fetch_assoc($query);
      $trailer_id     = $row['trailer_id'];
      $License_Plate  = $row['license_plate'];
      $affiliation_id = $row['affiliation_id'];

      $selected =  ' ';
      if($trailer_id == $trailerID && $trailerID != ""){
        $selected =  'selected="selected"';
      }
      $option .= '<option value="'.$trailer_id.':'.$affiliation_id.'" '.$selected.'>'.$License_Plate.'</option>';
    }
    return $option;
}



function convert($number){
  $txtnum1 = array('ศูนย์','หนึ่ง','สอง','สาม','สี่','ห้า','หก','เจ็ด','แปด','เก้า','สิบ');
  $txtnum2 = array('','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน');
  $number = str_replace(",","",$number);
  $number = str_replace(" ","",$number);
  $number = str_replace("บาท","",$number);
  $number = explode(".",$number);
  if(sizeof($number)>2){
    $number = number_format($number, 2);
  }

  $strlen = strlen($number[0]);
  $convert = '';
  for($i=0;$i<$strlen;$i++){
  	$n = substr($number[0], $i,1);
  	if($n!=0){
  		if($i==($strlen-1) AND $n==1){ $convert .= 'เอ็ด'; }
  		elseif($i==($strlen-2) AND $n==2){  $convert .= 'ยี่'; }
  		elseif($i==($strlen-2) AND $n==1){ $convert .= ''; }
  		else{ $convert .= $txtnum1[$n]; }
  		$convert .= $txtnum2[$strlen-$i-1];
  	}
  }
  $convert .= 'บาท';
  if($number[1]=='0' || $number[1]=='00' || $number[1]==''){
  $convert .= 'ถ้วน';
  }else{
  $strlen = strlen($number[1]);
  for($i=0;$i<$strlen;$i++){
  $n = substr($number[1], $i,1);
  	if($n!=0){
  	if($i==($strlen-1) AND $n==1){$convert
  	.= 'เอ็ด';}
  	elseif($i==($strlen-2) AND
  	$n==2){$convert .= 'ยี่';}
  	elseif($i==($strlen-2) AND
  	$n==1){$convert .= '';}
  	else{ $convert .= $txtnum1[$n];}
  	$convert .= $txtnum2[$strlen-$i-1];
  	}
  }
  $convert .= 'สตางค์';
  }
  return $convert;
}


function resizeImageToBase64($obj,$h,$w,$quality,$user_id_update,$path) {
  $img = "";
  if(isset($obj) && !empty($obj["name"]))
  {
    if(getimagesize($obj['tmp_name']))
    {
      $ext = pathinfo($obj["name"], PATHINFO_EXTENSION);
      if($ext == 'gif' || $ext !== 'png' || $ext !== 'jpg' )
      {
        if(!empty($h) || !empty($w))
        {
          $filePath       = $obj['tmp_name'];
          $image          = addslashes($filePath);
          $name           = addslashes($obj['name']);
          $new_images     = "thumbnails_".$user_id_update;

          $x  = 0;
          $y  = 0;

          list($width_orig, $height_orig) = getimagesize($filePath);

          if(empty($h)){
              if($width_orig > $w){
                $new_height  = $height_orig*($w/$width_orig);
                $new_width   = $w;
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else if(empty($w))
          {
              if($height_orig > $h){
                $new_height  = $h;
                $new_width   = $width_orig*($h/$height_orig);
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else
          {
            if($height_orig > $width_orig)
            {
              $new_height  = $height_orig*($w/$width_orig);
              $new_width   = $w;
            }else{
              $new_height  = $h;
              $new_width   = $width_orig*($h/$height_orig);
            }
          }

          $create_width   =  $new_width;
          $create_height  =  $new_height;


          if(!empty($h) && $new_height > $h){
             $create_height = $h;
          }

          if(!empty($w) && $new_width > $w){
            $create_width = $w;
          }


          $imageOrig;
          $imageResize    = imagecreatetruecolor($create_width, $create_height);
          $background     = imagecolorallocatealpha($imageResize, 255, 255, 255, 127);
          imagecolortransparent($imageResize, $background);
          imagealphablending($imageResize, false);
          imagesavealpha($imageResize, true);

          if($ext == 'png'){
            $imageOrig      = imagecreatefrompng($filePath);
            $new_images     = $new_images.".png";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagepng($imageResize,$path.$new_images);
          }else if ($ext == 'jpg'){
            $imageOrig      = imagecreatefromjpeg($filePath);
            $new_images     = $new_images.".jpg";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagejpeg($imageResize,$path.$new_images);
          }else if ($ext == 'gif'){
            $imageOrig      = imagecreatefromgif($filePath);
            $new_images     = $new_images.".gif";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagegif($imageResize,"$path".$new_images);
          }

          imagedestroy($imageOrig);
          imagedestroy($imageResize);

          $image   = file_get_contents($path.$new_images);
          $img     = 'data:image/png;base64,'.base64_encode($image);
        }else{
          $image   = file_get_contents($path.$name);
          $img     = 'data:image/png;base64,'.base64_encode($image);
        }

      }
    }
  }
  return $img;
}




function resizeImageBase64($imgBase,$h,$w,$quality,$user_id_update,$path) {
  $img = "";
  if(isset($imgBase) && !empty($imgBase))
  {
    if(!empty($h) || !empty($w))
    {
      $imageOrig      = imagecreatefromstring(base64_decode($imgBase));
      $new_images     = "thumbnails_".$user_id_update;

      $x  = 0;
      $y  = 0;

      $width_orig   = 400;
      $height_orig  = 300;

      if(empty($h)){
          if($width_orig > $w){
            $new_height  = $height_orig*($w/$width_orig);
            $new_width   = $w;
          }else{
            $new_height  = $height_orig;
            $new_width   = $width_orig;
          }
      }
      else if(empty($w))
      {
          if($height_orig > $h){
            $new_height  = $h;
            $new_width   = $width_orig*($h/$height_orig);
          }else{
            $new_height  = $height_orig;
            $new_width   = $width_orig;
          }
      }
      else
      {
        if($height_orig > $width_orig)
        {
          $new_height  = $height_orig*($w/$width_orig);
          $new_width   = $w;
        }else{
          $new_height  = $h;
          $new_width   = $width_orig*($h/$height_orig);
        }
      }

      $create_width   =  $new_width;
      $create_height  =  $new_height;


      if(!empty($h) && $new_height > $h){
         $create_height = $h;
      }

      if(!empty($w) && $new_width > $w){
        $create_width = $w;
      }


      $imageOrig;
      $imageResize    = imagecreatetruecolor($create_width, $create_height);
      $background     = imagecolorallocatealpha($imageResize, 255, 255, 255, 127);
      imagecolortransparent($imageResize, $background);
      imagealphablending($imageResize, false);
      imagesavealpha($imageResize, true);

      $new_images     = $new_images.".png";
      imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
      imagepng($imageResize,$path.$new_images);


      imagedestroy($imageOrig);
      imagedestroy($imageResize);

      $image   = file_get_contents($path.$new_images);
      $img     = 'data:image/png;base64,'.base64_encode($image);
    }
  }
  return $img;
}


function getAffiliationName($id){
  $str = "";
  if($id == '1'){
    $str = "รถบริษัท";
  }else if($id == '2'){
    $str = "รถร่วมบริษัท";
  }else if($id == '3'){
    $str = "รถร่วม";
  }

  return $str;

}


function DateThai($strDate){
  $dateStr = "";
  if($strDate != null && $strDate != ""){
    $strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));

    $dateStr = "$strDay/$strMonth/$strYear";
  }
  return $dateStr;
}

function formatDate($date,$format){
  $Datestr = "";
  if(!empty($date) && $date != "0000-00-00"){
    $d = strtotime($date);
    $Datestr = date($format, $d);
  }
  return $Datestr;
}

function formatDateTh($date){
  $Datestr = "";
  if(!empty($date)){
    $d = strtotime($date);
    $Datestr = date("d/m/Y", $d);
  }
  return $Datestr;
}

function chkNull($obj){
  if(!isset($obj) || empty($obj)){
    $obj = "";
  }
  return $obj;
}

function chkNulls($obj){
  if ( isset($obj) && 0 === count($obj)) {
   $obj = "";
  }
  return $obj;
}

function DBInsertPOST($arr,$taleName){
  unset($arr['button']);
  $arrKey   = array();
  $arrValue = array();
  foreach ($arr as $key => $value) {
    if($value != "")
    {
      $arrKey[]   = $key;
      $arrValue[] = "'".$value."'";
    }

  }
  $strKey   = implode(",",$arrKey);
  $strValue = implode(",",$arrValue);
  $data = "INSERT INTO $taleName ($strKey) VALUES ($strValue);";
  return $data;
}

function DBUpdatePOST($arr,$taleName,$pk){
  unset($arr['button']);
  $arrUpdate = array();
  foreach ($arr as $key => $value) {
    if($key == $pk){
      $where = "WHERE $key = '$value'";
    }else{
      if($value != "")
      {
        $arrUpdate[]   = "$key = '$value'";
      }

    }
  }
  // $arrUpdate[] = "date_update = now()";

  $strUpdate = implode(",",$arrUpdate);
  $data = "UPDATE $taleName SET $strUpdate $where;";
  return $data;
}



function zeroToDash($obj){
  $str = $obj;
  if(!empty($obj) && $obj == "0.00"){
    $str = "-";
  }
  return $str;
}

function chkNum($obj){
  if(!isset($obj) || empty($obj)){
      $obj = 0;
  }else if(!is_numeric($obj)){
    $obj = "0";
  }
  return $obj;
}

function chkNum2($obj){
  if(!isset($obj) || empty($obj)){
      $obj = "0";
  }else if(!is_numeric($obj)){
    $obj = "0";
  }
  return $obj;
}

?>
