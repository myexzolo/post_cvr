<!DOCTYPE html>
<html style="height: auto; min-height: 100%;">

<?php
include("inc/head.php");
include("inc/utils.php");

?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg" style="min-height: 500px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>ข้อมูลใบสั่งงาน</small>
      </h1>

      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>ข้อมูลรายวัน</li>
        <li class="active">ข้อมูลใบสั่งงาน</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <?php
          $date         = date('Y-m-d');
          $startDate    = date('Y-m-01', strtotime($date));
          $endDate      = date('Y-m-t', strtotime($date));

          $optionEmp    = getoptionEmp("","");
      ?>
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black">
          <div class="panel-heading">ค้นหาข้อมูลใบสั่งงาน</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">จากวันที่: </td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $startDate ?>" id="startDate">
                        <input type="hidden" value="<?= $startDate ?>" id="startDateTmp">
                      </td>
                      <td style="padding:5px;width:150px;" align="right">ถึงวันที่ :</td>
                      <td style="padding:5px;" align="left">
                        <input type="date" class="form-control" value="<?= $endDate ?>" id="endDate">
                        <input type="hidden" value="<?= $endDate ?>" id="endDateTmp">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">สถานะ :</td>
                      <td style="padding:5px;" align="left">
                        <select id="status" class="form-control select2" required >
                          <option value="1" >รอการอนุมัติ</option>
                          <option value="2" >อนุมัติเรียบร้อย</option>
                          <option value="3" >ยกเลิกสั่งงาน</option>
                          <option value="" >สถานะทั้งหมด</option>
                        </select>
                      </td>
                      <td style="padding:5px;" align="right">ชื่อพนักงานขับรถ :</td>
                      <td style="padding:5px;" align="left">
                        <select id="employeeId" class="form-control select2" style="width: 100%;" required >
                          <option value="" ></option>
                          <?= $optionEmp ?>
                        </select>
                    </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="right">เลขที่ใบสั่ง :</td>
                      <td style="padding:5px;" align="left">
                        <input type="text" class="form-control" value="" id="jobOrderNo">
                      </td>
                      <td style="padding:5px;" align="right">DP : </td>
                      <td style="padding:5px;" align="left">
                          <input type="text" class="form-control" value="" id="dp">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="search()" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="reset()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
        <div class="panel panel-black">
          <div class="panel-heading">รายการข้อมูลใบสั่งงาน
            <button class="btn btn-success pull-right btn-flat" style="position: relative;top:-7px;right:-12px" onclick="postURL('job_order.php?page=PO')">+ เพิ่มข้อมูลใบสั่งงาน</button>
          </div>
            <div class="box-body" >
                <div id="printableArea">
                  <div id="show-page">
                    <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
                  </div>
                </div>
            </div>
      <!--  # coding -->
      </div>

      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- Modal -->
</div>
<?php include("inc/foot.php"); ?>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script src="js/po.js?v=1" type="text/javascript"></script>
<script>
  $(document).ready(function() {
    search();
  });
</script>
</body>
</html>
