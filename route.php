<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>

        <small>กำหนดค่าใช้จ่ายตามเส้นทาง</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li class="active">กำหนดค่าใช้จ่ายตามเส้นทาง</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black">
          <div class="panel-heading">ค้นหาข้อมูลเส้นทาง</div>
            <div class="box-body">
              <div class="row" align="center">
                  <table>
                    <tr>
                      <td style="padding:5px;" align="right">รหัสเส้นทาง :</td>
                      <td style="padding:5px;" align="left">
                        <input value="" id="s_route_id" type="text" class="form-control" placeholder="รหัสเส้นทาง">
                      </td>
                      <td style="padding:5px;width:100px;" align="right">ต้นทาง :</td>
                      <td style="padding:5px;" align="left">
                        <input value="" id="s_source" type="text" class="form-control" placeholder="ต้นทาง">
                      </td>
                      <td style="padding:5px;width:100px;" align="right">ปลายทาง :</td>
                      <td style="padding:5px;" align="left">
                        <input value="" id="s_destination" type="text" class="form-control" placeholder="ปลายทาง">
                      </td>
                    </tr>
                    <tr>
                      <td style="padding:5px;padding-top:20px;" colspan="6" align="center">
                          <button type="button" id="btnSave" class="btn btn-primary btn-flat" onclick="show()" style="width:100px">ค้นหา</button>
                          <button type="button" onclick="reset()" class="btn btn-warning btn-flat" style="width:100px">ล้างค่า</button>
                      </td>
                    </tr>
                  </table>
                </div>
            </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="panel panel-black" >
          <div class="panel-heading">รายการค่าใช้จ่ายตามเส้นทาง
            <button class="btn btn-success btnAdd btn-flat" onclick="showForm()">+ เพิ่มเส้นทาง</button>
            <button class="btn btn-primary pull-right btn-flat" style="position: relative;top:-7px;right:100px;width:100px;" onclick="exportExcel()">
            <i class="fa fa-file-excel-o"></i> Excel</button>

          </div>
          <div class="box-body">
              <div id="show-page">
                <div class="overlay">Loading.... <i class="fa fa-circle-o-notch fa-spin"></i></div>
              </div>
          </div>
      </div>
      <!--  # coding -->
      </div>
    </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">ข้อมูลเส้นทาง</h4>
        </div>
        <form id="form-data" data-smk-icon="glyphicon-remove-sign" novalidate >
		    <div class="modal-body" id="show-form"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
          <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
        </div>
        </form>
      </div>
    </div>
  </div>

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->
<?php include("inc/footer.php"); ?>
<script src="js/route.js" type="text/javascript"></script>
</body>
</html>
