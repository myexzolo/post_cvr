<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-black-light sidebar-mini sidebar-collapse">
<div class="wrapper">

<?php

  include("inc/header.php");
  include("inc/sidebar.php");
  include("inc/utils.php");
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper bg">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small>บันทึกข้อมูลใบสั่งงาน</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-home"></i> หน้าหลัก</a></li>
        <li>ข้อมูลรายวัน</li>
        <li><a href="po_list2.php">ข้อมูลใบสั่งงาน</a></li>
        <li class="active">บันทึกข้อมูลใบสั่งงาน</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <?php
          $dateNow      = date('Y-m-d');

          $typeProcess = isset($_POST['typeProcess'])?$_POST['typeProcess']:"";

          $ID   = isset($_POST['id'])?$_POST['id']:"";
          $page = isset($_POST['page'])?$_POST['page']:"";


          $display  = "";
          $link     = "";
          $required = "";
          if($page == 'PO'){
            $display = "display:none;";
            $link    = 'po_list2.php';
          }else if($page == 'JOB'){
            $link     = 'job_list2.php';
            $required = 'required';
          }

          $job_order_no       = "";
          $delivery_type_id   = "";//รูปแบบการขนส่ง
          $job_order_date     = "";//วันที่ออกใบสั่งงาน
          $job_delivery_date  = "";//วันที่กำหนดส่งสินค้า
          $cust_id            = "";//รหัสลูกค้า
          $employee_id        = "";//รหัสพนักงาน
          $employee_id2       = "";//รหัสพนักงาน2
          $trailer_id         = "";//หัว หมายเลขทะเบียนรถ
          $away_regi_no       = "";//หาง
          $away_spec_no       = "";//สเปคหาง
          $affiliation_id     = "";//รหัส รถ(สังกัด)
          $route_id           = "";//รหัสเส้นทาง
          $source             = "";//ต้นทาง
          $destination        = "";//ปลายทาง
          $distance           = "";//ระยะทาง
          $cust_dp            = "";//DP
          $delivery_doc       = "";//ใบส่งของ
          $department         = "";//หน่วยงาน
          $product_name       = "";//สินค้า
          $number_of_trips    = "1";//จำนวนเที่ยว
          $weights            = "";//น้ำหนัก(ตัน)
          $shipping_amount    = "";//ราคาค่าขนส่ง
          $date_product_in    = "";//วันทีรับสินค้า
          $time_product_in    = "";//เวลารับสินค้า
          $date_product_out   = "";//วันที่ออกสินค้า
          $time_product_out   = "";//เวลาออกสินค้า
          $date_delivery_in   = "";//วันทีส่งสินค้า
          $time_delivery_in   = "";//เวลาเข้าส่งสินค้า
          $date_delivery_out  = "";//วันที่ออกส่งสินค้า
          $time_delivery_out  = "";//เวลาออกส่งสินค้า
          $fuel_cost          = 0;//ค่าน้ำมัน
          $fuel_litre         = 0;//จำนวนลิตร
          $blank_charge       = "";//ค่าตีเปล่า
          $allowance          = "";//เบี้ยเลี้ยง
          $kog_expense        = "";//หางคอก
          $allowance_oth      = "";//เบี้ยงเลี้ยงอื่นๆ
          $second_emp         = "";//ค่าขับรถผู้ช่วย
          $remark             = "";
          $shipment           = "";
          $price_type_id      = "";//รหัสการคำนวณ
          $one_trip_ton       = "0";//ราคาต่อตัน
          $ext_one_trip_ton   = "0";//ราคารถร่วมต่อตัน
          $price_per_trip     = "0";//ราคาต่อเที่ยว
          $ext_price_per_trip = "0";//ราคารถร่วมต่อเที่ยว
          $total_amount_receive = "0";//ราคาค่าขนส่ง
          $total_amount_ap_pay  = "0";//ราคาจ่ายรถร่วม
          $total_amount_allowance = "0";//รวมค่าใช้จ่าย
          $job_ended_clearance    = "0";//ค่าเคลียร์ค่าปรับ
          $job_ended_recap        = "0";//ค่าปะยาง
          $job_ended_expressway   = "0";//ค่าทางด่วน
          $job_ended_passage_fee  = "0";//ค่าธรรมเนียมผ่านท่าเรือ
          $job_ended_repaires     = "0";//ค่าซ่อม
          $job_ended_acc_expense  = "0";//ค่าทำบัญชี
          $job_ended_other_expense = "0";//คชจ.อื่นๆ
          $fuel_driver_bill       = "";//บิลน้ำมันจากคนขับ
          $job_status_id          = "";
          $job_order_profit       = "0";//กำไรเบื้องต้น
          $petrol_station         = "";
          $type_job               = "";
          $fuel_total             = 0;

          if($ID == ""){
            $d        = date('Y-m-d H:i:s');
            $strDay   = date('d',strtotime($d));
            $strMonth = date('m',strtotime($d));
            $strYear  = date("Y",strtotime($d));
            if($strYear < 2500){
              $strYear += 543;
            }
            $strYear  = substr($strYear,2,2);

            $Code = "TT".$strYear.$strMonth.$strDay;

            $job_status_id  = "1";

            //echo $Code;

            $sql = "SELECT max(job_order_no) as job_order_no FROM tb_job_order where job_order_no LIKE '$Code%'";
            $query  = mysqli_query($conn,$sql);
            $row    = mysqli_fetch_assoc($query);
            $jobOrderNo  = $row['job_order_no'];

            if(!empty($jobOrderNo)){
              $lastNum = substr($jobOrderNo,strlen($Code));
              $lastNum = $lastNum + 1;
              $job_order_no = $Code.sprintf("%03d", $lastNum);
            }else{
              $job_order_no = $Code.sprintf("%03d", 1);
            }
          }else{
            $sql = "SELECT * FROM tb_job_order where 	ID ='$ID'";
            $query  = mysqli_query($conn,$sql);
            $row    = mysqli_fetch_assoc($query);

            $job_order_no       = $row['job_order_no'];
            $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
            $job_order_date     = $row['job_order_date'];//วันที่ออกใบสั่งงาน
            $job_delivery_date  = $row['job_delivery_date'];//วันที่กำหนดส่งสินค้า
            $cust_id            = $row['cust_id'];//รหัสลูกค้า
            $employee_id        = $row['employee_id'];//รหัสพนักงาน
            $employee_id2       = $row['employee_id2'];//รหัสพนักงาน
            $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
            $away_regi_no       = $row['away_regi_no'];//หาง
            $away_spec_no       = $row['away_spec_no'];//สเปคหาง
            $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
            $route_id           = $row['route_id'];//รหัสเส้นทาง
            $source             = $row['source'];//ต้นทาง
            $destination        = $row['destination'];//ปลายทาง
            $distance           = $row['distance'];//ระยะทาง
            $cust_dp            = $row['cust_dp'];//DP
            $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
            $department         = $row['department'];//หน่วยงาน
            $product_name       = $row['product_name'];//สินค้า
            $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
            $weights            = $row['weights'];//น้ำหนัก(ตัน)
            $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
            $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
            $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
            $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
            $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
            $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
            $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
            $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
            $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
            $second_emp         = $row['second_emp'];;//ค่าขับรถผู้ช่วย
            $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
            $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
            $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
            $allowance          = $row['allowance'];//เบี้ยเลี้ยง
            $kog_expense        = $row['kog_expense'];//หางคอก
            $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
            $remark             = $row['remark'];
            $shipment           = $row['shipment'];
            $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
            $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
            $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
            $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
            $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
            $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
            $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
            $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
            $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
            $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
            $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
            $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
            $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
            $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
            $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
            $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
            $job_status_id            = $row['job_status_id'];
            $job_order_profit         = $row['job_order_profit'];
            $petrol_station           = $row['petrol_station'];//ปั้มน้ำมัน
            $type_job                 = $row['type_job'];


          }

          if($job_order_date == ""){
            $job_order_date = $dateNow;
          }

          $optionEmp            = getoptionEmp($employee_id,"");
          $optionEmp2           = getoptionEmp($employee_id2,"");
          $optionCustomer       = getoptionCustomer($cust_id);
          $optionTrailer        = getoptionTrailerNo($trailer_id);
          $optionDeliveryType   = getoptionDeliveryType($delivery_type_id);
          $optionAffiliation    = getoptionAffiliation($affiliation_id);
          $optionPetrol          = getoptionPetrol("");

          $licensePlate         = getLicensePlate($trailer_id);

      ?>
      <form id="form-data2" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
      <!-- <form data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" action="ajax/po/manage2.php" method="post"> -->
      <div class="row">
      <!-- Main row -->
      <div class="col-md-12">
        <div class="panel panel-black">
          <div class="panel-heading">ข้อมูลใบสั่งงาน
            <div class="pull-right" style="position: relative;top:-7px;right:-12px">
              <table>
                <tr>
                  <td>ค้นหา DP</td>
                  <td style="width:3px">&nbsp;:&nbsp;</td>
                  <td><div class="ui-widget"><input type="text" class="form-control autocomplete" value="" id="dps" ></div></td>
                </tr>
              </table>
            </div>
          </div>
            <div class="box-body" >
                <div id="show-page">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>เลขที่ใบสั่งงาน</label>
                        <input value="<?= $job_order_no ?>"  id="job_order_no" name="job_order_no" type="text" class="form-control" placeholder="No PO" required readonly>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>ขนส่งแบบ</label>
                        <select name="delivery_type_id"  id="delivery_type_id" class="form-control select2" style="width: 100%;" required >
                          <option value="" >เลือกแบบการขนส่ง</option>
                          <?= $optionDeliveryType ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>วันที่</label>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input value="<?= $job_order_date ?>" id="job_order_date" name="job_order_date" type="date" class="form-control" placeholder="" required onchange="genJobOrderNo()">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>กำหนดส่ง</label>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input value="<?= $job_delivery_date ?>" name="job_delivery_date" type="date" class="form-control" placeholder="" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>ลูกค้า</label>
                        <select name="cust_id"  id="cust_id" class="form-control select2" style="width: 100%;" required>
                          <option value="" >เลือกลูกค้า</option>
                          <?= $optionCustomer ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>หัว</label>
                        <select name="trailer_id"  id="trailer_id" class="form-control select2" style="width: 100%;" required onchange="chooseTrailer()">
                          <option value="" >&nbsp;</option>
                          <?= $optionTrailer ?>
                        </select>
                        <!-- <input value="<?= $licensePlate ?>" id="license_plate" type="text" class="form-control" placeholder="" required>
                        <input value="<?=$trailer_id ?>" id="trailer_id" name="trailer_id" type="hidden" class="form-control" placeholder="" required> -->
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>หาง</label>
                        <input value="<?=$away_regi_no ?>" name="away_regi_no" type="text" class="form-control" placeholder="หมายเลขทะเบียนหาง" >
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>Spec หาง</label>
                        <input value="<?=$away_spec_no ?>" name="away_spec_no" type="text" class="form-control" placeholder="Spac" required>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>ใบส่งของ</label>
                        <input value="<?= $delivery_doc ?>" name="delivery_doc" type="text" class="form-control" placeholder="ใบส่งของ" >
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>รถ(สังกัด)</label>
                        <select name="affiliation_id"  id="affiliation_id" class="form-control select2" style="width: 100%;" required >
                          <option value="" >เลือกสังกัด</option>
                          <?= $optionAffiliation ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>ประเภทงาน</label>
                        <select name="type_job"  id="type_job" class="form-control select2" style="width: 100%;" required >
                          <option value="" >เลือกประเภทงาน</option>
                          <option value="1" <?= ($type_job == '1' ? 'selected="selected"':'') ?>>งานประจำ</option>
                          <option value="2" <?= ($type_job == '2' ? 'selected="selected"':'') ?>>งานเสริมประจำ</option>
                          <option value="3" <?= ($type_job == '3' ? 'selected="selected"':'') ?>>งานเสริมทั่วไป</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                          <label>รหัสเส้นทาง</label>
                          <div class="input-group">
                            <input readonly value="<?= $route_id ?>" name="route_id" id="route_id" type="text" class="form-control" placeholder="รหัสเส้นทาง" required>
                            <span class="input-group-btn">
                              <button type="button" class="btn btn-block btn-success non-radius form-control" onclick="showForm()"  autocomplete="off">
                                <i class="fa fa-search"></i></button>
                            </span>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>สถานที่รับ</label>
                          <input readonly value="<?= $source ?>" name="source" id="source" type="text" class="form-control col-md-5" placeholder="สถานที่รับ" >
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label>สถานที่ส่ง</label>
                        <input readonly value="<?=$destination ?>" name="destination"  id="destination" type="text" class="form-control col-md-5" placeholder="สถานที่ส่ง">
                      </div>
                    </div>
                  </div>
                    <div class="row">
                      <div class="col-md-1">
                        <div class="form-group">
                          <label>ระยะทาง</label>
                          <input value="<?= $distance ?>" name="distance" id="distance" type="number" class="form-control" placeholder="" step="any" required>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                            <label>DP</label>
                            <div class="input-group">
                              <input readonly value="<?= $cust_dp ?>" name="cust_dp" id="cust_dp" type="text" class="form-control" placeholder="DP .." required>
                              <span class="input-group-btn">
                                <button type="button" class="btn btn-block btn-success non-radius form-control" onclick="showFormDP()"  autocomplete="off">
                                <i class="fa fa-list"></i></button>
                              </span>
                            </div>
                          </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>Shipment</label>
                          <input value="<?= $shipment ?>" name="shipment" type="text" class="form-control" placeholder="No Shipment" >
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>ชื่อพนักงาน</label>
                          <select name="employee_id"  id="employee_id" class="form-control select2" style="width: 100%;" required >
                            <option value="" >เลือกพนักงานขับรถ</option>
                            <?= $optionEmp ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>ชื่อพนักงานผู้ช่วย</label>
                          <select name="employee_id2"  id="employee_id2" class="form-control select2" style="width: 100%;" onchange="setAllowance()"  >
                            <option value="" >เลือกพนักงานขับรถ</option>
                            <?= $optionEmp2 ?>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>หน่วยงาน</label>
                          <input value="<?= $department?>" name="department" type="text" class="form-control" placeholder="หน่วยงาน" >
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label>สินค้า</label>
                          <input value="<?=$product_name ?>" name="product_name" type="text" class="form-control" placeholder="สินค้า" >
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>จำนวนเที่ยว</label>
                          <input value="<?=$number_of_trips ?>" name="number_of_trips" type="number" class="form-control" placeholder="จำนวนเที่ยว" >
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>น้ำหนัก(ตัน)</label>
                          <input value="<?=$weights ?>" name="weights" data-smk-type="decimal" id="weights" type="text"  class="form-control textNum" placeholder="น้ำหนัก" step="any" required >
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>ราคาค่าขนส่ง</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?=$shipping_amount ?>" data-smk-type="decimal" readonly name="shipping_amount" type="text" class="form-control textNum" placeholder="ราคาค่าขนส่ง"  step="any">
                          </div>
                        </div>
                      </div>
                    </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>วันทีรับสินค้า</label>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input value="<?= $date_product_in ?>" name="date_product_in" type="date" class="form-control" placeholder=""  required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>เวลาเข้า</label>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa  fa-clock-o"></i></span>
                          <input value="<?= $time_product_in ?>" name="time_product_in" type="time" class="form-control" placeholder="" >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>วันที่ออก</label>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input value="<?= $date_product_out ?>" name="date_product_out" type="date" class="form-control" placeholder="" >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>เวลาออก</label>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa  fa-clock-o"></i></span>
                          <input value="<?= $time_product_out ?>" name="time_product_out" type="time" class="form-control" placeholder="" >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>วันที่ส่งสินค้า</label>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input value="<?= $date_delivery_in ?>" name="date_delivery_in" type="date" class="form-control" placeholder="" required>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>เวลาเข้า</label>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa  fa-clock-o"></i></span>
                          <input value="<?= $time_delivery_in ?>" name="time_delivery_in" type="time" class="form-control" placeholder="" >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label>วันที่ออก</label>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa  fa-calendar"></i></span>
                          <input value="<?= $date_delivery_out ?>" name="date_delivery_out" type="date" class="form-control" placeholder="" >
                        </div>

                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="form-group">
                        <label>เวลาออก</label>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="fa  fa-clock-o"></i></span>
                          <input value="<?= $time_delivery_out ?>" name="time_delivery_out" type="time" class="form-control" placeholder="" >
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="text-center" style="background-color:#00ffff;height:30px;line-height:30px;margin-top:5px;margin-bottom:5px;">
                        <b>ค่าใช้จ่าย</b>
                      </div>
                  </div>
                  <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-2">
                          <div class="form-group">
                              <label>ปั้มน้ำมัน</label>
                              <select id="petrol_station_tmp" class="form-control select2" style="width: 100%;" onchange="clrfs()" >
                                <option value="" >เลือกปั้มน้ำมัน</option>
                                <?= $optionPetrol ?>
                              </select>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>ราคาน้ำมัน</label>
                            <div class="input-group">
                              <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                              <input value="" id="fuel_cost_tmp" data-smk-type="decimal" type="text" onkeyup="clrfc()" class="form-control textNum" placeholder=""  step="any">
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>ลิตร</label>
                            <input value="" id="fuel_litre_tmp" data-smk-type="decimal" type="text" onkeyup="clrfcl()" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label>เลขที่ใบเสร็จ</label>
                            <input value="" id="fuel_receipt_tmp" type="text" class="form-control" placeholder="">
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label>วันที่</label>
                            <input value="" id="fuel_date_tmp" type="date" class="form-control" onkeyup="clrfd()"  onchange="clrfd()" placeholder="">
                          </div>
                        </div>
                        <div class="col-md-1">
                          <div class="form-group">
                            <label>&nbsp;</label>
                            <button type="button" class="btn btn-block btn-success non-radius form-control" onclick="addPetrol()" ><i class="fa fa-plus"></i></button>
                          </div>
                        </div>
                        <div class="col-md-12">
                        <table class="table table-bordered " id="tablePetrol" width="100%">
                          <thead>
                            <tr class="text-center ">
                              <th>ปั้มน้ำมัน</th>
                              <th>ค่าน้ำมัน</th>
                              <th>จำนวนลิตร</th>
                              <th>รวม</th>
                              <th>เลขที่ใบเสร็จ</th>
                              <th>วันที่เติม</th>
                              <th style="width:50px;">ลบ</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                                $sql = "SELECT * FROM tb_petrol where job_order_no  ='$job_order_no'";
                                $query  = mysqli_query($conn,$sql);
                                $num    = mysqli_num_rows($query);
                                if($num > 0){
				$fuel_litre = 0;
                                for ($i=1; $i <= $num; $i++) {
                                  $row           = mysqli_fetch_assoc($query);
                                  $petrol_station_s   = $row['petrol_station'];
                                  $petrol_code_s      = $row['petrol_code'];
                                  $fuel_cost_s        = $row['fuel_cost'];
                                  $fuel_litre_s       = $row['fuel_litre'];
                                  $fuel_total_s       = $row['fuel_total'];
                                  $fuel_receipt_s     = $row['fuel_receipt'];
                                  $fuel_date_s        = $row['fuel_date'];

                                  if($fuel_total_s == 0)
                                  {
                                    $fuel_total_s = ($fuel_cost_s * $fuel_litre_s);
                                  }

                                  $fuel_total   += $fuel_total_s;
				  $fuel_litre   += $fuel_litre_s;

                                  if($fuel_litre == 0){
                                      //$fuel_litre   += $fuel_litre_s;
                                  }


                                  $idTr = "tr_".rand();
                              ?>
                              <tr id="<?=$idTr; ?>">
                                <td align="left"><?= $petrol_station_s ?>
                                    <input value="<?= $petrol_station_s ?>" id="petrol_station_s_<?=$idTr; ?>" name="petrol_station_s[]" type="hidden" class="form-control" >
                                    <input value="<?= $petrol_code_s ?>" id="petrol_code_s_<?=$idTr; ?>" name="petrol_code_s[]" type="hidden" class="form-control" >
                                </td>
                                <td align="right" class="trCodePet"><?= $fuel_cost_s ?>
                                    <input value="<?= $fuel_cost_s ?>" id="fuel_cost_s_<?=$idTr; ?>"  name="fuel_cost_s[]" type="hidden" class="form-control">
                                </td>
                                <td align="right"><?= $fuel_litre_s ?>
                                    <input value="<?= $fuel_litre_s ?>" id="fuel_litre_s_<?=$idTr; ?>" name="fuel_litre_s[]" type="hidden" class="form-control">
                                </td>
                                <td align="right"><?= number_format($fuel_total_s,2) ?>
                                    <input value="<?= $fuel_total_s ?>" id="fuel_total_s_<?=$idTr; ?>" name="fuel_total_s[]" type="hidden" class="form-control">
                                </td>
                                <td align="right"><?= $fuel_receipt_s ?>
                                    <input value="<?= $fuel_receipt_s ?>" id="fuel_receipt_s_<?=$idTr; ?>" name="fuel_receipt_s[]" type="hidden" class="form-control">
                                </td>
                                <td align="right"><?= $fuel_date_s != ""?formatDate($fuel_date_s,'d/m/Y'):""; ?>
                                    <input value="<?= $fuel_date_s ?>" id="fuel_date_s_<?=$idTr; ?>" name="fuel_date_s[]" type="hidden" class="form-control">
                                </td>
                                <td align="center"><button type="button" class="btn btn-warning btn-xs btn-flat" onclick="delPet('<?=$idTr; ?>')"><i class="fa fa-trash"></i></button></td>
                              </tr>
                              <?php
                                }
                              }else{
                              ?>
                              <tr id="blankPet">
                                <td>&nbsp;</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                              <?php
                              }
                            ?>
                          </tbody>
                        </table>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>จำนวนลิตร</label>
                          <input value="<?= $fuel_litre ?>" id="fuel_litre" name="fuel_litre" readonly data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>รวมค่าน้ำมัน</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= $fuel_total ?>" id="fuel_cost" name="fuel_cost" readonly data-smk-type="decimal" type="text" class="form-control textNum" placeholder=""  step="any">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>ค่าพขร.ผู้ช่วย</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= $second_emp ?>" id="second_emp" name="second_emp" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>ค่าตีเปล่า</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= $blank_charge ?>" id="blank_charge" name="blank_charge" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>เบี้ยเลี้ยง</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= $allowance ?>" id="allowance" name="allowance" type="text" data-smk-type="decimal" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>หางคอก</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= $kog_expense ?>" name="kog_expense" id="kog_expense" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>คชจ.บัญชี</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= $job_ended_acc_expense ?>" name="job_ended_acc_expense" id="job_ended_acc_expense" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>อื่นๆ</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= $allowance_oth ?>" name="allowance_oth" id="allowance_oth" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label>หมายเหตุ</label>
                            <textarea class="form-control" name="remark" id="remark" rows="3" placeholder="Remark ..." ><?= $remark ?></textarea>
                          </div>
                        </div>
                    </div>
                  </div>
                <div style="<?= $display?>" >
                  <div class="col-md-12">
                    <div class="text-center" style="background-color:#00ffff;height:30px;line-height:30px;margin-top:5px;margin-bottom:5px;">
                      <b>บันทึกค่าใช้จ่ายการเดินรถ</b>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-2">
                        <div class="form-group">
                          <label>คำนวณราคา</label>
                          <select name="price_type_id" onchange="calAmount()" id="price_type_id" class="form-control select2" style="width: 100%;" data-smk-msg="เลือกการคำนวณราคา"  <?= $required ?>>
                            <option value="" >เลือกการคำนวณ</option>
                            <option value="1" <?= ($price_type_id == '1' ? 'selected="selected"':'') ?>>ราคาต่อตัน</option>
                            <option value="2" <?= ($price_type_id == '2' ? 'selected="selected"':'') ?>>ราคาต่อเที่ยว</option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group has-warning">
                          <label>รวมค่าใช้จ่าย</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$total_amount_allowance,2, '.', ''); ?>" id="total_amount_allowance"  name="total_amount_allowance" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any" <?= $required ?>>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group has-success">
                          <label>กำไรเบื้องต้น</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$job_order_profit,2, '.', ''); ?>" id="job_order_profit"  name="job_order_profit"  type="text" class="form-control textNum" placeholder="" step="any" <?= $required ?>>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="text-center" style="background-color:#00ffff;height:30px;line-height:30px;margin-top:5px;margin-bottom:5px;">
                          <b>รถบริษัท</b>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>ราคาต่อตัน</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$one_trip_ton,2, '.', ''); ?>" id="one_trip_ton" name="one_trip_ton" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>ราคาต่อเที่ยว</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$price_per_trip,2, '.', ''); ?>" id="price_per_trip" name="price_per_trip" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group has-success">
                          <label>ราคาค่าขนส่ง</label>
                          <div class="input-group ">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$total_amount_receive,2, '.', ''); ?>" id="total_amount_receive"  name="total_amount_receive" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any" <?= $required ?>>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>ค่าปรับ</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$job_ended_clearance,2, '.', ''); ?>" id="job_ended_clearance" name="job_ended_clearance" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>ปะยาง</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$job_ended_recap,2, '.', ''); ?>" id="job_ended_recap" name="job_ended_recap" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>ทางด่วน</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$job_ended_expressway,2, '.', ''); ?>" id="job_ended_expressway" name="job_ended_expressway" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>ผ่านท่าเรือ</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$job_ended_passage_fee,2, '.', ''); ?>" name="job_ended_passage_fee" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>ค่าซ่อม</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$job_ended_repaires,2, '.', ''); ?>" name="job_ended_repaires" id="job_ended_repaires" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>คชจ.อื่นๆ</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$job_ended_other_expense,2, '.', ''); ?>" name="job_ended_other_expense" id="job_ended_other_expense" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>บิลน้ำมัน</label>
                          <input value="<?= $fuel_driver_bill ?>" name="fuel_driver_bill" id="fuel_driver_bill" type="text" class="form-control" placeholder="">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="text-center" style="background-color:#00ffff;height:30px;line-height:30px;margin-top:5px;margin-bottom:5px;">
                          <b>รถร่วม</b>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>ราคารถร่วมต่อตัน</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$ext_one_trip_ton,2, '.', ''); ?>" id="ext_one_trip_ton" name="ext_one_trip_ton" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label>ราคารถร่วมต่อเที่ยว</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$ext_price_per_trip,2, '.', ''); ?>" id="ext_price_per_trip" name="ext_price_per_trip" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group has-success">
                          <label>ราคาจ่ายรถร่วม</label>
                          <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-btc"></i></span>
                            <input value="<?= number_format((float)$total_amount_ap_pay,2, '.', ''); ?>" id="total_amount_ap_pay"  name="total_amount_ap_pay" data-smk-type="decimal" type="text" class="form-control textNum" placeholder="" step="any" <?= $required ?>>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                </div>
                  <input  type="hidden" value="<?= $ID ?>" name="id" type="text" >
                  <input  type="hidden" value="1" name="job_type_id" type="text" >
                  <input  type="hidden" value="<?= $page ?>" id="page" name="page" type="text" >
                  <input  type="hidden" value="<?= $job_status_id ?>" name="job_status_id" type="text" >
                </div>
                <div align="center">
                  <button type="button" class="btn btn-flat " onclick="calAmount()" style="width:100px;<?= $display?>">
                    <i class="fa fa-calculator"></i> คำนวณ
                  </button>
                  <button type="submit" class="btn btn-primary btn-flat " style="width:100px">
                    <i class="fa fa-save"></i> บันทึก
                  </button>
                  <button type="reset" class="btn btn-warning btn-flat " style="width:100px">
                    <i class="glyphicon glyphicon-remove"></i> ล้างค่า
                  </button>
                  <button type="button" onclick="gotoPage('<?= $link ?>')" class="btn btn-success btn-flat " style="width:100px">
                    <i class="fa fa-mail-reply"></i> ย้อนกลับ
                  </button>
                </div>
            </div>
      <!--  # coding -->
      </div>
    </div>
    </div>
    </form>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <!-- /.content-wrapper -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">เลือกเส้นทาง</h4>
        </div>
        <form id="formAddModule" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">

		    <div class="modal-body" >
          <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>ประเภทการค้นหา</label>
                <select id="searchBy" class="form-control select2" style="width: 100%;" required >
                  <option value="route_id">รหัสเส้นทาง</option>
                  <option value="source">ต้นทาง</option>
                  <option value="destination">ปลายทาง</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>ค้นหา</label>
                <div class="input-group">
                  <input value="" id="searchTxt" type="text" class="form-control" placeholder="Search Text" >
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-block btn-success non-radius form-control" onclick="searchRoute()"  autocomplete="off">
                      <i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <div id="show-form" style="height:550px"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel2">บันทึก DP</h4>
        </div>
        <form id="formDP" data-smk-icon="glyphicon-remove-sign" novalidate>
  		    <div class="modal-body" >
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label></label>
                  <div class="input-group">
                    <input value="" id="txtDP" type="text" class="form-control" placeholder="ADD DP" autocomplete="off" autofocus>
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-block btn-success non-radius form-control" onclick="addDP()" >
                        <i class="fa fa-plus"></i></button>
                    </span>
                  </div>
                </div>
              </div>
              <div class="col-md-6" align="left">
                <div class="form-group">
                  <label></label>
                  <div class="input-group" align="left">
                    <span id="alertTxt" style="color:red;font-size:14px;line-height:35px;"></span>
                  </div>
                </div>
              </div>
            </div>
              <input type="hidden" id="tmp_dp">
              <table class="table table-bordered table-striped " id="tableDP">
                <thead>
                  <tr class="text-center ">
                    <th style="width:50px;">ลำดับ</th>
                    <th>เลข DP</th>
                    <th style="width:50px;">ลบ</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">ปิด</button>
            <button type="submit" class="btn btn-primary btn-flat">บันทึก</button>
          </div>
        </form>
      </div>
    </div>
  </div>

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script>
$(function () {
   //Initialize Select2 Elements
   $('.select2').select2();
})
</script>
<script src="js/po.js?v=2" type="text/javascript"></script>
</body>
</html>
