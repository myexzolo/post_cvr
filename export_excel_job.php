<?php
include('conf/connect.php');
include('inc/utils.php');



$startDate   = isset($_GET['startDate'])?$_GET['startDate']:"";
$endDate     = isset($_GET['endDate'])?$_GET['endDate']:"";
$status        = isset($_GET['status'])?$_GET['status']:"";
$employeeId    = isset($_GET['employeeId'])?$_GET['employeeId']:"";
$jobOrderNo    = isset($_GET['jobOrderNo'])?$_GET['jobOrderNo']:"";
$dp            = isset($_GET['dp'])?$_GET['dp']:"";
$trailerId     = isset($_GET['trailerId'])?$_GET['trailerId']:"";
$awaySpecNo    = isset($_GET['awaySpecNo'])?$_GET['awaySpecNo']:"";
$cust_id       = isset($_GET['cust_id'])?$_GET['cust_id']:"";
$statusInv     = isset($_GET['statusInv'])?$_GET['statusInv']:"";
$productName   = isset($_GET['productName'])?$_GET['productName']:"";


$strExcelFileName="export_job_".$startDate.".xls";

header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

$numRow = 0;
$con = "";
if($startDate != "")
{
  $con .= " and jo.job_order_date between '". $startDate ."' and '". $endDate ."' ";
}

if($status != "")
{
  $con .= " and jo.job_status_id ='". $status ."' ";
}
if($employeeId != "")
{
  $con .= " and jo.employee_id = '". $employeeId ."' ";
}
if($jobOrderNo != "")
{
  $con .= " and jo.job_order_no LIKE '". $jobOrderNo ."%' ";
}
if($dp != "")
{
  $con .= " and jo.job_order_no in (select job_order_no FROM tb_dp where dp_code LIKE '". $dp ."%')";
}

if($trailerId != "")
{
  $con .= " and jo.trailer_id = '". $trailerId ."' ";
}

if($awaySpecNo != "")
{
  $con .= " and jo.away_spec_no LIKE '". $awaySpecNo ."%' ";
}

if($cust_id != "")
{
  $con .= " and jo.trailer_id = '". $cust_id ."' ";
}

if($statusInv != "")
{
  if($statusInv == "1"){
    $con .= " and jo.invoice_code is null ";
  }else if($statusInv == "2"){
    $con .= " and jo.invoice_code is not null ";
  }
}

if($productName != "")
{
  $con .= " and jo.product_name like '%". $productName ."%' ";
}

?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
  <div align="center" style="font-size:14px;"><b>รายงานใบสั่งจ้าง (PO) ประจำวันที่ <?= formatDateTh($startDate) ?></b></div>
  <?php
    //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
    $sql = "SELECT jo.*,em.employee_name, c.cust_name, t.license_plate
    FROM tb_job_order jo, tb_employee_master em, tb_customer_master c, tb_trailer t
    where jo.employee_id = em.employee_id $con and jo.cust_id = c.cust_id and job_status_id <> 3 and jo.trailer_id = t.trailer_id
    order by c.cust_id,jo.job_order_date DESC";
    //echo $sql;
    $query  = mysqli_query($conn,$sql);
    $num = mysqli_num_rows($query);
    $nameCus = "";
    $n = 0;

    $ton = 0;
    $fuel = 0;
    $litre = 0;
    $distancet = 0;
    for ($i=1; $i <= $num ; $i++) {
      $row = mysqli_fetch_assoc($query);
      $job_order_no         = $row['job_order_no'];
      $job_order_date       = date('d/m/Y', strtotime($row['job_order_date']));
      $job_delivery_date    = date('d/m/Y', strtotime($row['job_delivery_date']));
      $cust_dp              = $row['cust_dp'];
      $source               = $row['source'];
      $destination          = $row['destination'];
      $Employee_id          = $row['employee_id'];
      $Employee_Name        = $row['employee_name'];
      $cust_name            = $row['cust_name'];
      $affiliation_id       = $row['affiliation_id'];
      $away_regi_no         = $row['away_regi_no'];//หาง
      $job_status_id        = $row['job_status_id'];
      $shipment             = $row['shipment'];
      $cust_name            = $row['cust_name'];
      $away_spec_no         = $row['away_spec_no'];//สเปคหาง
      $license_plate        = $row['license_plate'];//หมายเลขทะเบียนรถ
      $product_name         = $row['product_name'];//สินค้า
      $weights              = $row['weights'];//น้ำหนัก(ตัน)
      $fuel_cost            = $row['fuel_cost'];//ค่าน้ำมัน
      $fuel_litre           = $row['fuel_litre'];//จำนวนลิตร
      $away_regi_no         = $row['away_regi_no'];//หาง
      $distance             = $row['distance'];//ระยะทาง

      if(is_numeric($weights)){
        $ton  +=  $weights;
      }

      if(is_numeric($fuel_cost)){
        $fuel  +=  $fuel_cost;
      }

      if(is_numeric($fuel_litre)){
        $litre  +=  $fuel_litre;
      }

      if(is_numeric($distance)){
        $distancet  +=  $distance;
      }

      if($nameCus != $cust_name){
        if($nameCus != ""){
          echo "</tbody>";
          echo "</table>";
          echo "<div align='right' style='font-size:10px;'>
          ระยะทาง ".number_format((float)$distancet, 2, '.', '')." Km
          จำนวน ".number_format((float)$ton, 3, '.', '')." ตัน
          ค่าน้ำมัน ".number_format((float)$fuel, 2, '.', '')." บาท
          น้ำมัน ".number_format((float)$litre, 2, '.', '')." ลิตร</div>";
        }
        $nameCus = $cust_name;
        $n = 0;
        $ton = 0;
        $fuel = 0;
        $litre = 0;
      ?>
      <br>
      <div><?= $nameCus ?></div>
      <table x:str border=1 cellpadding=0 cellspacing=1 width=100% style="border-collapse:collapse">
        <thead>
          <tr class="text-center">
            <th style="width:30px;border:1px solid black">No</th>
            <th style="border:1px solid black" class="text-center" >วันที่</th>
            <th style="border:1px solid black" class="text-center" >เลขที่ใบสั่ง</th>
            <th style="border:1px solid black" class="text-center" >กำหนดส่ง</td>
            <th style="border:1px solid black" class="text-center" >ต้นทาง</th>
            <th style="border:1px solid black" class="text-center" >ปลายทาง</th>
            <th style="width:70px;border:1px solid black" class="text-center" >ทะเบียนรถ</th>
            <th style="width:70px;border:1px solid black" class="text-center" >ทะเบียนหาง</th>
            <th style="border:1px solid black" class="text-center" >Spec หาง</th>
            <th style="border:1px solid black" class="text-center" >ชื่อพนักงานขับรถ</th>
            <th style="border:1px solid black" class="text-center" >ชื่อลูกค้า</th>
            <th style="width:90px;border:1px solid black" class="text-center" >DP</th>
            <th style="border:1px solid black" class="text-center" >ระยะทาง</th>
            <th style="border:1px solid black" class="text-center" >สินค้า</th>
            <th style="border:1px solid black" class="text-center" >น้ำหนัก(ตัน)</th>
            <th style="border:1px solid black" class="text-center" >ค่าน้ำมัน</th>
            <th style="border:1px solid black" class="text-center" >จำนวนลิตร</th>
          </tr>
        </thead>
        <tbody>
      <?php
      }
      $n++;
      ?>
      <tr class="text-center">
        <td style="border:1px solid black" align="center"><?= $n ?></td>
        <td style="border:1px solid black" align="center"><?= $job_order_date ?></td>
        <td style="border:1px solid black" ><?= $job_order_no ?></td>
        <td style="border:1px solid black" align="center"><?= $job_delivery_date ?></td>
        <td style="border:1px solid black" align="left"><?= $source ?></td>
        <td style="border:1px solid black" align="left"><?= $destination ?></td>
        <td style="border:1px solid black" align="center"><?= $license_plate ?></td>
        <td style="border:1px solid black" align="center"><?= $away_regi_no ?></td>
        <td style="border:1px solid black" align="left"><?= $away_spec_no ?></td>
        <td style="border:1px solid black" align="left"><?= $Employee_Name ?></td>
        <td style="border:1px solid black" align="left"><?= $cust_name ?></td>
        <td style="border:1px solid black" align="left"><?= $cust_dp ?></td>
        <td style="border:1px solid black" align="left"><?= $product_name ?></td>
        <td style="border:1px solid black" align="right"><?= number_format($distance,2); ?></td>
        <td style="border:1px solid black" align="right"><?= number_format($weights,3); ?></td>
        <td style="border:1px solid black" align="right"><?= number_format($fuel_cost,2); ?></td>
        <td style="border:1px solid black" align="right"><?= number_format($fuel_litre,2); ?></td>
      </tr>
    <?php } ?>
    </tbody>
    </table>
    <div align='right' style='font-size:10px;'>
    ระยะทาง <?= number_format((float)$distancet, 2, '.', '') ?> Km
    จำนวน <?= number_format((float)$ton, 3, '.', '')?> ตัน
    ค่าน้ำมัน <?=number_format((float)$fuel, 2, '.', '')?> บาท
    น้ำมัน <?= number_format((float)$litre, 2, '.', '')?> ลิตร
    </div>
</div>
<script>
window.onbeforeunload = function(){return false;};
setTimeout(function(){window.close();}, 10000);
</script>
</body>
</html>
