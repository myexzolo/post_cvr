<?php
include('conf/connect.php');
include('inc/utils.php');


$apInvoiceNo    = isset($_GET['apinvoiceCode'])?$_GET['apinvoiceCode']:"";
$strExcelFileName="export_apinvoices_".$apInvoiceNo.".xls";

header("Content-Type: application/x-msexcel; name=\"$strExcelFileName\"");
header("Content-Disposition: inline; filename=\"$strExcelFileName\"");
header("Pragma:no-cache");

$numRow = 0;
$con = "";


$con = "";

if($apInvoiceNo != "")
{
  $con .= " and jo.apinvoice_no ='". $apInvoiceNo ."'";
}

$sql = "SELECT a.*, d.department_name, d.address, d.tax, d.tel
FROM  tb_apinvoice a, tb_department_master d
where a.apinvoice_no ='$apInvoiceNo' and a.department_id = d.department_id";
//echo $sql;
$query  = mysqli_query($conn,$sql);
$num = mysqli_num_rows($query);

$row = mysqli_fetch_assoc($query);
$apinvoice_id       = $row['apinvoice_id'];
$apinvoice_no       = $row['apinvoice_no'];
$apinvoice_date     = formatDate($row['apinvoice_date'],'d/m/Y');
$maturity_date      = formatDate($row['maturity_date'],'d/m/Y');
$department_id      = $row['department_id'];//รหัสลูกค้า
$credit             = $row['credit'];
$provision          = $row['provision'];
$delivery_cost      = $row['delivery_cost'];
$discount           = $row['discount'];
$ot                 = $row['ot'];
$total_cost         = $row['total_cost'];
$remark             = $row['remark'];
$department_name    = $row['department_name'];
$address            = $row['address'];
$tax                = $row['tax'];
$tel                = $row['tel'];
?>
<html xmlns:o="urn:schemas-microsoft-com:office:office"xmlns:x="urn:schemas-microsoft-com:office:excel"xmlns="http://www.w3.org/TR/REC-html40">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<div id="SiXhEaD_Excel" align=center x:publishsource="Excel">
  <table style="width:800px;font-size:12pt;" id="tableDisplayExp">
    <thead>
      <tr>
        <td colspan="10" style="font-size:16pt;padding:15px;" align="center">บริษัท โชควิรัตน์ จำกัด</td>
      </tr>
      <tr>
        <td colspan="10" style="padding-bottom:5px;font-size:12pt;" align="center">24 หมู่ 1 ต.พระขาว อ.บางบาล จ.พระนครศรีอยุธยา เลขประจำตัวผู้เสียภาษี 0145566002853</td>
      </tr>
      <tr>
        <td colspan="10" style="padding:5px;font-size:14pt;" align="center">ใบจ่ายรถร่วม</td>
        </td>
      </tr>
      <tr>
        <td colspan="10" style="width:200px"></td>
      </tr>
      <tr>
  			<td colspan="6">
        <table style="width:100%;">
          <tr>
            <td style="width:120px;padding-top:5px;font-size:14px;" align="left">ชื่อ-Name</td>
            <td style="padding:5px;width:2px;font-size:14px;" align="left">:</td>
            <td style="padding-top:5px;font-size:14px;" align="left"><?= $department_name ?></td>
          </tr>
          <tr>
            <td style="padding-top:5px;font-size:14px;" align="left">ที่อยู่-Address</td>
            <td style="padding:5px;font-size:14px;" align="left">:</td>
            <td style="padding-top:5px;font-size:14px;" align="left"><?= $address ?></td>
          </tr>
        </table>
       </td>
  			<td colspan="4">
          <table style="width:100%;">
            <tr>
              <td style="width:70px;padding-top:5px;font-size:14px;" align="left">เลขที่-No</td>
              <td style="padding:5px;width:2px;font-size:14px;" align="left">:</td>
              <td style="padding-top:5px;font-size:14px;" align="left"><?= $apinvoice_no ?></td>
            </tr>
            <tr>
              <td style="padding-top:5px;font-size:14px;" align="left">วันที่</td>
              <td style="padding:5px;font-size:14px;" align="left">:</td>
              <td style="padding-top:5px;font-size:14px;" align="left"><?= $apinvoice_date ?></td>
            </tr>
          </table>
        </td>
  		</tr>
      <tr>
  			<th style="border: 1px solid;padding:5px;width:30px;vertical-align: middle;">ลำดับ</th>
  			<th style="border: 1px solid;padding:5px;width:50px;vertical-align: middle;">วันที่</th>
  			<th style="border: 1px solid;padding:5px;vertical-align:middle;">Shipment</th>
  			<th style="border: 1px solid;padding:5px;vertical-align:middle;">เลขที่ใบจ่าย<br>(DP)</th>
  			<th style="border: 1px solid;padding:5px;vertical-align:middle;">รายการ</th>
  			<th style="border: 1px solid;padding:5px;vertical-align:middle;">spac</th>
        <th style="border: 1px solid;padding:5px;width:55px;vertical-align:middle;">ทะเบียนรถ</th>
  			<th style="border: 1px solid;padding:5px;width:55px;vertical-align:middle;">จำนวนตัน/เที่ยว</th>
  			<th style="border: 1px solid;padding:5px;width:55px;vertical-align:middle;">ค่าขนส่ง</th>
  			<th style="border: 1px solid;padding:5px;width:60px;vertical-align:middle;">จำนวนเงิน</th>
  		</tr>
    </thead>
    <tbody>

  <?php
  //$sql = "SELECT * FROM tb_po_customer pc, tb_customer_master cm, tb_employee_master em where pc.Cust_ID = cm.Cust_ID and pc.Employee_No = em.Employee_No $con ";
  $sql = "SELECT jo.*,em.employee_name,t.license_plate
  FROM tb_job_order jo, tb_employee_master em, tb_trailer t
  where jo.employee_id = em.employee_id $con and  jo.trailer_id = t.trailer_id  $con
  order by jo.job_order_date";
  //echo $sql;
  $query  = mysqli_query($conn,$sql);
  $num = mysqli_num_rows($query);

  $totalAmont = 0;
  for ($i=1; $i <= $num ; $i++) {
    $row = mysqli_fetch_assoc($query);
    $Employee_Name      = $row['employee_name'];
    $license_plate      = $row['license_plate'];
    $id                 = $row['id'];
    $job_order_no       = $row['job_order_no'];
    $delivery_type_id   = $row['delivery_type_id'];//รูปแบบการขนส่ง
    $job_order_date     = formatDate($row['job_order_date'],'d/m/y');//วันที่ออกใบสั่งงาน
    $job_delivery_date  = formatDate($row['job_delivery_date'],'d/m/Y');;//วันที่กำหนดส่งสินค้า
    $cust_id            = $row['cust_id'];//รหัสลูกค้า
    $employee_id        = $row['employee_id'];//รหัสพนักงาน
    $trailer_id         = $row['trailer_id'];//หัว หมายเลขทะเบียนรถ
    $away_regi_no       = $row['away_regi_no'];//หาง
    $away_spec_no       = $row['away_spec_no'];//สเปคหาง
    $affiliation_id     = $row['affiliation_id'];//รหัส รถ(สังกัด)
    $route_id           = $row['route_id'];//รหัสเส้นทาง
    $source             = $row['source'];//ต้นทาง
    $destination        = $row['destination'];//ปลายทาง
    $distance           = $row['distance'];//ระยะทาง
    $cust_dp            = $row['cust_dp'];//DP
    $delivery_doc       = $row['delivery_doc'];//ใบส่งของ
    $department         = $row['department'];//หน่วยงาน
    $product_name       = $row['product_name'];//สินค้า
    $number_of_trips    = $row['number_of_trips'];//จำนวนเที่ยว
    $weights            = $row['weights'];//น้ำหนัก(ตัน)
    $shipping_amount    = $row['shipping_amount'];//ราคาค่าขนส่ง
    $date_product_in    = $row['date_product_in'];//วันทีรับสินค้า
    $time_product_in    = $row['time_product_in'];//เวลารับสินค้า
    $date_product_out   = $row['date_product_out'];//วันที่ออกสินค้า
    $time_product_out   = $row['time_product_out'];//เวลาออกสินค้า
    $date_delivery_in   = $row['date_delivery_in'];//วันทีส่งสินค้า
    $time_delivery_in   = $row['time_delivery_in'];//เวลาเข้าส่งสินค้า
    $date_delivery_out  = $row['date_delivery_out'];//วันที่ออกส่งสินค้า
    $time_delivery_out  = $row['time_delivery_out'];//เวลาออกส่งสินค้า
    $fuel_cost          = $row['fuel_cost'];//ค่าน้ำมัน
    $fuel_litre         = $row['fuel_litre'];//จำนวนลิตร
    $blank_charge       = $row['blank_charge'];//ค่าตีเปล่า
    $allowance          = $row['allowance'];//เบี้ยเลี้ยง
    $kog_expense        = $row['kog_expense'];//หางคอก
    $allowance_oth      = $row['allowance_oth'];//เบี้ยงเลี้ยงอื่นๆ
    $shipment           = $row['shipment'];
    $price_type_id      = $row['price_type_id'];//รหัสการคำนวณ
    $one_trip_ton       = $row['one_trip_ton'];//ราคาต่อตัน
    $ext_one_trip_ton   = $row['ext_one_trip_ton'];//ราคารถร่วมต่อตัน
    $price_per_trip     = $row['price_per_trip'];//ราคาต่อเที่ยว
    $ext_price_per_trip = $row['ext_price_per_trip'];//ราคารถร่วมต่อเที่ยว
    $total_amount_receive = $row['total_amount_receive'];//ราคาค่าขนส่ง
    $total_amount_ap_pay  = $row['total_amount_ap_pay'];//ราคาจ่ายรถร่วม
    $total_amount_allowance   = $row['total_amount_allowance'];//รวมค่าใช้จ่าย
    $job_ended_clearance      = $row['job_ended_clearance'];//ค่าเคลียร์ค่าปรับ
    $job_ended_recap          = $row['job_ended_recap'];//ค่าปะยาง
    $job_ended_expressway     = $row['job_ended_expressway'];//ค่าทางด่วน
    $job_ended_passage_fee    = $row['job_ended_passage_fee'];//ค่าธรรมเนียมผ่านท่าเรือ
    $job_ended_repaires       = $row['job_ended_repaires'];//ค่าซ่อม
    $job_ended_acc_expense    = $row['job_ended_acc_expense'];//ค่าทำบัญชี
    $job_ended_other_expense  = $row['job_ended_other_expense'];//คชจ.อื่นๆ
    $fuel_driver_bill         = $row['fuel_driver_bill'];//บิลน้ำมันจากคนขับ
    $job_status_id            = $row['job_status_id'];

    $period_code              = $row['period_code'];


    $price = 0;
    $amount = 0;
    if($price_type_id == 1){
      $amount =  $weights;
      $price  =  $ext_one_trip_ton;
      $total  =  ($weights * $ext_one_trip_ton);
    }else{
      $amount = 1;
      $price  = $ext_price_per_trip;
      $total  = $ext_price_per_trip;
    }
    $totalAmont += $total;
  ?>
    <tr>
      <td style="border-left: 1px solid;padding:5px;vertical-align:top;" class="text-center"><?= $i ?></td>
      <td style="border-left: 1px solid;padding:5px;vertical-align:top;" class="text-center" ><?= $job_order_date ?></td>
      <td style="border-left: 1px solid;padding:5px;vertical-align:top;" class="text-center" ><?= $shipment ?></td>
      <td style="border-left: 1px solid;padding:5px;vertical-align:top;" class="text-left" ><?= $cust_dp ?></td>
      <td style="border-left: 1px solid;padding:5px;vertical-align:top;" class="text-left" ><?= $source." - ".$destination  ?></td>
      <td style="border-left: 1px solid;padding:5px;vertical-align:top;" class="text-left" ><?= $away_spec_no ?></td>
      <td style="border-left: 1px solid;padding:5px;vertical-align:top;" class="text-left" ><?= $license_plate ?></td>
      <td style="border-left: 1px solid;padding:5px;vertical-align:top;" class="text-right" ><?= number_format($amount,3);?></td>
      <td style="border-left: 1px solid;padding:5px;vertical-align:top;" class="text-right" ><?= number_format($price,2); ?></td>
      <td style="border-left: 1px solid;border-right: 1px solid;padding:5px;vertical-align:top;" class="text-right" ><?= number_format($total,2); ?></td>
    </tr>
  <?php }
    if($num < 20){
      for($i = $num; $i< 20; $i++){
        echo "<tr>";
          echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;'>&nbsp;</td>";
          echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;'>&nbsp;</td>";
          echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;'>&nbsp;</td>";
          echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;'>&nbsp;</td>";
          echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;'>&nbsp;</td>";
          echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;'>&nbsp;</td>";
          echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;'>&nbsp;</td>";
          echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;'>&nbsp;</td>";
          echo "<td style='border-left: 1px solid;padding:5px;vertical-align:top;'>&nbsp;</td>";
          echo "<td style='border-left: 1px solid;border-right: 1px solid;padding:5px;vertical-align:top;'>&nbsp;</td>";
        echo "</tr>";
      }
    }
  ?>
  <tr>
    <td colspan="2" rowspan="3" style="border-left: 1px solid;border-top: 1px solid;border-bottom: 1px solid;padding:5px;vertical-align:top;"  class="text-left" >หมายเหตุ :</td>
    <td colspan="4" rowspan="3" style="border-right: 1px solid;border-top: 1px solid;border-bottom: 1px solid;padding:5px;vertical-align:top;"  class="text-left" ><?=$remark; ?></td>
    <td colspan="2" style="padding:5px;border-top: 1px solid;" class="text-left">รวมเงิน</td>
    <td colspan="2" style="border-right: 1px solid;padding:5px;border-top: 1px solid;" class="text-right"><b><?= number_format($delivery_cost,2); ?></b></td>
  </tr>
  <tr>
    <td colspan="2" style="padding:5px;"class="text-left">หัก ณ ที่จ่าย 1%</td>
    <td colspan="2" style="border-right: 1px solid;padding:5px;" class="text-right"><b><?= number_format($discount,2); ?></b></td>
  </tr>
  <tr>
    <td colspan="2" style="padding:5px;"class="text-left">สำรองจ่าย</td>
    <td colspan="2" style="border-right: 1px solid;padding:5px;" class="text-right"><b><?= number_format($provision,2); ?></b></td>
  </tr>
  <tr>
    <td colspan="6" style="border: 1px solid;padding:5px;" class="text-center" ><b><?= convert(number_format($total_cost,2));?></b></td>
    <td colspan="2" style="border-bottom: 1px solid;padding:5px;" class="text-left">รวมเงินสุทธิ</td>
    <td colspan="2" style="border-right: 1px solid;border-bottom: 1px solid;padding:5px;" class="text-right"><b><?= number_format($total_cost,2); ?></b></td>
  </tr>
  <tr>
    <td colspan="3" style="border: 1px solid;padding:5px;" align="center">
      <p>ผู้จัดทำเอกสาร-prepared by</p><br>
      <p>...................................................</p>
      <p>(.........../.........../...........)</p>
    </td>
    <td colspan="3" style="border: 1px solid;padding:5px;" align="center">
      <p>ผู้อนุมัติ-approved by</p><br>
      <p>...................................................</p>
      <p>(.........../.........../...........)</p></td>
    <td colspan="4" style="border: 1px solid;padding:5px;" align="center">
      <p>ผู้รับเอกสาร-received by</p><br>
      <p>...................................................</p>
      <p>(.........../.........../...........)</p></td>
    </td>
  </tr>
  </tbody>
  </table>
</div>
<script>
window.onbeforeunload = function(){return false;};
setTimeout(function(){window.close();}, 10000);
</script>
</body>
</html>
