<!DOCTYPE html>
<html>

<?php
include("inc/head.php");
$typeCompany = 2;
$typeCompanyName = "(เหล็ก)";
if($typeCompany == 2){
  $typeCompanyName = "(ไปรษณีย์)";
}
?>
<body class="login-page" style="height:100%">

  <div class="row bg" style="height:100%">
      <div class="col-md-4 col-md-offset-4" >
        <div class="login">
          <div align="center"><img src="images/logo.png" class="logo-login">
          <h4 style="color:#333;">เข้าสู่ระบบ<br><br>TRAILER CONTROL SYSTEMS <?= $typeCompanyName ?></h3></div>
          <form id="form1" smk-icon="glyphicon-remove" novalidate autocomplete="off">
              <div class="form-group">
                <input name="user" id="user" type="text" class="form-control" placeholder="Username" name="user" id="user" smk-text="กรุณากรอก Username" required>
              </div>
              <div class="form-group">
                <input name="pass" type="password" autocomplete="new-password" class="form-control" placeholder="Password" name="pass" id="pass" smk-text="กรุณากรอก Password" required>
                <input type="hidden" name="typeCompany" value="<?= $typeCompany ?>">
              </div>
              <button type="submit" class="btn bg-navy btn-block non-radius">LOGIN</button>
          </form>
        </div>
      </div>
    </div>

<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script type="text/javascript">
$(document).ready(function() {
    $("#user").focus();
    $('#form1').on("submit",function(e) {
        if ($("#form1").smkValidate()) {
          $.ajax({
                url: 'ajax/login/checkLogin.php',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                dataType: 'json'
            }).done(function( data ) {
                if (data.status == "danger") {
                    $.smkAlert({text: data.message , type: data.status});
                    $("#form1").smkClear();
                    $("#user").focus();
                } else {
                    $.smkProgressBar({
                      element:'body',
                      status:'start',
                      bgColor: '#000',
                      barColor: '#fff',
                      content: 'Loading...'
                    });
                    setTimeout(function(){
                      $.smkProgressBar({
                        status:'end'
                      });
                      window.location='index.php';
                    }, 1000);
                  }
            });
            e.preventDefault();
        }
       e.preventDefault();
    });
});

</script>
</body>
</html>
