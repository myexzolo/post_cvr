function showFormJobOrder(id,page){
  postURL('job_order.php?id='+id+"&page="+page);
}

function showFormJobOrder2(id,page){
  postURL('job_order2.php?id='+id+"&page="+page);
}

$(function() {
    $("#dps").autocomplete({
        source: "ajax/po/listDP.php",
        minLength: 5,
        select: function( event, ui ) {
            event.preventDefault();
            $("#dps").val(ui.item.value);
            var id    =  ui.item.id;
            var page  = $("#page").val();
            var url = 'job_order.php?id='+id+"&page="+page;
            //alert(url);
            postURL(url);
        }
    });
});

$(function() {
    // $("#license_plate").autocomplete({
    //     source: "ajax/po/listTrailer.php",
    //     minLength: 2,
    //     select: function( event, ui ) {
    //         event.preventDefault();
    //         $("#license_plate").val(ui.item.value);
    //         $("#trailer_id").val(ui.item.id);
    //     }
    // });
});

function setdataTrailer(obj){
  $.post("ajax/po/showJobRoute.php",{id:obj.value})
    .done(function( data ) {
      $('#show-form').html(data);
      setTimeout(function(){
          $('#tableDisplay').show();
      }, 1);
  });

}

function printPO(id)
{

  var url = "print_po.php?id=" + id;
  postURLBlank(url, "");
}


function postURLBlank(url, multipart) {
 var form = document.createElement("FORM");
 form.method = "POST";
 if(multipart) {
   form.enctype = "multipart/form-data";
 }
 form.style.display = "none";
 document.body.appendChild(form);
 form.target="_blank";
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}


function show(date,status){
  $.post("ajax/po/list.php",{startDate:date,status:status})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function show2(date,status){
  $.post("ajax/po/list2.php",{startDate:date,status:status})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function setAllowance(){
  var employee_id2     = $('#employee_id2').val();
  if(employee_id2 != ""){
    $('#second_emp').val($('#allowance').val());
  }else{
    $('#second_emp').val(0);
  }
}

function reset2(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  var status        = $('#status').val("1");
  var employeeId    = $('#employeeId').val("");
  var jobOrderNo    = $('#jobOrderNo').val("");

  search2();
}


function reset(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  var status        = $('#status').val("1");
  var employeeId    = $('#employeeId').val("");
  var jobOrderNo    = $('#jobOrderNo').val("");

  search();
}

function chooseTrailer(){
  var trailer_id  = $('#trailer_id').val();
  if(trailer_id != ""){
    var data = trailer_id.split(":");
    $('#affiliation_id').val(data[1]).trigger('change');
    $('#affiliation_id').smkValidate();
  }else{
    $('#affiliation_id').val("").trigger('change');
  }

}

function searchRoute(){
  var searchBy  = $('#searchBy').val();
  var searchTxt = $('#searchTxt').val();

  $.post("ajax/po/showJobRoute.php",{searchBy:searchBy,searchTxt,searchTxt})
    .done(function( data ) {
      $('#show-form').html(data);
      setTimeout(function(){
          $('#tableDisplay').show();
      }, 1);
  });


}

function genJobOrderNo(){
  var dateOrder  = $('#job_order_date').val();
  //alert(dateOrder);
  $.post("ajax/po/genJobOrderNo.php",{dateOrder:dateOrder})
    .done(function( data ) {
      //alert(data.jobOrderNo);
      if(data.status == 'success'){
        $('#job_order_no').val(data.jobOrderNo);
      }
  });
}

function search2(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var status        = $('#status').val();
  var employeeId    = $('#employeeId').val();
  var jobOrderNo    = $('#jobOrderNo').val();
  var dp            = $('#dp').val();
  var trailer_id    = $('#trailer_id').val();
  var cust_id       = $('#cust_id').val();

  $.post("ajax/po/list2.php",{startDate:startDate,endDate:endDate,status:status,employeeId:employeeId,page:'PO',jobOrderNo:jobOrderNo,dp:dp,trailer_id:trailer_id,cust_id:cust_id})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var status        = $('#status').val();
  var employeeId    = $('#employeeId').val();
  var jobOrderNo    = $('#jobOrderNo').val();
  var dp            = $('#dp').val();
  var trailer_id    = $('#trailer_id').val();

  $.post("ajax/po/list.php",{startDate:startDate,endDate:endDate,status:status,employeeId:employeeId,page:'PO',jobOrderNo:jobOrderNo,dp:dp,trailer_id:trailer_id})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function showForm(){
  $.post("ajax/po/showJobRoute.php")
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#show-form').html(data);
      $('#tableDisplay').show();
  });
}

function showFormDP(){
    $('#alertTxt').css("visibility", "hidden");
    $('#blank').remove();
    $(".trDP").remove();
    var dp = $('#cust_dp').val();
    $('#tmp_dp').val("");
    var tr = "";
    if(dp == ""){
      tr = "<tr id='blank'>"+
               "  <td>&nbsp;</td>"+
               "  <td></td>"+
               "  <td></td>"+
               "</tr>";
    }else{
      var arr = dp.split(" ");
      for( var i = 0; i < arr.length; i++ ) {
        var value =  arr[i];
        var id    = "id_"+i;
        var num   = (i+1);
        tr += "<tr id='"+id+"' class='trDP'>"+
                 "  <td class='tdnumber' align='center'>"+num+"</td>"+
                 "  <td class='trCode'>"+value+"</td>"+
                 "  <td align='center'><button type='button' class='btn btn-warning btn-xs btn-flat' onclick=\"delDP('"+id+"')\"><i class='fa fa-trash'></i></button></td>"+
              "</tr>";
      }
    }
    $('#tableDP tr:last').after(tr);
    $('#myModal2').modal('toggle');
    $('#txtDP').focus();
}

function addDP(){
  var code = $('#txtDP').val();
  var tmp_dp = $('#tmp_dp').val();
  if(code != ""){
    $.get("ajax/po/manageDp.php?code="+code+"&type=ADD")
      .done(function( data ) {
        if(data.add == "N"){
          $('#alertTxt').html("* เลข DP ซ้ำ");
          $('#alertTxt').css("visibility", "visible");
        }else{
          $('#txtDP').val("");
          $('#alertTxt').html("");
          $('#blank').remove();
          var rowCount = $('#tableDP tr').length;
          //rowCount = rowCount - 1;
          //alert(rowCount);
          //code = code.replace(/,/g, '-');
          codeId = code.replace(/[#|_|,|+|-|*|&|.]/g,'X');
          var id = "id_"+codeId;
          var tr = "<tr id='"+id+"' class='trDP'>"+
                   "  <td class='tdnumber' align='center'>"+rowCount+"</td>"+
                   "  <td class='trCode'>"+code+"</td>"+
                   "  <td align='center'><button type='button' class='btn btn-warning btn-xs btn-flat' onclick=\"delDP('"+id+"')\"><i class='fa fa-trash'></i></button></td>"+
                "</tr>";
          $('#tableDP tr:last').after(tr);
          if(tmp_dp == ""){
            tmp_dp = code;
          }else{
            tmp_dp += " "+code;
          }
          $('#tmp_dp').val(tmp_dp);
        }
        $('#txtDP').focus();
    });
  }else{
    $('#txtDP').focus();
    $('#alertTxt').html("* กรอกเลขที่ DP");
    $('#alertTxt').css("visibility", "visible");
  }
}
function clrfs(){
  if($('#petrol_station_tmp').val() != ""){
      $.smkRemoveError($('#petrol_station_tmp'));
  }
}

function clrfc(){
  if($('#fuel_cost_tmp').val() != ""){
      $.smkRemoveError($('#fuel_cost_tmp'));
  }
}

function clrfl(){
  if($('#fuel_litre_tmp').val() != ""){
      $.smkRemoveError($('#fuel_litre_tmp'));
  }
}

function clrfd(){
  // if($('#fuel_date_tmp').val() != ""){
  //     $.smkRemoveError($('#fuel_date_tmp'));
  // }
}

function addPetrol(){
  //alert($('#petrol_station_tmp').val());
  $.smkRemoveError($('#petrol_station_tmp'));
  $.smkRemoveError($('#fuel_cost_tmp'));
  $.smkRemoveError($('#fuel_litre_tmp'));
  $.smkRemoveError($('#fuel_total_tmp'));
  $.smkRemoveError($('#fuel_receipt_tmp'));
  $.smkRemoveError($('#fuel_date_tmp'));
  var flag = "Y";

  if($('#petrol_station_tmp').val() == ""){
    $.smkAddError($('#petrol_station_tmp'), "กรอกปั้มน้ำมัน");
    flag = "N";
  }
  if($('#fuel_cost_tmp').val() == ""){
    $.smkAddError($('#fuel_cost_tmp'), "กรอกค่าน้ำมัน");
    flag = "N";
  }
  if($('#fuel_litre_tmp').val() == ""){
    $.smkAddError($('#fuel_litre_tmp'), "กรอกจำนวนลิตร");
    flag = "N";
  }
  // if($('#fuel_date_tmp').val() == ""){
  //   $.smkAddError($('#fuel_date_tmp'), "กรอกวันที่");
  //   flag = "N";
  // }

  if(flag == "Y"){
    $('#blankPet').remove();
    var rowCount = $('#tablePetrol tr').length;

    var petrol_station_tmp  = $('#petrol_station_tmp').val();
    var fuel_cost_tmp       = parseFloat(checkNumeric($('#fuel_cost_tmp').val().replace(/,/g, "")));
    var fuel_litre_tmp      = parseFloat(checkNumeric($('#fuel_litre_tmp').val().replace(/,/g, "")));

    var fuel_date_tmp      = $('#fuel_date_tmp').val();
    var date_tmp           = "";
    if(fuel_date_tmp!= ""){
      date_tmp  = formatDateTH(fuel_date_tmp);
    }

    var res = petrol_station_tmp.split("|");
    var petrol_station_tmp  = res[1];
    var petrol_code_tmp     = res[0];

    var fuel_receipt_tmp    = $('#fuel_receipt_tmp').val();
    var fuel_total_tmp      = parseFloat(fuel_cost_tmp * fuel_litre_tmp);

    var id = "id_" + Math.floor(Math.random() * 101);

    var tr = "<tr id='"+id+"'>"+
             "  <td align='left'>"+petrol_station_tmp+
             "   <input value='"+petrol_station_tmp+"' name='petrol_station_s[]' type='hidden' class='form-control' >"+
             "   <input value='"+petrol_code_tmp+"' name='petrol_code_s[]' type='hidden' class='form-control' ></td>"+
             "  <td align='right' class='trCodePet'>"+fuel_cost_tmp+
             "  <input value='"+fuel_cost_tmp+"' id='fuel_cost_s_"+id+"' name='fuel_cost_s[]' type='hidden' class='form-control' ></td>"+
             "  <td align='right'>"+fuel_litre_tmp+
             "  <input value='"+fuel_litre_tmp+"' id='fuel_litre_s_"+id+"' name='fuel_litre_s[]' type='hidden' class='form-control' ></td>"+
             "  <td align='right'>"+addCommas(fuel_total_tmp.toFixed(2))+
             "  <input value='"+fuel_total_tmp+"' id='fuel_total_s_"+id+"' name='fuel_total_s[]' type='hidden' class='form-control' ></td>"+
             "  <td align='right'>"+fuel_receipt_tmp+
             "  <input value='"+fuel_receipt_tmp+"' id='fuel_receipt_s_"+id+"' name='fuel_receipt_s[]' type='hidden' class='form-control' ></td>"+
             "  <td align='right'>"+date_tmp+
             "  <input value='"+fuel_date_tmp+"' id='fuel_date_s_"+id+"' name='fuel_date_s[]' type='hidden' class='form-control' ></td>"+
             "  <td align='center'><button type='button' class='btn btn-warning btn-xs btn-flat' onclick=\"delPet('"+id+"')\"><i class='fa fa-trash'></i></button></td>"+
             "</tr>";
    $('#tablePetrol tr:last').after(tr);

    var fuel_cost   = parseFloat(checkNumeric($('#fuel_cost').val().replace(/,/g, "")));
    var fuel_litre  = parseFloat(checkNumeric($('#fuel_litre').val().replace(/,/g, "")));

    var fuel_costs  =  (fuel_cost + fuel_total_tmp);
    var fuel_litres = (fuel_litre + fuel_litre_tmp);
    //alert(fuel_cost + " ," + fuel_cost_tmp + " ," + fuel_costs);
    $('#fuel_cost').val(fuel_costs.toFixed(2));
    $('#fuel_litre').val(fuel_litres);
    //var fuel_costs = parseFloat(fuel_costs * fuel_litres);
    //$('#fuel_total').val(addCommas(fuel_costs.toFixed(2)));

    $.smkRemoveError($('#fuel_cost'));
    $.smkRemoveError($('#fuel_litre'));

    $('#petrol_station_tmp').val("");
    $('#fuel_cost_tmp').val("");
    $('#fuel_litre_tmp').val("");
    $('#fuel_receipt_tmp').val("");
    $('#fuel_date_tmp').val("");
  }

}

function addDPtxt(){
  var codes = "";
  $('.trCode').each(function() {
    var cellText = $(this).html();
    if(codes ==""){
      codes = cellText;
    }else{
      codes += " "+cellText;
    }
  });
  $('#cust_dp').val(codes);
  $('#myModal2').modal('hide');
  $('#cust_dp').smkValidate();
}


function delPet(id){

  var fuel_cost_id  = "#fuel_cost_s_"+id;
  var fuel_litre_id = "#fuel_litre_s_"+id;

  var fuel_cost_tmp   = parseFloat(checkNumeric($(fuel_cost_id).val().replace(/,/g, "")));
  var fuel_litre_tmp  = parseFloat(checkNumeric($(fuel_litre_id).val().replace(/,/g, "")));

  var fuel_cost   = parseFloat(checkNumeric($('#fuel_cost').val().replace(/,/g, "")));
  var fuel_litre  = parseFloat(checkNumeric($('#fuel_litre').val().replace(/,/g, "")));


  var fuel_costs  =  (fuel_cost - (fuel_cost_tmp*fuel_litre_tmp));
  var fuel_litres = (fuel_litre - fuel_litre_tmp);

  $('#'+id).remove();

  $('#fuel_cost').val(fuel_costs.toFixed(2));
  $('#fuel_litre').val(fuel_litres);

  if($('.trCodePet').length == 0){
    tr = "<tr id='blankPet'>"+
         "  <td>&nbsp;</td>"+
         "  <td></td>"+
         "  <td></td>"+
         "  <td></td>"+
         "</tr>";
    $('#tablePetrol tr:last').after(tr);
  }
}


function delDP(id){
  $('#'+id).remove();
  var num = 0;
  $('.tdnumber').each(function() {
    num++;
    $(this).html(num);
  });

  if($('.trCode').length == 0){
    tr = "<tr id='blank'>"+
         "  <td>&nbsp;</td>"+
         "  <td></td>"+
         "  <td></td>"+
         "</tr>";
    $('#tableDP tr:last').after(tr);
  }
  $('#txtDP').focus();
}


function cancelPo(id,jobOrderNo){
  $.smkConfirm({
    text:'ยืนยันการยกเลิกใบสั่งงาน เลขที่ :'+ jobOrderNo,
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/po/cancelPo.php",{id:id})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            search();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

function calAmount(){
  if ($('#formData').smkValidate()) {
    var affiliation_id        = $('#affiliation_id').val();//รถ(สังกัด) 1= รถบริษัท 2=รถร่วมบริษัท 3= รถร่วม
    var price_type_id         = $('#price_type_id').val();//ประเภทการคำนวณราคา 1=ราคาต่อตัน 2=ราคาต่อเที่ยว

    var weights               = $('#weights').val();//น้ำหนัก(ตัน)
    var total_amount_allowance  = $('#total_amount_allowance').val();//รวมค่าใช้จ่าย

    var total_amount_receive  = $('#total_amount_receive').val();//ราคาค่าขนส่ง
    var total_amount_ap_pay   = $('#total_amount_ap_pay').val();//ราคาจ่ายรถร่วม

    if(affiliation_id == ""){
      $("#affiliation_id" ).focus();
      //$('#affiliation_id').smkValidate();
      $.smkAddError($('#affiliation_id'), "เลือกรถ(สังกัด)");
    }else if(weights == ""){
      $( "#weights" ).focus();
      //$('#weights').smkValidate();
      $.smkAddError($('#weights'), "ระบุ น้ำหนัก(ตัน)");
    }else if(price_type_id == ""){
      $( "#price_type_id" ).focus();
      $.smkAddError($('#price_type_id'), "เลือกการคำนวณราคา");
    }else{
      $.smkRemoveError($('#affiliation_id'));
      $.smkRemoveError($('#weights'));
      $.smkRemoveError($('#price_type_id'));

      if(price_type_id == 1){////ต่อตัน
          var one_trip_ton          = $('#one_trip_ton').val();//ราคาต่อตัน
          var ext_one_trip_ton      = $('#ext_one_trip_ton').val();//ราคารถร่วมต่อตัน

          total_amount_receive = (checkNumeric(one_trip_ton) * weights);
          $('#total_amount_receive').val(total_amount_receive.toFixed(2));

          total_amount_ap_pay = (checkNumeric(ext_one_trip_ton) * weights);
          $('#total_amount_ap_pay').val(total_amount_ap_pay.toFixed(2));



      }else if(price_type_id == 2){////เที่ยว
        total_amount_receive  = parseFloat($('#price_per_trip').val());//ราคาต่อเที่ยว
        total_amount_ap_pay   = parseFloat($('#ext_price_per_trip').val());//ราคารถร่วมต่อเที่ยว

        $('#total_amount_receive').val(total_amount_receive.toFixed(2));
        $('#total_amount_ap_pay').val(total_amount_ap_pay.toFixed(2));
      }

      var total   = 0;
      var profit  = 0;
      if(affiliation_id == 3){
        total =  parseFloat($('#total_amount_ap_pay').val());
        $('#total_amount_allowance').val(total.toFixed(2));
        profit = (total_amount_receive - total_amount_ap_pay);
      }if(affiliation_id == 2){

      }if(affiliation_id == 1){
        var Fuel_Cost       = checkNumeric($('#fuel_cost').val());//ค่าน้ำมัน
        var fuel_Litre      = checkNumeric($('#fuel_litre').val());//จำนวนลิตร
        var Blank_Charge    = checkNumeric($('#blank_charge').val());//ค่าตีเปล่า
        var allowance       = checkNumeric($('#allowance').val());//เบี้ยเลี้ยง
        var second_emp      = checkNumeric($('#second_emp').val());//เบี้ยเลี้ยง ผู้ช่วย
        var kog_expense     = checkNumeric($('#kog_expense').val());//หางคอก
        var job_ended_acc_expense  = checkNumeric($('#job_ended_acc_expense').val());//คชจ.บัญชี
        var allowance_oth   = checkNumeric($('#allowance_oth').val());//อื่นๆ

        var t1 = (parseFloat(Fuel_Cost) + parseFloat(Blank_Charge) + parseFloat(allowance)+ parseFloat(second_emp) + parseFloat(kog_expense) + parseFloat(job_ended_acc_expense)+ parseFloat(allowance_oth));

        var jobEndedClearance     = checkNumeric($('#job_ended_clearance').val()); //ค่าปรับ
        var jobEndedRecap         = checkNumeric($('#job_ended_recap').val()); //ปะยาง
        var jobEndedExpressway    = checkNumeric($('#job_ended_expressway').val()); //ทางด่วน
        var jobEndedPassageFee    = checkNumeric($('#job_ended_passage_fee').val()); //ผ่านท่าเรือ
        var jobEndedRepaires      = checkNumeric($('#job_ended_repaires').val()); //ค่าซ่อม
        var jobEndedOtherExpense  = checkNumeric($('#job_ended_other_expense').val()); //ค่าใช้จ่ายอื่นๆ

        var t2 = (parseFloat(jobEndedClearance) + parseFloat(jobEndedRecap) + parseFloat(jobEndedExpressway) + parseFloat(jobEndedPassageFee) + parseFloat(jobEndedRepaires) + parseFloat(jobEndedOtherExpense));

        total = (t1 + t2);
        $('#total_amount_allowance').val(total.toFixed(2));
        profit = (total_amount_receive - total);
      }


      $('#job_order_profit').val(profit.toFixed(2));

    }
  }
}

function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = num;
  }
  return number;
}


function setDataRoute(route_id,source,destination,distance,allowance,kogExpense,allowanceOth,jobEndedAccExpense,oneTripTon,pricePerTrip,extOneTripTon,extPricePerTrip){
  $('#route_id').val(route_id);
  $('#source').val(source);
  $('#destination').val(destination);
  $('#distance').val(distance);
  $('#allowance').val(allowance);
  $('#kog_expense').val(kogExpense);
  $('#allowance_oth').val(allowanceOth);
  $('#job_ended_acc_expense').val(jobEndedAccExpense);
  $('#one_trip_ton').val(oneTripTon);
  $('#price_per_trip').val(pricePerTrip);
  $('#ext_one_trip_ton').val(extOneTripTon);
  $('#ext_price_per_trip').val(extPricePerTrip);

  var employee_id2     = $('#employee_id2').val();
  if(employee_id2 != ""){
    $('#second_emp').val(allowance);
  }else{
    $('#second_emp').val(0);
  }


  $('#myModal').modal('toggle');
  $('#route_id').smkValidate();
  $('#distance').smkValidate();
}



function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     document.getElementById("tableDisplay").style.fontSize = "11px";
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

function printInvice(){
  var receive_id = $('#receive_id').val();
  var data = $('#data').val();
  var name = $('#name').val();
  var invoice_code = $('#invoiceCode').val();
  var taxId = $('#taxId').val();
  var address = $('#address').html();
  alert(address);
  //var pam = "";
  var pam = "?receive_id="+receive_id+"&&name="+name+"&&data="+data+"&&invoice_code="+invoice_code+"&&taxId="+taxId+"&&address="+address;
  //alert(pam);
  //var myWindow = window.open("pos_process.php"+pam, "", "width=5,height=5");
  var myWindow = window.open("print_invoice.php"+pam,'', 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no,visible=none', '');
  $('#myModal').modal('hide');
  $('#formAdd').smkClear();
}


$('#formDP').on('submit', function(event) {
  event.preventDefault();
  addDPtxt();
});


$('#form-data').on('submit', function(event) {
  event.preventDefault();
  if ($('#form-data').smkValidate()) {
    var page = $('#page').val();
    if (page == "JOB"){
      calAmount();
    }
    $.ajax({
        url: 'ajax/po/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#form-data').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        if(page == "PO"){
          gotoPage("po_list.php");
        }else{
          gotoPage("job_list.php");
        }
      }, 1000);
    }).fail(function (jqXHR, exception) {
      //alert("fail");
        // setTimeout(function(){
        //   $.smkProgressBar({status:'end'});
        //   $('#formAdd').smkClear();
        //   showPage();
        //   $.smkAlert({text: jqXHR.responseText ,type: 'danger'});
        //   $('#myModal').modal('toggle');
        // }, 1000);
    });
  }
});


$('#form-data2').on('submit', function(event) {
  event.preventDefault();
  if ($('#form-data2').smkValidate()) {
    var page = $('#page').val();
    if (page == "JOB"){
      calAmount();
    }
    $.ajax({
        url: 'ajax/po/manage2.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#form-data').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        if(page == "PO"){
          gotoPage("po_list2.php");
        }else{
          gotoPage("job_list2.php");
        }
      }, 1000);
    }).fail(function (jqXHR, exception) {
      //alert("fail");
        // setTimeout(function(){
        //   $.smkProgressBar({status:'end'});
        //   $('#formAdd').smkClear();
        //   showPage();
        //   $.smkAlert({text: jqXHR.responseText ,type: 'danger'});
        //   $('#myModal').modal('toggle');
        // }, 1000);
    });
  }
});


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
