function reset(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  $('#trailerId').val("");
  $('#employeeId').val("");
  $('#source').val("");
  $('#destination').val("");
  search();
}

function reset2(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  $('#trailerId').val("");
  $('#employeeId').val("");
  $('#source').val("");
  $('#destination').val("");
  search2();
}


function search(){
  var startDate       = $('#startDate').val();
  var endDate         = $('#endDate').val();
  var trailerId       = $('#trailerId').val();
  var employeeId      = $('#employeeId').val();
  var source          = $('#source').val();
  var destination     = $('#destination').val();
  //alert(employeeId);
  $.post("ajax/income/list.php",{startDate:startDate,endDate:endDate,trailerId:trailerId,employeeId:employeeId,source:source,destination:destination})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function list(){
  var year            = $('#s_year').val();
  var month           = $('#s_month').val();
  var trailerId       = $('#trailerId').val();
  //alert(employeeId);
  $.post("ajax/income/list_income.php",{year:year,month:month,trailerId:trailerId})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function resetList(){
  $('#s_month').val($('#month').val());
  $('#s_year').val($('#year').val());
  $('#trailerId').val("");
  list();
}


function list_up(){
  var year            = $('#s_year').val();
  var month           = $('#s_month').val();
  var trailerId       = $('#trailerId').val();
  var employeeId      = $('#employeeId').val();
  var income_h_id     = $('#income_h_id').val();
  //alert(income_h_id);
  $.post("ajax/income/lists.php",{year:year,month:month,trailer_id:trailerId,income_h_id:income_h_id,employeeId:employeeId})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function searchHis(){
  var startDate       = $('#startDate').val();
  var endDate         = $('#endDate').val();
  var trailerId       = $('#trailerId').val();
  var employeeId      = $('#employeeId').val();
  var source          = $('#source').val();
  var destination     = $('#destination').val();
  //alert(employeeId);
  $.post("ajax/income/listHis.php",{startDate:startDate,endDate:endDate,trailerId:trailerId,employeeId:employeeId,source:source,destination:destination})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function editIncome(id,month,year,trailerID){
  var url = "income_up.php?income_h_id="+id+"&month="+month+"&year="+year+"&trailerID="+trailerID;
  postURL(url);
}
function search2(){
  var year            = $('#s_year').val();
  var month           = $('#s_month').val();
  var trailerId       = $('#trailerId').val();
  //alert(employeeId);
  $.post("ajax/income/list2.php",{year:year,month:month,trailerId:trailerId})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}


function calAmount(){
  var jobProfits  = checkNumeric($('#jobProfits').html().replace(/,/g, ''));
  var other1  = checkNumeric($('#other_value1').val().replace(/,/g, '')) ;
  var other2  = checkNumeric($('#other_value2').val().replace(/,/g, ''));
  var other3  = checkNumeric($('#other_value3').val().replace(/,/g, ''));
  // var txtOther1  = checkNumeric($('#txtOther1').val());
  // var txtOther2  = checkNumeric($('#txtOther2').val());
  // var txtOther3  = checkNumeric($('#txtOther3').val());
  var salary  = checkNumeric($('#salary').val().replace(/,/g, ''));
  var bill    = checkNumeric($('#bill').val().replace(/,/g, ''));
  var diligence  = checkNumeric($('#diligence').val().replace(/,/g, ''));
  var labor  = checkNumeric($('#labor').val().replace(/,/g, ''));

  var totalSub = (other1 + other2 + other3 + salary + bill + diligence + labor);
  //alert(jobProfits+" >>> "+ totalSub);
  var total   = (jobProfits - totalSub);
  //alert(total);
  $('#total').val(total.toFixed(2));
}


$("#form-data").one("submit", submitFormFunction);
function submitFormFunction(event) {
    event.preventDefault();
    if ($('#form-data').smkValidate()) {
      calAmount();
      $.ajax({
          url: 'ajax/income/manage.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
        $.smkProgressBar();
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          gotoPage('income_list.php');
          $.smkAlert({text: data.message,type: data.status});
        }, 1000);
      }).fail(function (jqXHR, exception) {
        //alert("fail");
      });
    }
}

function exportExcel(){
  var year            = $('#s_year').val();
  var month           = $('#s_month').val();
  var trailerId       = $('#trailerId').val();

  var url = "export_excel_match.php?year="+year+"&month="+month+"&trailerId="+trailerId;
  window.open(url,'_blank');
}

function printOrder(){
  calAmount();

  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var trailerId       = $('#trailerId').val();
  var employeeId      = $('#employeeId').val();
  var source          = $('#source').val();
  var destination     = $('#destination').val();

  var other1          = $('#other1').val();
  var other2          = $('#other2').val();
  var other3          = $('#other3').val();
  var txtOther1       = $('#txtOther1').val();
  var txtOther2       = $('#txtOther2').val();
  var txtOther3       = $('#txtOther3').val();
  var salary          = $('#salary').val();
  var bill            = $('#bill').val();
  var diligence       = $('#diligence').val();
  var total           = $('#total').val();
  var id_h            = $('#id_h').val();


  //alert(total);
  if(total != 0){
      $.post("ajax/income/listExp.php",{
        startDate:startDate,
        endDate:endDate,
        trailerId:trailerId,
        employeeId:employeeId,
        source:source,
        destination:destination,
        other1:other1,
        other2:other2,
        other3:other3,
        txtOther1:txtOther1,
        txtOther2:txtOther2,
        txtOther3:txtOther3,
        salary:salary,
        bill:bill,
        diligence:diligence,
        total:total,
        id_h:id_h
      })
        .done(function( data ) {
          //alert(data);
          $('#show-page-exp').html(data);
          printDiv('printableArea');
          search();
      });
  }



}


function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = parseFloat(num);
  }
  return number;
}

function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     //document.getElementById("tableDisplay").style.fontSize = "7px";
     $('#signature').show();
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

function printDiv2(divName) {

     var originalContents = document.body.innerHTML;

     document.getElementById("hd0").style.height = "30px";
     document.getElementById("hd1").style.fontSize = "13px";
     document.getElementById("hd2").style.fontSize = "13px";

     var x = document.getElementsByClassName("tabf");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       x[i].style.fontSize = "9px";
       x[i].style.width = "100%";
       x[i].classList.remove("table-bordered");
     }

     var x = document.getElementsByClassName("tdb");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       x[i].style.fontSize = "90%";
       x[i].style.width = "auto";
       x[i].style.border = "1px solid black";
     }

     var x = document.getElementsByClassName("divo");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       x[i].style.width = "100%";
       x[i].style.overflowX = "";
     }
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
