function reset(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  var cust_id     = $('#cust_id').val("");
  search();
}

function printReceipt(receiptCode){
  $.post("ajax/receipt/listExp2.php",{receiptCode:receiptCode})
    .done(function( data ) {
      $('#show-page-exp').html(data);
      $('#copy').html($('#master').html());

      $(".receipt").each(function(index) {

        if(index == 1){
          $( this ).html("สำเนา");
        }
      });
      setTimeout(function(){
        printDiv('printableArea');
      }, 1000);
  });
}

function printReceiptPost(receiptCode){
  $.post("ajax/receipt/listExpPost2.php",{receiptCode:receiptCode})
    .done(function( data ) {
      $('#show-page-exp').html(data);
      $('#copy').html($('#master').html());

      $(".receipt").each(function(index) {

        if(index == 1){
          $( this ).html("สำเนา");
        }
      });
      setTimeout(function(){
        printDiv('printableArea');
      }, 1000);
  });
}

function editReceipt(receiptCode,custId,custName){
  var url = "receipt.php?receiptCode="+receiptCode+"&custId="+custId+"&custName="+custName;
  postURL(url, null);
}


function search(){
  var cust_id       = $('#cust_id').val();
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  $.post("ajax/receipt/list.php",{cust_id:cust_id,startDate:startDate,endDate:endDate})
  .done(function(data) {
      $('#show-page').html(data);
  });
}

function searchReceipt(){
  var cust_id         = $('#cust_id').val();
  var receiptCode     = $('#receiptCode').val();
  var custName        = $('#cust_id option:selected').text();
  var customerName    = $('#customerName').val();

  if(customerName != ""){
    custName = customerName;
  }
  //alert(custName);
  if(cust_id == ""){
    $('#submitReceipt').attr("disabled", true);
  }
  $.post("ajax/receipt/listReceipt.php",{cust_id:cust_id,receiptCode:receiptCode,custName:custName})
    .done(function(data) {
      $('#show-page').html(data);
      //alert(parseInt($('#numRecipt').val()));
      if(parseInt($('#numRecipt').val()) > 0){
        $('#submitReceipt').attr("disabled", false);
      }
  });
}

function clearDataInv(){
  $('#invoiceCode').val("");
  $('#invoiceDate').val("");
}

function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = num;
  }
  return number;
}

function checkAll(obj) {
  if(obj.checked){
    var length = $('.checkJobId').length;

    if(length > 20){
      var num = 0;
      $('.checkJobId').each(function(){
        if(num == 20){
          return false;
        }else{
          $(this).prop('checked', true);
          num++;
        }
      });
    }else{
      $('.checkJobId').prop('checked', true);
    }

    $('#submitReceipt').attr("disabled", false);

  }else{
    $('.checkJobId').prop('checked', false);
    $('#submitReceipt').attr("disabled", true);
  }
}
function chkList(obj){
  var num = 0;

  $( ".checkJobId" ).each(function( index ) {
      if(this.checked){
        num++;
      }
      if(num > 20){
        obj.checked = false;
      }
  });
  if(num == 0){
    $('#submitReceipt').attr("disabled", true);
  }else{
    $('#submitReceipt').attr("disabled", false);
  }
}


function exportExcel(invoiceCode){
  var url = "export_excel_invoice.php?invoiceCode="+invoiceCode;
  window.open(url,'_blank');
}

function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     //document.getElementById("tableDisplayExp").style.fontSize = "11px";
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}


$('#form-data').on('submit', function(event) {
  event.preventDefault();
  if ($('#form-data').smkValidate()) {
    $.ajax({
        url: 'ajax/receipt/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        gotoPage('receipt_list.php');
        $.smkAlert({text: data.message,type: data.status});
      }, 1000);
    }).fail(function (jqXHR, exception) {
      //alert("fail");
    });
  }
});


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
