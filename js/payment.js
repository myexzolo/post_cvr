function reset(){
  $('#startDate').val($('#startDateTmp').val());
  $('#endDate').val($('#endDateTmp').val());
  $('#receiptCode').val("");
  $('#invoiceCode').val("");
  $('#cust_id').val("");
  search();
}

function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var custId        = $('#cust_id').val();
  var receiptCode   = $('#receiptCode').val();
  var invoiceCode   = $('#invoiceCode').val();
  //alert(employeeId);
  $.post("ajax/payment/list.php",{
    startDate:startDate,
    endDate:endDate,
    custId:custId,
    receiptCode:receiptCode,
    invoiceCode:invoiceCode,
  })
    .done(function( data ) {
      $('#show-page').html(data);
  });
}



function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = parseInt(num);
  }
  return number;
}

function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     document.getElementById("tableDisplay").style.fontSize = "11px";

     document.getElementById("hd0").height = "30px";
     document.getElementById("hd1").style.fontSize = "13px";
     document.getElementById("hd2").style.fontSize = "13px";
     document.getElementById("trh").style.height = "22px";

     var element = document.getElementById("tableDisplay");
     element.classList.remove("table-bordered");
     var x = document.getElementsByClassName("tdb");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       x[i].style.border = "1px solid black";
       x[i].style.height = "15px";
     }

     //$('#signature').show();
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
