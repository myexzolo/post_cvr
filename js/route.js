
  function show(){
    var source      = $('#s_source').val();
    var destination = $('#s_destination').val();
    var route_id    = $('#s_route_id').val();

    $.post("ajax/route/show.php",{source:source,destination:destination,route_id:route_id})
      .done(function( data ) {
        $('#show-page').html(data);
    });
  }

  function reset(){
    $('#s_source').val("");
    $('#s_destination').val("");
    $('#s_route_id').val("");
    $.post("ajax/route/show.php",{source:"",destination:"",route_id:"",route_id:""})
      .done(function( data ) {
        $('#show-page').html(data);
    });
  }

  function showForm(value=""){
    $.post("ajax/route/form.php",{value:value})
      .done(function( data ) {
        $('#myModal').modal({backdrop:'static'});
        $('#show-form').html(data);
    });
  }

  function exportExcel(){
    var source = $('#s_source').val();
    var destination = $('#s_destination').val();
    var route_id    = $('#s_route_id').val();

    var pam = "source="+source+"&destination="+destination+"&route_id="+route_id;
    var url = "ajax/route/excel.php?"+pam;
    //alert(url);
    window.open(url,'_blank');
  }

  function removeRow(value){
    $.smkConfirm({
      text:'Are You Sure?',
      accept:'Yes',
      cancel:'Cancel'
    },function(res){
      // Code here
      if (res) {
        $.post("ajax/route/delete.php",{value:value})
          .done(function( data ) {
            $.smkProgressBar();
            setTimeout(function(){
              $.smkProgressBar({status:'end'});
              show();
              $.smkAlert({text: data.message,type: data.status});
            }, 1000);
        });
      }
    });
  }

  $('#form-data').on('submit', function(event) {
    event.preventDefault();
    if ($('#form-data').smkValidate()) {
        $.ajax({
            url: 'ajax/route/manage.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $('#form-data').smkClear();
            show();
            $.smkAlert({text: data.message,type: data.status});
            $('#myModal').modal('toggle');
          }, 1000);
        });
    }
  });

  $(document).ready(function() {
    $("#panel1").smkPanel({
      hide: 'full,remove'
    });
    show();
  });
