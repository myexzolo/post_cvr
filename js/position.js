
  function show(){
    $.post("ajax/position/show.php")
      .done(function( data ) {
        $('#show-page').html(data);
    });
  }

  function showForm(value=""){
    $.post("ajax/position/form.php",{value:value})
      .done(function( data ) {
        $('#myModal').modal({backdrop:'static'});
        $('#show-form').html(data);
    });
  }
  function removeRow(value){
    $.smkConfirm({
      text:'Are You Sure?',
      accept:'Yes',
      cancel:'Cancel'
    },function(res){
      // Code here
      if (res) {
        $.post("ajax/position/delete.php",{value:value})
          .done(function( data ) {
            $.smkProgressBar();
            setTimeout(function(){
              $.smkProgressBar({status:'end'});
              show();
              $.smkAlert({text: data.message,type: data.status});
            }, 1000);
        });
      }
    });
  }

  $('#form-data').on('submit', function(event) {
    event.preventDefault();
    if ($('#form-data').smkValidate()) {
        $.ajax({
            url: 'ajax/position/manage.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $('#form-data').smkClear();
            show();
            $.smkAlert({text: data.message,type: data.status});
            $('#myModal').modal('toggle');
          }, 1000);
        });
    }
  });

  $(document).ready(function() {
    $("#panel1").smkPanel({
      hide: 'full,remove'
    });
    show();
  });
