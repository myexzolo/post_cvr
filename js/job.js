function showFormJobOrder(id,page){
  postURL('job_order.php?id='+id+"&page="+page);
}

function showFormJobOrder2(id,page){
  postURL('job_order2.php?id='+id+"&page="+page);
}

function show(date,status){
  $.post("ajax/po/list.php",{startDate:date,status:status})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function show2(date,status){
  $.post("ajax/po/list2.php",{startDate:date,status:status})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function reset(){
  $('#startDate').val($('#startDateTmp').val());
  $('#endDate').val($('#endDateTmp').val());
  $('#status').val("1");
  $('#employeeId').val("");
  $('#dp').val("");
  search();
}

function reset2(){
  $('#startDate').val($('#startDateTmp').val());
  $('#endDate').val($('#endDateTmp').val());
  $('#status').val("1");
  $('#employeeId').val("");
  $('#dp').val("");
  search2();
}

function chooseTrailer(){
  var trailer_id  = $('#trailer_id').val();
  if(trailer_id != ""){
    var data = trailer_id.split(":");
    $('#affiliation_id').val(data[1]);
  }
}


function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var status        = $('#status').val();
  var employeeId    = $('#employeeId').val();
  var dp            = $('#dp').val();

  $.post("ajax/po/list.php",{startDate:startDate,endDate:endDate,status:'status',employeeId:employeeId,page:'JOB',dp:dp})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function search2(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var status        = $('#status').val();
  var employeeId    = $('#employeeId').val();
  var dp            = $('#dp').val();
  var trailer_id    = $('#trailer_id').val();
  var cust_id       = $('#cust_id').val();


  $.post("ajax/po/list2.php",{startDate:startDate,endDate:endDate,status:'status',employeeId:employeeId,page:'JOB',dp:dp,trailer_id:trailer_id,cust_id:cust_id})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}


function showForm(){
  $.post("ajax/po/showJobRoute.php")
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#show-form').html(data);
      $('#tableDisplay').show();
  });
}

function calAmount(){
  if ($('#formData').smkValidate()) {
    var affiliation_id        = $('#affiliation_id').val();//รถ(สังกัด) 1= รถบริษัท 2=รถร่วมบริษัท 3= รถร่วม
    var price_type_id         = $('#price_type_id').val();//ประเภทการคำนวณราคา 1=ราคาต่อตัน 2=ราคาต่อเที่ยว

    var weights               = $('#weights').val();//น้ำหนัก(ตัน)
    var total_amount_allowance  = $('#total_amount_allowance').val();//รวมค่าใช้จ่าย

    var total_amount_receive  = $('#total_amount_receive').val();//ราคาค่าขนส่ง
    var total_amount_ap_pay   = $('#total_amount_ap_pay').val();//ราคาจ่ายรถร่วม

    if(affiliation_id == ""){
      $("#affiliation_id" ).focus();
      //$('#affiliation_id').smkValidate();
      $.smkAddError($('#affiliation_id'), "เลือกรถ(สังกัด)");
    }else if(weights == ""){
      $( "#weights" ).focus();
      //$('#weights').smkValidate();
      $.smkAddError($('#weights'), "ระบุ น้ำหนัก(ตัน)");
    }else if(price_type_id == ""){
      $( "#price_type_id" ).focus();
      $.smkAddError($('#price_type_id'), "เลือกการคำนวณราคา");
    }else{
      $.smkRemoveError($('#affiliation_id'));
      $.smkRemoveError($('#weights'));
      $.smkRemoveError($('#price_type_id'));

      if(price_type_id == 1){////ต่อตัน
          var one_trip_ton          = $('#one_trip_ton').val();//ราคาต่อตัน
          var ext_one_trip_ton      = $('#ext_one_trip_ton').val();//ราคารถร่วมต่อตัน

          total_amount_receive = (checkNumeric(one_trip_ton) * weights);
          $('#total_amount_receive').val(total_amount_receive.toFixed(2));

          total_amount_ap_pay = (checkNumeric(ext_one_trip_ton) * weights);
          $('#total_amount_ap_pay').val(total_amount_ap_pay.toFixed(2));



      }else if(price_type_id == 2){////เที่ยว
        total_amount_receive  = parseInt($('#price_per_trip').val());//ราคาต่อเที่ยว
        total_amount_ap_pay   = parseInt($('#ext_price_per_trip').val());//ราคารถร่วมต่อเที่ยว

        $('#total_amount_receive').val(total_amount_receive.toFixed(2));
        $('#total_amount_ap_pay').val(total_amount_ap_pay.toFixed(2));
      }

      var total   = 0;
      var profit  = 0;
      if(affiliation_id == 3){
        total =  parseInt($('#total_amount_ap_pay').val());
        $('#total_amount_allowance').val(total.toFixed(2));
        profit = (total_amount_receive - total_amount_ap_pay);
      }if(affiliation_id == 2){

      }if(affiliation_id == 1){
        var Fuel_Cost       = checkNumeric($('#fuel_cost').val());//ค่าน้ำมัน
        var fuel_Litre      = checkNumeric($('#fuel_litre').val());//จำนวนลิตร
        var Blank_Charge    = checkNumeric($('#blank_charge').val());//ค่าตีเปล่า
        var allowance       = checkNumeric($('#allowance').val());//เบี้ยเลี้ยง
        var kog_expense     = checkNumeric($('#kog_expense').val());//หางคอก
        var job_ended_acc_expense  = checkNumeric($('#job_ended_acc_expense').val());//คชจ.บัญชี
        var allowance_oth   = checkNumeric($('#allowance_oth').val());//อื่นๆ

        var t1 = (parseInt(Fuel_Cost) + parseInt(Blank_Charge) + parseInt(allowance) + parseInt(kog_expense) + parseInt(job_ended_acc_expense)+ parseInt(allowance_oth));

        var jobEndedClearance     = checkNumeric($('#job_ended_clearance').val()); //ค่าปรับ
        var jobEndedRecap         = checkNumeric($('#job_ended_recap').val()); //ปะยาง
        var jobEndedExpressway    = checkNumeric($('#job_ended_expressway').val()); //ทางด่วน
        var jobEndedPassageFee    = checkNumeric($('#job_ended_passage_fee').val()); //ผ่านท่าเรือ
        var jobEndedRepaires      = checkNumeric($('#job_ended_repaires').val()); //ค่าซ่อม
        var jobEndedOtherExpense  = checkNumeric($('#job_ended_other_expense').val()); //ค่าใช้จ่ายอื่นๆ

        var t2 = (parseInt(jobEndedClearance) + parseInt(jobEndedRecap) + parseInt(jobEndedExpressway) + parseInt(jobEndedPassageFee) + parseInt(jobEndedRepaires) + parseInt(jobEndedOtherExpense));

        total = (t1 + t2);
        $('#total_amount_allowance').val(total.toFixed(2));
        profit = (total_amount_receive - total);
      }


      $('#job_order_profit').val(profit.toFixed(2));

    }
  }
}

function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = num;
  }
  return number;
}


function setDataRoute(route_id,source,destination,distance,allowance,kogExpense,allowanceOth,jobEndedAccExpense,oneTripTon,pricePerTrip,extOneTripTon,extPricePerTrip){
  $('#route_id').val(route_id);
  $('#source').val(source);
  $('#destination').val(destination);
  $('#distance').val(distance);
  $('#allowance').val(allowance);
  $('#kog_expense').val(kogExpense);
  $('#allowance_oth').val(allowanceOth);
  $('#job_ended_acc_expense').val(jobEndedAccExpense);
  $('#one_trip_ton').val(oneTripTon);
  $('#price_per_trip').val(pricePerTrip);
  $('#ext_one_trip_ton').val(extOneTripTon);
  $('#ext_price_per_trip').val(extPricePerTrip);

  $('#myModal').modal('toggle');
}



function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     document.getElementById("tableDisplay").style.fontSize = "11px";
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

function printInvice(){
  var receive_id = $('#receive_id').val();
  var data = $('#data').val();
  var name = $('#name').val();
  var invoice_code = $('#invoiceCode').val();
  var taxId = $('#taxId').val();
  var address = $('#address').html();
  alert(address);
  //var pam = "";
  var pam = "?receive_id="+receive_id+"&&name="+name+"&&data="+data+"&&invoice_code="+invoice_code+"&&taxId="+taxId+"&&address="+address;
  //alert(pam);
  //var myWindow = window.open("pos_process.php"+pam, "", "width=5,height=5");
  var myWindow = window.open("print_invoice.php"+pam,'', 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no,visible=none', '');
  $('#myModal').modal('hide');
  $('#formAdd').smkClear();
}


$('#form-data').on('submit', function(event) {
  event.preventDefault();
  if ($('#form-data').smkValidate()) {
    $.ajax({
        url: 'ajax/po/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      //alert(data.message);
      //$('#myModal').modal('toggle');
      // $.smkProgressBar();
      // setTimeout(function(){
      //   $.smkProgressBar({status:'end'});
      //   $('#formAdd').smkClear();
      //   showPage();
      $.smkAlert({text: data.message,type: data.status});
      gotoPage("po_list.php");
      //   $('#myModal').modal('toggle');
      // }, 1000);
    }).fail(function (jqXHR, exception) {
      //alert("fail");
        // setTimeout(function(){
        //   $.smkProgressBar({status:'end'});
        //   $('#formAdd').smkClear();
        //   showPage();
        //   $.smkAlert({text: jqXHR.responseText ,type: 'danger'});
        //   $('#myModal').modal('toggle');
        // }, 1000);
    });
  }
});


$('#form-data2').on('submit', function(event) {
  event.preventDefault();
  if ($('#form-data2').smkValidate()) {
    $.ajax({
        url: 'ajax/po/manage2.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      //alert(data.message);
      //$('#myModal').modal('toggle');
      // $.smkProgressBar();
      // setTimeout(function(){
      //   $.smkProgressBar({status:'end'});
      //   $('#formAdd').smkClear();
      //   showPage();
      $.smkAlert({text: data.message,type: data.status});
      gotoPage("po_list2.php");
      //   $('#myModal').modal('toggle');
      // }, 1000);
    }).fail(function (jqXHR, exception) {
      //alert("fail");
        // setTimeout(function(){
        //   $.smkProgressBar({status:'end'});
        //   $('#formAdd').smkClear();
        //   showPage();
        //   $.smkAlert({text: jqXHR.responseText ,type: 'danger'});
        //   $('#myModal').modal('toggle');
        // }, 1000);
    });
  }
});


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
