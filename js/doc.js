
function reset(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  search();
}

function resetDoc(){
  $('#custId').val("");
  searchDoc();
}

function searchDoc(){
  var custId        = $('#custId').val();
  var docReturnCode = $('#docReturnCode').val();
  $.post("ajax/doc/listDoc.php",{custId:custId,docReturnCode:docReturnCode})
    .done(function(data) {
      $('#show-page').html(data);
  });
}


function checkAll(obj) {
  if(obj.checked){
    var length = $('.checkJobId').length;

    if(length > 10){
      var num = 0;
      $('.checkJobId').each(function(){
        if(num == 10){
          return false;
        }else{
          $(this).prop('checked', true);
          num++;
        }
      });
    }else{
      $('.checkJobId').prop('checked', true);
    }
  }else{
    $('.checkJobId').prop('checked', false);
  }
}

function edit(docReturnCode,custId){
  var url = "docReturn.php?docReturnCode="+docReturnCode+"&custId="+custId;
  postURL(url, null);
}

function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();

  $.post("ajax/doc/list.php",{startDate:startDate,endDate:endDate})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = num;
  }
  return number;
}

function printDoc(docReturnCode){
  $.post("ajax/doc/listExp.php",{docReturnCode:docReturnCode})
    .done(function( data ) {
      $('#show-page-exp').html(data);
      printDiv('printableArea');
  });
}


function printDiv(divName) {
     var originalContents = document.body.innerHTML;
     document.getElementById("tableDisplayExp").style.fontSize = "11px";
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}



$('#form-data').on('submit', function(event) {
  event.preventDefault();
  if ($('#form-data').smkValidate()) {
    $.ajax({
        url: 'ajax/doc/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        gotoPage("doc_return_list.php");
        $.smkAlert({text: data.message,type: data.status});
      }, 1000);
    }).fail(function (jqXHR, exception) {

    });
  }
});


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
