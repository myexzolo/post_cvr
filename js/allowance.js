function reset(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  search();
}

function expPeriod(periodCode,startDate,endDate){
  var typeCompany = $('#typeCompany').val();
  var url = "";
  if(typeCompany == 1){
    url = "ajax/allowance/listExp.php";
  }else{
    url = "ajax/allowance/listExpPost.php";
  }

  $.post(url,{periodCode:periodCode,startDate:startDate,endDate:endDate})
    .done(function( data ) {
      $('#show-page-exp').html(data);
      printDiv('printableArea');
  });
}

function expPeriodV2(periodCode,startDate,endDate){
  var typeCompany = $('#typeCompany').val();
  var url = "";
  if(typeCompany == 1){
    url = "allowance_lists.php?periodCode="+ periodCode + "&&startDate="+ startDate + "&&endDate="+endDate;
  }else{
    url = "allowance_lists_post.php?periodCode="+ periodCode + "&&startDate="+ startDate + "&&endDate="+endDate;
  }
  //alert(url);
  postURLblank(url);
}


function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();

  $.post("ajax/allowance/list.php",{startDate:startDate,endDate:endDate})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function searchJob(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var dateApp       = $('#dateApp').val();
  var employeeId    = $('#employeeId').val();
  var trailer_id    = $('#trailer_id').val();
  var employeeId2   = $('#employeeId2').val();

  $.post("ajax/allowance/listAllowance.php",{startDate:startDate,endDate:endDate,dateApp:dateApp,employeeId:employeeId,trailer_id:trailer_id,employeeId2:employeeId2})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}



function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = num;
  }
  return number;
}

function checkAll(obj) {
  if(obj.checked){
    $('.checkJobId').prop('checked', true);
  }else{
    $('.checkJobId').prop('checked', false);
    $('#totalTransfer').html("0.00");
    $('#total_transfer').val(0);
  }
  calAllowance();
}

function calAllowance(){
  var transfer = 0;
  $( ".checkJobId" ).each(function( index ) {
      //console.log( index + ": " + $( this ).text());

      if(this.checked){
        var id = "#transfer"+ $( this ).val();
        var id2 = "#transfer2"+ $( this ).val();
        var cost2 = 0;
        if (typeof id2 !== "undefined") {
            cost2  = $(id2).html();
            if(typeof cost2 !== "undefined")
            {
                cost2   = cost2.replace(/,/g,'');
            }else{
                cost2   = 0;
            }
            //console.log(id2 +": "+ cost2);
        }
        var cost = $(id).html();
        cost = cost.replace(/,/g,'');
        transfer += parseFloat(cost);
        transfer += parseFloat(cost2);
      }
  });
  $('#totalTransfer').html(transfer.toFixed(2));
  $('#total_transfer').val(transfer.toFixed(2));
}


function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     //document.getElementById("tableDisplay").style.fontSize = "7px";
     $('#signature').show();
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}

function exportExcelV2(periodCode,startDate,endDate){
  var url = "export_excel_allowance.php?startDate="+encodeURI(startDate)+"&endDate="+encodeURI(endDate)+"&periodCode="+periodCode;
  window.open(url,'_blank');
}


function exportExcel(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var url = "export_excel_allowance.php?startDate="+startDate+"&endDate="+endDate;
  window.open(url,'_blank');
}



$('#form-data').on('submit', function(event) {
  event.preventDefault();
  if ($('#form-data').smkValidate()) {
    $.ajax({
        url: 'ajax/allowance/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        gotoPage('allowance_list.php');
        $.smkAlert({text: data.message,type: data.status});
      }, 1000);
    }).fail(function (jqXHR, exception) {
      //alert("fail");
    });
  }
});


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
