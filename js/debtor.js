function reset(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  $('#cust_id').val("");
  $('#statusInv').val("");

  search();
}

function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var custId        = $('#cust_id').val();
  var statusInv     = $('#statusInv').val();
  //alert(statusInv);
  $.post("ajax/debtor/list.php",{startDate:startDate,endDate:endDate,custId:custId,statusInv:statusInv})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function search2(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var custId        = $('#cust_id').val();
  //alert(statusInv);
  $.post("ajax/debtor/list2.php",{startDate:startDate,endDate:endDate,custId:custId})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}


function reset2(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  $('#cust_id').val("");
  search2();
}




function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = parseInt(num);
  }
  return number;
}

function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     //document.getElementById("tableDisplay").style.fontSize = "7px";
     //document.getElementById("hd0").style.height  = "40px";

     document.getElementById("hd1").style.fontSize = "13px";
     document.getElementById("hd2").style.fontSize = "13px";
     var x = document.getElementsByClassName("tabf");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       x[i].style.fontSize = "10px";
       x[i].style.width = "100%";
       x[i].classList.remove("table-bordered");
     }

     var x = document.getElementsByClassName("divf");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       x[i].style.fontSize = "12px";
     }

     var x = document.getElementsByClassName("tdb");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       //x[i].style.width = "auto";
       x[i].style.border = "1px solid black";
     }


     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
