function randomPass(){
  var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz!&#@";
  var pass = "";
  var maxs = 10;
  for(var x=0; x<=maxs; x++){
    var i = Math.floor(Math.random()*chars.length);
    pass += chars.charAt(i);
  }
  return pass;
}

function validateTime(obj) {
    var n = obj.value.length;
    if(n == 1){
      var H = parseInt(obj.value);
      if(H > 2){
        obj.value = "2";
      }else{
        if(!Number.isInteger(H)){
          obj.value = "";
        }
      }
    }else if(n == 2){
      var HH = parseInt(obj.value);
      if(HH > 23 && Number.isInteger(HH)){
        obj.value = "23";
        obj.value = obj.value + ":";
      }else{
        if(!Number.isInteger(HH)){
          obj.value = obj.value.substring(0, 1);
        }else{
          obj.value = HH;
        }
      }
    }else if(n == 3){
      var c = parseInt(obj.value.substring(2, 3));
      if(c != ":"){
        obj.value = obj.value.substring(0, 2) + ":";
      }
    }else if(n == 4){
      var s = parseInt(obj.value.substring(3, 4));
      if(s > 5){
        obj.value = obj.value.substring(0, 3) + "5";
      }else{
        if(!Number.isInteger(s)){
          obj.value = obj.value.substring(0, 3);
        }
      }
    }else if(n == 5){
      var HH  = parseInt(obj.value.substring(0, 2));
      var SS  = parseInt(obj.value.substring(3, 5));
      var s   = parseInt(obj.value.substring(4, 5));

      if(!Number.isInteger(HH) || !Number.isInteger(SS)){
        obj.value = "";
      }else if(!Number.isInteger(s)){
        obj.value = obj.value.substring(0, 4);
      }
    }
}

function genCodeID(){
  var member_group_name = $("#member_group_id option:selected").text();
  if(member_group_name != ""){
    var codeArr = member_group_name.split(":");
    var code    = codeArr[0].trim();
    $.post("ajax/member/genCodeID.php",{member_group_code:code})
      .done(function( data ) {
        $('#member_code').val(data);
    });
  }else{
      $('#member_code').val("");
  }

}

function takePhoto(id,type){
  //var windowoption = "width=800,height=500,menubar=0,resizable=0,scrollbars=0,location=0,status=0,titlebar=0,toolbar=0";
  var parm  = "id="+btoa(id)+"&type="+btoa(type);
  var url     = "takePhoto.php?"+ parm;
  //var name = "takePhoto";
  //window.open(url, name, windowoption);
  window.location.replace(url);
}


var objPackage;
var MEMBERID;
var NAME;
var PERSONNELID;
var DATESELECT;
var PACKAGEID;
var GROUPPACKAGEID;
var MEMBERPACKUSE = 0;

function showPagePackage(memberId,name,personnelId){
  var header = "<button class='btn btn-success btn-flat btnBack' onclick='goPage(\"member.php\")'><i style='font-size:12px' class='fa fa-mail-reply'></i> Back</button>"
    +"&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;Management Package : " + name
    +" <button class='btn btn-success btn-flat btnAdd' "
    +" onclick=\"showFormPackage('"+ memberId +"','"+name+"','"+personnelId+"','')\">+ ADD PACKAGE</button>";
  $('#nameHeader').html(header);
  MEMBERID = memberId;
  NAME = name;
  PERSONNELID = personnelId;
  DATESELECT = 0;
  $.post("ajax/member/showPakageMember.php",{memberId:memberId,personnelId:personnelId,name:name})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}


function getDetailMember(memberId,packageMemberId){
  var packageName = "";
  var packageUse = "";
  var groupClassId = "";
  var packageUnit = 0;
  //alert(packageMemberId);
  $.post("ajax/member/getPackage.php",{memberId:memberId,packageMemberId:packageMemberId})
    .done(function( data ) {
      //alert(data[0].package_member_id);
      if(data[0].package_member_id){
        packageName     = data[0].package_name;
        packageUse      = data[0].package_use +"/"+ data[0].package_num;
        packageUnit     = data[0].package_unit;
        groupClassId    = data[0].group_class_id;
        if(packageUnit == 1){
            MEMBERPACKUSE = 10 - data[0].package_use;
        }else if(packageUnit == 2){
            MEMBERPACKUSE = 210 - data[0].package_use;
        }else if(packageUnit == 3){
            MEMBERPACKUSE = 10000 - data[0].package_use;
        }else if(packageUnit == 4){
            MEMBERPACKUSE = data[0].package_num - data[0].package_use;
        }else if(packageUnit == 5){
            MEMBERPACKUSE = 20000;
        }

        if(groupClassId == 4)
        {
          btnAddPT = "<button class='btn btn-success btn-flat btnAdd' onclick='showFormClassPP()'>+ ADD CLASS PP</button>";
        }
      }
  });

  var btnAddPT = "";

  setTimeout(function(){
    var header = "<button class='btn btn-success btn-flat btnBack' onclick=\"showPagePackage("+memberId+",'"+NAME+"',"+PERSONNELID+")\"><i style='font-size:12px' class='fa fa-mail-reply'></i> Back</button>"
      +"&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;Management Booking : " + NAME + "&ensp;&ensp;Package : " + packageName  + "&ensp;&ensp; Use : " + packageUse + btnAddPT;
    $('#nameHeader').html(header);
  }, 100);
}

function showFormClassPP(){
  $.post("ajax/member/formClassPrivate.php",{memberId:MEMBERID,packageMemberId:PACKAGEID,personnelSaleId:PERSONNELID})
    .done(function( data ) {
      $('#myModalClassPP').modal({backdrop: 'static'});
      $('#show-form3').html(data);
  });
}

function showPageBooking(memberId,name,personnelId,value,classId,packageMemberId,groupClassId){

  //MEMBERPACKUSE = 0;
  getDetailMember(memberId,packageMemberId);

  MEMBERID = memberId;
  NAME = name;
  PERSONNELID = personnelId;
  DATESELECT = 0;
  PACKAGEID  = packageMemberId;
  GROUPPACKAGEID = groupClassId;
  if(GROUPPACKAGEID == 4){
    $.post("ajax/member/classPrivate.php",{memberId:memberId,personnelId:personnelId,name:name,value:value,classId:classId,packageMemberId:packageMemberId})
      .done(function( data ) {
        $('#show-page').html(data);
    });
  }else{
    $.post("ajax/member/classSchedules.php",{memberId:memberId,personnelId:personnelId,name:name,value:value,classId:classId,packageMemberId:packageMemberId})
      .done(function( data ) {
        $('#show-page').html(data);
    });
  }
}

function setDateSelect(value){
  getDetailMember(MEMBERID,PACKAGEID);
  DATESELECT += value;
  $.post("ajax/member/classSchedules.php",{memberId:MEMBERID,personnelId:PERSONNELID,name:NAME,value:DATESELECT,classId:'',packageMemberId:PACKAGEID})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function setDateSelectDef(){
  getDetailMember(MEMBERID,PACKAGEID);
  DATESELECT = 0;
  $.post("ajax/member/classSchedules.php",{memberId:MEMBERID,personnelId:PERSONNELID,name:NAME,value:DATESELECT,classId:'',packageMemberId:PACKAGEID})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function showForm(value=""){
  $('#myModalLabel').html("Management Member");
  $('#btnSave').show();
  $.post("ajax/member/formMember.php",{value:value})
    .done(function( data ) {
      $('#myModal').modal({backdrop: 'static'});
      $('#show-form').html(data);

      if($('#member_code').val() == ""){
        genCodeID();
      }
  });
}

function showFormInvite(memberId){
  $('#myModalLabel').html("Management Member");
  $('#btnSave').show();
  $.post("ajax/member/formMember.php",{memberId:memberId})
    .done(function( data ) {
      $('#myModal').modal({backdrop: 'static'});
      $('#show-form').html(data);

      if($('#member_code').val() == ""){
        genCodeID();
      }
  });
}

function showFormView(value=""){
  $('#myModalLabel').html("Management Member");
  $('#btnSave').hide();
  $.post("ajax/member/formMember.php",{value:value,disabled:'disabled'})
    .done(function( data ) {
      $('#myModal').modal({backdrop: 'static'});
      $('#show-form').html(data);

      if($('#member_code').val() == ""){
        genCodeID();
      }
  });
}

function genPackage(id,idbtn,code){
  $('#group_class_id').val(id);
  $('#price_package_id').val();
  $('#voucherRang').html("");
  if(code == "VOUC"){
    var dataVoucher = "";
    dataVoucher +="<div class='col-md-4'><div class='form-group'>";
    dataVoucher +="    <label>Start Voucher Number</label>";
    dataVoucher +="    <input value='"+$('#startVoucher').val()+"' name='start_voucher' id='start_voucher' class='form-control' type='number' placeholder='Start' required>";
    dataVoucher +="    </div></div>";
    dataVoucher +="<div class='col-md-4'><div class='form-group'>";
    dataVoucher +="    <label>End Voucher Number</label>";
    dataVoucher +="    <input value='"+$('#endVoucher').val()+"' name='end_voucher' id='end_voucher' class='form-control' type='number' placeholder='End' required>";
    dataVoucher +="    </div></div>";
    //alert(dataVoucher);
    $('#voucherRang').html(dataVoucher);
  }
  $(".bg-navy").removeClass("bg-navy").addClass("btn-default");
  $('#'+idbtn).addClass('btn-fontActive');
  $('#'+idbtn).removeClass('btn-default').addClass('bg-navy');
  $.post("ajax/member/genPackage.php",{id:id})
    .done(function( data ) {
      $('#package').html(data);
  });

  $.post("ajax/member/genOfferPackage.php",{id:id})
    .done(function( data ) {
      $('#offerGroupPackage').html(data);
  });
}

function discountClick(val,idbtn){
  $(".bg-navy.discount").removeClass("bg-navy").addClass("btn-default");
  if($('#discount').val() != val){
    $('#'+idbtn).removeClass('btn-default').addClass('bg-navy');
    $('#discount').val(val);
  }else{
    $('#discount').val("");
  }
  $('#'+idbtn).addClass('btn-fontActive');
}

function changeActive(obj){
  $('#activeDate').val("");
  $('#expireDate').val("");
  $('#activeDate').attr("readonly", true);
  $('#expireDate').attr("readonly", true);
  if(obj.value == 3){
    $('#activeDate').attr("readonly", false);
    $('#expireDate').attr("readonly", false);
  }
}

function offerClick(idbtn,offerNum,offerUnit){
  //alert(">>" + offerNum +">>" + offerUnit +">>" + $('#offer_num').val() +">>" + $('#offer_unit').val());
  $(".bg-navy.offer").removeClass("bg-navy").addClass("btn-default");
  if($('#offer_num').val() != offerNum && $('#offer_unit').val() != offerUnit){
    $('#'+idbtn).removeClass('btn-default').addClass('bg-navy');
    $('#offer_num').val(offerNum);
    $('#offer_unit').val(offerUnit);
  }else{
    $('#offer_num').val("");
    $('#offer_unit').val("");
  }
  $('#'+idbtn).addClass('btn-fontActive');
}

function addPackage(id,idbtn,obj){
  $('#price_package_id').val(id);
  $(".bg-navy.package").removeClass("bg-navy").addClass("btn-default");
  $('#'+idbtn).removeClass('btn-default').addClass('bg-navy');
}

function addPax(idBtn){
  var res = idBtn.split("_");
  var inp   = res[1] + "_"  + res[2] + "_" + res[3] + "_" + res[4];
  var idInp = "input_" + inp;
  var inpPaxSpan        = idInp +"_paxSpan";
  var inpDateBook       = idInp +"_pax";

  var num  = $('#'+inpDateBook).val();
  //alert(num);
  if(parseInt(num) < 3){
    num++;
    $('#'+inpDateBook).val(num);
    $('#'+inpPaxSpan).html(num);
  }

}

function delBooking(idBtn,class_schedule_id,class_name,day,dateClass,dateBook,expire_class_date,expire_booking_date,classBookingId){
  $.smkConfirm({
    text: "ต้องการยกเลิก Class : " + class_name + "<br> วันที่ : " + day + " ?",
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    if (res) {
      //alert(classBookingId);
      $.post("../member/ajax/booking/cencelBooking.php",{classBookingId:classBookingId})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $('#formAddBooking').smkClear();
            setDateSelectDef();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });

}

function addBooking(idBtn,class_schedule_id,class_name,day,dateClass,dateBook,expire_class_date,expire_booking_date,addPax){

  var res = idBtn.split("_");
  var inp   = res[1] + "_"  + res[2] + "_" + res[3] + "_" + res[4];
  var idInp = "input_" + inp;

  var inpClassId        = idInp +"_classId";
  var inpDateBook       = idInp +"_dateBook";
  var inpeExpireClass   = idInp +"_expireClassDate";
  var inpExpireBooking  = idInp +"_expireBookingDate";

  //alert($('#'+inpClassId).val());
  if($('#'+inpClassId).val() == ""){
    MEMBERPACKUSE--;
  }else{
    MEMBERPACKUSE++;
  }

  if(MEMBERPACKUSE >= 0){
    //alert(idBtn+","+class_schedule_id+","+class_name+","+day+","+dateClass+","+dateBook+","+expire_class_date+","+expire_booking_date);
    if($('#'+inpClassId).val() == ""){
      $('#'+inpClassId).val(class_schedule_id);
      $('#'+inpDateBook).val(dateBook);
      $('#'+inpeExpireClass).val(expire_class_date);
      $('#'+inpExpireBooking).val(expire_booking_date);
      $('#'+idBtn).removeClass('btnClassBooking').addClass('bg-navy');
      if(addPax > 0)
      {
        var inpAddPax         = idInp +"_addPax";
        var inpPaxSpan        = idInp +"_paxSpan";
        var inpDateBook       = idInp +"_pax";
        $('#'+inpDateBook).val(0);
        $('#'+inpPaxSpan).html("0");
        $('#'+inpAddPax).show();
      }
    }else{
      $('#'+inpClassId).val("");
      $('#'+inpDateBook).val("");
      $('#'+inpeExpireClass).val("");
      $('#'+inpExpireBooking).val("");
      $('#'+idBtn).removeClass('bg-navy').addClass('btnClassBooking');
      if(addPax > 0)
      {
        var inpAddPax         = idInp +"_addPax";
        var inpPaxSpan        = idInp +"_paxSpan";
        var inpDateBook       = idInp +"_pax";
        $('#'+inpDateBook).val(0);
        $('#'+inpPaxSpan).html("0");
        $('#'+inpAddPax).hide();
      }
    }
  }else{
    MEMBERPACKUSE = 0;
    $.alert({
    title: 'Alert!',
    content: 'ไม่สามารถเพิ่ม Class ได้',
});
  }
}


function addListPackage(){
  createRow(objPackage,'Pending', 'color:red' , 'P');

  for(var x=0 ;x < $( ".pmt" ).length ; x++){
    var id  = $( ".pmt" )[x].id;
    var obj = $('#' + id).val();
    if(obj != ""){
      createRow(obj,'Free', '' , 'F');
    }
  }
   resetPackage();
}

function createRow(obj,status,style, type){
  if(obj != ""){
    var res = obj.split('||');
    var pricePackageId  = res[0];
    var packageName     = res[1];
    var packagePerNum   = res[2];
    var packageUnit     = res[3];

    var unitName;
    if(packageUnit == 1){
      unitName = "วัน";
    }else if(packageUnit == 2){
      unitName = "เดือน";
    }else if(packageUnit == 3){
      unitName = "ปี";
    }else if(packageUnit == 4){
      unitName = "ครั้ง";
    }
    var id = "tr_pk_"+pricePackageId;
    var inpH   = "<input type='hidden' name='id[]'    value='"+ pricePackageId +"'>";
        inpH  += "<input type='hidden' name='type[]'  value='"+ type +"'>";
        inpH  += "<input type='hidden' name='discount[]'  value='"+ disCountNum +"'>";
    disCountNum = 0;
    var addRow = "";
        addRow += '<tr id="'+id+'">';
        addRow += "<td style='width: 10px'><button type='button' onclick=\"deleteRow('"+id+"')\" class='btn btn-sm btn-danger btn-flat'>ลบ</button></td>";
        addRow += "<td class='text-center text-truncate' style='line-height: 30px'>"+ packageName + "</td>";
        addRow += "<td class='text-center text-truncate' style='line-height: 30px'>"+ packagePerNum + "</td>";
        addRow += "<td class='text-center' style='line-height: 30px'>"+inpH+"0</td>";
        addRow += "<td class='text-center' style='line-height: 30px'>"+ unitName +"</td>";
        addRow += "<td class='text-center' style='line-height: 30px ;"+style+"'><b>"+status+"</b></td>";
        addRow += "</tr>";
    $('#tablePackage').append(addRow);
    $(".bg-navy").removeClass("bg-navy").addClass("btn-default");
  }
}

function resetPackage(){
  objPackage = "";
  $('#discount').val("");
  $('#price_package_id').val("");
  $('#offer_num').val("");
  $('#offer_unit').val("");
  $('#voucherRang').html("");
  var data      ='<div style="height:74px"></div>';
  var data2     ='<div style="height:54px"></div>';
  $("#activeSelect").val(0);
  $('#package').html(data);
  $('#offerGroupPackage').html(data2);
  $(".bg-navy").removeClass("bg-navy").addClass("btn-default");
}

function goPage(url){
  window.location = url;
}

function deleteRow(id){
  objPackage = "";
  $('#'+id).remove();
}

function showFormPackage(value="", name , personnel_id,packageMemberId){
  $('#btnSave2').show();
  $('#myModalLabel2').html("Package : " + name);
  $.post("ajax/member/formPackageV2.php",{value:value,personnel_id:personnel_id,packageMemberId:packageMemberId})
    .done(function( data ) {
      $('#show-form2').html(data);

      if($('#group_class_id').val() > 0){
         var gc = $('#group_class_id').val();
         var code = "";
         if($('#startVoucher').val() != ""){
           code = "VOUC";
         }
         var idbtnGc = "btnGc"+gc;
         $('#'+idbtnGc).removeClass('btn-default').addClass('bg-navy').addClass('btn-fontActive');
         genPackage(gc,idbtnGc,code);

         setTimeout(function(){
           if($('#package_id').val() != ""){
             var idbtnPack =  "btn_pack"+ $('#package_id').val();
             $('#'+idbtnPack).removeClass('btn-default').addClass('bg-navy').addClass('btn-fontActive');
           }

           if($('#offer_num').val() > 0){
              var offer_num  = $('#offer_num').val();
              var offer_unit = $('#offer_unit').val();
              var idbtnoff = "btn_offer_"+offer_num+offer_unit;
              $('#'+idbtnoff).removeClass('btn-default').addClass('bg-navy').addClass('btn-fontActive');
           }
         }, 300);
      }

      if($('#discount').val() > 0){
         var dis = $('#discount').val();
         var idbtnDis = "btnDis"+dis;
         $('#'+idbtnDis).removeClass('btn-default').addClass('bg-navy').addClass('btn-fontActive');
      }
      //alert($('#activeType').val());
      if($('#activeType').val() != ""){
        $("#activeSelect").val($('#activeType').val());
        if($('#activeType').val() == 3){
          $('#activeDate').attr("readonly", false);
          $('#expireDate').attr("readonly", false);
        }
      }

      $('#myModalPakage').modal({backdrop: 'static'});
  });
}


function showPage(){
  $.post("ajax/member/showMember.php")
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function ReadSmartCard(){
  $.post("ajax/member/smartCard.php")
    .done(function( data) {
      if(data){
        var result = data[0];
        if(result.STATUS == '1'){
          $('#member_name').val(result.NAME_TH);
          $('#member_lname').val(result.SURNAME_TH);
          $('#member_id_card').val(result.ID_CARD);;
          $('#member_birthdate').val(formatDate(result.BIRTHDATE));
          if(result.SEX == 1){
            $('#member_sex_m').prop('checked', true);
          }else{
            $('#member_sex_f').prop('checked', true);
          }
          $('#member_address').val(result.ADDRESS + chkstr(result.MOO) + chkstr(result.SOI) + chkstr(result.ROAD)
           + chkstr(result.TUMBOL)+ chkstr(result.AMPHURE)+ chkstr(result.PROVINCE));
        }else{
          setTimeout(function(){
            $.smkAlert({text: result.message,type: result.status});
          }, 1000);
        }
      }
  });
  setTimeout(function(){
    $.get("ajax/member/clearCard.php");
  }, 3000);
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function chkstr(str){
  return str !=""? " " + str : "";
}

function generatePass(){
  $('.pass').val(randomPass());
}
bodyResize();

function removeMember(value){
  $.smkConfirm({
    text:'Are You Sure?',
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/member/delMember.php",{value:value})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showPage();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

function removeMemberPackage(value){
$.smkConfirm({
  text:'Are You Sure?',
  accept:'Yes',
  cancel:'Cancel'
},function(res){
  // Code here
  if (res) {
    $.post("ajax/member/delMemberPackage.php",{value:value})
      .done(function( data ) {
        $.smkProgressBar();
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          showPagePackage(MEMBERID,NAME,PERSONNELID);
          $.smkAlert({text: data.message,type: data.status});
        }, 1000);
    });
  }
});
}

function delClassPP(classPrivateId,packageMemberId){
$.smkConfirm({
  text:'Are You Sure?',
  accept:'Yes',
  cancel:'Cancel'
},function(res){
  // Code here
  if (res) {
    $.post("ajax/member/delMemberPackagePP.php",{classPrivateId:classPrivateId,packageMemberId:packageMemberId})
      .done(function( data ) {
        $.smkProgressBar();
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          showPageBooking(MEMBERID,NAME,PERSONNELID,0,'',PACKAGEID,GROUPPACKAGEID);
          $.smkAlert({text: data.message,type: data.status});
        }, 1000);
    });
  }
});
}

$("#panel2").smkPanel({
  hide: 'full,remove'
});
showPage();

$('#formClassPP').on('submit', function(event) {
  event.preventDefault();
  if ($('#formClassPP').smkValidate()) {
    $.ajax({
        url: 'ajax/member/manageClassPrivate.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
        $.smkProgressBar();
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          $('#formClassPP').smkClear();
          showPageBooking(MEMBERID,NAME,PERSONNELID,0,'',PACKAGEID,GROUPPACKAGEID);
          $.smkAlert({text: data.message,type: data.status});
          $('#myModalClassPP').modal('toggle');
        }, 1000);
    }).fail(function (jqXHR, exception) {
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formClassPP').smkClear();
        showPageBooking(MEMBERID,NAME,PERSONNELID,0,'',PACKAGEID,GROUPPACKAGEID);
        $.smkAlert({text: jqXHR.responseText ,type: 'danger'});
        //$('#myModalClassPP').modal('toggle');
      }, 1000);
    });
  }
});

$('#formPackage').on('submit', function(event) {
  event.preventDefault();
  if($('#start_voucher').val() > 0 && $('#end_voucher').val() > 0){
    var start = parseInt($('#start_voucher').val());
    var end = parseInt($('#end_voucher').val());
    if(end < start){
      $('#end_voucher').val("");
    }
  }
  if ($('#formPackage').smkValidate()) {
    $.ajax({
        url: 'ajax/member/manageMemberPackage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      //alert(data.message);
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formPackage').smkClear();
        showPagePackage(MEMBERID,NAME,PERSONNELID);
        $.smkAlert({text: data.message,type: data.status});
        $('#myModalPakage').modal('toggle');
      }, 1000);
    }).fail(function (jqXHR, exception) {
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          $('#formPackage').smkClear();
          showPagePackage(MEMBERID,NAME,PERSONNELID);
          $.smkAlert({text: jqXHR.responseText ,type: 'danger'});
          $('#myModalPakage').modal('toggle');
        }, 1000);
    });
  }
});


$('#formMember').on('submit', function(event) {
  event.preventDefault();
  if ($('#formMember').smkValidate()) {
    $.ajax({
        url: 'ajax/member/manageMember.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      //alert(data.message);
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formMember').smkClear();
        showPage();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    }).fail(function (jqXHR, exception) {
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          $('#formMember').smkClear();
          showPage();
          $.smkAlert({text: jqXHR.responseText ,type: 'danger'});
          $('#myModal').modal('toggle');
        }, 1000);
    });
  }
});
