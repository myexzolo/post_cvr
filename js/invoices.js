function reset(){
  var cust_id     = $('#cust_id').val("");
  search();
}

function expInvoice(invoiceCode){
  // var url = "print_invoices.php?invoiceCode="+invoiceCode;
  // postURL(url, null);
  var typeCompany       = $('#typeCompany').val();
  var url = "";
  if(typeCompany == "2"){
    url = "ajax/invoices/listExp_post.php";
  }else{
    url = "ajax/invoices/listExp.php";
  }
  $.post(url,{invoiceCode:invoiceCode})
    .done(function( data ) {
      $('#show-page-exp').html(data);
      printDiv('printableArea');
  });
}

function expInvoice2(invoiceCode){
  var url = "";

  url = "ajax/invoices/listExp_post_month.php";

  $.post(url,{invoiceCode:invoiceCode})
    .done(function( data ) {
      $('#show-page-exp').html(data);
      printDiv('printableArea');
  });
}



function editInvoice(invoiceCode){
  var url = "invoice.php?invoiceCode="+invoiceCode;
  postURL(url, null);
}

function editInvoice2(invoiceCode,contract){
  var url = "invoice_post.php?invoiceCode="+invoiceCode+"&contract="+ contract;
  postURL(url, null);
}

function delInvoice2(invoiceCode){
  bootbox.confirm("คุณต้องการยกเลิก Invoice : "+ invoiceCode + " ?", function(result){
    /* your callback code */
    if(result){
      $.post("ajax/invoices/manageDel2.php",{invoiceCode:invoiceCode})
      .done(function(data) {
          search2();
      });
    }
})
}

function delInvoice(invoiceCode){
  bootbox.confirm("คุณต้องการยกเลิก Invoice : "+ invoiceCode + " ?", function(result){
    /* your callback code */
    if(result){
      $.post("ajax/invoices/manageDel2.php",{invoiceCode:invoiceCode})
      .done(function(data) {
          search();
      });
    }
})
}


function search(){
  var cust_id       = $('#cust_id').val();
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var trailer_id    = $('#trailer_id').val();
  $.post("ajax/invoices/list.php",{cust_id:cust_id,startDate:startDate,endDate:endDate,trailer_id:trailer_id})
  .done(function(data) {
      $('#show-page').html(data);
  });
}

function search2(){
  var contract      = $('#contract').val();
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  $.post("ajax/invoices/list2.php",{contract:contract,startDate:startDate,endDate:endDate})
  .done(function(data) {
      $('#show-page').html(data);
  });
}

function searchInvoice(){
  var cust_id         = $('#cust_id').val();
  var invoiceCode     = $('#invoiceCode').val();
  var invoiceDate     = $('#invoiceDate').val();
  var route_id        = $('#route_id').val();

  $.post("ajax/invoices/listInvoice.php",{cust_id:cust_id,invoiceCode:invoiceCode,invoiceDate:invoiceDate,route_id:route_id})
    .done(function(data) {
      $('#show-page').html(data);
      if(invoiceCode != ""){
        cal();
      }
  });

}

function reset2(){
  var month           = $('#month').val($('#month_tmp').val());
  var year            = $('#year').val($('#year_tmp').val());
  var contract        = $('#contract').val("");
  searchInvoice2();
}

function searchInvoice2(){
  var month           = $('#month').val();
  var year            = $('#year').val();
  var contract        = $('#contract').val();
  var invoiceCode     = $('#invoiceCode').val();
  var invoiceDate     = $('#invoiceDate').val();
  //alert(cust_id+","+invoiceCode+","+invoiceDate);
  $.post("ajax/invoices/listInvoice2.php",{contract:contract,invoiceCode:invoiceCode,invoiceDate:invoiceDate,month:month,year:year})
    .done(function(data) {
      $('#show-page').html(data);
      if(contract != "" && invoiceCode == ""){
        document.getElementById("checkApp").checked = true;
        checkAll2(document.getElementById("checkApp"));
      }

  });
}



function clearDataInv(){
  $('#invoiceCode').val("");
  $('#invoiceDate').val("");
}

function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = num;
  }
  return number;
}

function checkAll(obj) {
  if(obj.checked){
    var length = $('.checkJobId').length;
    var numMax = 300;
    if(length > numMax){
      var num = 0;
      $('.checkJobId').each(function(){
        if(num == numMax){
          return false;
        }else{
          $(this).prop('checked', true);
          num++;
        }
      });
    }else{
      $('.checkJobId').prop('checked', true);
    }
  }else{
    $('.checkJobId').prop('checked', false);
    $('#delivery_cost').val(0);
    $('#total_cost').val(0);
  }
  cal();
}

function checkAll2(obj) {
  if(obj.checked){
    $('.checkJobId').each(function(){
      $(this).prop('checked', true);
      $('#delivery_cost').val($('#delivery_cost_tmp').val());
    });
  }else{
    $('.checkJobId').prop('checked', false);
    $('#delivery_cost').val(0);
    $('#total_cost').val(0);
  }
  cal2();
}


function chkList(obj){
  var num = 0;
  $( ".checkJobId" ).each(function( index ) {
      if(this.checked){
        num++;
      }
      if(num > 300){
        obj.checked = false;
      }
  });
}

function cal(){
  var transfer = 0;
  var num = 0;
  $( ".checkJobId" ).each(function( index ) {
      //console.log( index + ": " + $( this ).text());
      if(this.checked){
        var id = "#net"+ $( this ).val();
        var cost = $(id).html();
        transfer += parseFloat(cost.replace(/,/g, ""));
      }
  });
  var ot          = parseFloat(checkNumeric($('#ot').val().replace(/,/g, "")));
  var discount    = parseFloat(checkNumeric($('#discount').val().replace(/,/g, "")));
  var provision   = parseFloat(checkNumeric($('#provision').val().replace(/,/g, "")));

  var amount =  (transfer + provision + ot) - discount;
  //alert(transfer+","+ provision+","+ ot+","+ discount);

  $('#delivery_cost').val(transfer.toFixed(2));
  $('#total_cost').val(amount.toFixed(2));
}

function cal2(){
  var transfer    = parseFloat(checkNumeric($('#delivery_cost').val().replace(/,/g, "")));
  var num = 0;

  var ot          = parseFloat(checkNumeric($('#ot').val().replace(/,/g, "")));
  var discount    = parseFloat(checkNumeric($('#discount').val().replace(/,/g, "")));
  var provision   = parseFloat(checkNumeric($('#provision').val().replace(/,/g, "")));


  var amount =  (transfer + provision + ot) - discount;
  //alert(transfer+","+ provision+","+ ot+","+ discount);

  //$('#delivery_cost').val(transfer.toFixed(2));
  $('#total_cost').val(amount.toFixed(2));
}

function exportExcel(invoiceCode){
  var typeCompany       = $('#typeCompany').val();
  var url = "";
  if(typeCompany == "2"){
    url = "export_excel_invoice_post.php?invoiceCode="+invoiceCode;
  }else{
    url = "export_excel_invoice.php?invoiceCode="+invoiceCode;
  }

  window.open(url,'_blank');
}

function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     document.getElementById("tableDisplayExp").style.fontSize = "11px";

     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}


$("#form-data").one("submit", submitFormFunction);
function submitFormFunction(event) {
    event.preventDefault();
    if ($('#form-data').smkValidate()) {
      cal();
      $.ajax({
          url: 'ajax/invoices/manage.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
        $.smkProgressBar();
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          gotoPage('invoices_list.php');
          $.smkAlert({text: data.message,type: data.status});
        }, 1000);
      }).fail(function (jqXHR, exception) {
        //alert("fail");
      });
    }
}


$("#form-data2").one("submit", submitFormFunction2);
function submitFormFunction2(event) {
    event.preventDefault();
    if ($('#form-data2').smkValidate()) {
      cal2();
      $.ajax({
          url: 'ajax/invoices/manage2.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
        $.smkProgressBar();
        setTimeout(function(){
          $.smkProgressBar({status:'end'});
          gotoPage('invoices_list_post.php');
          $.smkAlert({text: data.message,type: data.status});
        }, 1000);
      }).fail(function (jqXHR, exception) {
        //alert("fail");
      });
    }
}


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
