
  function show(){
    var employeeName = $('#s_employee_name').val();
    $.post("ajax/employee/show_edit.php",{employeeName:employeeName})
      .done(function( data ) {
        $('#show-page').html(data);
    });
  }

  function reset(){
    $('#s_employee_name').val("");
    show();
  }

  $('#form-data').on('submit', function(event) {
    event.preventDefault();
    if ($('#form-data').smkValidate()) {
        $.smkProgressBar();
        $.ajax({
            url: 'ajax/employee/manage_edit.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $('#form-data').smkClear();
            show();
            $.smkAlert({text: data.message,type: data.status});
            $('#myModal').modal('toggle');
          }, 1000);
        }).fail(function() {
          $.smkProgressBar({status:'end'});
        });
    }
  });

  $(document).ready(function() {
    $("#panel1").smkPanel({
      hide: 'full,remove'
    });
    show();
  });
