function reset(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var endDate       = $('#endDate').val($('#endDateTmp').val());
  $('#employeeId').val("");
  search();
}

function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var employeeId      = $('#employeeId').val();
  //alert(employeeId);
  $.post("ajax/sal/list.php",{startDate:startDate,endDate:endDate,employeeId:employeeId})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function exportExcel(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var employeeId      = $('#employeeId').val();

  var pam = "startDate="+startDate+"&endDate="+endDate+"&status="+status+
            "&employeeId="+employeeId
  var url = "export_excel_sal.php?"+pam;
  //alert(url);
  window.open(url,'_blank');
}


function calAmount(){
  var jobProfits  = checkNumeric($('#jobProfits').html().replace(/,/g, ''));
  var other1  = checkNumeric($('#other1').val().replace(/,/g, '')) ;
  var other2  = checkNumeric($('#other2').val().replace(/,/g, ''));
  var other3  = checkNumeric($('#other3').val().replace(/,/g, ''));
  var txtOther1  = checkNumeric($('#txtOther1').val());
  var txtOther2  = checkNumeric($('#txtOther2').val());
  var txtOther3  = checkNumeric($('#txtOther3').val());
  var salary  = checkNumeric($('#salary').val().replace(/,/g, ''));
  var bill    = checkNumeric($('#bill').val().replace(/,/g, ''));
  var diligence  = checkNumeric($('#diligence').val().replace(/,/g, ''));

  var totalSub = (other1 + other2 + other3 + salary + bill + diligence);
  //alert(totalSub);
  var total   = (jobProfits - totalSub);
  $('#total').val(total);
}

function printOrder(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var trailerId       = $('#trailerId').val();
  var employeeId      = $('#employeeId').val();
  var source          = $('#source').val();
  var destination     = $('#destination').val();

  $.post("ajax/income/listExp.php",{startDate:startDate,endDate:endDate,trailerId:trailerId,employeeId:employeeId,source:source,destination:destination})
    .done(function( data ) {
      $('#show-page-exp').html(data);
      printDiv('printableArea');
  });
}



function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = parseInt(num);
  }
  return number;
}

function printDiv(divName) {

     var originalContents = document.body.innerHTML;
     document.getElementById("hd0").style.height = "30px";
     document.getElementById("hd1").style.fontSize = "13px";
     document.getElementById("hd2").style.fontSize = "13px";

     var x = document.getElementsByClassName("tabf");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       x[i].style.fontSize = "8px";
       x[i].style.width = "100%";
       x[i].classList.remove("table-bordered");
     }

     var x = document.getElementsByClassName("divf");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       x[i].style.fontSize = "11px";
     }

     var x = document.getElementsByClassName("tdb");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       x[i].style.fontSize = "93%";
       x[i].style.width = "auto";
       x[i].style.border = "1px solid black";
     }

     var x = document.getElementsByClassName("divo");
     for(var i=0 ; i < x.length ; i++ ){
       //alert(i);
       x[i].style.width = "100%";
       x[i].style.overflowX = "";
     }

     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
