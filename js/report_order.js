
function reset(){
  var startDate     = $('#startDate').val($('#startDateTmp').val());
  var startDate     = $('#endDate').val($('#endDateTmp').val());
  var status        = $('#status').val("2");
  var employeeId    = $('#employeeId').val("");
  var jobOrderNo    = $('#jobOrderNo').val("");
  var dp            = $('#dp').val("");
  var trailerId     = $('#trailerId').val("");
  var awaySpecNo    = $('#awaySpecNo').val("");
  var cust_id       = $('#cust_id').val("");
  var statusInv     = $('#statusInv').val("");
  var productName   = $('#productName').val("");
  search();
}


function search(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var status        = $('#status').val();
  var employeeId    = $('#employeeId').val();
  var jobOrderNo    = $('#jobOrderNo').val();
  var dp            = $('#dp').val();
  var trailerId     = $('#trailerId').val();
  var awaySpecNo    = $('#awaySpecNo').val();
  var cust_id       = $('#cust_id').val();
  var statusInv     = $('#statusInv').val();
  var productName   = $('#productName').val();

  $.post("ajax/report/list_order.php",{
    startDate:startDate,
    endDate:endDate,
    status:status,
    employeeId:employeeId,
    jobOrderNo:jobOrderNo,
    dp:dp,
    trailerId:trailerId,
    awaySpecNo:awaySpecNo,
    statusInv:statusInv,
    productName:productName,
    cust_id:cust_id
  })
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function checkNumeric(num){
  var number = 0;
  if($.isNumeric(num)){
    number = num;
  }
  return number;
}

function printOrder(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var status        = $('#status').val();
  var employeeId    = $('#employeeId').val();
  var jobOrderNo    = $('#jobOrderNo').val();
  var dp            = $('#dp').val();
  var trailerId     = $('#trailerId').val();
  var awaySpecNo    = $('#awaySpecNo').val();
  var cust_id       = $('#cust_id').val();
  var statusInv     = $('#statusInv').val();
  var productName   = $('#productName').val();
  $.post("ajax/report/list_orderExp.php",{
      startDate:startDate,
      endDate:endDate,
      status:status,
      employeeId:employeeId,
      jobOrderNo:jobOrderNo,
      dp:dp,
      trailerId:trailerId,
      awaySpecNo:awaySpecNo,
      statusInv:statusInv,
      productName:productName,
      cust_id:cust_id
  })
    .done(function( data ) {
      $('#show-page-exp').html(data);
      printDiv('printableArea');
  });
}


function exportExcel(){
  var startDate     = $('#startDate').val();
  var endDate       = $('#endDate').val();
  var status        = $('#status').val();
  var employeeId    = $('#employeeId').val();
  var jobOrderNo    = $('#jobOrderNo').val();
  var dp            = $('#dp').val();
  var trailerId     = $('#trailerId').val();
  var awaySpecNo    = $('#awaySpecNo').val();
  var cust_id       = $('#cust_id').val();
  var statusInv     = $('#statusInv').val();
  var productName   = $('#productName').val();

  var pam = "startDate="+startDate+"&endDate="+endDate+"&status="+status+
            "&employeeId="+employeeId+"&jobOrderNo="+jobOrderNo+"&dp="+dp+
            "&trailerId="+trailerId+"&awaySpecNo="+awaySpecNo+
            "&statusInv"+statusInv+"&productName="+productName+"&cust_id="+cust_id
  var url = "export_excel_job.php?"+pam;
  //alert(url);
  window.open(url,'_blank');
}


function printDiv(divName) {
     var originalContents = document.body.innerHTML;
     var printContents = document.getElementById(divName).innerHTML;
     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}



$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}
